module net.gcvs.util {
    requires static lombok;

    exports net.gcvs.util;
    opens net.gcvs.util;
}