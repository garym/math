package net.gcvs.math;

import java.math.BigInteger;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GCDTestPerformnce {

    @Test
    public void testPerformance() {
        // 299th and 300th fibonacci numbers, ie maximize the gcd call depth
        BigInteger a = new BigInteger("137347080577163115432025771710279131845700275212767467264610201");
        BigInteger b = new BigInteger("222232244629420445529739893461909967206666939096499764990979600");
        // 1000th and 1001th fibonacci number, thanks to
        // http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibCalcX.html
        a = new BigInteger(
                "43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875");
        b = new BigInteger(
                "70330367711422815821835254877183549770181269836358732742604905087154537118196933579742249494562611733487750449241765991088186363265450223647106012053374121273867339111198139373125598767690091902245245323403501");

        int loops = 10000;

        // ensure jvm optimizer doesnt fuck up method
        int idx1 = 0, idx2 = 0, idx3 = 0;
        Object[] list1 = new Object[loops * 2];
        Object[] list2 = new Object[loops];
        Object[] list3 = new Object[loops];

        long start1 = System.currentTimeMillis();
        for (int i = 0; i < loops; i++) {
            BigInteger gcd = GCD.gcd(a, b);
            BigInteger adivgcd = a.divide(gcd);
            BigInteger bdivgcd = b.divide(gcd);
            list1[idx1++] = adivgcd;
            list1[idx1++] = bdivgcd;
        }
        long end1 = System.currentTimeMillis();

        long start2 = System.currentTimeMillis();
        for (int i = 0; i < loops; i++) {
            BigInteger[] gcdm = GCD.gcdm(a, b);
            list2[idx2++] = gcdm;
        }
        long end2 = System.currentTimeMillis();

        long start3 = System.currentTimeMillis();
        for (int i = 0; i < loops; i++) {
            BigInteger gcd = a.gcd(b);
            list3[idx3++] = gcd;
        }
        long end3 = System.currentTimeMillis();

        System.out.println("gcd took " + (end1 - start1) + " millis, gcdm took " + (end2 - start2) + " millis, BigInteger.gcd took " + (end3 - start3) + " millis");
        int objects = 0;
        for (int i = 0; i < list2.length; i++) {
            BigInteger af = (BigInteger) list1[i * 2];
            BigInteger bf = (BigInteger) list1[i * 2 + 1];
            BigInteger[] m = (BigInteger[]) list2[i];
            if (af != null) {
                assertTrue(af.compareTo(m[1]) == 0);
                assertTrue(bf.compareTo(m[2]) == 0);
                objects++;
            }
        }
        System.out.println("found " + objects + " results");
    }

}
