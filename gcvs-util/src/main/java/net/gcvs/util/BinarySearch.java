package net.gcvs.util;

import java.util.function.IntToLongFunction;

public class BinarySearch {
    private BinarySearch() {
    }

    /**
     * Same contract as Arrays.binarySearch
     * if key found, return index
     * else return (-(insertion point) - 1)
     */
    public static int binarySearch(IntToLongFunction f, int from, int to, long key) {
        if (from > to) throw new IllegalArgumentException();
        if (from == to) return from;

        long fromValue = f.applyAsLong(from);
        if (key < fromValue) {
            return -1;
        }
        if (key == fromValue) {
            return from;
        }
        long toValue = f.applyAsLong(to - 1);
        if (toValue < key) {
            return -to - 1;
        }
        if (toValue == key) {
            return to - 1;
        }

        // assert from < index < to - 1
        for (; ; ) {
            int mid = (from + to) / 2;
            if (mid == from) return -to - 1;
            long midValue = f.applyAsLong(mid);
            if (midValue == key) {
                return mid;
            }
            if (midValue < key) {
                from = mid;
            } else {
                to = mid;
            }
        }
    }
}
