package net.gcvs.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// |A B|   |E F|   |AE + BG AF + BH|
// |C D| * |G H| = |CE + DG CF + DH|

// |A B C|   |J K L|   |AJ + BM + CP AK + BN + CQ AL + BO + CR|
// |D E F| * |M N O| = |DJ + EM + FP DK + EN + FQ DL + EO + FR|
// |G H I|   |P Q R|   |GJ + HM + IP GK + HN + IQ GL + HO + IR|
public class Parity {
	public final Terms terms;
	private final Map<TermSet, TermSet> uniqueSets = new HashMap<>();
	final List<TermSet> sets = new ArrayList<>();
	final BitSet[] setsWithTerm;

	public Parity(final Terms terms) {
		this.terms = terms;
		this.setsWithTerm = new BitSet[terms.length];
		Arrays.setAll(setsWithTerm, i -> new BitSet(terms.length));
	}

	public TermSet create(final String line) {
		final String[] ts = line.replaceAll("[ +-]+", " ").split("[ +-]");
		return create(ts);
	}

	public TermSet create(final String... ts) {
		int[] tids = new int[ts.length];
		Arrays.setAll(tids, i -> terms.findTerm(ts[i]));
		return new TermSet(tids);
	}

	public TermSet create(final String tm, final String tn) {
		String[] ms = tm.split("[ +-]");
		String[] ns = tn.split("[ +-]");
		String[] ts = new String[ms.length * ns.length];
		int idx = 0;
		for (String m : ms) {
			for (String n : ns) {
				ts[idx++] = m + n;
			}
		}
		TermSet set = create(ts);
		String mlab = ms.length > 1 ? "(" + tm + ")" : tm;
		String nlab = ns.length > 1 ? "(" + tn + ")" : tn;
		set.label = mlab + " * " + nlab;
		return set;
	}

	public TermSet add(final TermSet set) {
		TermSet found = uniqueSets.get(set);
		if (found != null) {
			return found;
		}
		final int index = sets.size();
		sets.add(set);
		for (int tid = set.nextSetBit(0); tid != -1; tid = set.nextSetBit(tid + 1)) {
			setsWithTerm[tid].set(index);
		}
		return set;
	}

	public void addAll(final Iterable<TermSet> list) {
		for (final TermSet set : list) {
			add(set);
		}
	}

	public TermSet add(final String line) {
		return add(create(line));
	}

	public TermSet add(final String tm, final String tn) {
		return add(create(tm, tn));
	}

	public boolean possibleToFind(final TermSet target) {
		TermSet bits = new TermSet();
		bits.or(target);
		for (TermSet set : sets) {
			bits.andNot(set);
		}

		if (!bits.isEmpty()) {
//			System.out.println("impossible: " + bits);
		}
		return bits.isEmpty();
	}

	public List<TermSet> find(final TermSet target) {
		if (!possibleToFind(target)) {
			return null;
		}

		BitSet chosen = new BitSet();
		BitSet available = new BitSet();
		available.set(0, sets.size(), true);

		TermSet modTarget = target.clone();

		boolean found = find(chosen, available, modTarget);

		if (found) {
			List<TermSet> list = new ArrayList<>();
			for (int i = chosen.nextSetBit(0); i != -1; i = chosen.nextSetBit(i + 1)) {
				list.add(sets.get(i));
			}
			return list;
		}

		return null;
	}

	boolean find(final BitSet chosen, final BitSet available, final TermSet target) {
//		System.out.println("find(" + chosen.cardinality() + ", " + available.cardinality() + ", " + target + ")");
		if (target.isEmpty()) {
//			System.out.println("found with size: " + chosen.cardinality());
			return true;
		}
//		if (chosen.cardinality() >= 10000) {
//			return false;
//		}

		// find term in the smallest number of sets
		int lowestSetCount = Integer.MAX_VALUE;
		int setIndex = -1;
		for (int tid = target.nextSetBit(0); tid != -1; tid = target.nextSetBit(tid + 1)) {
			int setCount = 0;
			int matchingSet = -1;
			for (int sidx = available.nextSetBit(0); sidx != -1; sidx = available.nextSetBit(sidx + 1)) {
				if (sets.get(sidx).get(tid)) {
					setCount++;
					matchingSet = sidx;
				}
			}
			if (setCount < lowestSetCount) {
				lowestSetCount = setCount;
				setIndex = matchingSet;
			}
		}

		// nothing found
		if (setIndex == -1) {
			return false;
		}

		// recurse, first with matchingSet then without

		chosen.set(setIndex);
		available.clear(setIndex);
		TermSet set = sets.get(setIndex);
		target.xor(set);
		boolean found = find(chosen, available, target);
		if (found) {
			return true;
		}
		target.xor(set);
		chosen.clear(setIndex);

		found = find(chosen, available, target);

		return found;
	}

	public String toString(final TermSet set) {
		String sep = "";
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 128; i++) {
			if (set.get(i)) {
				sb.append(sep);
				sb.append(terms.name(i));
				sep = " ";
			}
		}
		return sb.toString();
	}

	public void print() {
		for (TermSet set : sets) {
			System.out.println(set);
		}
	}

	public static void assess(final Parity p, final Mat mn) {
		int count = 0;
		for (int r = 0; r < 3; r++) {
			for (int c = 0; c < 3; c++) {
				List<TermSet> soln = p.find(p.create(mn.term(r, c)));
				if (soln != null) {
					count++;
				}
			}
		}
		System.out.println("parity p with " + p.sets.size() + " sets, solves " + count + " targets");
	}

	public static void twobytwo() {
		Mat m = new Mat(2, 2);
		Mat n = new Mat(2, 2);
		Mat mn = m.mul(n);

		Terms terms = Terms.create(m.terms().copyNames(), n.terms().copyNames());

		Parity p = new Parity(terms);
		TermSet.toString = p::toString;

		p.add("B D", "G H");
		p.add("A D", "E H");
		p.add("A C", "E F");
		p.add("A B", "H");
		p.add("A", "F H");
		p.add("D", "G E");
		p.add("C D", "E");

		p.print();

		for (int i = 0; i < 4; i++) {
			TermSet target = p.create(mn.term(i));
			System.out.println("target: " + target);
			List<TermSet> list = p.find(target);
			System.out.println(list);
		}

	}

	public static String[] choose(final int n, final String... from) {
		List<String> list = new ArrayList<>();
		Sequence s = new Sequence(0, from.length - 1, n);
		int[] seq = s.createCombination();
		do {
			final StringBuilder sb = new StringBuilder();
			String sep = "";
			for (int i = 0; i < seq.length; i++) {
				sb.append(sep);
				sb.append(from[seq[i]]);
				sep = " ";
			}
			list.add(sb.toString());

		} while (s.nextCombination(seq));
		return list.toArray(new String[list.size()]);
	}

	public List<TermSet> reduce(final List<TermSet> list) {
		// can't remove sets that include a unique term
		List<TermSet> eligible = new ArrayList<>(list);
		int[] termCount = new int[terms.length];
		for (TermSet set : list) {
			for (int t = set.nextSetBit(0); t != -1; t = set.nextSetBit(t + 1)) {
				termCount[t]++;
			}
		}
		for (int t = 0; t < termCount.length; t++) {
			if (termCount[t] == 1) {
				Iterator<TermSet> it = eligible.iterator();
				while (it.hasNext()) {
					if (it.next().get(t)) {
						it.remove();
						break;
					}
				}
			}
		}

		List<TermSet> remove = new ArrayList<>();

		if (eligible.size() > 0) {
			for (int i = 0; i < eligible.size(); i++) {
				TermSet toReplace = eligible.get(i);
				Parity p = new Parity(terms);
				for (TermSet t : eligible) {
					if (t != toReplace) {
						p.add(t);
					}
				}
				List<TermSet> found = p.find(toReplace);

				if (found != null) {
					remove.addAll(found);
					remove.add(toReplace);
					break;
				}
			}
		}

		if (remove.size() == 0) {
			return list;
		}
		List<TermSet> reduced = new ArrayList<>(list);
		reduced.removeAll(remove);

		return reduce(reduced);
	}

	public Parity copy() {
		Parity p = new Parity(terms);
		for (final TermSet set : sets) {
			p.add(set);
		}
		return p;
	}

	public static void main(String[] args) {
		Mat m = new Mat(3, 3);
		Mat n = new Mat(3, 3);
		Mat mn = m.mul(n);

		Terms terms = Terms.create(m.terms().copyNames(), n.terms().copyNames());

		// |A B| * |E F| = |AE + BG AF + BH|
		// |C D| * |G H| = |CE + DG CF + DH|

		// |A B C| * |J K L| = |AJ + BM + CP AK + BN + CQ AL + BO + CR|
		// |D E F| * |M N O| = |DJ + EM + FP DK + EN + FQ DL + EO + FR|
		// |G H I| * |P Q R| = |GJ + HM + IP GK + HN + IQ GL + HO + IR|

		Parity p = new Parity(terms);
		TermSet.toString = p::toString;

//		p.add("A E I", "J N R");
//
//		p.add("A B C", "J M P");
//		p.add("A B", "M P");
//		p.add("B C", "J M");

		p.add("A B C", "L");
		p.add("C", "L O R");
		p.add("B C", "L O");
		p.add("C", "L");

		p.add("G H I", "J");
		p.add("I", "J M P");
		p.add("H I", "J M");
		p.add("I", "J");

		p.add("A E I", "J N R");

		final TermSet target00 = p.create(mn.term(0, 0));
		final TermSet target01 = p.create(mn.term(0, 1));
		final TermSet target02 = p.create(mn.term(0, 2));
		final TermSet target10 = p.create(mn.term(1, 0));
		final TermSet target11 = p.create(mn.term(1, 1));
		final TermSet target12 = p.create(mn.term(1, 2));
		final TermSet target20 = p.create(mn.term(2, 0));
		final TermSet target21 = p.create(mn.term(2, 1));
		final TermSet target22 = p.create(mn.term(2, 2));

		List<TermSet> T02 = p.find(target02);
		List<TermSet> T20 = p.find(target20);
		System.out.println("(0, 2) = " + T02);
		System.out.println("(2, 0) = " + T20);

		assess(p, mn);

		{
			String[] ms = { "A", "B", "C", "G", "H", "I" }; // "D", "E", "F",
			String[] ns = { "J", "K", "L", "M", "N", "O", "P", "Q", "R" };
			String[] nsub = { "Q", "K", "N" };
			TermSet target = target01;
			findTarget(p, mn, target, ms, ns, nsub);
			List<TermSet> sets = p.find(target);
			System.out.println("(0, 1) = " + sets);

			assess(p, mn);
		}

		{
			List<TermSet> T_10 = p.find(target10);
			System.out.println("(1, 0) = " + T_10);

			String[] ms = { "D", "E", "F", "G", "H", "I" };
			String[] ns = { "J", "K", "L", "M", "N", "O", "P", "Q", "R" };
			String[] nsub = { "J", "M", "P" };
			TermSet target = target10;
			findTarget(p, mn, target, ms, ns, nsub);

			List<TermSet> sets = p.find(target);
			System.out.println("(1, 0) = " + sets);

			assess(p, mn);
		}

		{
			List<TermSet> T_21 = p.find(target21);
			System.out.println("(2, 1) = " + T_21);

			String[] ms = { "D", "E", "F", "G", "H", "I" };
			String[] ns = { "J", "K", "L", "M", "N", "O", "P", "Q", "R" };
			String[] nsub = { "J", "M", "P" };
			TermSet target = target21;
			findTarget(p, mn, target, ms, ns, nsub);

			List<TermSet> sets = p.find(target);
			System.out.println("(2, 1) = " + sets);

			assess(p, mn);
		}

		System.out.println("size of p is " + p.sets.size());

		List<TermSet> red = p.reduce(p.sets);
		System.out.println("reduced to: " + red.size());

		List<TermSet> T12 = p.find(target12);
		System.out.println("(1, 2) = " + T12);

		List<TermSet> T00 = p.find(target00);
		System.out.println("(0, 0) = " + T00);
		List<TermSet> T11 = p.find(target11);
		System.out.println("(1, 1) = " + T11);
		List<TermSet> T22 = p.find(target22);
		System.out.println("(2, 2) = " + T22);

	}

	private static void findTarget(Parity p, Mat mn, final TermSet target, String[] ms, final String[] ns, final String[] contains) {
		Parity copy = p.copy();

		int count = 0;
		for (int alen = 1; alen <= 3; alen++) {
			for (int blen = 1; blen <= 3; blen++) {
				String[] as = choose(alen, ms); // "D", "E", "F",
				String[] bs = choose(blen, ns);
				for (String a : as) {
					for (String b : bs) {
						boolean test = false;
						for (String c : contains) {
							if (b.contains(c)) {
								test = true;
								break;
							}
						}

						if (test) {
							// System.out.println(p.add(a, b));
							count++;
							copy.add(a, b);
						}
					}
				}
			}
		}
		System.out.println("count: " + count);

		System.out.println("target: " + target + " using " + copy.sets.size() + " sets");
		List<TermSet> list = copy.find(target);
//			System.out.println(list);
		System.out.println("found with " + list.size() + " sets");
		list.removeAll(p.sets);
		List<TermSet> smaller = copy.reduce(list);
//			System.out.println(smaller);
		System.out.println("reduced to size: " + smaller.size());

		p.addAll(smaller);

	}
}
