package net.gcvs.math.polynomial;

import java.util.ArrayList;
import java.util.List;

public class PowerSeriesGenerator<V> {
    private final SeriesAlgebra<V> seriesAlgebra;
    private final Series<V> series;
    private final List<Series<V>> powers = new ArrayList<>();

    public PowerSeriesGenerator(SeriesAlgebra<V> seriesAlgebra, final Series<V> series) {
        this.seriesAlgebra = seriesAlgebra;
        this.series = series;
        powers.add(new IterableIterator<>(series.iterator()));
    }

    public Series<V> power(final long power) {
        if (power < 0) throw new IllegalArgumentException();
        if (power == 0) return seriesAlgebra.one();
        Series<V> s = null;

        Series<V> p = powers.get(0);
        for (int idx = 0; ; idx++) {
            final long bit = 1L << idx;
            if (idx == powers.size()) {
                p = seriesAlgebra.multiply(p, p);
                powers.add(p);
            } else {
                p = powers.get(idx);
            }

            if ((power & bit) != 0) {
                if (s == null) {
                    s = p;
                } else {
                    s = seriesAlgebra.multiply(s, p);
                }
            }
            if (bit == Long.highestOneBit(power)) {
                return s;
            }
        }
    }
}
