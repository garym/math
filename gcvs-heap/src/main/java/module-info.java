module net.gcvs.heap {
    requires static lombok;

    exports net.gcvs.heap;
    exports net.gcvs.heap.vanilla;

    opens net.gcvs.heap;
}