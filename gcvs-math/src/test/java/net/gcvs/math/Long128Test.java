package net.gcvs.math;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Long128Test {

    @ParameterizedTest
    @ValueSource(longs = {Long.MIN_VALUE, Long.MAX_VALUE, 0, -1, 1})
    void toString_sampleValues(final long value) {
        Long128 v = new Long128(value);
        assertEquals(String.valueOf(value), v.toString());
    }

    @Test
    void toString_MIN_VALUE() {
        assertEquals("-170141183460469231731687303715884105728", Long128.MIN_VALUE.toString());
    }

    @Test
    void toString_MAX_VALUE() {
        assertEquals("170141183460469231731687303715884105727", Long128.MAX_VALUE.toString());
    }

    static String[][] constructor_String() {
        List<String[]> list = new ArrayList<>();
        for (int i = 0; i < 16; i++) {
            final BigInteger value = new BigInteger("2").pow(i << 3).subtract(BigInteger.ONE);
            list.add(new String[]{value.toString()});
            list.add(new String[]{value.negate().toString()});
        }
        return list.toArray(String[][]::new);
    }

    @ParameterizedTest
    @MethodSource("constructor_String")
    void constructor_String(String value) {
        assertEquals(value, new Long128(value).toString());
    }

    @ParameterizedTest
    @CsvSource({"123, 234", "234, 345"})
    void multiply_samples(String a, String b) {
        Long128 v0 = new Long128(a);
        Long128 v1 = new Long128(b);

        assertEquals(new BigInteger(a).multiply(new BigInteger(b)).toString(), v0.multiply(v1).toString());
    }

    @Test
    public void test() {

        Long128 v0 = new Long128(Long.MAX_VALUE);
        Long128 v1 = new Long128(Long.MAX_VALUE);

        assertEquals(v0.multiply(v1).toString(), BigInteger.valueOf(Long.MAX_VALUE).multiply(BigInteger.valueOf(Long.MAX_VALUE)).toString());

    }
}
