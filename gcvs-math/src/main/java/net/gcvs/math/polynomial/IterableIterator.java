package net.gcvs.math.polynomial;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class IterableIterator<V> implements Series<V> {
    private final List<Term<V>> materialized = new ArrayList<>();
    private final Iterator<Term<V>> iterator;

    public IterableIterator(final Iterator<Term<V>> iterator) {
        this.iterator = iterator;
    }

    private boolean hasNext(final int index) {
        while (index >= materialized.size()) {
            if (!iterator.hasNext()) {
                return false;
            }
            materialized.add(iterator.next());
        }
        return true;
    }

    private Term<V> next(final int index) throws NoSuchElementException {
        if (!hasNext(index)) {
            throw new NoSuchElementException();
        }
        return materialized.get(index);
    }

    @Override
    public Iterator<Term<V>> iterator() {
        return new Iterator<>() {
            int idx = 0;

            @Override
            public boolean hasNext() {
                return IterableIterator.this.hasNext(idx);
            }

            @Override
            public Term<V> next() {
                return IterableIterator.this.next(idx++);
            }
        };
    }
}
