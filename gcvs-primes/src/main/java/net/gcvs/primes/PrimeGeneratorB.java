package net.gcvs.primes;

import net.gcvs.util.LongArray;

import java.util.BitSet;

public class PrimeGeneratorB implements PrimeGenerator {
    private static final int BATCH = 1 << 30;

    /**
     * returns list of primes less than or equal to n
     *
     * @param n
     * @return
     */
    public LongArray longArray(long n) {
        final LongArray primes = new LongArray(500_000_000);

        long start = 2;
        long end = start + BATCH;
        final BitSet bits = new BitSet(BATCH);

        final int sqrtn = (int) Math.ceil(Math.sqrt(n));


        int batch = BATCH;
        while (start <= n) {
            if (start + batch > n) {
                batch = (int) (n + 1 - start);
            }
            bits.set(0, batch);
            bits.clear(batch, BATCH);
            // clear out multiples
            for (int pidx = 0; pidx < primes.size(); pidx++) {
                final long p = primes.get(pidx);
                final long p2 = p * p;
                if (p2 >= end) {
                    break;
                }

                long pstart = start + p - 1;
                pstart -= pstart % p;
                // pstart is now the first multiple of p larger than start

                if (pstart < p2) {
                    pstart = p2;
                }

                for (int step = (int) (pstart - start); step < batch; step += p) {
                    bits.clear(step);
                }
            }

            for (int i = bits.nextSetBit(0); i != -1; i = bits.nextSetBit(i + 1)) {
                final long p = start + i;
                primes.add(p);

                if (p > sqrtn) {
                    // prime i not necessary to sieve out, we're
                    continue;
                }
                for (long m = p * p - start; m >= 0 && m < batch; m += p) {
                    bits.clear((int) m);
                }
            }
            start += BATCH;
            end += BATCH;
        }
        return primes;
    }

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    public BitSet primes(int n) {
        int end = n == Integer.MAX_VALUE ? n : n + 1;
        BitSet primes = new BitSet(end);
        primes.set(2, end);
        int sqrt = (int) Math.ceil(Math.sqrt(n));
        for (int i = primes.nextSetBit(2); i != -1; i = primes.nextSetBit(i + 1)) {
            if (i > sqrt) {
                break;
            }
            int j = primes.nextSetBit(i + 1);
            if (j == -1) {
                for (int m = i * i; m >= 0 && m <= n; m += i) {
                    primes.clear(m);
                }
            } else {
                int x = i * i, y = j * j;

//                int c = (n - y) / j;
                for (; 0 <= y && y <= n; y += j) {
                    primes.clear(x);
                    primes.clear(y);
                    x += i;
                    if (x < y && 0 <= x && x <= n) {
                        primes.clear(x);
                        x += i;
                    }
                }
                while (0 <= x && x <= n) {
                    primes.clear(x);
                    x += i;
                }


                i = j;
            }
        }
        if (n == Integer.MAX_VALUE) {
            primes.set(Integer.MAX_VALUE);
        }
        return primes;
    }
}
