package net.gcvs.primes;

import java.util.BitSet;

/**
 * stores odd indexed bits
 */
public class PrimeSet {
    private static final int ADDRESS_BITS_PER_WORD = 6;
    /* Used to shift left or right for a partial word mask */
    private static final long WORD_MASK = 0xffffffffffffffffL;

    private static final int BITS_PER_WORD = 1 << ADDRESS_BITS_PER_WORD;

    final long[] words;

    public PrimeSet(final int length) {
        this.words = new long[length];
    }

    public static int wordIndex(int bitIndex) {
        return bitIndex >> ADDRESS_BITS_PER_WORD;
    }

    public void clear(int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] &= ~(1L << bitIndex);
    }

    public static void clear(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] &= ~(1L << bitIndex);
    }

    public boolean getLong(long bitIndex) {
        bitIndex >>= 1;
        int wordIndex = longWordIndex(bitIndex);
        return (words[wordIndex] & (1L << bitIndex)) != 0;
    }

    public void setLong(long bitIndex) {
        bitIndex >>= 1;
        int wordIndex = longWordIndex(bitIndex);
        words[wordIndex] |= (1L << bitIndex);
    }

    public static void set(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] |= (1L << bitIndex);
    }

    public static int nextSetBit(long[] words, int fromIndex) {
        fromIndex >>= 1;
        final int wordsInUse = words.length;
        if (fromIndex < 0) return -1; // word overflow protection

        int u = wordIndex(fromIndex);
        if (u >= wordsInUse)
            return -1;

        long word = words[u] & (WORD_MASK << fromIndex);

        for (; ; ) {
            if (word != 0) return (((u * BITS_PER_WORD) + Long.numberOfTrailingZeros(word)) << 1) + 1;
            if (++u == wordsInUse) return -1;
            word = words[u];
        }
    }

    public static int[] expandToArray(long[] set) {
        // count primes
        int count = 1;
        for (long bits : set) {
            count += Long.bitCount(bits);
        }

        // collect primes
        int[] ints = new int[count];
        ints[0] = 2;
        int top = 1;
        for (int i = nextSetBit(set, 3); i != -1; i = nextSetBit(set, i + 2)) {
            ints[top++] = i;
        }

        return ints;
    }

    static int longWordIndex(long bitIndex) {
        return (int) (bitIndex >> ADDRESS_BITS_PER_WORD);
    }

    static long nextLongSetBit(long[] words, long fromIndex) {
        fromIndex >>= 1;
        final int wordsInUse = words.length;
        if (fromIndex < 0) return -1; // word overflow protection

        int u = longWordIndex(fromIndex);
        if (u >= wordsInUse)
            return -1;

        long word = words[u] & (WORD_MASK << fromIndex);

        for (; ; ) {
            if (word != 0) return (((((long) u) * BITS_PER_WORD) + Long.numberOfTrailingZeros(word)) << 1) + 1;
            if (++u == wordsInUse) return -1;
            word = words[u];
        }
    }

    public static long[] expandToLongArray(long[] set) {
        // count primes
        int count = 1;
        for (long bits : set) {
            count += Long.bitCount(bits);
        }

        // collect primes
        long[] longs = new long[count];
        longs[0] = 2;
        int top = 1;
        for (long i = nextLongSetBit(set, 3); i != -1; i = nextLongSetBit(set, i + 2)) {
            longs[top++] = i;
        }

        return longs;
    }

    public static BitSet arrayToBitSet(int[] primes) {
        BitSet set = new BitSet(primes[primes.length - 1] + 1);
        for (int p : primes) {
            set.set(p);
        }
        return set;
    }

    public long count() {
        long count = 0;
        for (long bits : words) {
            count += Long.bitCount(bits);
        }
        return count;
    }

    public long max() {
        return (((long) words.length) << 7) - 1;
    }
}
