package net.gcvs.heap.blob;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.swap;

public class Blobular<T> {


    private final List<T> source;
    private final Comparator<T> comparator;

    class Blob {
        Blink min;
        Blink max;
        List<Blink> blinks = new ArrayList<>();

        Blob(Blink blink) {
            this.min = blink;
            this.max = blink;
            blinks.add(blink);
        }

        Blob left;
        Blob right;
    }

    @RequiredArgsConstructor
    class Blink {
        final T t;
        Blink prev;
        Blink next;
    }

    private static int parent(int idx) {
        return (idx - 1) / 2;
    }

    private static int left(int idx) {
        return (idx + 1) * 2 - 1;
    }

    private static int right(int idx) {
        return (idx + 1) * 2;
    }

    private static <T> T parent(final List<T> heap, int idx) {
        int i = parent(idx);
        return i >= 0 ? heap.get(i) : null;
    }

    private static <T> T left(final List<T> heap, int idx) {
        int i = left(idx);
        return i < heap.size() ? heap.get(i) : null;
    }

    private static <T> T right(final List<T> heap, int idx) {
        int i = right(idx);
        return i < heap.size() ? heap.get(i) : null;
    }

    public Blobular(List<T> list, Comparator<T> comparator) {
        this.source = list;
        this.comparator = comparator;
        final List<Blink> heap = list.stream()
                .map(Blink::new)
                .collect(Collectors.toList());

        for (int i = heap.size() / 2; i >= 0; i--) {
            Blink blob = heap.get(i);
            Blink left = left(heap, i);
            Blink right = right(heap, i);

//            if (left < right) {
//                if (left < blob) {
//                    swap(heap, i, left(i));
//                    if (blob < right) {
//
//                    } else {
//
//                    }
//                }
//            }

            if (comparator.compare(blob.t, left.t) > 0) {
                swap(heap, i, left(i));
                // is blob <> right, is left <> right
            } else if (comparator.compare(blob.t, right.t) > 0) {
                swap(heap, i, right(i));
                // right, blob, left
            } else {

            }

        }


    }

}
