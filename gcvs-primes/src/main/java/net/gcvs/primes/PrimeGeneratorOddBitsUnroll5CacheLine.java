package net.gcvs.primes;

import static net.gcvs.primes.PrimeSet.expandToArray;

/**
 * experimental
 */
public class PrimeGeneratorOddBitsUnroll5CacheLine implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;
    private static final int BITS_PER_WORD = 1 << ADDRESS_BITS_PER_WORD;

    /* Used to shift left or right for a partial word mask */
    private static final long WORD_MASK = 0xffffffffffffffffL;

    private static int wordIndex(int bitIndex) {
        return bitIndex >> ADDRESS_BITS_PER_WORD;
    }

    private static int nextSetBit(long[] words, int fromIndex) {
        fromIndex >>= 1;
        final int wordsInUse = words.length;
        if (fromIndex < 0) return -1; // word overflow protection

        int u = wordIndex(fromIndex);
        if (u >= wordsInUse)
            return -1;

        long word = words[u] & (WORD_MASK << fromIndex);

        for (; ; ) {
            if (word != 0) return (((u * BITS_PER_WORD) + Long.numberOfTrailingZeros(word)) << 1) + 1;
            if (++u == wordsInUse) return -1;
            word = words[u];
        }
    }

    private static void clear(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] &= ~(1L << bitIndex);
    }

    private static void set(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] |= (1L << bitIndex);
    }

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        // primes = 0..n
        long[] primes = new long[((n - 1) >> 7) + 1];
        final int primesLength = primes.length;
        final int nextPrime = Stripes.sieveStripe2(primes);

        final int nmod = ((n - 1) >> 1) & 0x3f;
        if (nmod != 0x3f) {
            primes[primesLength - 1] &= (1L << (nmod + 1)) - 1;
        }
        final int max = n >> 1;
        final int sqrt = (int) Math.ceil(Math.sqrt(n));
        final int minsqrt = Math.min(63, sqrt);
        int p;
        for (p = nextSetBit(primes, nextPrime); p != -1 && p <= minsqrt; p = nextSetBit(primes, p + 2)) {
            int bitIndex = (p * p) >> 1;
            int wordIndex = bitIndex >> ADDRESS_BITS_PER_WORD;
            for (; wordIndex < primesLength; wordIndex++) {
                bitIndex &= 63;
                long strip = primes[wordIndex];
                for (; bitIndex < 64; bitIndex += p) {
                    strip &= ~(1L << bitIndex);
                }
                primes[wordIndex] = strip;
            }
        }
        if (p != -1) {
            int q = nextSetBit(primes, p + 2);

            if (q != -1 && q <= sqrt) {
                int r = nextSetBit(primes, q + 2);

                if (r != -1 && r <= sqrt) {
                    int s = nextSetBit(primes, r + 2);

                    if (s != -1 && s <= sqrt) {
                        int t = nextSetBit(primes, s + 2);

                        while (t != -1 && t <= sqrt) {
                            int mp = (p * p) >> 1;
                            int mq = (q * q) >> 1;
                            int mr = (r * r) >> 1;
                            int ms = (s * s) >> 1;
                            int mt = (t * t) >> 1;
                            for (; mt <= max; mp += p, mq += q, mr += r, ms += s, mt += t) {
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
                                primes[mt >> ADDRESS_BITS_PER_WORD] &= ~(1L << mt);

                                if (mt - ms < s) continue;
                                ms += s;
                                primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);

                                if (ms - mr < r) continue;
                                mr += r;
                                primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);

                                if (mr - mq < q) continue;
                                mq += q;
                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);

                                if (mq - mp < p) continue;
                                mp += p;
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                            }

                            for (; ms <= max; mp += p, mq += q, mr += r, ms += s) {
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);

                                if (ms - mr < r) continue;
                                mr += r;
                                primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);

                                if (mr - mq < q) continue;
                                mq += q;
                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);

                                if (mq - mp < p) continue;
                                mp += p;
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                            }
                            for (; mr <= max; mp += p, mq += q, mr += r) {
                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);

                                if (mr - mq < q) continue;
                                mq += q;
                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);

                                if (mq - mp < p) continue;
                                mp += p;
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                            }
                            for (; mq <= max; mp += p, mq += q) {
                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);

                                if (mq - mp < p) continue;
                                mp += p;
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                            }
                            for (; mp <= max; mp += p) {
                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                            }

//                            for (; ms <= max; mp += p, mq += q, mr += r, ms += s) {
//                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
//                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
//                                primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
//                                primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
//                            }
//                            for (; mr <= max; mp += p, mq += q, mr += r) {
//                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
//                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
//                                primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
//                            }
//                            for (; mq <= max; mp += p, mq += q) {
//                                primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
//                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
//                            }
//                            for (; mp <= max; mp += p) {
//                                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
//                            }
                            p = nextSetBit(primes, t + 2);
                            q = nextSetBit(primes, p + 2);
                            r = nextSetBit(primes, q + 2);
                            s = nextSetBit(primes, r + 2);
                            t = nextSetBit(primes, s + 2);
                        }
                        // drain s
                        if (s != -1 && s <= sqrt) {
                            for (int ms = (s * s) >> 1; ms <= max; ms += s) {
                                primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
                            }
                        }
                    }
                    // drain r
                    if (r != -1 && r <= sqrt) {
                        for (int mr = (r * r) >> 1; mr <= max; mr += r) {
                            primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                        }
                    }
                }
                // drain q
                if (q != -1 && q <= sqrt) {
                    for (int mq = (q * q) >> 1; mq <= max; mq += q) {
                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                    }
                }
            }
            // drain p
            if (p != -1 && p <= sqrt) {
                for (int mp = (p * p) >> 1; mp <= max; mp += p) {
                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                }
            }
        }

        return expandToArray(primes);
    }
}
