package net.gcvs.math;

import java.math.BigInteger;

import static net.gcvs.math.BigRational.*;

public interface BigRationals {
    static BigRational mediant(BigRational a, BigRational b) {
        return reduce(a.getNumerator().add(b.getNumerator()), a.getDenominator().add(b.getDenominator()));
    }

    /**
     * reduce and simplify are synonyms
     */
    static BigRational reduce(final BigInteger num, final BigInteger den) {
        return reduce(1, num, den);
    }

    /**
     * reduce and simplify are synonyms
     */
    static BigRational simplify(final BigInteger num, final BigInteger den) {
        return reduce(1, num, den);
    }

    /**
     * apply the gcd test to return an IRREDUCIBLE BigRational
     *
     * @param signum
     * @param num
     * @param den
     * @return
     */
    static BigRational reduce(final int signum, final BigInteger num, final BigInteger den) {
        int _signum = Integer.compare(signum, 0) * num.signum();

        if (den.signum() == 0) {
            if (_signum > 0) {
                return POSITIVE_INFINITY;
            }
            if (_signum < 0) {
                return NEGATIVE_INFINITY;
            }
            return NaN;
        }
        if (_signum == 0) {
            return ZERO;
        }

        _signum *= den.signum();

        BigInteger n = num.abs();
        BigInteger d = den.abs();
        if (n.compareTo(BigInteger.ONE) == 0) {
            return new BigRational(_signum, BigInteger.ONE, d, true);

        }
        if (d.compareTo(BigInteger.ONE) == 0) {
            return new BigRational(_signum, n, BigInteger.ONE, true);
        }
        BigInteger gcd = n.gcd(d);
        if (gcd.compareTo(BigInteger.ONE) > 0) {
            n = n.divide(gcd);
            d = d.divide(gcd);
        }
        return new BigRational(_signum, n, d, true);
    }
}
