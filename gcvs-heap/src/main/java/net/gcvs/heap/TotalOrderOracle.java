package net.gcvs.heap;

import java.util.*;

/**
 * Generic Comparator that can generate a usage report.  Useful for profiling algorithms that make use of comparison operations.
 *
 * @param <T>
 */
public class TotalOrderOracle<T> implements Comparator<T> {

    public enum Op {
        LESS_THAN, EQUALS, GREATER_THAN;

        public Op reverse() {
            return this == LESS_THAN ? GREATER_THAN : this == EQUALS ? EQUALS : LESS_THAN;
        }
    }

    private final int maxSize;

    private Comparator<T> comparator;

    private boolean growable;
    private int size;
    private Op[][] order;
    private boolean[][] checked;
    private Map<T, Integer> indexes = new IdentityHashMap<>();

    // true when complete ordering is found
    boolean totalOrder;

    boolean throwError;

    // counters
    private long compareCalls;
    private long factCalls;
    private int newFacts;
    private long redundantFacts;
    private long duplicateFacts;

    public TotalOrderOracle(final int n) {
        order = new Op[n][n];
        checked = new boolean[n][n];
        this.maxSize = n;
        for (int i = 0; i < n; i++) {
            order[i][i] = Op.EQUALS;
            checked[i][i] = true;
        }
    }

    public TotalOrderOracle(final Comparator<T> comparator) {
        this(16);
        this.comparator = comparator;
        this.growable = true;
    }

    public void countOnly() {
        indexes = null;
    }

    /**
     * Theoretical maximum number of binary comparisons to uniquely identify one of the n! permutations
     *
     * @param n size of unsorted input
     * @return n log n, the number of binary facts needed to identify one of n! permutations
     */
    public static int theoreticalMaximumFactsToIdentifyOneOfNFactorialPermutations(final int n) {
        return (int) Math.ceil(n * Math.log(n) / Math.log(2));
    }

    public TotalOrderOracle(final int n, final Comparator<T> comparator) {
        this(n);
        this.comparator = comparator;
    }

    public TotalOrderOracle(final T[] list, final Comparator<T> comparator) {
        this(list.length);
        this.comparator = comparator;
        this.size = list.length;
        this.indexes = new HashMap<>();
        for (int i = 0; i < list.length; i++) {
            indexes.put(list[i], i);
        }
    }

    private void grow(final int minCapacity) {
        if (minCapacity <= order.length) {
            return;
        }
        if (!growable) {
            throw new IllegalStateException("size is fixed");
        }
        final int newLength = order.length * 3 / 2;
        final Op[][] newOrder = new Op[newLength][];
        Arrays.setAll(newOrder, i -> {
            if (i < order.length) {
                return Arrays.copyOf(order[i], newLength);
            }
            final Op[] row = new Op[newLength];
            row[i] = Op.EQUALS;
            return row;
        });
        final boolean[][] newChecked = new boolean[newLength][];
        Arrays.setAll(newChecked, i -> {
            if (i < order.length) {
                return Arrays.copyOf(checked[i], newLength);
            }
            final boolean[] row = new boolean[newLength];
            row[i] = true;
            return row;
        });

        this.order = newOrder;
        this.checked = newChecked;
    }

    public void lessThan(final int a, final int b) {
        fact(Op.LESS_THAN, a, b);
    }

    public void equals(final int a, final int b) {
        fact(Op.EQUALS, a, b);
    }

    public void greaterThan(final int a, final int b) {
        fact(Op.GREATER_THAN, a, b);
    }

    public void cmp(final int a, final int b, final int cmp) {
        if (cmp < 0) {
            fact(Op.LESS_THAN, a, b);
        } else if (cmp == 0) {
            fact(Op.EQUALS, a, b);
        } else {
            fact(Op.GREATER_THAN, a, b);
        }
    }

    boolean square(final Op[][] a, final Op[][] c) {
        boolean changed = false;
        boolean totalOrder = true;
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            LOOP:
            for (int j = 0; j < len; j++) {
                Op cmp = a[i][j];
                if (cmp != null) {
                    c[i][j] = cmp;
                    continue;
                }
                // look for 1 level transitive relationship: A op B && B op C => A op C
                for (int k = 0; k < len; k++) {
                    final Op cmpi = a[i][k];
                    final Op cmpj = a[k][j];
                    if (cmpi != null && cmpi == cmpj) {
                        cmp = cmpi;
                        c[i][j] = cmp;
                        changed = true;
                        continue LOOP;
                    }
                }
                totalOrder = false;
            }
        }
        this.totalOrder = totalOrder;
        return changed;
    }

    boolean propagate(final int i, final int j) {
        final Op cmp = order[i][j];
        Objects.requireNonNull(cmp);
        return false;
    }

    Op transitive(final Op[][] a, final int i, final int j) {
        return null;
    }

    void fact(final Op relation, final int a, final int b) {
        factCalls++;

        final Op fact = order[a][b];

        final boolean duplicate = checked[a][b] || checked[b][a];
        if (duplicate) {
            duplicateFacts++;
        }
        checked[a][b] = true;
        checked[b][a] = true;

        if (fact == null) {
            newFacts++;

            order[a][b] = relation;
            order[b][a] = relation.reverse();
            // make all deductions

            // slower matrix multiplication method
            if (false) {
                propagateInfo();
            } else {

                // should be relatively efficient propagating the new comparison info
                // quicker than matrix multiplication
                final LinkedList<Spread> spreads = new LinkedList<>();
                spreads.add(new Spread(a, b, relation));
                while (spreads.size() > 0) {
                    final Spread spread = spreads.removeFirst();
                    spread(spread, spreads);
                }
            }

            if (totalOrder) {
                System.out.println("total ordering found");
            }

        } else {
            if (!duplicate) {
                redundantFacts++;
            }

            if (throwError) {
                throw new RuntimeException("fact already known");
            }
        }
    }

    private void propagateInfo() {
        final int len = order.length;
        Op[][] product = new Op[len][len];
        for (int i = 0; i < len; i++) {
            boolean changed = square(order, product);
            if (!changed) {
                break;
            }
            // update order with matrix multiplication result
            final Op[][] tmp = order;
            order = product;
            product = tmp;
        }
    }

    private static class Spread {
        final int a, b;
        final Op op;

        public Spread(final int a, final int b, final Op op) {
            this.a = a;
            this.b = b;
            this.op = op;
        }
    }

    private void spread(final Spread s, final LinkedList<Spread> spreads) {
        final int a = s.a;
        final int b = s.b;
        final Op op = s.op;

        final int len = order.length;
        for (int k = 0; k < len; k++) {
            if (k == a || k == b) {
                continue;
            }
            final Op kopa = order[k][a]; // a op k
            final Op bopk = order[b][k]; // k op b
            if (op == kopa && bopk == null) {
                // k op a && a op b => k op b
                order[k][b] = op;
                order[b][k] = op.reverse();
                spreads.add(new Spread(k, b, op));
            } else if (op == bopk && kopa == null) {
                // a op b && b op k => a op k
                order[a][k] = op;
                order[k][a] = op.reverse();
                spreads.add(new Spread(a, k, op));
            }
        }
    }

    @Override
    public int compare(final T o1, final T o2) {
        compareCalls++;
        final int cmp = comparator.compare(o1, o2);
        if (indexes != null) {
            Integer a = indexes.get(o1);
            if (a == null && (size < maxSize || growable)) {
                a = size++;
                indexes.put(o1, a);
            }
            Integer b = indexes.get(o2);
            if (b == null && (size < maxSize || growable)) {
                b = size++;
                indexes.put(o2, b);
            }

            grow(size);

            if (a == null || b == null) {
                throw new NullPointerException();
            }
            cmp(a, b, cmp);
        }
        return cmp;
    }

    public void report() {
        System.out.println("calls to compare: " + compareCalls + " (calls to the Comparable interface method)");
        System.out.println("   calls to fact: " + factCalls + " (calls to the TotalOrderOracle fact method)");
        System.out.println("       new facts: " + newFacts + " (calls to fact that presented new ordering info)");
        System.out.println(" duplicate facts: " + duplicateFacts + " (repeat calls with the same fact)");
        System.out.println(" redundant facts: " + redundantFacts + " (calls to fact, already deduced by previous facts)");
    }

    public void reset() {
        compareCalls = 0;
        factCalls = 0;
        duplicateFacts = 0;
        redundantFacts = 0;
    }

    public long getCompareCalls() {
        return compareCalls;
    }
}
