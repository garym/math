package net.gcvs.primes;

public class Stripes {
    private static final int ADDRESS_BITS_PER_WORD = 6;
    private static final int[] STRIPE_PRIMES = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53};

    static int stripe2(long[] stripe, int... primes) {
        final int addressBitsPerWord = ADDRESS_BITS_PER_WORD;
        final int stripeLength = stripe.length;
        long longs = 1;
        stripe[0] = 0xFFFFFFFFFFFFFFFFL;
        for (int p : primes) {
            long shiftLongs = longs;
            longs *= p;
            for (int end = (int) Math.min(longs, stripeLength); shiftLongs < end; shiftLongs <<= 1) {
                System.arraycopy(stripe, 0, stripe, (int) shiftLongs, (int) Math.min(shiftLongs, end - shiftLongs));
            }
            if (longs >= stripeLength) {
                return p;
            }
            int bitIndex = p >> 1;
            stripe[bitIndex >> addressBitsPerWord] &= ~(1L << bitIndex);

            int bitIndex1 = (p * p) >> 1;
            int wordIndex1 = bitIndex1 >> addressBitsPerWord;
            for (; wordIndex1 < longs; wordIndex1++) {
                bitIndex1 &= 0x3f;
                long strip = stripe[wordIndex1];
                for (; bitIndex1 < 64; bitIndex1 += p) {
                    strip &= ~(1L << bitIndex1);
                }
                stripe[wordIndex1] = strip;
            }
        }
        return 53;
    }

    static int stripe(long[] stripe, int... primes) {
        final int stripeLength = stripe.length;
        int longs = 1;
        stripe[0] = 0xFFFFFFFFFFFFFFFFL;
        for (int p : primes) {
            int shiftLongs = longs;
            longs *= p;
            for (int end = Math.min(longs, stripeLength); shiftLongs < end; shiftLongs <<= 1) {
                System.arraycopy(stripe, 0, stripe, shiftLongs, Math.min(shiftLongs, end - shiftLongs));
            }
            if (longs >= stripeLength) {
                return p;
            }
            PrimeGeneratorOddBits.clear(stripe, p);
            final int step = p << 1;
            final int n = longs << 7;
            for (int m = p * p; m < n; m += step) {
                PrimeGeneratorOddBits.clear(stripe, m);
            }
        }
        return 53;
    }

    static int sieveStripe2(long[] primes) {
        int nextPrime = stripe2(primes, STRIPE_PRIMES);

        primes[0] ^= 0b1;
        for (int p : STRIPE_PRIMES) {
            if (p == nextPrime) {
                break;
            }
            primes[0] ^= 1L << (p >> 1);
        }
        return nextPrime;
    }

    static int sieveStripe(long[] primes) {
        int nextPrime = stripe(primes, STRIPE_PRIMES);

        primes[0] ^= 0b1;
        for (int p : STRIPE_PRIMES) {
            if (p == nextPrime) {
                break;
            }
            primes[0] ^= 1L << (p >> 1);
        }
        return nextPrime;
    }

}
