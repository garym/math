package net.gcvs.math.algebra;

public record Complex<T>(T real, T imaginary) {
}
