package net.gcvs.util;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class TimingTest {
    private static final long FAST_MS = 10;
    private static final String MESSAGE = "MESSAGE";
    private static final String RESULT = "RESULT";

    List<String> log = new ArrayList<>();

    @BeforeEach
    void beforeAll() {
        Timing.logger = log::add;
    }

    @AfterAll
    static void afterAll() {
        Timing.logger = System.out::println;
    }

    void checkedRunnableDelay(long ms) throws InterruptedException {
        Thread.sleep(ms);
    }

    String callableDelay(long ms) throws InterruptedException {
        Thread.sleep(ms);
        return RESULT;
    }

    String supplierDelay(long ms) {
        return RESULT;
    }


    @Test
    void time_checkedRunnable() {
        Timing.time(() -> checkedRunnableDelay(FAST_MS));
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(" took \\d+ millis", log.get(0)));
    }

    @Test
    void time_callable() {
        String result = Timing.time(() -> callableDelay(FAST_MS));
        assertEquals(RESULT, result);
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(" took \\d+ millis", log.get(0)));
    }

    @Test
    void time_StringCheckedRunnable() {
        Timing.time(MESSAGE, () -> checkedRunnableDelay(FAST_MS));
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(MESSAGE + " took \\d+ millis", log.get(0)));
    }

    @Test
    void time_StringCallable() {
        String result = Timing.time(MESSAGE, () -> callableDelay(FAST_MS));
        assertEquals(RESULT, result);
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(MESSAGE + " took \\d+ millis", log.get(0)));
    }

    @Test
    void run() {
        Timing.run(() -> checkedRunnableDelay(FAST_MS));
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(" took \\d+ millis", log.get(0)));
    }

    @Test
    void run_String() {
        Timing.run(MESSAGE, () -> checkedRunnableDelay(FAST_MS));
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(MESSAGE + " took \\d+ millis", log.get(0)));
    }

    @Test
    void run_Exception() {
        assertThrows(RuntimeException.class, () -> Timing.run(() -> {
            throw new RuntimeException();
        }));
    }

    @Test
    void call() {
        final String result = Timing.call(() -> callableDelay(FAST_MS));
        assertEquals(RESULT, result);
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(" took \\d+ millis", log.get(0)));
    }

    @Test
    void call_String() {
        final String result = Timing.call(MESSAGE, () -> callableDelay(FAST_MS));
        assertEquals(RESULT, result);
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(MESSAGE + " took \\d+ millis", log.get(0)));
    }

    @Test
    void call_Exception() {
        assertThrows(RuntimeException.class, () -> Timing.call(() -> {
            throw new RuntimeException();
        }));
    }

    @Test
    void repeat() {
        final List<String> result = Timing.repeat(() -> supplierDelay(1), 5);
        assertEquals(5, result.size());
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(".*took \\d+ millis.*5 calls.*average.*", log.get(0)));
    }

    @Test
    void repeat_String() {
        final List<String> result = Timing.repeat(MESSAGE, () -> supplierDelay(1), 5);
        assertEquals(5, result.size());
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(MESSAGE + " took \\d+ millis.*5 calls.*average.*", log.get(0)));
    }

    @Test
    void lastTime() {
        Timing.call(() -> callableDelay(200));
        assertTrue(Timing.lastTime() >= 200);
    }

    @Test
    void start_tear() throws InterruptedException {
        Timing.start();
        callableDelay(200);
        final long time = Timing.tear(MESSAGE);
        assertTrue(time >= 200);
        assertEquals(1, log.size());
        assertTrue(Pattern.matches(MESSAGE + " took \\d+ millis", log.get(0)));
    }
}