package net.gcvs.util;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.LongStream;

/**
 * ArrayList functionality for long primitives
 *
 * @author gary
 */
public class LongArray {

    private long[] array;
    private int size;
    private final boolean shared;

    public LongArray() {
        this(10);
    }

    public LongArray(int capacity) {
        this(new long[capacity], 0, false);
    }

    public LongArray(long[] array) {
        this(array.clone(), array.length, false);
    }

    private LongArray(long[] array, int size, boolean shared) {
        this.array = array;
        this.size = size;
        this.shared = shared;
    }

    public static LongArray of(long[] array) {
        return new LongArray(array, array.length, true);
    }

    public int size() {
        return size;
    }

    public void add(long i) {
        if (size == array.length) {
            array = grow(size + 1);
        }
        array[size++] = i;
    }

    private long[] grow(int minCapacity) {
        if (shared) throw new UnsupportedOperationException("cannot grow shared long array");
        int oldCapacity = array.length;
        int newCapacity = newLength(oldCapacity, minCapacity - oldCapacity, oldCapacity >> 1);
        return array = Arrays.copyOf(array, newCapacity);
    }

    private static int newLength(int oldLength, int minGrowth, int prefGrowth) {
        int newLength = Math.max(minGrowth, prefGrowth) + oldLength;
        return newLength;
    }

    public long[] toArray() {
        return Arrays.copyOf(array, size);
    }

    public long remove(final int idx) {
        Objects.checkIndex(idx, size);
        long value = array[idx];
        System.arraycopy(array, idx + 1, array, idx, --size - idx);
        return value;
    }

    public LongStream stream() {
        return Arrays.stream(array, 0, size);
    }

    public long get(int idx) {
        Objects.checkIndex(idx, size);
        return array[idx];
    }

    public String toString() {
        return Arrays.toString(Arrays.copyOf(array, size));
    }

    public boolean contains(long l) {
        for (int i = 0, len = size; i < len; i++) {
            if (l == array[i]) {
                return true;
            }
        }
        return false;
    }
}
