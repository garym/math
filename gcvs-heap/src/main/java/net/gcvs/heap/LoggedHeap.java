package net.gcvs.heap;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class LoggedHeap<T> implements Heap<T> {

	private final Heap<T> heap;

	private long insertCalls;
	private long extractMinCalls;

	private long insertMillis;
	private long extractMinMillis;

	private long findMinCalls;
	private long findMinMillis;

	private long isEmptyCalls;
	private long isEmptyMillis;

	boolean record = true;

	Writer out;
	int chars;

	private void write(final String s) throws IOException {
		out.write(s);
		chars += s.length();
		if (chars >= 80) {
			chars = 0;
			// out.write("\n");
		}
	}

	public LoggedHeap(final Heap<T> heap) {
		this.heap = heap;
		try {
			out = new FileWriter("heap.log");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void insert(final T t) {
		insertCalls++;
		insertMillis -= System.currentTimeMillis();
		try {
			write("I");
			heap.insert(t);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			insertMillis += System.currentTimeMillis();
		}
	}

	@Override
	public T extractMin() {
		extractMinCalls++;
		extractMinMillis -= System.currentTimeMillis();
		try {
			write("E");
			return heap.extractMin();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			extractMinMillis += System.currentTimeMillis();
		}
	}

	@Override
	public T findMin() {
		findMinCalls++;
		findMinMillis -= System.currentTimeMillis();
		try {
			if (false)
				write("F");
			return heap.findMin();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			findMinMillis += System.currentTimeMillis();
		}
	}

	@Override
	public boolean isEmpty() {
		isEmptyCalls++;
		isEmptyMillis -= System.currentTimeMillis();
		try {
			return false;
		} finally {
			isEmptyMillis += System.currentTimeMillis();
		}
	}

	public void report() {
		System.out.println("     insert calls: " + insertCalls);
		System.out.println("    insert millis: " + insertMillis);
		System.out.println(" extractMin calls: " + extractMinCalls);
		System.out.println("extractMin millis: " + extractMinMillis);
		System.out.println("    findMin calls: " + findMinCalls);
		System.out.println("   findMin millis: " + findMinMillis);
	}
}
