package net.gcvs.primes;

/**
 * experimental
 */
public class PrimeGeneratorNativeBits implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;
    private static final int BITS_PER_WORD = 1 << ADDRESS_BITS_PER_WORD;

    /* Used to shift left or right for a partial word mask */
    private static final long WORD_MASK = 0xffffffffffffffffL;

    private static int wordIndex(int bitIndex) {
        return bitIndex >> ADDRESS_BITS_PER_WORD;
    }

    private static int nextSetBit(long[] words, int fromIndex) {
        final int wordsInUse = words.length;
        if (fromIndex < 0) throw new IndexOutOfBoundsException("fromIndex < 0: " + fromIndex);

        int u = wordIndex(fromIndex);
        if (u >= wordsInUse)
            return -1;

        long word = words[u] & (WORD_MASK << fromIndex);

        while (true) {
            if (word != 0) return (u * BITS_PER_WORD) + Long.numberOfTrailingZeros(word);
            if (++u == wordsInUse) return -1;
            word = words[u];
        }
    }

    private static void clear(long[] words, int bitIndex) {
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] &= ~(1L << bitIndex);
    }

    private static void set(long[] words, int bitIndex) {
        int wordIndex = wordIndex(bitIndex);

        words[wordIndex] |= (1L << bitIndex);
    }

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        // primes = 0..n
        long[] primes = new long[(n >> 6) + 1];
        primes[0] = 0xaaaaaaaaaaaaaaacL;
        for (int i = 1; i < primes.length; i++) {
            primes[i] = 0xaaaaaaaaaaaaaaaaL;
        }
        final int nmod = n & 0x3f;
        if (nmod != 0x3f) {
            primes[primes.length - 1] &= (1L << (nmod + 1)) - 1;
        }
        if (n == Integer.MAX_VALUE) {
            clear(primes, n);
        }
        int sqrt = (int) Math.ceil(Math.sqrt(n));
        int minsqrt = Math.min(63, sqrt);
        int p;
        for (p = nextSetBit(primes, 3); p != -1 && p <= minsqrt; p = nextSetBit(primes, p + 2)) {
            final int step = p << 1;
            for (int m = p * p; m >= 0 && m <= n; m += step) {
                clear(primes, m);
            }
        }
        for (; p != -1 && p <= sqrt; p = nextSetBit(primes, p + 2)) {
            final int step = p << 1;
            for (int m = p * p; m >= 0 && m <= n; m += step) {
                clear(primes, m);
            }
        }

        int count = 0;
        for (long prime : primes) {
            count += Long.bitCount(prime);
        }
        if (n == Integer.MAX_VALUE) {
            count++;
        }
        int[] ints = new int[count];
        ints[0] = 2;
        int top = 1;
        for (int i = nextSetBit(primes, 3); i != -1; i = nextSetBit(primes, i + 2)) {
            ints[top++] = i;
        }
        if (n == Integer.MAX_VALUE) {
            ints[top] = Integer.MAX_VALUE;
        }
        return ints;
    }
}
