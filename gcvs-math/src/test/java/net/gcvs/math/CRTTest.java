package net.gcvs.math;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigInteger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CRTTest {

    static Stream<Arguments> eea() {
        return Stream.of(
                Arguments.of(10, 14),
                Arguments.of(7, 5),
                Arguments.of(121393, 196418),
                Arguments.of(225851433717L, 139583862445L)
        );
    }

    @MethodSource
    @ParameterizedTest
    void eea(long a, long b) {

        final long[] eea = CRT.eea(a, b);

        long gcd = eea[0] * a + eea[1] * b;

        assertTrue(gcd >= 1);
        assertTrue(gcd <= a);
        assertTrue(gcd <= b);
        assertEquals(0, a % gcd);
        assertEquals(0, b % gcd);
    }

    static Stream<Arguments> inverse() {
        return Stream.of(
                Arguments.of(5, 7),
                Arguments.of(7, 13),
                Arguments.of(121393, 196418),
                Arguments.of(139583862445L, 225851433717L),
                Arguments.of(1, 1024),
                Arguments.of(1023, 1024)
        );
    }

    @MethodSource
    @ParameterizedTest
    void inverse(long x, long n) {

        long gcd = GCD.gcd(x, n);
        long inverse = CRT.inverse(x, n);

        assertEquals(BigInteger.valueOf(gcd), BigInteger.valueOf(x).multiply(BigInteger.valueOf(inverse)).mod(BigInteger.valueOf(n)));
    }

}