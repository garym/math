package net.gcvs.math.algebra;

public record DivMod<V>(V div, V mod) {
}

