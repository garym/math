package net.gcvs.math;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@org.junit.jupiter.api.Disabled
public class PartitionGeneratorTest {
    /**
     * https://oeis.org/A000041
     */
    public static final int[] PARTITION_SEQUENCE = {
            1, 1, 2, 3, 5, 7, 11, 15, 22, 30,
            42, 56, 77, 101, 135, 176, 231, 297, 385, 490,
            627, 792, 1002, 1255, 1575, 1958, 2436, 3010, 3718, 4565,
            5604, 6842, 8349, 10143, 12310, 14883, 17977, 21637, 26015, 31185,
            37338, 44583, 53174, 63261, 75175, 89134, 105558, 124754, 147273, 173525
    };

    @Test
    @Tag("UnitTest")
    void test_PartitionGenerator_int() {
        for (int i = 1; i < 10; i++) {
            PartitionGenerator gen = new PartitionGenerator(i);
            assertEquals(i, gen.getN());
            assertEquals(1, gen.getMinPartSize());
            assertEquals(i, gen.getMaxPartSize());
            assertEquals(1, gen.getMinPartCount());
            assertEquals(i, gen.getMaxPartCount());
        }

        assertThrows(IllegalStateException.class, () -> new PartitionGenerator(0));
        assertThrows(IllegalStateException.class, () -> new PartitionGenerator(-1));
    }

    @Test
    @Tag("UnitTest")
    void test_PartitionGenerator_int_int_int() {
        PartitionGenerator gen = new PartitionGenerator(5, 3, 2);
        assertEquals(5, gen.getN());
        assertEquals(1, gen.getMinPartSize());
        assertEquals(3, gen.getMaxPartSize());
        assertEquals(1, gen.getGivenMinPartCount());
        assertEquals(2, gen.getMaxPartCount());

        assertThrows(IllegalStateException.class, () -> new PartitionGenerator(5, 1, 4));
        assertThrows(IllegalStateException.class, () -> new PartitionGenerator(7, 3, 2));
    }

    @Test
    @Tag("UnitTest")
    void test_PartitionGenerator_int_int_int_int_int() {
        PartitionGenerator gen = new PartitionGenerator(7, 2, 4, 3, 5);
        assertEquals(7, gen.getN());
        assertEquals(2, gen.getMinPartSize());
        assertEquals(4, gen.getMaxPartSize());
        assertEquals(3, gen.getMinPartCount());
        assertEquals(5, gen.getGivenMaxPartCount());

        assertThrows(IllegalStateException.class, () -> new PartitionGenerator(5, 2, 5, 3, 5));
        assertThrows(IllegalStateException.class, () -> new PartitionGenerator(7, 3, 2, 1, 5));
        assertThrows(IllegalStateException.class, () -> new PartitionGenerator(7, 3, 3, 6, 5));
    }


    /**
     * validate reference p(n) sequences
     */
    @Test
    @Tag("UnitTest")
    void test_PARTITION_SEQUENCE_count() {
        for (int n = 1; n < PARTITION_SEQUENCE.length; n++) {
            PartitionGenerator gen = new PartitionGenerator(n);
            assertEquals(PARTITION_SEQUENCE[n], count(gen));
        }
    }

    /**
     * validate reference p(n) sequences
     */
    @Test
    @Tag("UnitTest")
    void test_PARTITION_SEQUENCE_parts() {
        for (int n = 1; n < PARTITION_SEQUENCE.length; n++) {
            PartitionGenerator gen = new PartitionGenerator(n);
            assertAllPartsValid(gen);
        }
    }

    @Test
    @Tag("UnitTest")
    void test_p_31_1_3_13_13() {
        verifyRangedPartition(31, 1, 3, 13, 13);
    }

    @Test
    @Tag("UnitTest")
    void test_p_31_2_4_8_9() {
        verifyRangedPartition(31, 2, 4, 8, 9);
    }

    @Test
    @Tag("UnitTest")
    void test_p_31_2_5_7_8() {
        // 2 must be immediately transferred to sum
        verifyRangedPartition(31, 2, 5, 7, 8);
    }

    @Test
    @Tag("UnitTest")
    void test_p_31_3_5_7_8() {
        verifyRangedPartition(31, 3, 5, 7, 8);
    }

    @Test
    @Tag("UnitTest")
    void test_p_31_3_6_6_7() {
        verifyRangedPartition(31, 3, 6, 6, 7);
    }

    @Test
    @Tag("UnitTest")
    void test_p_31_4_8_4_6() {
        verifyRangedPartition(31, 4, 8, 4, 6);
    }

    @Test
    @Tag("UnitTest")
    void test_reset() {
        PartitionGenerator gen = new PartitionGenerator(31, 5, 10, 1, 5);
        gen.next();
        int[] part = gen.parts();
        System.out.println(gen);
        assertArrayEquals(new int[]{10, 10, 6, 5, 0}, part);
    }

    @Test
    @Tag("UnitTest")
    void test_toArray() {
        PartitionGenerator gen = new PartitionGenerator(10);
        while (gen.next()) {
            int[] parts = gen.toArray();
            assertArrayAreTheSame(gen.parts(), parts);
        }
    }

    @Test
    @Tag("AdhocTest")
    void test_parts() {
        PartitionGenerator gen = new PartitionGenerator(31, 3, 5, 7, 8);
        PartitionGenerator g = new PartitionGenerator(gen.getN());

        System.out.println("expected");
        while (g.next()) {
            if (gen.isPartitionValid(g.parts())) {
                System.out.println(g);
            }
        }

        System.out.println("actual");
        while (gen.next()) {
            System.out.println(gen);
            if (Arrays.equals(gen.parts(), new int[]{8, 7, 6, 5, 5, 0})) {
                System.out.println("next one breaks");
            }
        }
    }

    @Test
    @Tag("AdhocTest")
    void test_print() {
        int n = 110;
        PartitionGenerator gen = new PartitionGenerator(n, 1, n, 1, n);
        long count = 0;
        while (gen.next()) {
//            System.out.println(gen);
            count++;
        }
        System.out.println("generated " + count + " partitions");
        System.out.println(" counted " + PartitionGenerator.loops + " loops");
        System.out.println(" counted " + PartitionGenerator.tests + " tests");
        System.out.println(" counted " + PartitionGenerator.testParts + " testParts");
    }

    static int verifyRangedPartition(int n, int minPartSize, int maxPartSize, int minPartCount, int maxPartCount) {
        String str = "p(" + n + ", " + minPartSize + ", " + maxPartSize + ", " + minPartCount + ", " + maxPartCount + ")";
        // count values
        PartitionGenerator gen = new PartitionGenerator(n, minPartSize, maxPartSize, minPartCount, maxPartCount);
        PartitionGenerator ngen = new PartitionGenerator(gen.getN());
        int count = 0;
        while (ngen.next()) {
            if (gen.isPartitionValid(ngen.parts())) {
                count++;
            }
        }
        assertEquals(count, count(gen), str);

        // compare values
        gen = new PartitionGenerator(n, minPartSize, maxPartSize, minPartCount, maxPartCount);
        ngen = new PartitionGenerator(gen.getN());
        while (ngen.next()) {
            if (gen.isPartitionValid(ngen.parts())) {
                assertTrue(gen.next());
                assertArrayAreTheSame(ngen.parts(), gen.parts());
            }
        }
        return count;
    }

    private static void assertArrayAreTheSame(int[] list, int[] prefix) {
        assertTrue(prefix.length <= list.length);
        for (int i = 0; i < prefix.length; i++) {
            assertEquals(list[i], prefix[i]);
        }
        for (int i = prefix.length; i < list.length; i++) {
            assertEquals(0, list[i]);
        }
    }

    public static int count(PartitionGenerator gen) {
        int count = 0;
        while (gen.next()) {
            count++;
        }
        return count;
    }

    public void assertAllPartsValid(PartitionGenerator gen) {
        while (gen.next()) {
            assertValidPart(gen);
        }
    }

    public static void assertValidPart(PartitionGenerator gen) {
        int[] parts = gen.parts();
        int len = 0;
        int sum = 0;
        int max = parts[0];
        int min = max;
        for (int part : parts) {
            if (part == 0) {
                break;
            }
            len++;
            sum += part;
            min = Math.min(part, min);
        }

        assertEquals(gen.getN(), sum);
        assertTrue(max <= gen.getMaxPartSize());
        assertTrue(min >= gen.getMinPartSize());
        assertTrue(len <= gen.getMaxPartCount());
        assertTrue(len >= gen.getMinPartCount());
    }
}
