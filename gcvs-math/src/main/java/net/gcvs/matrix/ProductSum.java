package net.gcvs.matrix;

import java.util.List;

public class ProductSum extends Expression {
	private final int[] productFactors;
	private final Product[] products;

	public ProductSum(final Terms terms, int[] factors, final int[] productFactors, final Product[] productList) {
		super(terms, factors);
		this.productFactors = productFactors;
		this.products = productList;
	}

	public String toString() {
		final StringBuilder sb = new StringBuilder();
		String sep = "";
		for (int i = 0, len = products.length; i < len; i++) {
			final Product p = products[i];
			final int factor = productFactors[i];
			if (factor >= 1) {
				sb.append(sep);
				if (factor > 1) {
					sb.append(factor);
				}
			} else {
				sb.append(" - ");
				if (factor < -1) {
					sb.append(-factor);
				}
			}
			sb.append(p);
			sep = " + ";
		}
		return sb.toString();
	}

	public static ProductSum add(final Terms terms, final int[] productFactors, final Product[] productList) {
		final int[] termFactors = terms.newInstance();
		for (int i = 0; i < productList.length; i++) {
			final Product p = productList[i];
			for (int f = 0; f < p.factors.length; f++) {
				termFactors[f] += productFactors[i] * p.factors[f];
			}
		}
		return new ProductSum(terms, termFactors, productFactors, productList);
	}

	public Product product(final int idx) {
		return products[idx];
	}
	
	public static ProductSum create(final Terms terms, final int[] seq, final List<Product> products) {
		final int length = Sequence.cardinality(seq);
		final int[] termFactors = terms.newInstance();
		final int[] productFactors = new int[length];
		final Product[] ps = new Product[length];
		int idx = 0;
		for (int i = 0; i < seq.length; i++) {
			final int factor = seq[i];
			if (seq[i] == 0) {
				continue;
			}
			productFactors[idx] = factor;
			ps[idx] = products.get(i);
			ps[idx].addTo(factor, termFactors);
			idx++;
		}
		return new ProductSum(terms, termFactors, productFactors, ps);
	}
}
