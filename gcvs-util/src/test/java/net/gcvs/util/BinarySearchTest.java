package net.gcvs.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.function.IntToLongFunction;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BinarySearchTest {
    @Test
    void binarySearch_whenBadRange_thenExceptionThrown() {
        IntToLongFunction fn = x -> x * (long) x;
        assertThrows(IllegalArgumentException.class, () -> BinarySearch.binarySearch(fn, 10, 5, 36));
    }

    @Test
    void binarySearch_whenZeroRange_thenReturnFromOffset() {
        IntToLongFunction fn = x -> x * (long) x;
        assertEquals(10, BinarySearch.binarySearch(fn, 10, 10, 36));
    }

    @Test
    void binarySearch_whenValueFoundAtStart_thenReturnStart() {
        IntToLongFunction fn = x -> x * (long) x;
        assertEquals(5, BinarySearch.binarySearch(fn, 5, 10, 25));
    }

    @Test
    void binarySearch_whenValueFoundAtNearEnd_thenReturnValue() {
        IntToLongFunction fn = x -> x * (long) x;
        assertEquals(9, BinarySearch.binarySearch(fn, 5, 10, 81));
    }

    @Test
    void binarySearch_whenDuplicateValuesInFunction_thenReturnAnyMatchingIndex() {
        IntToLongFunction fn = x -> 1;
        int index = BinarySearch.binarySearch(fn, 0, 10, 1);
        assertEquals(0, index);
    }

    @Test
    void binarySearch_whenKeyAtMidPoint_thenReturnMidPoint() {
        assertEquals(2, BinarySearch.binarySearch(of(1, 2, 3, 4, 5), 0, 5, 3));
    }

    static Stream<Arguments> binarySearch_whenFunction() {
        IntToLongFunction xSquared = x -> x * (long) x;
        IntToLongFunction halfX = x -> x / 2;
        return Stream.of(
                Arguments.of(xSquared),
                Arguments.of(halfX)
        );
    }

    @ParameterizedTest
    @MethodSource("binarySearch_whenFunction")
    void binarySearch_whenFunctionExpandedIntoArray_thenSearchMatchesArraysBinarySearch(IntToLongFunction fn) {
        final long[] extracted = extract(fn, 0, 100);

        for (int key = -10; key <= 102 * 102; key++) {
            final int index = BinarySearch.binarySearch(fn, 0, 100, key);
            int aidx = Arrays.binarySearch(extracted, key);
            if (aidx < 0) {
                assertEquals(aidx, index);
            } else {
                assertEquals(key, extracted[aidx]);
            }
        }
    }

    static IntToLongFunction of(long... longs) {
        return i -> longs[i];
    }

    static long[] extract(IntToLongFunction fn, int from, int to) {
        long[] list = new long[to - from];
        for (int i = 0; i < list.length; i++) {
            list[i] = fn.applyAsLong(from + i);
        }
        return list;
    }
}