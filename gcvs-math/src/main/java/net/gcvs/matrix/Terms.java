package net.gcvs.matrix;

import java.util.Arrays;

public final class Terms {
	public final int length;
	final String[] names;

	public Terms(final String[] names) {
		this.names = names;
		this.length = names.length;
	}

	public String[] export() {
		return names.clone();
	}

	public String name(final int term) {
		return names[term];
	}

	public String[] copyNames() {
		return names.clone();
	}

	public int findTerm(final String term) {
		final String[] terms = this.names;
		for (int i = 0; i < terms.length; i++) {
			if (term.equals(terms[i])) {
				return i;
			}
		}
		return -1;
	}

	public int[] newInstance() {
		return new int[length];
	}

	/**
	 * currently unused
	 */
	public Expression generate(final String... names) {
		final int[] factors = new int[names.length];
		Arrays.fill(factors, 1);
		return generate(factors, names);
	}

	public Expression generate(final int[] factors, final String[] names) {
		final int[] set = newInstance();
		for (int i = 0, len = names.length; i < len; i++) {
			final int t = findTerm(names[i]);
			if (t == -1) {
				// throws exception if name not found
				throw new RuntimeException(names[i]);
			}
			set[t] = factors[i];
		}
		return new Expression(this, set);
	}

	public String toString() {
		return Arrays.toString(names);
	}

	public Expression expression(final int[] terms) {
		return new Expression(this, terms);
	}

	public static Terms create(final String[] as, final String[] bs) {
		final String[] list = new String[as.length * bs.length];
		int idx = 0;
		for (int i = 0; i < as.length; i++) {
			for (int j = 0; j < bs.length; j++) {
				list[idx++] = as[i] + bs[j];
			}
		}
		return new Terms(list);
	}
}