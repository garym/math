package net.gcvs.stopwatch.aspect;

import net.gcvs.stopwatch.Stopwatch;
import net.gcvs.stopwatch.Watch;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StopwatchAspectTest {

    @Watch
    void work() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void time() {
        work();
    }

}