package net.gcvs.math.algebra;

public class ComplexAlgebra<T> implements Algebra<Complex<T>> {

    private final Complex<T> zero;
    private final Complex<T> one;

    final Algebra<T> algebra;

    public ComplexAlgebra(final Algebra<T> algebra) {
        this.algebra = algebra;
        this.zero = new Complex<>(algebra.zero(), algebra.zero());
        this.one = new Complex<>(algebra.one(), algebra.zero());
    }

    @Override
    public Complex<T> zero() {
        return zero;
    }

    @Override
    public Complex<T> one() {
        return one;
    }

    @Override
    public Complex<T> constant(long value) {
        return new Complex<>(algebra.constant(value), algebra.zero());
    }

    @Override
    public Complex<T> add(Complex<T> a, Complex<T> b) {
        return new Complex<>(algebra.add(a.real(), b.real()), algebra.add(a.imaginary(), b.imaginary()));
    }

    @Override
    public Complex<T> subtract(Complex<T> a, Complex<T> b) {
        return new Complex<>(algebra.subtract(a.real(), b.real()), algebra.subtract(a.imaginary(), b.imaginary()));
    }

    @Override
    public Complex<T> multiply(Complex<T> a, Complex<T> b) {
        return new Complex<>(algebra.subtract(algebra.multiply(a.real(), b.real()), algebra.multiply(a.imaginary(), b.imaginary())),
                algebra.add(algebra.multiply(a.real(), b.imaginary()), algebra.multiply(a.imaginary(), b.real())));
    }

    @Override
    public Complex<T> divide(Complex<T> a, Complex<T> b) {
        // realise the denominator
        final T den = algebra.add(algebra.multiply(b.real(), b.real()), algebra.multiply(b.imaginary(), b.imaginary()));
        return new Complex<>(
                algebra.divide(algebra.add(algebra.multiply(a.real(), b.real()), algebra.multiply(a.imaginary(), b.imaginary())), den),
                algebra.divide(algebra.subtract(algebra.multiply(a.imaginary(), b.real()), algebra.multiply(a.real(), b.imaginary())), den)
        );
    }

    @Override
    public DivMod<Complex<T>> divideAndRemainder(Complex<T> a, Complex<T> b) {
        return null;
    }

    @Override
    public Complex<T>[] newArray(int length) {
        return new Complex[length];
    }

    @Override
    public String toString(Complex<T> a) {
        final T zero = algebra.zero();
        if (zero.equals(a.imaginary())) {
            return algebra.toString(a.real());
        }
        if (zero.equals(a.real())) {
            if (algebra.one().equals(a.imaginary())) {
                return "i";
            }
            return algebra.toString(a.imaginary()).concat("i");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append('(');
        sb.append(algebra.toString(a.real()));
        sb.append(" + ");
        sb.append(algebra.toString(a.imaginary()));
        sb.append("i)");
        return sb.toString();
    }
}
