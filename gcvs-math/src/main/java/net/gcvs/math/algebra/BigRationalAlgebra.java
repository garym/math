package net.gcvs.math.algebra;

import net.gcvs.math.BigRational;

public class BigRationalAlgebra implements Algebra<BigRational> {

    private final boolean simplify;

    public BigRationalAlgebra() {
        this(true);
    }

    public BigRationalAlgebra(boolean simplify) {
        this.simplify = simplify;
    }

    private BigRational simplify(BigRational r) {
        return simplify ? r.simplify() : r;
    }

    @Override
    public BigRational zero() {
        return BigRational.ZERO;
    }

    @Override
    public BigRational one() {
        return BigRational.ONE;
    }

    @Override
    public BigRational constant(long value) {
        return BigRational.valueOf(value);
    }

    @Override
    public BigRational add(BigRational a, BigRational b) {
        return simplify(a.add(b));
    }

    @Override
    public BigRational subtract(BigRational a, BigRational b) {
        return simplify(a.subtract(b));
    }

    @Override
    public BigRational multiply(BigRational a, BigRational b) {
        return simplify(a.multiply(b));
    }

    @Override
    public BigRational divide(BigRational a, BigRational b) {
        return simplify(a.divide(b));
    }

    @Override
    public DivMod<BigRational> divideAndRemainder(BigRational a, BigRational b) {
        BigRational[] bigRationals = a.divideAndRemainder(b);
        return new DivMod<>(simplify(bigRationals[0]), simplify(bigRationals[1]));
    }

    @Override
    public BigRational[] newArray(int length) {
        return new BigRational[length];
    }

    @Override
    public String toString(BigRational a) {
        return a.toString();
    }
}
