package net.gcvs.primes;

import java.util.BitSet;

class PrimesHelper {
    static int[] toArray(BitSet set) {
        int[] ints = new int[set.cardinality()];
        int idx = 0;
        boolean max = set.get(Integer.MAX_VALUE);
        if (max) {
            set.clear(Integer.MAX_VALUE);
        }
        for (int i = set.nextSetBit(0); i != -1; i = set.nextSetBit(i + 1)) {
            ints[idx++] = i;
        }
        if (max) {
            ints[idx] = Integer.MAX_VALUE;
            set.set(Integer.MAX_VALUE);
        }
        return ints;
    }
}
