package net.gcvs.math.polynomial;

public record Term<V>(long degree, V coefficient) {
}
