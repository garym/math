package net.gcvs.util;

import lombok.Getter;

import java.util.List;
import java.util.Random;

/**
 * I keep re-inventing the shuffle wheel, so here is a reusable version.
 * <p>
 * not threadsafe yet.
 *
 * @author gary
 */
public class Shuffle {

    @Getter
    private final Random random;

    public Shuffle(Random random) {
        this.random = random;
    }

    public <T> void shuffle(final T[] list) {
        for (int i = list.length; i > 0; ) {
            final int idx = random.nextInt(i--);
            final T tmp = list[idx];
            list[idx] = list[i];
            list[i] = tmp;
        }
    }

    public void shuffle(final int[] list) {
        for (int i = list.length; i > 0; ) {
            final int idx = random.nextInt(i--);
            final int tmp = list[idx];
            list[idx] = list[i];
            list[i] = tmp;
        }
    }

    public void shuffle(final long[] list) {
        for (int i = list.length; i > 0; ) {
            final int idx = random.nextInt(i--);
            final long tmp = list[idx];
            list[idx] = list[i];
            list[i] = tmp;
        }
    }

    public <T> void shuffle(final List<T> list) {
        for (int i = list.size(); i > 0; ) {
            final int idx = random.nextInt(i--);
            final T tmp = list.get(idx);
            list.set(idx, list.get(i));
            list.set(i, tmp);
        }
    }
}
