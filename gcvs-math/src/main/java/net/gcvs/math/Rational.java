package net.gcvs.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public abstract class Rational extends Number implements Comparable<Rational> {

    /**
     * Similar to {@link BigDecimal#intValueExact()}
     *
     * @throws ArithmeticException
     */
    public abstract int intValueExact();

    /**
     * Similar to {@link BigDecimal#longValueExact()}
     *
     * @throws ArithmeticException
     */
    public abstract long longValueExact();

    /**
     * Similar to {@link Double#isNaN()}
     */
    public abstract boolean isNaN();

    /**
     * Similar to {@link Double#isInfinite()}
     */
    public abstract boolean isInfinite();

    /**
     * Similar to {@link BigDecimal#signum()}
     */
    public abstract int signum();


    public abstract BigInteger getNumerator();

    public abstract BigInteger getDenominator();

    public abstract boolean isSimplestForm();

    public abstract Rational negate();

    public abstract Rational reciprocal();

    public abstract Rational abs();

    public abstract Rational reduce();

    /**
     * this translation can be lossy.
     */
    public BigDecimal toBigDecimal() {
        final var num = getNumerator();
        final var den = getDenominator();
        final var signum = signum();
        BigDecimal numDecimal = new BigDecimal(num);
        BigDecimal denDecimal = new BigDecimal(den);

        // this comment and code snippet is taken from BigDecimal divide() implementation
        // if the decimal expansion terminates, the expansion can have no more than (a.precision() + ceil(10*b.precision)/3) digits
        MathContext mc = new MathContext((int) Math.min(numDecimal.precision() +
                        (long) Math.ceil(10.0 * denDecimal.precision() / 3.0),
                Integer.MAX_VALUE),
                RoundingMode.HALF_UP);

        BigDecimal d = numDecimal.divide(new BigDecimal(den), mc);
        return signum >= 0 ? d : d.negate();
//        return new BigDecimal(getNumerator()).divide(new BigDecimal(getDenominator()), MathContext.DECIMAL32);
    }
}
