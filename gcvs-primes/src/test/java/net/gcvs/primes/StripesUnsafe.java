package net.gcvs.primes;

import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.Arrays;

public class StripesUnsafe {
    static final Unsafe unsafe;

    static {
        try {
            Field f = Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);
            unsafe = (Unsafe) f.get(null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    {
        long[] list = new long[10];
        Arrays.setAll(list, i -> i + 1);

        System.out.println(Arrays.toString(list));
        for (int i = 0; i < list.length; i++) {
            System.out.println(unsafe.getLong(list, Unsafe.ARRAY_LONG_BASE_OFFSET + i * Unsafe.ARRAY_LONG_INDEX_SCALE));
        }

        copyMemory(list, 2, 4, 4);
        System.out.println(Arrays.toString(list));

        unsafe.copyMemory(0l, 0l, 0l);

    }

    public static void copyMemory(long[] array, int from, int to, int length) {
        unsafe.copyMemory(
                array, Unsafe.ARRAY_LONG_BASE_OFFSET + from * Unsafe.ARRAY_LONG_INDEX_SCALE,
                array, Unsafe.ARRAY_LONG_BASE_OFFSET + to * Unsafe.ARRAY_LONG_INDEX_SCALE,
                length * Unsafe.ARRAY_LONG_INDEX_SCALE);
    }

    public static long sizeOf(Object object) {
        return unsafe.getAddress(normalize(unsafe.getInt(object, 4L)) + 12L);
    }

    private static long normalize(int value) {
        if (value >= 0) return value;
        return (~0L >>> 32) & value;
    }

    static long toAddress(Object obj) {
        Object[] array = new Object[]{obj};
        long baseOffset = unsafe.arrayBaseOffset(Object[].class);
        return normalize(unsafe.getInt(array, baseOffset));
    }

    static Object fromAddress(long address) {
        Object[] array = new Object[]{null};
        long baseOffset = unsafe.arrayBaseOffset(Object[].class);
        unsafe.putLong(array, baseOffset, address);
        return array[0];
    }

}
