package net.gcvs.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.gcvs.matrix.storage.Store;

public class Span {
	final Store store;
	final Terms terms;

	List<Expression> targets = new ArrayList<>();

	public List<Product> pool = new ArrayList<>();

	public Span(final Store store) {
		this.store = store;
		this.terms = store.terms;
	}

	static class NameSign {
		List<String> names = new ArrayList<>();
		List<Integer> signs = new ArrayList<>();
	}

	public static NameSign read(final String e1) {
		NameSign ns = new NameSign();
		List<String> names = ns.names;
		List<Integer> signs = ns.signs;
		Integer sign = 1;
		for (int i = 0; i < e1.length(); i++) {
			char c = e1.charAt(i);
			switch (c) {
			case ' ':
				continue;
			case '+':
				sign = 1;
				break;
			case '-':
				sign = -1;
				break;
			default:
				names.add(Character.toString(c));
				signs.add(sign);
				sign = null;
			}
		}
		return ns;
	}

	public void add(final String e1, final String e2) {
		NameSign ns1 = read(e1);
		NameSign ns2 = read(e2);
		String[] e1Names = ns1.names.toArray(new String[ns1.names.size()]);
		List<Integer> e1Signs = ns1.signs;
		String[] e2Names = ns2.names.toArray(new String[ns2.names.size()]);
		List<Integer> e2Signs = ns2.signs;

		Sequence s1 = new Sequence(0, 1, e1Names.length);
		Sequence s2 = new Sequence(0, 1, e2Names.length);
		int[] s1seq = s1.create();
		int[] s2seq = s2.create();
		int[] f1 = s1seq.clone();
		int[] f2 = s2seq.clone();
		SEQ1: do {
			if (s1seq[0] == 0) {
				continue;
			}
			for (int i = 0; i < f1.length; i++) {
				f1[i] = s1seq[i] == 1 ? 1 : -1;
				Integer sgn = e1Signs.get(i);
				if (sgn != null && sgn != f1[i]) {
					continue SEQ1;
				}
			}
			Expression exp1 = store.m.terms().generate(f1, e1Names);
			s2.reset(s2seq);
			SEQ2: do {
				if (s2seq[0] == 0) {
					continue;
				}
				for (int i = 0; i < f2.length; i++) {
					f2[i] = s2seq[i] == 1 ? 1 : -1;
					Integer sgn = e2Signs.get(i);
					if (sgn != null && sgn != f2[i]) {
						continue SEQ2;
					}
				}
				Expression exp2 = store.n.terms().generate(f2, e2Names);

				Product p = Product.multiply(terms, exp1, exp2);
				pool.add(p);

			} while (s2.next(s2seq));
		} while (s1.next(s1seq));

	}

	public static void main(String[] args) {
		Store store = new Store(3, 3, 3);
		System.out.println(store.m);
		System.out.println(store.n);
		System.out.println(store.m.mul(store.n));

		System.out.println(Arrays.toString(store.m.terms().export()));
		System.out.println(Arrays.toString(store.n.terms().export()));

		Span span = new Span(store);

//		|A B|   |E F|   |AE + BG AF + BH|
//		|C D| * |G H| = |CE + DG CF + DH|
		
//		|A B C|   |J K L|   |AJ + BM + CP AK + BN + CQ AL + BO + CR|
//		|D E F| * |M N O| = |DJ + EM + FP DK + EN + FQ DL + EO + FR|
//		|G H I|   |P Q R|   |GJ + HM + IP GK + HN + IQ GL + HO + IR|


//		span.add("A B", "J K");
//		span.add("B", "J");
//		span.add("A", "K");

		
		if (true) {
			span.add("A+E+I", "J+N+R");

			span.add("A D G", "J K L");
			span.add("B E H", "M N O");
			span.add("C F I", "P Q R");
//
//			span.add("A B C", "N");
//			span.add("A B C", "R");
//			span.add("D E F", "J");
//			span.add("D E F", "R");
//			span.add("G H I", "J");
//			span.add("G H I", "N");
			// span.add("A B C", "N R");
			// span.add("D E F", "A R");
			// span.add("G H I", "A N");

		}
		
		System.out.println(span.pool);
		System.out.println(span.pool.size());
		
		Expression t = store.targets[0];
		System.out.println("target: " + t);
		List<ProductSum> pss = span.find(t, false);
		for (ProductSum ps : pss) {
			System.out.println("product sum: " + ps);
		}



		// AL + BO + CR
		// (A B C) (O R) + (A-B)(L -O R)
		// AO+ AR+ BO BR+ CO? _CR _AL AO- AR- BL? BO BR- = _AL BO CR
		// BO CO

		// (A + B + C) (O + R) + (A + B)(L - O - R)
		// AO + AR + BO + BR + CO + CR + AL - AO - AR + BL - BO - BR =
		// CO + CR + AL + BL - (CO + BL - BO)

		// (A B C) (J M P) = AJ BM CP
		// 9 (A E I) (J N R) = AJ* AN/ AR/ EJ/ EN/ ER IJ/ IN IR/
		// 4 (B E) (M N) = BM* BN/ EM/ EN/
		// 4 (C I) (P R) = CP* CR/ IP/ IR/
		// 6 (A B C) (N R) = AN/ AR/ BN/ BR CN CR/
		// 6 (E I) (J M P) = EJ/ EM/ EP IJ/ IM IP/
		// ER IN BR CN IM EP

		// (D E F) (K N Q) = DK + EN + FQ
		// (A E I) (J N R) = AJ/ AN/ AR EJ/ EN* ER/ IJ IN/ IR/
		// (A D) (J K) = AJ/ AK/ DJ/ DK*
		// (F I) (Q R) = FQ* FR/ IQ/ IR/
		// (D E F) (J R) = DJ/ DR EJ/ ER/ FJ FR/
		// (A I) (K N Q) = AK/ AN/ AQ IK IN/ IQ/
		// AR IJ DR FJ AQ IK

		// (G H I) (L O R) = GL HO IR
		// (A E I) (J N R) = AJ/ AN AR/ EJ EN/ ER/ IJ/ IN/ IR*
		// (A G) (J L) = AJ/ AL/ GJ/ GL*
		// (E H) (N O) = EN/ EO/ HN/ HO*
		// (G H I) (J N) = GJ/ GN HJ HN/ IJ/ IN/
		// (A E) (L O R) = AL/ AO AR/ EL EO/ ER/
		// AN EJ GN HJ AO EL

		// (D E F) (J M P) = DJ EM FP

	}

	public List<ProductSum> find(final Expression e, final boolean first) {
		List<ProductSum> list = new ArrayList<>();
		int[] sum = terms.newInstance();
		Sequence sequence = new Sequence(-1, 1, pool.size());
		final int[] seq = sequence.create();
		do {
			Arrays.fill(sum, 0);
			for (int i = 0; i < seq.length; i++) {
				final int factor = seq[i];
				if (factor == 0) {
					continue;
				}
				pool.get(i).addTo(factor, sum);
			}
//			System.out.println(Arrays.toString(seq) + ": " + new Expression(terms, sum));

			if (e.matches(sum)) {
				System.out.println("found: " + Arrays.toString(seq));
				ProductSum ps = ProductSum.create(terms, seq, pool);
				list.add(ps);
				if (first) {
					break;
				}
			}
		} while (sequence.next(seq));

		return list;
	}

}
