package net.gcvs.matrix;

import java.util.function.Function;

public final class TermSet {
    static Function<TermSet, String> toString;

    public long set0;
    public long set1;

    public String label;

    public TermSet() {
    }

    public TermSet(final long set0, final long set1) {
        this.set0 = set0;
        this.set1 = set1;
    }

    public TermSet(final int... ts) {
        long set0 = 0;
        long set1 = 0;
        for (int tid : ts) {
            if (tid >= 64) {
                set1 |= 1L << (tid - 64);
            } else {
                set0 |= 1L << tid;
            }
        }
        this.set0 = set0;
        this.set1 = set1;
    }

    public int nextSetBit(final int fromIndex) {
        if (fromIndex < 0)
            throw new IndexOutOfBoundsException("fromIndex < 0: " + fromIndex);

        long word;
        if (fromIndex < 64) {
            word = set0 & (-1L << fromIndex);
            if (word != 0) {
                return Long.numberOfTrailingZeros(word);
            }
            word = set1;
        } else {
            word = set1 & (-1L << (fromIndex - 64));
        }
        if (word != 0) {
            return 64 + Long.numberOfTrailingZeros(word);
        }
        return -1;
    }

    public int cardinality() {
        return Long.bitCount(set0) + Long.bitCount(set1);
    }

    public void or(final TermSet set) {
        this.set0 |= set.set0;
        this.set1 |= set.set1;
    }

    public void xor(final TermSet set) {
        this.set0 ^= set.set0;
        this.set1 ^= set.set1;
    }

    public void and(final TermSet set) {
        this.set0 &= set.set0;
        this.set1 &= set.set1;
    }

    public void andNot(final TermSet set) {
        this.set0 &= ~set.set0;
        this.set1 &= ~set.set1;
    }

    public boolean isEmpty() {
        return set0 == 0L && set1 == 0L;
    }

    public TermSet clone() {
        return new TermSet(set0, set1);
    }

    public boolean get(final int term) {
        if (term < 0) {
            return false;
        }
        if (term < 64) {
            return (set0 & (1L << term)) != 0;
        }
        if (term < 128) {
            return (set1 & (1L << (term - 64))) != 0;
        }
        return false;
    }

    public int hashCode() {
        return Long.hashCode(set0 ^ set1);
    }

    public boolean equals(final Object o) {
        if (o instanceof TermSet ts) {
            return set0 == ts.set0 && set1 == ts.set1;
        }
        return false;
    }

    public String toString() {
        if (label != null) {
            return label;
        }
        if (toString != null) {
            return toString.apply(this);
        }
        String sep = "";
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 128; i++) {
            if (get(i)) {
                sb.append(sep);
                sb.append(i);
                sep = " ";
            }
        }
        return sb.toString();
    }
}