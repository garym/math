package net.gcvs.math;

/**
 * Deconstruction of a java float, an IEEE 754 binary32
 */
public class Flot {

    public static final int EXPONENT_00 = 0;
    public static final int EXPONENT_FF = 0xff;
    public static final int EXPONENT_BIAS = 0x7f;

    /**
     * sign bit, 0 or 1
     */
    int s;
    /**
     * 8 bit unsigned biased exponent: 2 ^ (e - 127)
     */
    int e;
    /**
     * 23 bit signed mantissa
     */
    int m;

    public Flot(float f) {
        int i = Float.floatToRawIntBits(f);
        s = i >>> 31;
        e = (i >>> 23) & 0xff;
        m = i & 0x7fffff;
    }

    public Flot(int s, int e, int m) {
        if ((s & 0xFFFFFFFE) != 0) {
            throw new IllegalArgumentException("s must be 0 or 1");
        }
        e += EXPONENT_BIAS;
        if ((e & 0xFFFFFF00) != 0) {
            throw new IllegalArgumentException("e must be between -127 and 128");
        }
        if ((m & 0xFF800000) != 0) {
            throw new IllegalArgumentException("m must be between 0 and 2 ^ 23 - 1");
        }
        this.s = s;
        this.e = e;
        this.m = m;
    }

    public float floatValue() {
        return Float.intBitsToFloat(toRawIntBits());
    }

    public int toRawIntBits() {
        return (s << 31) | (e << 23) | m;
    }

    public boolean isInfinity() {
        return e == EXPONENT_FF && m == 0;
    }

    public boolean isNaN() {
        return e == EXPONENT_FF && m != 0;
    }

    public boolean isZero() {
        return e == EXPONENT_00 && m == 0;
    }

    public boolean isSubNormal() {
        return e == EXPONENT_00 && m != 0;
    }

    public int getExponent() {
        return e - EXPONENT_BIAS;
    }

    @Override
    public String toString() {
        return "Flot{" +
                "s=" + s +
                ", e=" + e +
                ", m=" + m +
                '}';
    }
}
