package net.gcvs.matrix;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public final class Products implements Iterable<Product> {
	private static final BitSet EMPTY = new BitSet();

	public final Terms terms;
	private final List<Product> products;

	private final BitSet[] productsWithTerms;
	private BitSet[] productsWithCardinality;

	public Products(final Terms terms, final List<Product> products) {
		this.terms = terms;
//		Shuffle.shuffle(products);
		this.products = products;
		this.productsWithTerms = new BitSet[terms.length];
		Arrays.setAll(productsWithTerms, i -> new BitSet(products.size()));

		int maxCardinality = 0;
		for (int pidx = 0, plen = products.size(); pidx < plen; pidx++) {
			final Product p = products.get(pidx);
			for (int i = 0, len = p.length(); i < len; i++) {
				final int term = p.term(i);
				productsWithTerms[term].set(pidx);
			}
			maxCardinality = Math.max(maxCardinality, p.length());
		}
		productsWithCardinality = new BitSet[maxCardinality + 1];
		Arrays.setAll(productsWithCardinality, i -> new BitSet(products.size()));
		for (int i = 0, len = products.size(); i < len; i++) {
			final Product p = products.get(i);
			productsWithCardinality[p.length()].set(i);
		}
	}

	@Override
	public Iterator<Product> iterator() {
		return products.iterator();
	}

	public Product get(final int idx) {
		return products.get(idx);
	}

	public int size() {
		return products.size();
	}

	public BitSet withTerm(final int term) {
		return productsWithTerms[term];
	}

	public BitSet withCardinality(final int cardinality) {
		if (cardinality < 0) {
			return EMPTY;
		}
		if (cardinality >= productsWithCardinality.length) {
			return EMPTY;
		}
		final BitSet set = productsWithCardinality[cardinality];
		return set == null ? EMPTY : set;
	}

	public BitSet all() {
		final BitSet set = new BitSet(products.size());
		set.set(0, products.size(), true);
		return set;
	}

	public Stream<Product> stream() {
		return products.stream();
	}

	public Stream<Product> parallelStream() {
		return products.parallelStream();
	}
}
