package net.gcvs.math.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MultiplySeries<V> implements Series<V> {
    private final SeriesAlgebra<V> seriesAlgebra;
    private final Series<V> a;
    private final Series<V> b;


    public MultiplySeries(final SeriesAlgebra<V> seriesAlgebra, Series<V> a, Series<V> b) {
        this.seriesAlgebra = seriesAlgebra;
        this.a = a;
        this.b = b;
    }

    private static class Link<V> {
        Link<V> next;
        Term<V> term;

        private Link(Link<V> next, Term<V> term) {
            this.next = next;
            this.term = term;
        }
    }

    @Override
    public Iterator<Term<V>> iterator() {
        final Peekerator<Term<V>> ait = Peekerator.of(a.iterator());
        final Peekerator<Term<V>> bit = Peekerator.of(b.iterator());

        return new Iterator<>() {

            // a links decrease in value
            Link<V> ahead = new Link<>(null, ait.next());

            // b links increase in value
            final Link<V> bhead = new Link<>(null, bit.next());
            Link<V> bmax = bhead;
            long nextTerm;

            private Term<V> calculate(long term) {
                while (ait.hasNext() && ait.peek().degree() <= term) {
                    ahead = new Link<>(ahead, ait.next());
                }
                while (bit.hasNext() && bit.peek().degree() <= term) {
                    bmax.next = new Link<>(null, bit.next());
                    bmax = bmax.next;
                }

                Link<V> as = ahead;
                Link<V> bs = bhead;
                V value = null;
                while (as != null && bs != null) {
                    final long sumTerm = as.term.degree() + bs.term.degree();
                    if (sumTerm == term) {
                        V product = seriesAlgebra.getAlgebra().multiply(as.term.coefficient(), bs.term.coefficient());

                        if (value == null) {
                            value = product;
                        } else {
                            value = seriesAlgebra.getAlgebra().add(value, product);
                        }
                        as = as.next;
                        bs = bs.next;
                    } else if (sumTerm < term) {
                        // increase term
                        bs = bs.next;
                    } else {
                        // decrease term
                        as = as.next;
                    }
                }

                return new Term<>(term, value == null ? seriesAlgebra.getAlgebra().zero() : value);
            }


            @Override
            public boolean hasNext() {
                long topa = ait.hasNext() ? ait.peek().degree() : ahead.term.degree();
                long topb = bit.hasNext() ? bit.peek().degree() : bmax.term.degree();
                return topa + topb >= nextTerm;
            }

            @Override
            public Term<V> next() {
                if (!hasNext()) throw new NoSuchElementException();
                return calculate(nextTerm++);
            }
        };
    }
}
