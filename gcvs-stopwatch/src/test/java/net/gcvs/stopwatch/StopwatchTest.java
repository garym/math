package net.gcvs.stopwatch;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class StopwatchTest {

    void doSleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Watch
    void sleep(long millis) {
        Stopwatch.start("sleep");
        doSleep(millis);
        Stopwatch.stop();
    }

    @Watch
    void booya() {
        Stopwatch.start("booya");
        sleep(100);
        Stopwatch.mark("loop");
        for (int i = 0; i < 10; i++) {
            sleep(50);
        }
        Stopwatch.mark("yourgod");
        allah(3);
        Stopwatch.stop();
    }

    @Watch
    void allah(int calls) {
        Stopwatch.start("allah");
        sleep(50);
        for (int i = 0; i < calls; i++) {
            sleep(10);
        }
        Stopwatch.stop();
    }

    @Test
    void watch() {
        try (Stopwatch stopwatch = Stopwatch.start("test")) {
            booya();
            allah(6);
            sleep(200);

            final StopwatchReport report = stopwatch.report();

            String reportStr = report.generate(10, 3);

            assertTrue(reportStr.contains("sleep"));
            assertTrue(reportStr.contains("booya"));
            assertTrue(reportStr.contains("allah"));

//            System.out.println(reportStr);
        }
    }

    @Watch
    void mainwatch() {
        doSleep(10);
        Stopwatch.mark("mark-50");
        sleep(200);
        Stopwatch.mark("mark-200");
        doSleep(200);
    }

    @Test
    void mark() {
        try (Stopwatch stopwatch = Stopwatch.start("pre")) {
            sleep(100);
        }
        try (Stopwatch stopwatch = Stopwatch.start("mark")) {
            mainwatch();
//            final StopwatchReport report = stopwatch.report();
//            String reportStr = report.generate(10, 3);
//            System.out.println(reportStr);
            long now = System.currentTimeMillis();
            assertTrue(stopwatch.getSelfElapsed(now) > 200);
            assertTrue(stopwatch.getSelfElapsed(now) < 400);
        }
    }
}