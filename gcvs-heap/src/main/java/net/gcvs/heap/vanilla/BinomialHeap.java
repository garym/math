package net.gcvs.heap.vanilla;

import java.util.Comparator;
import java.util.Stack;

import net.gcvs.heap.Heap;

public class BinomialHeap<T> implements Heap<T> {

	private static class Node<T> {
		Node<T> leftChild;
		Node<T> rightSibling;

		T value;

		public int size() {
			return size(true);
		}

		public int size(final boolean includeRightSibling) {
			int size = 1;
			if (leftChild != null) {
				size += leftChild.size();
			}
			if (includeRightSibling && rightSibling != null) {
				size += rightSibling.size();
			}
			return size;
		}

		public Node(final T value) {
			this.value = value;
		}

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("[");
			String sep = "";
			for (Node<T> n = this; n != null; n = n.rightSibling) {
				sb.append(sep);
				sb.append(n.size(false));
				sep = ", ";
			}
			sb.append("]");
			return sb.toString();
		}
	}

	private final Comparator<T> order;
	private Node<T> root;
	private int size;
	private Node<T> min;

	public BinomialHeap(final Comparator<T> order) {
		this.order = order;
	}

	private Stack<Node<T>> nodes = new Stack<>();

	private final Node<T> wrap(final T t) {
		if (nodes.size() == 0) {
			return new Node<>(t);
		}
		final Node<T> node = nodes.pop();
		node.leftChild = null;
		node.rightSibling = null;
		node.value = t;
		return node;
	}

	@Override
	public void insert(final T t) {
		final Node<T> node = wrap(t);
		if (size == 0) {
			root = node;
			min = node;
			size = 1;
		} else {
			root = meld(root, size, node, 1);
			size++;
		}

		// invariant
		// assert root.size() == size;
	}

	@Override
	public T findMin() {
		if (size <= 0) {
			return null;
		}
		T value = root.value;
		for (Node<T> ref = root.rightSibling; ref != null; ref = ref.rightSibling) {
			if (order.compare(value, ref.value) > 0) {
				value = ref.value;
			}
		}
		return value;
	}

	@Override
	public T extractMin() {
		Node<T> min = root;
		if (min == null) {
			return null;
		}
		Node<T> minRef = null;
		int minTreeSize = size & (-size);
		int treeBits = size - minTreeSize;
		for (Node<T> nextRef = min; nextRef.rightSibling != null; nextRef = nextRef.rightSibling) {
			final int treeSize = treeBits & (-treeBits);
			if (order.compare(min.value, nextRef.rightSibling.value) > 0) {
				min = nextRef.rightSibling;
				minRef = nextRef;
				minTreeSize = treeSize;
			}
			treeBits -= treeSize;
		}
		final T value = min.value;
		final Node<T> head;
		if (minRef == null) {
			head = min.rightSibling;
		} else {
			head = root;
			// remove binomial tree root
			minRef.rightSibling = min.rightSibling;
		}
		root = meld(reverse(min.leftChild), minTreeSize - 1, head, size - minTreeSize);

		min.leftChild = null;
		min.rightSibling = null;
		size--;
		// recycle min
		nodes.push(min);

		// invariant
		// assert root == null && size == 0 || root.size() == size;

		return value;
	}

	@Override
	public boolean isEmpty() {
		return size <= 0;
	}

	long reverseMillis;

	Node<T> reverse(final Node<T> node) {
		reverseMillis -= System.currentTimeMillis();
		Node<T> tree = node;
		Node<T> heap = null;
		while (tree != null) {
			final Node<T> nextTree = tree.rightSibling;
			tree.rightSibling = heap;
			heap = tree;
			tree = nextTree;
		}
		reverseMillis += System.currentTimeMillis();
		return heap;
	}

	public void meld(final Heap<T> h2) {
		throw new UnsupportedOperationException();
	}

	long meldMillis;

	/**
	 * a and b are root lists of binomial heaps of given sizes
	 */
	Node<T> meld(final Node<T> a, final int sizeA, final Node<T> b, final int sizeB) {
		if (sizeA <= 0) {
			return b;
		}
		if (sizeB <= 0) {
			return a;
		}
		// assert sizeA == a.size();
		// assert sizeB == b.size();

		meldMillis -= System.currentTimeMillis();

		Node<T> carry = null;
		Node<T> da = a;
		Node<T> db = b;
		Node<T> head = null;
		Node<T> tail = null;

		for (long bit = 1; bit <= sizeA || bit <= sizeB; bit <<= 1) {
			final boolean aset = (bit & sizeA) != 0;
			final boolean bset = (bit & sizeB) != 0;
			Node<T> p = null;
			if (aset) {
				if (bset) {
					final Node<T> k1 = da;
					final Node<T> k2 = db;
					da = da.rightSibling;
					db = db.rightSibling;
					k1.rightSibling = null;
					k2.rightSibling = null;

					p = carry;
					carry = meld(k1, k2);

				} else {
					p = da;
					da = da.rightSibling;
					p.rightSibling = null;

					if (carry != null) {
						carry = meld(carry, p);
						p = null;
					}
				}
			} else if (bset) {
				p = db;
				db = db.rightSibling;
				p.rightSibling = null;

				if (carry != null) {
					carry = meld(carry, p);
					p = null;
				}
			} else {
				p = carry;
				carry = null;
			}
			if (p != null) {
				if (head == null) {
					head = p;
				} else {
					tail.rightSibling = p;
				}
				tail = p;
			}
		}
		if (carry != null) {
			if (head == null) {
				head = carry;
			} else {
				tail.rightSibling = carry;
			}
		}

		meldMillis += System.currentTimeMillis();

		return head;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		String sep = "";
		for (Node<T> n = this.root; n != null; n = n.rightSibling) {
			sb.append(sep);
			sb.append(n.size(false));
			sep = ", ";
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	Node<T> meld(final Node<T> a, final Node<T> b) {
		// assert: a and b are binomial trees of the same height
		// assert a.size() == b.size();

		if (order.compare(a.value, b.value) <= 0) {
			b.rightSibling = a.leftChild;
			a.leftChild = b;
			return a;
		}
		a.rightSibling = b.leftChild;
		b.leftChild = a;
		return b;
	}

	public void report() {
		System.out.println("   reverse: " + reverseMillis + " millis");
		System.out.println("      meld: " + meldMillis + " millis");
	}
}
