package net.gcvs.math.polynomial;

import net.gcvs.math.algebra.Algebra;
import net.gcvs.math.algebra.DivMod;

import java.util.Arrays;

public class PolyAlgebra<V> implements Algebra<Poly<V>> {
    private final Poly<V> zero;
    private final Poly<V> one;

    final Algebra<V> algebra;

    public PolyAlgebra(Algebra<V> algebra) {
        this.algebra = algebra;
        this.zero = constant(algebra.zero());
        this.one = constant(algebra.one());
    }

    private V[] array(final V value) {
        V[] array = algebra.newArray(1);
        array[0] = value;
        return array;
    }

    @Override
    public Poly<V> zero() {
        return zero;
    }

    @Override
    public Poly<V> one() {
        return one;
    }

    public Poly<V> constant(final long value) {
        return constant(algebra.constant(value));
    }

    public Poly<V> constant(final V value) {
        final V[] cs = algebra.newArray(1);
        cs[0] = value;
        return new Poly<>(algebra, cs);
    }

    @Override
    public Poly<V> add(Poly<V> a, Poly<V> b) {
        V[] cs = algebra.newArray(Math.max(a.length(), b.length()));
        Arrays.fill(cs, algebra.zero());
        for (int i = 0; i < a.length(); i++) {
            cs[i] = a.coefficient(i);
        }
        for (int i = 0; i < b.length(); i++) {
            cs[i] = algebra.add(cs[i], b.coefficient(i));
        }
        return new Poly<>(algebra, cs);
    }

    @Override
    public Poly<V> subtract(Poly<V> a, Poly<V> b) {
        V[] cs = algebra.newArray(Math.max(a.length(), b.length()));
        Arrays.fill(cs, algebra.zero());
        for (int i = 0; i < a.length(); i++) {
            cs[i] = a.coefficient(i);
        }
        for (int i = 0; i < b.length(); i++) {
            cs[i] = algebra.subtract(cs[i], b.coefficient(i));
        }
        return new Poly<>(algebra, cs);
    }

    @Override
    public Poly<V> multiply(Poly<V> a, Poly<V> b) {
        V[] cs = algebra.newArray(a.length() + b.length() - 1);
        for (int i = 0; i < cs.length; i++) {
            cs[i] = algebra.zero();
        }
        for (int i = 0; i < a.length(); i++) {
            for (int j = 0; j < b.length(); j++) {
                cs[i + j] = algebra.add(cs[i + j], algebra.multiply(a.coefficient(i), b.coefficient(j)));
            }
        }
        return new Poly<>(algebra, cs);
    }

    @Override
    public Poly<V> divide(Poly<V> a, Poly<V> b) {
        return divideAndRemainder(a, b).div();
    }

    @Override
    public DivMod<Poly<V>> divideAndRemainder(Poly<V> a, Poly<V> b) {
        if (b.degree() > a.degree()) {
            return new DivMod<>(zero, b);
        }

        V[] quot = algebra.newArray(a.length() - b.degree());
        V[] rem = algebra.newArray(a.length());
        Arrays.setAll(rem, a::get);
        final V d = b.get(b.degree());
        for (int i = rem.length - 1; i >= b.degree(); i--) {
            int shift = i - b.degree();
            V r = rem[i];

            V q = algebra.divide(r, d);
            quot[shift] = q;

            rem[i] = algebra.zero();
            for (int j = 0; j < b.degree(); j++) {
                rem[j + shift] = algebra.subtract(rem[j + shift], algebra.multiply(b.get(j), q));
            }
        }
        return new DivMod<>(new Poly<>(algebra, quot), new Poly<>(algebra, Arrays.copyOf(rem, b.degree())));
    }

    @Override
    public Poly<V>[] newArray(int length) {
        return new Poly[length];
    }

    @Override
    public String toString(Poly<V> a) {
        return a.toString();
    }

    public Poly<V> divide(final Poly<V> a, V b) {
        V[] cs = algebra.newArray(a.length());
        for (int i = 0; i < cs.length; i++) {
            cs[i] = algebra.divide(cs[i], b);
        }
        return new Poly<>(algebra, cs);
    }

    public Poly<V> poly(V... coefficients) {
        return new Poly<>(algebra, coefficients);
    }

    public final Poly<V> root(V root) {
        final V[] coeffs = algebra.newArray(2);
        coeffs[0] = algebra.subtract(algebra.zero(), root);
        coeffs[1] = algebra.one();
        return new Poly<>(algebra, coeffs);
    }

    public Poly<V> withRoots(V... roots) {
        Poly<V> poly = one();
        for (V root : roots) {
            Poly<V> linear = root(root);
            poly = multiply(poly, linear);
        }
        return poly;
    }

    public Poly<V> differentiate(final Poly<V> poly) {
        final int len = poly.length() + 1;
        V[] cs = algebra.newArray(len);
        for (int i = 0; i < len; i++) {
            cs[i] = algebra.multiply(poly.get(i + 1), algebra.constant(i + 1));
        }
        return new Poly<>(algebra, cs);
    }

    public Poly<V> integrate(final Poly<V> poly) {
        final int len = poly.length() + 1;
        V[] cs = algebra.newArray(len);
        for (int i = 1; i < len; i++) {
            cs[i] = algebra.divide(poly.get(i - 1), algebra.constant(i));
        }
        return new Poly<>(algebra, cs);
    }

    public Poly<V> of(final Series<V> series, final long maxDegree) {
        V[] cs = algebra.newArray(Math.toIntExact(maxDegree + 1));
        for (Term<V> term : series.truncate(maxDegree)) {
            cs[(int) term.degree()] = term.coefficient();
        }
        return new Poly<>(algebra, cs);
    }
}
