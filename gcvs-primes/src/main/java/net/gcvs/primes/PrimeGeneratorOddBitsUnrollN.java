package net.gcvs.primes;

import static net.gcvs.primes.PrimeSet.expandToArray;

/**
 * experimental
 */
public class PrimeGeneratorOddBitsUnrollN implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;
    private static final int BITS_PER_WORD = 1 << ADDRESS_BITS_PER_WORD;

    /* Used to shift left or right for a partial word mask */
    private static final long WORD_MASK = 0xffffffffffffffffL;

    /**
     * timing for various n: seems to hit sweet spot at n=31
     * N(1) = 13085
     * N(2) = 10399
     * N(3) = 8670
     * N(4) = 8465
     * N(5) = 7602
     * N(6) = 6053
     * N(7) = 6477
     * N(8) = 7270
     * N(9) = 7195
     * N(10) = 6956
     * N(11) = 6729
     * N(12) = 6590
     * N(13) = 6537
     * N(14) = 6267
     * N(15) = 6197
     * N(16) = 6127
     * N(17) = 6044
     * N(18) = 6009
     * N(19) = 5954
     * N(20) = 5925
     * N(21) = 5910
     * N(22) = 5900
     * N(23) = 5847
     * N(24) = 5921
     * N(25) = 5903
     * N(26) = 5940
     * N(27) = 5857
     * N(28) = 5896
     * N(29) = 5836
     * N(30) = 5862
     * N(31) = 5407
     * N(32) = 5973
     * N(33) = 6054
     * N(34) = 6045
     * N(35) = 6047
     * N(36) = 6053
     * N(37) = 6110
     * N(38) = 6190
     * N(39) = 6209
     */
    private int unroll;

    public PrimeGeneratorOddBitsUnrollN() {
        this(31);
    }

    public PrimeGeneratorOddBitsUnrollN(int unroll) {
        this.unroll = unroll;
    }

    private static int wordIndex(int bitIndex) {
        return bitIndex >> ADDRESS_BITS_PER_WORD;
    }

    private static int nextSetBit(long[] words, int fromIndex) {
        fromIndex >>= 1;
        final int wordsInUse = words.length;
        if (fromIndex < 0) return -1; // word overflow protection

        int u = wordIndex(fromIndex);
        if (u >= wordsInUse)
            return -1;

        long word = words[u] & (WORD_MASK << fromIndex);

        for (; ; ) {
            if (word != 0) return (((u * BITS_PER_WORD) + Long.numberOfTrailingZeros(word)) << 1) + 1;
            if (++u == wordsInUse) return -1;
            word = words[u];
        }
    }

    private static void clear(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] &= ~(1L << bitIndex);
    }

    private static void set(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] |= (1L << bitIndex);
    }

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        // primes = 0..n
        long[] primes = new long[((n - 1) >> 7) + 1];
        final int nextPrime = Stripes.sieveStripe2(primes);

        final int nmod = ((n - 1) >> 1) & 0x3f;
        if (nmod != 0x3f) {
            primes[primes.length - 1] &= (1L << (nmod + 1)) - 1;
        }
        int sqrt = (int) Math.ceil(Math.sqrt(n));
        int minsqrt = Math.min(63, sqrt);
        int p;
        for (p = nextSetBit(primes, nextPrime); p != -1 && p <= minsqrt; p = nextSetBit(primes, p + 2)) {
            final int step = p << 1;
            for (int m = p * p; m >= 0 && m <= n; m += step) {
                clear(primes, m);
            }
        }
        final int max = n >> 1;

        if (p != -1) {
            final int unroll = this.unroll;
            int[] ps = new int[unroll];
            int[] mps = new int[unroll];
            ps[0] = p;
            while (p != -1 && p <= sqrt) {
                int p2 = p * p; // prime sieved beyond p2 might not be prime
                int pc = 0;
                // pop mps[0]
                while (pc < ps.length) {
                    ps[pc] = p;
                    mps[pc] = (p * p) >> 1;
                    pc++;
                    p = nextSetBit(primes, p + 2);
                    if (p == -1 || p > p2 || p > sqrt) {
                        break;
                    }
                }
//                System.out.println(pc);

                for (int s = pc - 1; s >= 0; s--) {
                    while (mps[s] <= max) {
                        for (int i = 0; i <= s; i++) {
                            primes[mps[i] >> ADDRESS_BITS_PER_WORD] &= ~(1L << mps[i]);
                            mps[i] += ps[i];
                        }
                    }
                }
            }
        }

        return expandToArray(primes);
    }
}
