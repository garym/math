package net.gcvs.primes;

import net.gcvs.heap.vanilla.BinaryHeap;
import net.gcvs.util.LongArray;

import java.util.BitSet;

public class PrimeGeneratorOdd implements PrimeGenerator {

    static class P implements Comparable<P> {
        long next;
        final long pee;
        long p1;
        long p2;


        public P(final long p, final long next) {
            this.next = next;
            this.pee = p;
            this.p1 = p;
            this.p2 = p * p;
        }

        @Override
        public int compareTo(P o) {
            int cmp = Long.compare(this.next, o.next);
            if (cmp == 0) {
                return Long.compare(o.p1, p1);
            }
            return cmp;
        }

        public String toString() {
            return "P{" + next + ", " + p1 + "}";
        }

        public void updateNext() {
            if (next == p2) {
                p1 *= pee;
                p2 *= pee;
            }
            next += 2 * p1;
        }
    }

    @Override
    public LongArray longArray(long n) {
        final int sqrtn = (int) Math.ceil(Math.sqrt(n));
        LongArray array = new LongArray();

        BinaryHeap<P> queue = new BinaryHeap<>();
        queue.insert(new P(3, 3 * 3));
        array.add(2);
        array.add(3);
        int i = 5;
        while (i <= n) {
            P p = queue.findMin();
            if (i < p.next) {
                array.add(i);
                if (true || i <= sqrtn) {
                    queue.insert(new P(i, 3 * i));
                }
                i += 2;
                continue;
            } else {
                System.out.println("composite because " + p);
            }
            while (queue.findMin().next == i) {
                P q = queue.extractMin();
                q.updateNext();
                queue.insert(q);
            }
            i += 2;
        }
        return array;
    }

    @Override
    public BitSet primes(int n) {
        return null;
    }
}
