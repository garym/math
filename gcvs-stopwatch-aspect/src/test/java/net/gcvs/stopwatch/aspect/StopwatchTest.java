package net.gcvs.stopwatch.aspect;

import net.gcvs.stopwatch.Stopwatch;
import net.gcvs.stopwatch.StopwatchReport;
import net.gcvs.stopwatch.Watch;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Watch
class StopwatchTest {

    void doSleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Watch
    void sleep(long millis) {
        doSleep(millis);
    }

    @Watch
    void booya() {
        sleep(100);
        for (int i = 0; i < 10; i++) {
            sleep(50);
        }
        allah(3);
    }

    @Watch("god")
    void allah(int calls) {
        sleep(50);
        for (int i = 0; i < calls; i++) {
            sleep(10);
        }
    }

    @Test
    void watch() {
        try (Stopwatch stopwatch = Stopwatch.start("test")) {
            booya();
            allah(6);
            sleep(200);

            final StopwatchReport report = stopwatch.report();

            String reportStr = report.generate(10, 3);

            assertTrue(reportStr.contains("sleep"));
            assertTrue(reportStr.contains("booya"));
            assertFalse(reportStr.contains("allah"));
            assertTrue(reportStr.contains("god"));

            System.out.println(reportStr);
        }
    }
}