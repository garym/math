package net.gcvs.math.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BinaryOperator;

public class TermBinaryOpSeries<V> implements Series<V> {
    private final Series<V> a;
    private final Series<V> b;

    private final BinaryOperator<V> op;

    public TermBinaryOpSeries(final Series<V> a, final Series<V> b, final BinaryOperator<V> op) {
        this.a = a;
        this.b = b;
        this.op = op;
    }

    @Override
    public Iterator<Term<V>> iterator() {
        return new Iterator<>() {
            private final Peekerator<Term<V>> ait = Peekerator.of(a.iterator());
            private final Peekerator<Term<V>> bit = Peekerator.of(b.iterator());

            @Override
            public boolean hasNext() {
                return ait.hasNext() || bit.hasNext();
            }

            @Override
            public Term<V> next() {
                if (!hasNext()) throw new NoSuchElementException();

                if (!ait.hasNext()) {
                    return bit.next();
                }
                if (!bit.hasNext()) {
                    return ait.next();
                }

                Term<V> at = ait.peek();
                Term<V> bt = bit.peek();

                if (at.degree() == bt.degree()) {
                    ait.next();
                    bit.next();
                    final V coefficient = op.apply(at.coefficient(), bt.coefficient());
                    return new Term<>(at.degree(), coefficient);
                }

                if (at.degree() < bt.degree()) {
                    return ait.next();
                }
                return bit.next();
            }
        };
    }
}
