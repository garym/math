package net.gcvs.stopwatch.aspect;

import net.gcvs.stopwatch.Stopwatch;
import net.gcvs.stopwatch.Watch;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class StopwatchAspect {
    @Pointcut("execution(@net.gcvs.stopwatch.Watch * *.*(..))")
    public void watchMethod() {
    }

    @Pointcut("execution(* (@net.gcvs.stopwatch.Watch *).*(..))")
    public void methodOfWatchClass() {
    }

    @Around("watchMethod() && @annotation(methodLevelWatch)")
    public Object adviseWatchMethods(ProceedingJoinPoint pjp, Watch methodLevelWatch)
            throws Throwable {
        return aroundWatch(pjp, methodLevelWatch);
    }

    @Around("methodOfWatchClass() && !watchMethod() && @within(classLevelWatch)")
    public Object adviseMethodsOfWatchClass(ProceedingJoinPoint pjp, Watch classLevelWatch)
            throws Throwable {
        return aroundWatch(pjp, classLevelWatch);
    }

    public Object aroundWatch(ProceedingJoinPoint pjp, Watch watch)
            throws Throwable {
        final String name = watch.value().length() == 0 ? pjp.getSignature().getName() : watch.value();
        if (watch.before()) {
            System.out.format("stopwatch: before %s\n", name);
        }
        Stopwatch.start(name);
        try {
            Object result = pjp.proceed();
            if (watch.logResult()) {
                System.out.format("stopwatch: result = %s\n", result);
            }
            return result;
        } finally {
            long time = Stopwatch.stop();
            if (watch.log() || time > watch.slowLog()) {
                System.out.format("stopwatch: %s took %d millis\n", name, time);
            }
        }
    }
}
