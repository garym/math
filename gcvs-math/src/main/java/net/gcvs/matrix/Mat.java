package net.gcvs.matrix;

import java.util.Arrays;

public class Mat {
	static char nextVariable = 'A';

	public static char nextName() {
		return nextVariable++;
	}

	public String name;

	final String[][] ids;

	public Mat(final String[][] ids) {
		this.ids = ids;
	}

	public int w() {
		return ids[0].length;
	}

	public int rows() {
		return h();
	}

	public int columns() {
		return w();
	}

	public int h() {
		return ids.length;
	}

	public int wh() {
		return w() * h();
	}

	public String term(final int row, final int column) {
		return ids[row][column];
	}

	public String term(final int idx) {
		return ids[idx / h()][idx % h()];
	}

	public Terms terms() {
		String[] ms = new String[wh()];
		Arrays.setAll(ms, i -> term(i));
		return new Terms(ms);
	}

	public Mat(final int rows, final int columns) {
		this.ids = new String[rows][columns];
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				ids[row][column] = Character.toString(nextName());
			}
		}
	}

	public String toString() {
		final StringBuilder sb = new StringBuilder();
		String sep;
		for (String[] row : ids) {
			sep = "|";
			for (String value : row) {
				sb.append(sep);
				sb.append(value);
				sep = " ";
			}
			sb.append("|\n");
		}
		return sb.toString();
	}

	/**
	 * Mat(a, x) * Mat(x, b) = Mat(a, b);
	 */
	public Mat mul(final Mat n) {
		if (this.ids[0].length != n.ids.length) {
			throw new RuntimeException();
		}
		String[][] mul = new String[this.ids.length][n.ids[0].length];
		for (int i = 0; i < mul.length; i++) {
			String[] row = mul[i];
			for (int j = 0; j < row.length; j++) {
				final StringBuilder sb = new StringBuilder();
				String sep = "";
				for (int k = 0; k < n.ids.length; k++) {
					sb.append(sep);
					sep = " + ";
					sb.append(this.ids[i][k]);
					sb.append(n.ids[k][j]);
				}
				row[j] = sb.toString();
			}
		}
		return new Mat(mul);
	}

	public void setTerms(final String[] terms) {
		int idx = 0;
		for (int row = 0; row < rows(); row++) {
			for (int column = 0; column < columns(); column++) {
				ids[row][column] = terms[idx++];
			}
		}
	}
}