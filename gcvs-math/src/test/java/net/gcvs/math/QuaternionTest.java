package net.gcvs.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QuaternionTest {
    public static final double DELTA = 0.0000001;

    @Test
    void test() {

        Quaternion p = new Quaternion(1, -3, 5, -7);
        Quaternion q = new Quaternion(-2, 11, -13, 17);

        // (p ⊗ q)* = q* ⊗ p*
        assertEquals(p.multiply(q).conjugate(), q.conjugate().multiply(p.conjugate()));

        assertSimilar(Quaternion.ONE, p.multiply(p.inverse()));
        assertSimilar(Quaternion.ONE, q.multiply(q.inverse()));
    }

    /**
     * because, pesky double rounding errors
     */
    void assertSimilar(Quaternion expected, Quaternion actual) {
        assertEquals(expected.p0(), actual.p0(), DELTA);
        assertEquals(expected.p1(), actual.p1(), DELTA);
        assertEquals(expected.p2(), actual.p2(), DELTA);
        assertEquals(expected.p3(), actual.p3(), DELTA);
    }

}