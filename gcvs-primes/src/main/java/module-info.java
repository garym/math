module net.gcvs.primes {
    requires static lombok;

    requires net.gcvs.util;
    requires net.gcvs.heap;

    requires jdk.unsupported;

    exports net.gcvs.primes;
}