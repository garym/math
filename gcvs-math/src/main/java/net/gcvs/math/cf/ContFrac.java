package net.gcvs.math.cf;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ContFrac {


    /**
     * https://perl.plover.com/classes/cftalk/INFO/gosper.txt
     *
     * given CF x, returns CF for (ax + b) / (cx + d)
     */
    public static ContinuedFraction transform(Generator<BigInteger> x, M m) {

        BigInteger a = m.a();
        BigInteger b = m.b();
        BigInteger c = m.c();
        BigInteger d = m.d();


        List<BigInteger> answer = new ArrayList<>();
        for (BigInteger xi = x.next(); xi != null; xi = x.next()) {
            BigInteger a1 = a.multiply(xi).add(b);
            BigInteger c1 = c.multiply(xi).add(d);
            b = a;
            a = a1;
            d = c;
            c = c1;

            if (c.signum() != 0 && d.signum() != 0) {
                final BigInteger[] adivmodc = a.divideAndRemainder(c);
                final BigInteger[] bdivmodd = b.divideAndRemainder(d);

                if (adivmodc[0].compareTo(bdivmodd[0]) == 0) {
                    answer.add(adivmodc[0]);
                    System.out.println(adivmodc[0]);
                    BigInteger c2 = adivmodc[1];
                    BigInteger d2 = bdivmodd[1];

                    a = c;
                    c = c2;
                    b = d;
                    d = d2;
                }
            }

        }


        System.out.println(answer);
        return null;
    }

}
