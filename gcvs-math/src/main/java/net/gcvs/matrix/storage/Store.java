package net.gcvs.matrix.storage;

import net.gcvs.matrix.Expression;
import net.gcvs.matrix.Mat;
import net.gcvs.matrix.Terms;
import net.gcvs.util.Join;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/*
 * dummy class to replace missing net.gcvs.persist.DB class
 */
class DB {
    public static List<Matrix> readAll(Class<Matrix> matrixClass) {
        return new ArrayList<>();
    }

    public static void create(Matrix m, Matrix n) {
    }
}

public class Store {
    public Mat m;
    public Mat n;
    public Terms terms;
    public Expression[] targets;

    /**
     * examine matrix multiplication: of Mat(m, k) * Mat(k, n) = Mat(m, n)
     *
     * @param m
     * @param k
     * @param n
     */
    public Store(final int m, final int k, final int n) {
        this.m = new Mat(m, k);
        this.m.name = "M";
        this.n = new Mat(k, n);
        this.n.name = "N";
        init();
    }

    public void init() {
        List<Matrix> ms = DB.readAll(Matrix.class);
        if (ms.size() == 0) {
            Matrix m = new Matrix();
            m.name = this.m.name;
            m.rows = this.m.rows();
            m.columns = this.m.columns();
            m.terms = Join.join(" ", this.m.terms().export());
            Matrix n = new Matrix();
            n.name = this.n.name;
            n.rows = this.n.rows();
            n.columns = this.n.columns();
            n.terms = Join.join(" ", this.n.terms().export());
            DB.create(m, n);
        } else if (ms.size() != 2) {
            throw new RuntimeException("wrong number of matrices");
        }
        Matrix m1 = ms.get(0);
        Matrix n1 = ms.get(1);
        if (m1.name.equals(n.name)) {
            Matrix t = m1;
            m1 = n1;
            n1 = t;
            if (!m1.name.equals(m.name)) {
                throw new RuntimeException();
            }
        } else {
            if (!m1.name.equals(m.name)) {
                throw new RuntimeException();
            }
            if (!n1.name.equals(n.name)) {
                throw new RuntimeException();
            }
        }
        if (n.rows() != n1.rows || n.columns() != n1.columns || m.rows() != m1.rows || m.columns() != m1.columns) {
            throw new RuntimeException();
        }
        n.setTerms(n1.terms.split(" "));
        m.setTerms(m1.terms.split(" "));

        this.terms = Terms.create(m.terms().copyNames(), n.terms().copyNames());

        this.targets = genTargets(m, n);
    }

    public Expression[] genTargets(final Mat a, final Mat b) {
        Mat m = a.mul(b);
        Terms list = m.terms();
        Expression[] expressions = new Expression[list.length];
        for (int i = 0; i < list.length; i++) {
            final int[] vector = this.terms.newInstance();
            String[] tokens = list.name(i).split(" \\+ ");
            for (String term : tokens) {
                int t = this.terms.findTerm(term);
                vector[t]++;
            }
            expressions[i] = terms.expression(vector);
        }
        return expressions;
    }

    public static int choose(final int n, final int r) {
        BigInteger product = BigInteger.ONE;
        for (int i = 1; i <= r; i++) {
            product = product.multiply(new BigInteger("" + (n - (i - 1))));
        }
        for (int i = 1; i <= r; i++) {
            product = product.divide(new BigInteger("" + i));
        }
        return product.intValue();
    }

    public static void main(String[] args) {
        Store store = new Store(3, 3, 3);
        System.out.println(store.m.mul(store.n));

        int size = 0;
        for (int m = 1; m <= 9; m++) {
            int mc = choose(9, m);
            for (int n = 1; n <= 9; n++) {
                int nc = choose(9, n);
                int count = mc * nc;
                System.out.println(m + "x" + n + " = " + count);
                size += count;
            }
        }
        System.out.println("total: " + size);
    }
}
