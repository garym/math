package net.gcvs.heap.vanilla;

import java.util.Comparator;

import net.gcvs.heap.Heap;

public class PairingHeap<T> implements Heap<T> {

	static class PairingTree<T> {
		// TODO back is apparently required for decreaseKey
		// PairingTree<T> back;
		PairingTree<T> next; // from wiki: possibly eliminate back by setting last child.next to parent and boolean "end of list" flag
		PairingTree<T> child;

		T elem;

		public PairingTree(final T elem) {
			this.elem = elem;
		}
	}

	final Comparator<T> order;

	PairingTree<T> root;

	public PairingHeap(final Comparator<T> order) {
		this.order = order;
	}

	@Override
	public void insert(final T t) {
		root = merge(new PairingTree<T>(t), root);
	}

	@Override
	public boolean isEmpty() {
		return root == null;
	}

	@Override
	public T findMin() {
		return root == null ? null : root.elem;
	}

	@Override
	public T extractMin() {
		if (isEmpty()) {
			return null;
		}
		T min = root.elem;
		root = mergePairs(root.child);
		return min;
	}

	PairingTree<T> mergePairs(final PairingTree<T> list) {
		if (list == null) {
			return null;
		}
		if (list.next == null) {
			return list;
		}
		PairingTree<T> list0 = list;
		PairingTree<T> list1 = list.next;
		PairingTree<T> tail = list1.next;
		list0.next = null; // detach from the list
		list1.next = null; // detach from the list
		return merge(merge(list0, list1), mergePairs(tail));
	}

	PairingTree<T> merge(final PairingTree<T> a, final PairingTree<T> b) {
		if (a == null) {
			return b;
		}
		if (b == null) {
			return a;
		}
		if (order.compare(a.elem, b.elem) <= 0) {
			b.next = a.child;
			a.child = b;
			return a;
		}
		a.next = b.child;
		b.child = a;
		return b;
	}
}
