package net.gcvs.math;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static net.gcvs.math.PartitionGeneratorTest.verifyRangedPartition;

public class PartitionGeneratorTestPerformance {
    @Test
    @Tag("PerformanceTest")
    public void test_p_110_2_110_1_110() {
        final int count = verifyRangedPartition(110, 1, 110, 1, 110);
        System.out.println("generated " + count + " partitions");
    }

    /**
     * This method assumes the validity of p(n), and uses it to validate p(n, a, b, c, d) for all a, b, c, d.
     * <p>
     * Verifies every permutation in p(n) that satisfies the a, b, c, d constraint is present in p(n, a, b, c, d)
     */
    @Test
    @Tag("ExhaustiveTest")
    public void test_parts_compare() {
        long partitions = 0;
        final int n = 31;
        for (int minPartSize = 1; minPartSize <= n; minPartSize++) {
            for (int maxPartSize = minPartSize; maxPartSize <= n; maxPartSize++) {
                for (int minPartCount = (n + maxPartSize - 1) / maxPartSize; minPartCount <= n / minPartSize; minPartCount++) {
                    for (int maxPartCount = minPartCount; maxPartCount <= n / minPartSize; maxPartCount++) {
                        partitions += verifyRangedPartition(n, minPartSize, maxPartSize, minPartCount, maxPartCount);
                    }
                }
            }
        }
        System.out.println("processed " + partitions + " partitions");
    }

    @Test
    @Tag("AdhocTest")
    public void test_performance() {
        PartitionGenerator gen = new PartitionGenerator(110);
        int count = 0;
        while (gen.next()) {
            count++;
        }
        System.out.println(count);
    }

}
