package net.gcvs.math.polynomial;

import net.gcvs.math.algebra.DivMod;
import net.gcvs.math.algebra.DoubleAlgebra;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PolyTest {

    PolyAlgebra<Double> algebra = new PolyAlgebra<>(new DoubleAlgebra());

    @Test
    public void zero() {
        final Poly<Double> poly = algebra.poly();

        assertEquals(0, poly.eval(0.0));
        assertEquals(0, poly.eval(1.0));
        assertEquals(0, poly.eval(2.0));
        assertEquals(0, poly.eval(10.0));
        assertEquals(0, poly.eval(-10.0));
    }

    @Test
    public void constant() {
        final Poly<Double> poly = algebra.poly(123.0);

        assertEquals(123.0, poly.eval(0.0));
        assertEquals(123.0, poly.eval(1.0));
        assertEquals(123.0, poly.eval(2.0));
        assertEquals(123.0, poly.eval(10.0));
        assertEquals(123.0, poly.eval(-10.0));
    }

    @Test
    public void linear() {
        final Poly<Double> poly = algebra.poly(0.0, 1.0);

        assertEquals(0.0, poly.eval(0.0));
        assertEquals(1.0, poly.eval(1.0));
        assertEquals(2.0, poly.eval(2.0));
        assertEquals(10.0, poly.eval(10.0));
        assertEquals(-10.0, poly.eval(-10.0));
    }

    @Test
    public void quadratic() {
        final Poly<Double> poly = algebra.poly(0.0, 0.0, 1.0);

        assertEquals(0.0, poly.eval(0.0));
        assertEquals(1.0, poly.eval(1.0));
        assertEquals(4.0, poly.eval(2.0));
        assertEquals(100.0, poly.eval(10.0));
        assertEquals(100.0, poly.eval(-10.0));
    }

    @Test
    public void root() {
        final Poly<Double> poly = algebra.root(321.98);
        assertEquals(0.0, poly.eval(321.98));
    }

    @Test
    public void test() {
        final Poly<Double> poly = algebra.withRoots(1.0, 2.0, 3.0);
        assertEquals(0.0, poly.eval(1.0));
        assertEquals(0.0, poly.eval(2.0));
        assertEquals(0.0, poly.eval(3.0));
    }

    @Test
    public void test_divide() {
        Poly<Double> num = algebra.poly(1.0, -2.0, 1.0, 1.0);

        final Poly<Double> den = algebra.poly(-1.0, 1.0, 2.0);

//        final DivMod<Poly<Double>> q = num.divide(den);


    }

}
