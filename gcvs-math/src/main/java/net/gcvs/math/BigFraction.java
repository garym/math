package net.gcvs.math;

/**
 * The class you're looking for is {@link BigRational}
 * <p>
 * I keep confusing the two class names, this is to remind me I decided on {@link BigRational} not {@link BigFraction}
 *
 * @author gary
 */
@Deprecated
final class BigFraction extends BigRational {
    private BigFraction() {
        super(0, 0);
        throw new RuntimeException();
    }
}
