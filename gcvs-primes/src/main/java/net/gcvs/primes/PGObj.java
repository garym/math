package net.gcvs.primes;

import java.util.function.IntSupplier;

/**
 * limited by stack size, in my JVM, limit was 468187. Only 267637 when this.next assigned a lambda.
 */
public class PGObj implements IntSupplier {
    private int nextValue = 2;
    private IntSupplier next = () -> nextValue++;

    @Override
    public int getAsInt() {
        final IntSupplier tail = next;
        final int prime = tail.getAsInt();
        // a lambda here uses twice as much stack
        this.next = new IntSupplier() {
            @Override
            public int getAsInt() {
                int p;
                do p = tail.getAsInt(); while (p % prime == 0);
                return p;
            }
        };
        return prime;
    }
}
