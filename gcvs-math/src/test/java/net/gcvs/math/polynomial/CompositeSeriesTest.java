package net.gcvs.math.polynomial;

import net.gcvs.math.BigRational;
import net.gcvs.math.algebra.BigRationalAlgebra;
import org.junit.jupiter.api.Test;

class CompositeSeriesTest {

    BigRationalAlgebra algebra = new BigRationalAlgebra();
    SeriesAlgebra<BigRational> seriesAlgebra = new SeriesAlgebra<>(algebra);
    PolyAlgebra<BigRational> polyAlgebra = new PolyAlgebra<>(algebra);
    StandardSeries<BigRational> standardSeries = new StandardSeries<>(algebra);

    @Test
    void test() {

        Series<BigRational> f = Series.<BigRational>builder().algebra(algebra)
                .constant(1, 2)
                .constant(0, 3)
                .build();
        Series<BigRational> g = Series.<BigRational>builder().algebra(algebra)
                .constant(2, 1)
                .build();

        Series<BigRational> gf = seriesAlgebra.composite(g, f);
        Series<BigRational> fg = seriesAlgebra.composite(f, g);

        System.out.println(seriesAlgebra.toString(f, 10));
        System.out.println(seriesAlgebra.toString(g, 10));

        System.out.println(seriesAlgebra.toString(gf, 10));
        System.out.println(seriesAlgebra.toString(fg, 10));


    }

    @Test
    void e_reciprocal_reciprocal_equals_e() {

        final Series<BigRational> reciprocal = standardSeries.reciprocal();

        Series<BigRational> a = standardSeries.e();

        Series<BigRational> b = seriesAlgebra.composite(reciprocal, a);
        Series<BigRational> c = seriesAlgebra.composite(b, reciprocal);

        System.out.println(seriesAlgebra.toString(a, 10));
        System.out.println(seriesAlgebra.toString(b, 10));
        System.out.println(seriesAlgebra.toString(c, 10));
    }
}