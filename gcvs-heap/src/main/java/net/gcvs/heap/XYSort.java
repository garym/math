package net.gcvs.heap;

import net.gcvs.heap.vanilla.BinaryHeap;
import net.gcvs.heap.vanilla.PairingHeap;

import java.util.*;
import java.util.function.Function;

public class XYSort {
	static abstract class Spot {
		public final int cx;
		public final int cy;

		public Spot(final int cx, final int cy) {
			this.cx = cx;
			this.cy = cy;
		}

		public abstract int sum();
	}

	static class Sum extends Spot {
		public final int x, y;
		public final int sum;

		public Sum(final int cx, final int cy, final int x, final int y) {
			super(cx, cy);
			this.x = x;
			this.y = y;
			this.sum = x + y;
		}

		public int sum() {
			return sum;
		}

		public String toString() {
			return "(" + cx + ", " + cy + "): " + x + " + " + y + " = " + sum;
		}
	}

	static enum Dir {
		X, Y;
	}

	static class Dot extends Spot {
		final Dot bx, by;
		Spot x;
		Spot y;
		private int xsum, ysum;

		public Dot(final int cx, final int cy, final Dot bx, final Dot by, final Sum x, final Sum y) {
			super(cx, cy);
			this.bx = bx;
			this.by = by;
			this.x = x;
			this.y = y;
			xsum = x.sum();
			ysum = y.sum();
		}

		public int sum() {
			final int minsum = xsum <= ysum ? xsum : ysum;
			// TODO maybe cache minsum
			return minsum;
		}

		public String toString() {
			return "(" + cx + ", " + cy + ") -> "
					+ (xsum <= ysum ? "X" : "Y")
					+ " to " + (xsum <= ysum ? x.toString() : y.toString());
		}

		public void raiseX(final Dot dot) {
			if (x != dot && x instanceof Dot) {
				throw new RuntimeException("cannot replace dot");
			}
			this.x = dot;
			final int newSum = dot.sum();
			if (newSum < xsum) {
				throw new RuntimeException("cannot lower sum");
			}
			if (xsum < newSum) {
				this.xsum = newSum;
				bx.raiseX(this);
				by.raiseY(this);
			}
		}

		public void raiseY(final Dot dot) {
			if (y != dot && y instanceof Dot) {
				throw new RuntimeException("cannot replace dot");
			}
			this.y = dot;
			final int newSum = dot.sum();
			if (newSum < ysum) {
				throw new RuntimeException("cannot lower sum");
			}
			if (ysum < newSum) {
				this.ysum = newSum;
				bx.raiseX(this);
				by.raiseY(this);
			}
		}
	}

	public static void pair(final int[] x, final int[] y) {
		Spot[][] sum = new Spot[y.length][x.length];
		for (int xi = 0; xi < x.length; xi++) {
			for (int yi = 0; yi < y.length; yi++) {
				sum[yi][xi] = new Sum(xi, yi, x[xi], y[yi]);
			}
		}
		int minx = 0;
		int miny = 0;

		int cx = 0, cy = 0;
		int it = 0, count = x.length * y.length;
		while (minx < x.length && miny < y.length && it++ < count) {
			Spot s = sum[cy][cx];
			System.out.println("(" + cx + ", " + cy + "): " + s);
			if (s instanceof Dot) {
				throw new RuntimeException("visited twice");
			}
			if (cx + 1 == x.length) {
				miny++;
				// only one next move
				Spot sy = sum[cy + 1][minx];
				cx = sy.cx;
				cy = sy.cy;
				continue;
			}
			if (cy + 1 == y.length) {
				minx++;
				// only one next move
				Spot sy = sum[miny][cx + 1];
				cx = sy.cx;
				cy = sy.cy;
				continue;
			}

			final Dot bx = cx > 0 ? (Dot) sum[cy][cx - 1] : null;
			final Dot by = cy > 0 ? (Dot) sum[cy - 1][cx] : null;

			final Sum sx = (Sum) sum[cy][cx + 1];
			final Sum sy = (Sum) sum[cy + 1][cx];

			final Dot d = new Dot(cx, cy, bx, by, sx, sy);
			sum[cy][cx] = d;
			Dot mod = d;
			while (mod.cy > miny) {
				Dot uy = (Dot) sum[mod.cy - 1][cx];
				uy.y = mod;
				mod = uy;

			}

			// reassess dots above and to left

			Spot tx = sum[miny][cx + 1];
			while (tx instanceof Dot) {
				// tx = ((Dot) tx).direction == Dir.X ? sum[tx.cy][tx.cx + 1] : sum[tx.cy + 1][tx.cx];
			}
			Spot ty = sum[cy + 1][minx];
			while (ty instanceof Dot) {
				// ty = ((Dot) ty).direction == Dir.X ? sum[ty.cy][ty.cx + 1] : sum[ty.cy + 1][ty.cx];
			}
			if (tx.sum() <= ty.sum()) {
				cx = tx.cx;
				cy = tx.cy;
			} else {
				cx = ty.cx;
				cy = ty.cy;
			}
		}
	}

	public static void pair2(final int[] x, final int[] y) {
		int[] xval = new int[x.length];
		int[] xofs = new int[x.length];
		int[] yval = new int[y.length];
		int[] yofs = new int[y.length];

		int[][] sum = new int[y.length][x.length];
		for (int xi = 0; xi < x.length; xi++) {
			for (int yi = 0; yi < y.length; yi++) {
				sum[yi][xi] = x[xi] + y[yi];
			}
		}

		for (int i = 0; i < x.length; i++) {
			xval[i] = sum[0][i];
		}
		for (int i = 0; i < y.length; i++) {
			yval[i] = sum[i][0];
		}

		int minx = 0;
		int miny = 0;
		int cx = 0;
		int cy = 0;
		while (cx < x.length - 1 && cy < y.length - 1) {
			print(xval, xofs, yval, yofs);

			int conflicts = 0;
			System.out.println(x[cx] + " + " + y[cy] + " = " + sum[cx][cy]);
			xofs[cx]++;
			xval[cx] = sum[xofs[cx]][cx];
			// xval[cx] has increased. xval[cx + 1] might be smaller
			if (cx + 1 < x.length) {
				if (xval[cx + 1] < xval[cx]) {
					System.out.println("conflict x");
					conflicts++;
					cx = cx + 1;
				}
			}

			yofs[cy]++;
			yval[cy] = sum[cy][yofs[cy]];
			// yval[cy] has increased. yval[cy + 1] might be smaller
			if (cy + 1 < y.length) {
				if (yval[cy + 1] < yval[cy]) {
					System.out.println("conflict y");
					conflicts++;
					cy = cy + 1;
				}
			}
			print(xval, xofs, yval, yofs);

			if (conflicts != 1) {
				throw new RuntimeException("invalid number of conflicts: " + conflicts);
			}

		}
	}

	static long s_cmp = 0;
	static long s_equals = 0;

	// static long s_addCalls = 0;
	static long s_siftHeadCalls = 0;

	static long s_doubleAdd;
	static long s_xSift;
	static long s_ySift;
	static long s_pop;

	static class S {
		int cx, cy; // final if not for object reuse
		int x, y; // final
		int sum; // final

		S() {
			cx = cy = x = y = sum = 0;
		}

		public S(final int cx, final int cy, final int x, final int y) {
			init(cx, cy, x, y);
		}

		public void init(final int cx, final int cy, final int x, final int y) {
			this.cx = cx;
			this.cy = cy;
			this.x = x;
			this.y = y;
			this.sum = x + y;
		}

		public String toString() {
			return x + " + " + y + " = " + sum;
		}
	}

	static class LinkedS extends S {
		LinkedS next;

		LinkedS() {
		}

		public LinkedS(final int cx, final int cy, final int x, final int y) {
			super(cx, cy, x, y);
		}

		public void init(final int cx, final int cy, final int x, final int y) {
			super.init(cx, cy, x, y);
			this.next = null;
		}

		public final void add(final LinkedS s) {
			_add(s);
		}

		protected void _add(final LinkedS s) {
			if (next == null) {
				next = s;

			} else {
				final int cmp = order.compare(s, next);
				if (cmp == 0) {
					s_equals++;
				}
				if (cmp <= 0) {
					s.next = next;
					next = s;
				} else {
					next._add(s);
				}
			}
		}

		public LinkedS peek() {
			return next;
		}

		public LinkedS pop() {
			if (next == null) {
				return null;
			}
			final LinkedS value = next;
			next = value.next;
			return value;
		}

		public LinkedS create(final int cx, final int cy, final int x, final int y) {
			return new LinkedS(cx, cy, x, y);
		}
	}

	public static Comparator<S> order = (a, b) -> {
		s_cmp++;
		return Integer.compare(a.sum, b.sum);
	};

	public static void pair3(final int[] x, final int[] y) {
		final long start = System.currentTimeMillis();

		// reset comparison counter
		s_cmp = 0;

		Function<Comparator<S>, Heap<S>> supplier;
		supplier = BinaryHeap::new;
		supplier = net.gcvs.heap.BinaryHeap::new;
		// supplier = heap.vanilla.BinomialHeap::new;
		supplier = PairingHeap::new;

		// dummy container for linked list
		final int maxHeapSize = Math.max(x.length, y.length);
		final LoggedHeap<S> root = new LoggedHeap<>(supplier.apply(order));

		// a = b = mix(1, 10000, 3000);
		// 140829209: vanilla BinaryHeap
		// 140076612: BinaryHeap (with remember child order)
		// 120314855: BinaryHeap (with delayed extract)
		// 120136696: BinaryHeap (with both the above)
		// 108096429: BinomialHeap
		// 103625160: MergeSort
		// 77639452: PairingHeap
		// 72875811: Tim Sort: Arrays.sort()
		// 9000000: n^2
		// root.delayExtract = true;

		int length = 0;

		// add the smallest pair
		root.insert(new S(0, 0, x[0], y[0]));
		length++;

		int lastsum = root.findMin().sum;

		int[] xtop = new int[x.length];
		int[] ytop = new int[y.length];
		long lengthSum = 0;
		long maxLength = 0;
		int[] lengthCounts = new int[Math.max(x.length, y.length) + 1];
		while (root.findMin() != null) {

			lengthSum += length;
			maxLength = Math.max(maxLength, length);
			lengthCounts[length]++;

			// find min, dont extract it yet
			final S min = root.extractMin();
//			System.out.println(min.sum);
			length--;

			// assert: sequence is not decreasing
			if (min.sum < lastsum) {
				throw new RuntimeException("invalid order");
			}
			lastsum = min.sum;

			// System.out.println(min);

			xtop[min.cx]++;
			ytop[min.cy]++;

			final boolean addx = min.cy + 1 < y.length && ytop[xtop[min.cx]] == min.cx;
			final boolean addy = min.cx + 1 < x.length && xtop[ytop[min.cy]] == min.cy;

			// maybe add adjacent right and adjacent down
			if (addx) {
				if (addy) {

					int xsum = x[min.cx] + y[min.cy + 1];
					int ysum = x[min.cx + 1] + y[min.cy];
					// s_cmp++;
					if (xsum > ysum) {
						// add corner
						final S sx = new S(min.cx, min.cy + 1, x[min.cx], y[min.cy + 1]);

						// add corner
						min.init(min.cx + 1, min.cy, x[min.cx + 1], y[min.cy]);
						root.insert(min);
						length++;

						root.insert(sx);
						length++;

						// System.out.println("x>y");
					} else {
						// add corner
						final S sy = new S(min.cx + 1, min.cy, x[min.cx + 1], y[min.cy]);

						// add corner
						min.init(min.cx, min.cy + 1, x[min.cx], y[min.cy + 1]);
						root.insert(min);
						length++;

						root.insert(sy);
						length++;

						// System.out.println("x<=y");
					}

					s_doubleAdd++;

				} else {
					// add corner
					min.init(min.cx, min.cy + 1, x[min.cx], y[min.cy + 1]);
					root.insert(min);
					length++;

					s_xSift++;
				}
			} else if (addy) {
				// add corner
				min.init(min.cx + 1, min.cy, x[min.cx + 1], y[min.cy]);
				root.insert(min);
				length++;
				s_ySift++;
			} else {
				// root.extractMin();
				s_pop++;
			}
		}
		final long end = System.currentTimeMillis();

		System.out.println("took " + (end - start) + " millis");

		System.out.println(x.length + "x" + y.length + " (=" + (x.length * y.length) + ") completed with " + s_cmp + " comparisons, " + s_equals + " equal");
		final double log = Math.log(x.length);
		System.out.println("log(" + x.length + ") = " + log);
		System.out.println("n ^ 2 ratio: " + ((double) s_cmp) / (x.length * y.length)
				+ ", n ^ 2 log(n) ratio: " + ((double) s_cmp) / (x.length * y.length * log));

		System.out.println("average / max heap length: " + (lengthSum / (x.length * y.length)) + " / " + maxLength);
		System.out.println(s_siftHeadCalls + " siftHead calls");
		System.out.println("double adds: " + s_doubleAdd + ", x sifts: " + s_xSift + ", y sifts: " + s_ySift);
		for (int i = 0; i < lengthCounts.length; i++) {
			if (lengthCounts[i] > 0) {
//				 System.out.println("length: " + i + ", count: " + lengthCounts[i]);
			}
		}

		root.report();
	}

	public static void print(final int[] xval, final int[] xofs, final int[] yval, final int[] yofs) {
		if (true)
			return;
		System.out.println(Arrays.toString(xofs));
		System.out.println(Arrays.toString(xval));
		System.out.println(Arrays.toString(yofs));
		System.out.println(Arrays.toString(yval));
	}

	static Random r = new Random(1234);

	public static int[] mix(final int min, final int max, final int count) {
		int[] list = new int[count];
		final int range = max - min + 1;
		Arrays.setAll(list, i -> min + r.nextInt(range));
		Arrays.sort(list);
		return list;
	}

	public static int[] naturals(final int count) {
		return sequence(1, count, 1);
	}

	public static int[] sequence(final int start, final int count, final int step) {
		int[] naturals = new int[count];
		for (int i = 0; i < naturals.length; i++) {
			naturals[i] = start + i * step;
		}
		return naturals;
	}

	public static int[] merge(final int[] a, final int[] b, int[] c) {
		if (a.length == 0) {
			return b;
		}
		if (b.length == 0) {
			return a;
		}
		if (c == null || c.length != a.length + b.length) {
			c = new int[a.length + b.length];
		}
		int ai = 0, bi = 0, ci = 0;
		int av = a[0], bv = b[0];
		for (;;) {
			s_mergecmp++;
			if (av < bv) {
				c[ci++] = av;
				if (++ai < a.length) {
					av = a[ai];
					continue;
				}
				System.arraycopy(b, bi, c, ci, c.length - ci);
				return c;
			}
			c[ci++] = bv;
			if (++bi < b.length) {
				bv = b[bi];
				continue;
			}
			System.arraycopy(a, ai, c, ci, c.length - ci);
			return c;
		}
	}

	static long s_mergecmp;

	public static void pair4(final int[] a, final int[] b) {
		final long start = System.currentTimeMillis();
		s_mergecmp = 0;
		int[][] sums = new int[a.length][b.length];
		Integer[] integers = new Integer[a.length * b.length];
		int idx = 0;
		for (int x = 0; x < a.length; x++) {
			for (int y = 0; y < b.length; y++) {
				integers[idx++] = sums[x][y] = a[x] + b[y];
			}
		}
		List<int[]> merge = new ArrayList<>();
		for (int i = 0; i < sums.length; i++) {
			merge.add(sums[i]);
		}

		while (merge.size() > 1) {
			int[] x = merge.remove(0);
			int[] y = merge.remove(0);
			int[] z = merge(x, y, null);
			merge.add(z);
		}
		int[] c = merge.get(0);
		for (int i = 0; i < c.length; i++) {
			// System.out.println(c[i]);
		}
		final long end = System.currentTimeMillis();
		System.out.println("took " + (end - start) + " millis");
		System.out.println(s_mergecmp + " merge comparisons");

		System.out.println("cartesian product tim sort: Arrays.sort()");
		TotalOrderOracle<Integer> oracle = new TotalOrderOracle<>(Integer::compare);
		oracle.countOnly();
		Arrays.sort(integers, oracle);
		long end2 = System.currentTimeMillis();
		System.out.println("took " + (end2 - end) + " millis");
		System.out.println(oracle.getCompareCalls() + " compare calls");
	}

	public static void main(String[] args) {
		int[] a = { 1, 3, 7, 13 };
		int[] b = { 2, 5, 11, 17 };
//		pair3(a, b);
//		pair3(sequence, sequence);

		// naturals = mix(1, 10000, 2000);
		a = sequence(0, 20, 10);
		b = sequence(0, 20, 1);
		a = naturals(5000);

		a = mix(1, 10000, 3000);
		// a = mix(1, 10000, 3000);

		b = a;
		// a = sequence(1, 10, 1);
		// b = sequence(11, 10, 1);
		System.out.println("xysort");
		pair3(a, b);
		System.out.println("cartesian product mergesort");
		pair4(a, b);
		System.out.println("pair3 / pair4 = " + ((double) s_cmp / s_mergecmp));

	}
}
