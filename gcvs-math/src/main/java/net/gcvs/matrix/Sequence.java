package net.gcvs.matrix;

import java.util.Arrays;

import net.gcvs.math.GCD;

/**
 * extension of int[]
 * 
 * @author gary
 *
 */
public final class Sequence {
	public final int min;
	public final int max;
	public final int length;

	public Sequence(final int min, final int max, final int length) {
		this.min = min;
		this.max = max;
		this.length = length;
	}

	public int[] create() {
		final int[] self = new int[length];
		reset(self);
		return self;
	}

	public int[] createCombination() {
		if (max - min < length) {
			// must be able to choose length distinct values from min to max
			throw new RuntimeException();
		}
		final int[] self = new int[length];
		int value = min;
		for (int i = 0; i < self.length; i++) {
			self[i] = value++;
		}
		return self;
	}

	public void reset(final int[] self) {
		Arrays.fill(self, min);
	}

	public boolean next(final int[] self) {
		for (int i = 0, len = self.length; i < len; i++) {
			int v = ++self[i];

			if (v > max) {
				self[i] = v = min;
				continue;
			}
			return true;
		}
		return false;
	}

	public boolean nextCombination(final int[] self) {
		int vmax = max;
		int len = self.length;
		for (int i = len - 1; i >= 0; i--) {
			int v = ++self[i];

			if (v <= vmax--) {
				for (int j = i + 1; j < len; j++) {
					self[j] = ++v;
				}
				return v <= max;
			}
		}
		return false;
	}

	public static boolean isSmallestPositive(final int[] self) {
		for (final int factor : self) {
			if (factor > 0) {
				return true;
			}
			if (factor < 0) {
				return false;
			}
		}
		return false;
	}

	public static int cardinality(final int[] self) {
		int cardinality = 0;
		for (final int factor : self) {
			if (factor != 0) {
				cardinality++;
			}
		}
		return cardinality;
	}

	public static boolean isEmpty(final int[] self) {
		for (final int t : self) {
			if (t != 0) {
				return false;
			}
		}
		return true;
	}

	public static boolean gcdIsOne(final int[] self) {
		int gcd = 0;
		for (int i = 0; i < self.length; i++) {
			final int value = Math.abs(self[i]);
			if (value == 0) {
				continue;
			}
			if (gcd == 0) {
				gcd = value;
			} else {
				gcd = GCD.gcd(gcd, value);
			}
		}
		return gcd == 1;
	}

}
