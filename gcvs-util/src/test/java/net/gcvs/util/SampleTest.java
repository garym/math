package net.gcvs.util;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SampleTest {

    Random random = new Random() {
        public double nextDouble() {
            return 0.5;
        }
    };

    @Test
    void naturals() {
        assertArrayEquals(new int[]{}, Sample.naturals(0));
        assertArrayEquals(new int[]{1}, Sample.naturals(1));
        assertArrayEquals(new int[]{1, 2}, Sample.naturals(2));
        assertArrayEquals(new int[]{1, 2, 3, 4}, Sample.naturals(4));
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, Sample.naturals(10));
    }

    @Test
    void concat() {
        assertArrayEquals(new int[]{}, Sample.concat(new int[]{}, new int[]{}));
        assertArrayEquals(new int[]{1, 2, 3}, Sample.concat(new int[]{1, 2, 3}, new int[]{}));
        assertArrayEquals(new int[]{4, 5, 6}, Sample.concat(new int[]{}, new int[]{4, 5, 6}));
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, Sample.concat(new int[]{1, 2, 3}, new int[]{4, 5, 6}));
    }

    @Test
    void cross() {
        assertArrayEquals(new int[]{}, Sample.cross(new int[]{}, new int[]{}, (a, b) -> a * b));
        assertArrayEquals(new int[]{}, Sample.cross(new int[]{1, 2, 3}, new int[]{}, (a, b) -> a * b));
        assertArrayEquals(new int[]{}, Sample.cross(new int[]{}, new int[]{2, 4, 6}, (a, b) -> a * b));
        assertArrayEquals(new int[]{6}, Sample.cross(new int[]{2}, new int[]{3}, (a, b) -> a * b));
        assertArrayEquals(new int[]{10, 15}, Sample.cross(new int[]{2, 3}, new int[]{5}, (a, b) -> a * b));
        assertArrayEquals(new int[]{6, 10}, Sample.cross(new int[]{2}, new int[]{3, 5}, (a, b) -> a * b));
        assertArrayEquals(new int[]{10, 14, 15, 21}, Sample.cross(new int[]{2, 3}, new int[]{5, 7}, (a, b) -> a * b));
    }

    @Test
    void weightedChoice_whenCertainty_thenPredictable() {
        assertEquals(true, Sample.weightedChoice(random, 0, 1));
        assertEquals(false, Sample.weightedChoice(random, 1, 0));
    }

    @Test
    void weightedChoice_whenLow_thenFalse() {
        assertEquals(false, Sample.weightedChoice(random, 2, 1));
    }

    @Test
    void weightedChoice_whenHigh_thenTrue() {
        assertEquals(true, Sample.weightedChoice(random, 1, 2));
    }
}