module net.gcvs.math {
    requires static lombok;
    requires net.gcvs.util;

    exports net.gcvs.math;
    exports net.gcvs.math.polynomial;
    exports net.gcvs.math.algebra;
    exports net.gcvs.matrix;

    opens net.gcvs.math;
    exports net.gcvs.math.cf;
    opens net.gcvs.math.cf;
}