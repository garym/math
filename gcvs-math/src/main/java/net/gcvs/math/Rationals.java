package net.gcvs.math;

import java.math.BigInteger;

public class Rationals {
    /**
     * we need fraction in faro series to the left of this, with denominator <=
     * this.getDenominator().
     * <p>
     * then repeatedly take mediant between the two fractions while denominator <=
     * maxDenominator
     * <p>
     * mediant of a/b and c/d = (a + c) / (b + d)
     * <p>
     * a/b < c/d => ad < bc => bc - ad = 1 (iff a
     *
     * @param maxDenominator
     * @return
     */
    public static BigRational decrement(BigRational r, final BigInteger maxDenominator) {
        // this implementation is not right
        BigInteger[] divAndRem = maxDenominator.divideAndRemainder(r.getDenominator());
        BigInteger n = r.getNumerator().multiply(divAndRem[0]).subtract(BigInteger.ONE);
        BigInteger d = maxDenominator.subtract(divAndRem[1]);
        return BigRationals.reduce(n, d);
    }

    public static void eea(BigInteger a, BigInteger b) {
        if (a.compareTo(b) < 0) {
            eea(b, a);
            return;
        }
        BigInteger r0 = a;
        BigInteger r1 = b;

        BigInteger s0 = BigInteger.ONE;
        BigInteger s1 = BigInteger.ZERO;

        BigInteger t0 = BigInteger.ZERO;
        BigInteger t1 = BigInteger.ONE;

        while (r1.signum() > 0) {
            BigInteger[] divmod = r0.divideAndRemainder(r1);
            BigInteger q = divmod[0];
            BigInteger r2 = divmod[1];

            BigInteger s2 = s0.subtract(q.multiply(s1));
            BigInteger t2 = t0.subtract(q.multiply(t1));

            r0 = r1;
            r1 = r2;
            s0 = s1;
            s1 = s2;
            t0 = t1;
            t1 = t2;

        }

        System.out.println("gcd = " + r0);
        System.out.println(a + " * " + s0 + " + " + b + " * " + t0 + " = " + a.multiply(s0).add(b.multiply(t0)));

        // 7 * 2 - 3 * 5 = -1

    }

    public static void main(String[] args) {
        eea(BigInteger.valueOf(7), BigInteger.valueOf(3));
        eea(BigInteger.valueOf(5), BigInteger.valueOf(2));
    }
}
