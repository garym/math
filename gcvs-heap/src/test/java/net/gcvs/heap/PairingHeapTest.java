package net.gcvs.heap;

import net.gcvs.heap.vanilla.PairingHeap;
import org.junit.jupiter.api.Test;

public class PairingHeapTest {

    @Test
    public void test() {
        var heap = new PairingHeap<>(Integer::compare);

        heap.insert(3);
        heap.insert(2);
        heap.insert(1);
        System.out.println(heap.extractMin());
        System.out.println(heap.extractMin());
        System.out.println(heap.extractMin());

    }
}
