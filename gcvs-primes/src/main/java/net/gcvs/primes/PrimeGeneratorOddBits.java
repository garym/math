package net.gcvs.primes;

import static net.gcvs.primes.PrimeSet.expandToArray;

/**
 * experimental
 */
public class PrimeGeneratorOddBits implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;
    private static final int BITS_PER_WORD = 1 << ADDRESS_BITS_PER_WORD;

    /* Used to shift left or right for a partial word mask */
    private static final long WORD_MASK = 0xffffffffffffffffL;

    private static int wordIndex(int bitIndex) {
        return bitIndex >> ADDRESS_BITS_PER_WORD;
    }

    private static int nextSetBit(long[] words, int fromIndex) {
        fromIndex >>= 1;
        final int wordsInUse = words.length;
        if (fromIndex < 0) return -1; // word overflow protection

        int u = wordIndex(fromIndex);
        if (u >= wordsInUse)
            return -1;

        long word = words[u] & (WORD_MASK << fromIndex);

        for (; ; ) {
            if (word != 0) return (((u * BITS_PER_WORD) + Long.numberOfTrailingZeros(word)) << 1) + 1;
            if (++u == wordsInUse) return -1;
            word = words[u];
        }
    }

    static void clear(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] &= ~(1L << bitIndex);
    }

    private static void set(long[] words, int bitIndex) {
        bitIndex >>= 1;
        int wordIndex = wordIndex(bitIndex);
        words[wordIndex] |= (1L << bitIndex);
    }

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        // primes = 0..n
        long[] primes = new long[((n - 1) >> 7) + 1];
        final int nextPrime = Stripes.sieveStripe2(primes);

        // cap at n, clear bits beyond n
        final int nmod = ((n - 1) >> 1) & 0x3f;
        if (nmod != 0x3f) {
            primes[primes.length - 1] &= (1L << (nmod + 1)) - 1;
        }

        final int sqrt = (int) Math.ceil(Math.sqrt(n));
        final int minsqrt = Math.min(63, sqrt);
        int p;
        for (p = nextSetBit(primes, nextPrime); p != -1 && p <= minsqrt; p = nextSetBit(primes, p + 2)) {
            final int step = p << 1;
            for (int m = p * p; m >= 0 && m <= n; m += step) {
                clear(primes, m);
            }
        }
        for (int max = n >> 1; p != -1 && p <= sqrt; p = nextSetBit(primes, p + 2)) {
            for (int mp = (p * p) >> 1; mp <= max; mp += p) {
                primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
            }
        }

        return expandToArray(primes);
    }
}
