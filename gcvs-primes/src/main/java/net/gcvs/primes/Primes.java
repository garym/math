package net.gcvs.primes;

import net.gcvs.util.LongArray;

import java.util.BitSet;

public class Primes {
    private static PrimeGenerator primeGenerator = new PrimeGeneratorA();
    private static IntPrimeGenerator intPrimeGenerator = new PrimeGeneratorOddBitsUnroll6CacheLineReorderMemWrites();

    public static LongArray longArray(long n) {
        return primeGenerator.longArray(n);
    }

    public static int[] intArray(int n) {
        return intPrimeGenerator.intArray(n);
    }

    /**
     * @deprecated - BitSet is no longer state of the art in gcvs prime generation, use {@link #intArray(int)} instead.
     */
    @Deprecated
    public static BitSet primes(int n) {
        return PrimeSet.arrayToBitSet(intArray(n));
    }
}
