package net.gcvs.matrix;

public class Product extends Expression {
	final Expression a;
	final Expression b;

	public Product(final Terms terms, final int[] factors, final Expression a, final Expression b) {
		super(terms, factors);
		this.a = a;
		this.b = b;
	}

	public String toString() {
		final StringBuilder sb = new StringBuilder();
		if (a.length() > 1) {
			sb.append("(").append(a).append(")");
		} else {
			sb.append(a);
		}
		sb.append(" * ");
		if (b.length() > 1) {
			sb.append("(").append(b).append(")");
		} else {
			sb.append(b);
		}
		return sb.toString(); // + expression(terms, factors);
	}

	public static Product multiply(final Terms terms, final Expression a, final Expression b) {
		final int[] mseq = a.factors;
		final int[] nseq = b.factors;
		final int[] prod = terms.newInstance();
		for (int i = 0; i < mseq.length; i++) {
			final int mv = mseq[i];
			if (mv == 0) {
				continue;
			}
			for (int j = 0; j < nseq.length; j++) {
				final int nv = nseq[j];
				if (nv == 0) {
					continue;
				}
				final String name = a.terms.name(i) + b.terms.name(j);
				final int term = terms.findTerm(name);
				// make sure the product contains valid terms
				if (term == -1) {
					throw new RuntimeException(name);
				}
				prod[term] = mv * nv;
			}
		}
		return new Product(terms, prod, a, b);
	}
}