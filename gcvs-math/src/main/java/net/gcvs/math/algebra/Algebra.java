package net.gcvs.math.algebra;

public interface Algebra<V> {

    V zero();

    V one();

    V constant(long value);

    V add(V a, V b);

    V subtract(V a, V b);

    V multiply(V a, V b);

    V divide(V a, V b);

    DivMod<V> divideAndRemainder(V a, V b);

    V[] newArray(int length);

    String toString(V a);
}
