package net.gcvs.math;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FloatsTest {

    @Test
    @Disabled
    void test() {
        System.out.println(Floats.q_rsqrt(1));

        for (int i = 1; i <= 100; i++) {
            System.out.println("q_rsqrt(" + i + "): " + Floats.q_rsqrt(i) + ", 1/sqrt: " + (float) (1.0f / Math.sqrt(i)));
        }
    }

    @Test
    @Disabled
    void speed() {
        long start = System.currentTimeMillis();
        float f = 0;
        for (int i = 1; i < 10_000_000; i++) {
            f += Floats.q_rsqrt(i);
        }
        System.out.println((System.currentTimeMillis() - start) + " millis, sum: " + f);
        f = 0;
        for (int i = 1; i < 10_000_000; i++) {
            f += 1.0f / Math.sqrt(i);
        }
        System.out.println((System.currentTimeMillis() - start) + " millis, sum: " + f);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 0", "1, 0, 0", "0, 255, 0", "1, 255, 0",
            "0, 0, 8388607", "1, 0, 8388607", "0, 255, 8388607", "1, 255, 8388607",
            "0, 1, 0", "1, 1, 0", "0, 254, 0", "1, 254, 0",
            "0, 1, 8388607", "1, 1, 8388607", "0, 254, 8388607", "1, 254, 8388607",
            "0, 127, 0", "1, 127, 0",
            "0, 127, 8388607", "1, 127, 8388607",

    })
    void sample_floats(int s, int e, int m) {
        final Flot f = new Flot(s, e - Flot.EXPONENT_BIAS, m);
        float value = f.floatValue();
        final Flot d = new Flot(value);
        assertEquals(f.s, d.s);
        assertEquals(f.e, d.e);
        assertEquals(f.m, d.m);
    }


    @Test
    @Disabled
    void test_all_floats() {
        Flot f = new Flot(0);
        for (int s = 0; s <= 1; s++) {
            for (int e = 0; e <= 255; e++) {
                for (int m = 0; m < 0x800000; m++) {
                    f.s = s;
                    f.e = e;
                    f.m = m;

                    float value = f.floatValue();
                    Flot d = new Flot(value);
                    assertEquals(f.s, d.s);
                    assertEquals(f.e, d.e);
                    assertEquals(f.m, d.m);
                }
            }
        }
    }

    @Test
    void small_values() {
        assertEquals(0.25f, new Flot(0, -2, 0).floatValue());
    }

    @Test
    void construct_0_25f() {
        final Flot flot = new Flot(0.25f);
        assertEquals(0, flot.s);
        assertEquals(125, flot.e);
        assertEquals(0, flot.m);
    }


    @Test
    void test_values() {
        Flot zero = new Flot(0f);
        assertEquals(0, zero.s);
        assertEquals(0, zero.e);
        assertEquals(0, zero.m);
        assertEquals(0f, new Flot(0, -127, 0).floatValue());
        assertEquals(0.5f, new Flot(0, -1, 0).floatValue());
        assertEquals(-0.5f, new Flot(1, -1, 0).floatValue());
        assertEquals(1f, new Flot(0, 0, 0).floatValue());
        assertEquals(-1f, new Flot(1, 0, 0).floatValue());
    }


}
