package net.gcvs.heap;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class TotalOrderOracleTest {

	Integer[] naturals(final int n) {
		Integer[] order = new Integer[n];
		for (int i = 0; i < n; i++) {
			order[i] = i + 1;
		}
		return order;
	}

	@Test
	public void test1() {
		Integer[] list = { 1, 2, 3, 4 };
		TotalOrderOracle<Object> too = new TotalOrderOracle<>(5);

		too.lessThan(0, 1);
		too.lessThan(1, 2);
		too.lessThan(2, 3);
		too.lessThan(3, 4);
	}

	public static <T> void reverse(final T[] list) {
		for (int i = list.length / 2; i >= 0; i--) {
			int ci = list.length - 1 - i;
			T tmp = list[i];
			list[i] = list[ci];
			list[ci] = tmp;
		}
	}

	Integer[] cross(final int[] a, final int[] b) {
		Integer[] list = new Integer[a.length * b.length];
		int idx = 0;
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				list[idx++] = a[i] + b[j];
			}
		}
		return list;
	}

	void mul(final int[] a, final int f) {
		Arrays.setAll(a, i -> a[i] * f);
	}

	void add(final int[] a, final int f) {
		Arrays.setAll(a, i -> a[i] + f);
	}

	@Test
	void test2() {
		int[] a = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int[] b = a.clone();
		add(b, 1);
		mul(b, a.length);
		Integer[] list = cross(a, b);
		System.out.println(Arrays.toString(list));
		TotalOrderOracle<Integer> cmp = new TotalOrderOracle<>(list, Integer::compare);

		
		for (int i = 0; i < a.length - 1; i++) {
			cmp.lessThan(i, i + 1);
			cmp.lessThan(i, i * a.length);
		}
		
		
		// shuffle(list);
		Arrays.sort(list, cmp);

		cmp.report();
	}
}
