package net.gcvs.primes;

import org.openjdk.jmh.annotations.*;

@BenchmarkMode(Mode.AverageTime)
@Fork(value = 1, warmups = 0)
@Warmup(iterations = 2)
@Measurement(time = 10, iterations = 3)
public class StripesBenchmark {

    @State(Scope.Thread)
    public static class MyState {
        final int n = Integer.MAX_VALUE;
        long[] primes = new long[((n - 1) >> 7) + 1];
    }

    @Benchmark
    public int stripes(MyState state) {
        return Stripes.sieveStripe(state.primes);
    }

    @Benchmark
    public int stripes2(MyState state) {
        return Stripes.sieveStripe2(state.primes);
    }
}
