package net.gcvs.math.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * lazy 1 step lookahead iterator
 */
public final class Peekerator<T> implements Iterator<T> {
    private final Iterator<T> it;
    private T peek;

    public Peekerator(final Iterator<T> it) {
        this.it = it;
    }

    @Override
    public boolean hasNext() {
        return peek != null || it.hasNext();
    }

    @Override
    public T next() {
        T value = peek;
        if (value != null) {
            peek = null;
            return value;
        }
        return it.next();
    }

    /**
     * like next() but doesn't consume the value
     */
    public T peek() {
        if (peek == null) {
            if (it.hasNext()) {
                peek = it.next();
            }
            if (peek == null) {
                throw new NoSuchElementException();
            }
        }
        return peek;
    }

    public static <T> Peekerator<T> of(final Iterator<T> it) {
        return new Peekerator<>(it);
    }
}
