package net.gcvs.math.cf;

@FunctionalInterface
public interface Generator<T> {
    T next();

    static <T> Generator<T> of(T... values) {
        return new Generator<T>() {
            int idx;

            @Override
            public T next() {
                return idx < values.length ? values[idx++] : null;
            }
        };
    }
}
