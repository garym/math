package net.gcvs.util;

public final class Join {
    private Join() {
    }

    public static <T> String join(final String sep, final T... objects) {
        if (objects.length == 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder(objects[0].toString());
        for (int i = 1, len = objects.length; i < len; i++) {
            sb.append(sep).append(objects[i]);
        }
        return sb.toString();
    }
}
