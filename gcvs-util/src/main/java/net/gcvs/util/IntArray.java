package net.gcvs.util;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.IntStream;

/**
 * An expandable array of int primitives.
 *
 * @author gary
 */
public class IntArray {
    private static final int MAX_CAPACITY = Integer.MAX_VALUE;

    private int[] array;
    private int size;

    public IntArray() {
        this(16);
    }

    public IntArray(final int capacity) {
        array = new int[capacity];
    }

    public IntArray(int[] array) {
        this.array = array.clone();
        this.size = array.length;
    }

    public void add(final int i) {
        int size = this.size;
        if (size == array.length) {
            int newSize = (int) Math.min(MAX_CAPACITY, Math.multiplyFull(array.length, 2));
            Objects.checkIndex(size, newSize);
            array = Arrays.copyOf(array, newSize);
        }
        array[size++] = i;
        this.size = size;
    }

    public int[] toArray() {
        return Arrays.copyOf(array, size);
    }

    public int get(final int idx) {
        Objects.checkIndex(idx, size);
        return array[idx];
    }

    public int size() {
        return size;
    }

    public int remove(final int idx) {
        Objects.checkIndex(idx, size);
        int value = get(idx);
        System.arraycopy(array, idx + 1, array, idx, --size - idx);
        return value;
    }

    public IntStream stream() {
        return Arrays.stream(array, 0, size);
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "[]";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append('[').append(array[0]);
        for (int i = 1; i < size; i++) {
            sb.append(", ").append(array[i]);
        }
        sb.append(']');
        return sb.toString();
    }

    public boolean contains(int value) {
        for (int i = 0, len = size; i < len; i++) {
            if (value == array[i]) {
                return true;
            }
        }
        return false;
    }
}
