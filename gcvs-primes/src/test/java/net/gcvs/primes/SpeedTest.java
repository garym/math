package net.gcvs.primes;

import net.gcvs.util.LongArray;
import net.gcvs.util.Timing;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("slow")
public class SpeedTest {


    @Test
    public void testSpeed() {
        long n = 100_000_000;
        n = 1000;
        long start = System.currentTimeMillis();
        final LongArray primesA = new PrimeGeneratorA().longArray(n);
        long mid = System.currentTimeMillis();
        final LongArray primesOdd = new PrimeGeneratorOdd().longArray(n);
        long end = System.currentTimeMillis();
        System.out.println(primesA.size() + ", " + (mid - start));
        System.out.println(primesOdd.size() + ", " + (end - mid));

        for (int i = 0; i < primesOdd.size(); i++) {
            if (!primesA.contains(primesOdd.get(i))) {
                System.out.println("fake prime: " + primesOdd.get(i));
            }
        }
    }

    @Test
    void primeGeneratorA() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorA().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorB() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorB().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorNativeBits() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorNativeBits().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBits() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBits().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll2() {
        int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll2().intArray(Integer.MAX_VALUE));
//        primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll2().intArray(Integer.MAX_VALUE));
        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll3() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll3().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll4() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll4().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll5() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll5().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll6() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll6().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll5CacheLine() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll5CacheLine().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnrollN() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnrollN().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll6CacheLine() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll6CacheLine().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorOddBitsUnroll6CacheLineReorderMemWrites() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnroll6CacheLineReorderMemWrites().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void primeGeneratorLongOddBits() {
        final int[] primes = Timing.call(() -> new PrimeGeneratorLongOddBits().intArray(Integer.MAX_VALUE));

        report(primes);

        assertEquals(105097565, primes.length);
    }

    @Test
    void long_primeGeneratorLongOddBits() {
        PrimeSet primeSet = Timing.call(() -> new PrimeGeneratorLongOddBits().primeSet(Integer.MAX_VALUE));

        report(primeSet);

        assertEquals(105097565, 1 + primeSet.count());
    }

    @Test
    void long_primeGeneratorLongOddBits_max() {
        PrimeSet primeSet = Timing.call(() -> new PrimeGeneratorLongOddBits().primeSet(47L * Integer.MAX_VALUE));

        report(primeSet);

        //  1L * Integer.MAX_VALUE => found 105097565 primes, max 2147483647
        //  2L * Integer.MAX_VALUE => found 203280221 primes, max 4294967291
        //  3L * Integer.MAX_VALUE => found 299164159 primes, max 6442450939
        //  4L * Integer.MAX_VALUE => found 393615806 primes, max 8589934583
        //  5L * Integer.MAX_VALUE => found 487025115 primes, max 10737418213
        //  6L * Integer.MAX_VALUE => found 579626882 primes, max 12884901877
        //  7L * Integer.MAX_VALUE => found 671562744 primes, max 15032385527
        //  8L * Integer.MAX_VALUE => found 762939111 primes, max 17179869143

        // 16L * Integer.MAX_VALUE => found 1480206279 primes, max 34359738337
        // 17L * Integer.MAX_VALUE => found 1568616884 primes, max 36507221957
        // 18L * Integer.MAX_VALUE => found 1656810703 primes, max 38654705641, took 182966 millis
        // 19L * Integer.MAX_VALUE => found 1744803208 primes, max 40802189291, took 191387 millis
        // 20L * Integer.MAX_VALUE => found 1832604293 primes, max 42949672903, took 203571 millis
        // 21L * Integer.MAX_VALUE => found 1920231107 primes, max 45097156519, took 214687 millis
        // 22L * Integer.MAX_VALUE => found 2007681239 primes, max 47244640231, took 227772 millis
        // 23L * Integer.MAX_VALUE => found 2094974025 primes, max 49392123853, took 233612 millis
        // 24L * Integer.MAX_VALUE => found 2182112796 primes, max 51539607521, took 248340 millis

        // 47L * Integer.MAX_VALUE => found 4154834833 primes, max 100931731337, took 514236 millis

        // 466L * Integer.MAX_VALUE => found
        // 512L * Integer.MAX_VALUE => found

        // "sieve primes up to 1,000,000,000,000 should take less than a minute"
        assertEquals(105097565, 1 + primeSet.count());
    }

    private static void report(int[] primes) {
        System.out.println("found " + primes.length + " primes, max " + primes[primes.length - 1]);
    }

    private static void report(long[] primes) {
        System.out.println("found " + primes.length + " primes, max " + primes[primes.length - 1]);
    }

    private static void report(PrimeSet primes) {
        long max = primes.max();
        while (!primes.getLong(max)) {
            max--;
        }
        System.out.println("found " + (1 + primes.count()) + " primes, max " + max);
    }

    @Test
    void difftwo() {
        final int[] ps0 = Timing.call(() -> new PrimeGeneratorOddBitsUnroll5().intArray(Integer.MAX_VALUE));
        final int[] ps1 = Timing.call(() -> new PrimeGeneratorOddBitsUnrollN().intArray(Integer.MAX_VALUE));

        for (int i = 0; i < ps0.length; i++) {
            if (ps0[i] != ps1[i]) {
                System.out.println();
            }
        }
    }

    @Test
    void bestN() {
        final int[] warmup = Timing.call(() -> new PrimeGeneratorOddBitsUnrollN().intArray(Integer.MAX_VALUE));

        for (int i = 1; i < 40; i++) {
            final int n = i;
            final int[] primes = Timing.call(() -> new PrimeGeneratorOddBitsUnrollN(n).intArray(Integer.MAX_VALUE));
            System.out.println("N(" + i + ") = " + Timing.lastTime());
            assertEquals(105097565, primes.length);
        }

    }
}
