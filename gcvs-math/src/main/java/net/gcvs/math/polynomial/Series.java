package net.gcvs.math.polynomial;

/**
 * iterable in increasing order of degree
 * <p>
 * check out partial differentiation for series transformation ideas
 * www.youtube.com/watch?v=2dwQUUDt5Is&t=0s&ab_channel=Morphocular
 */
public interface Series<V> extends Iterable<Term<V>> {
    default TruncatedSeries<V> truncate(final long maxDegree) {
        // repackage a truncated series
        if (this instanceof TruncatedSeries<V> truncatedSeries) {
            if (truncatedSeries.getMaxDegree() <= maxDegree) {
                return truncatedSeries;
            }
            return new TruncatedSeries<>(truncatedSeries.getSeries(), maxDegree);
        }
        return new TruncatedSeries<>(this, maxDegree);
    }

    static <V> SeriesBuilder<V> builder() {
        return new SeriesBuilder<>();
    }
}
