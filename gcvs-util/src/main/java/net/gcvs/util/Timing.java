package net.gcvs.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Timing {
    private Timing() {
    }

    static Consumer<String> logger = System.out::println;

    private static ThreadLocal<Long> lastTime = new ThreadLocal<>();

    public static void time(CheckedRunnable r) {
        run("", r);
    }

    public static void time(String name, CheckedRunnable r) {
        run(name, r);
    }

    public static <T> T time(Callable<T> r) {
        return call("", r);
    }

    public static <T> T time(String name, Callable<T> r) {
        return call(name, r);
    }

    public static void run(CheckedRunnable r) {
        run("", r);
    }

    public static void run(String name, CheckedRunnable r) {
        long start = System.currentTimeMillis();
        try {
            r.run();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            final long time = System.currentTimeMillis() - start;
            lastTime.set(time);
            logger.accept(name + " took " + time + " millis");
        }
    }

    public static <T> T call(Callable<T> r) {
        return call("", r);
    }

    public static <T> T call(String name, Callable<T> r) {
        long start = System.currentTimeMillis();
        try {
            return r.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            final long time = System.currentTimeMillis() - start;
            lastTime.set(time);
            logger.accept(name + " took " + time + " millis");
        }
    }

    public static <T> List<T> repeat(Supplier<T> r, int times) {
        return repeat("", r, times);
    }

    public static <T> List<T> repeat(String name, Supplier<T> r, int times) {
        List<T> list = new ArrayList<>(times);
        long start = System.currentTimeMillis();
        try {
            for (int i = 0; i < times; i++) {
                final T value = r.get();
                list.add(value);
            }
            return list;
        } finally {
            final long time = System.currentTimeMillis() - start;
            lastTime.set(time);
            logger.accept(name + " took " + time + " millis, " + times + " calls, " + new BigDecimal(time).divide(new BigDecimal(times), 2, RoundingMode.HALF_EVEN) + " millis average per call");
        }
    }

    public static long lastTime() {
        return lastTime.get();
    }

    private static long start;

    public static void start() {
        start = System.currentTimeMillis();
    }

    public static long tear(String name) {
        long time = System.currentTimeMillis() - start;
        logger.accept(name + " took " + time + " millis");
        start = System.currentTimeMillis();
        return time;
    }
}
