package net.gcvs.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongFractionTest {
    public static LongFraction lf(long n, long d) {
        return new LongFraction(n, d);
    }

    @Test
    void power() {
        assertEquals(lf(2, 3).power(0), lf(1, 1));
        assertEquals(lf(2, 3).power(1), lf(2, 3));
        assertEquals(lf(2, 3).power(-1), lf(3, 2));
        assertEquals(lf(1, 3).power(2), lf(1, 9));
        assertEquals(lf(2, 3).power(-2), lf(9, 4));
        assertEquals(lf(11, 17).power(3), lf(11 * 11 * 11, 17 * 17 * 17));
        assertEquals(lf(11, 17).power(-3), lf(17 * 17 * 17, 11 * 11 * 11));
    }

    @Test
    void add() {
        assertEquals(lf(0, 3).add(lf(5, 7)), lf(5, 7));
        assertEquals(lf(1, 3).add(lf(0, 7)), lf(1, 3));
        assertEquals(lf(2, 3).add(lf(5, 7)), lf(29, 21));
    }

    @Test
    void subtract() {
    }

    @Test
    void simplify() {
    }

    @Test
    void compareTo() {
    }

    @Test
    void testHashCode() {
    }

    @Test
    void testEquals() {
    }

    @Test
    void testToString() {
    }

    @Test
    void mul() {
    }

    @Test
    void multiply() {
    }

    @Test
    void signum() {
    }

    @Test
    void doubleValue() {
    }

    @Test
    void getNumerator() {
    }

    @Test
    void getDenominator() {
    }

    @Test
    void toBigRational() {
    }
}
