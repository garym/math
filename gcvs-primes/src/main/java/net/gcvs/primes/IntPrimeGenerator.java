package net.gcvs.primes;

public interface IntPrimeGenerator {
    int[] intArray(int n);
}
