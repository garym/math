package net.gcvs.heap;

/**
 * Generic Heap contract
 *
 * @param <T>
 */
public interface Heap<T> {
    // useful
    boolean isEmpty();

    void insert(final T t);

    /**
     * simply return the top element of the heap
     *
     * @return
     */
    T findMin();

    T extractMin();

    default void decreaseKey(final T x, final T k) {
        throw new UnsupportedOperationException();
    }

    default void delete(final T x) {
        throw new UnsupportedOperationException();
    }

    /**
     * compare the two root elements, the smaller remains the root of the result, the larger element and its subtree is appended as a child of this root.
     *
     * @param heap
     */
    default void meld(final Heap<T> heap) {
        throw new UnsupportedOperationException();
    }
}
