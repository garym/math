package net.gcvs.math.polynomial;

import net.gcvs.math.BigRational;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TermGeneratorTest {

    @Test
    void iterate_stopWhenNull() {

        TermGenerator<BigRational> tg = i -> i > 5 ? null : BigRational.valueOf(i);

        List<Term<BigRational>> terms = new ArrayList<>();

        assertTimeoutPreemptively(Duration.ofMillis(500), () -> tg.forEach(terms::add));

        assertEquals(6, terms.size());
        assertEquals(0, terms.get(0).degree());
        assertEquals(1, terms.get(1).degree());
        assertEquals(2, terms.get(2).degree());
        assertEquals(3, terms.get(3).degree());
        assertEquals(4, terms.get(4).degree());
        assertEquals(5, terms.get(5).degree());

        assertEquals(BigRational.valueOf(0), terms.get(0).coefficient());
        assertEquals(BigRational.valueOf(1), terms.get(1).coefficient());
        assertEquals(BigRational.valueOf(2), terms.get(2).coefficient());
        assertEquals(BigRational.valueOf(3), terms.get(3).coefficient());
        assertEquals(BigRational.valueOf(4), terms.get(4).coefficient());
        assertEquals(BigRational.valueOf(5), terms.get(5).coefficient());

    }

}