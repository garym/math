package net.gcvs.math;

public class Floats {

    public static float q_rsqrt(float number) {
        int i;
        float x2, y;
        float threehalfs = 1.5f;

        x2 = number * 0.5f;
        y = number;
        i = Float.floatToRawIntBits(number);
        i = 0x5f3759df - (i >> 1);
        y = Float.intBitsToFloat(i);
        y = y * (threehalfs - (x2 * y * y));
//        y = y * (threehalfs - (x2 * y * y));
        return y;
    }

}
