package net.gcvs.stopwatch;

import lombok.RequiredArgsConstructor;
import net.gcvs.util.ConsoleColors;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StopwatchReport {
    private static final String PATH_NAME_PREFIX = " - ";

    private static final Comparator<TimerInfo> SELF_TIME = Comparator.comparingLong(a -> a.selfTime.total);
    private static final Comparator<TimerInfo> ELAPSED_TIME = Comparator.comparingLong(a -> a.elapsedTime.total);

    private static String lpad(int width, Object obj) {
        String text = String.valueOf(obj);
        StringBuilder sb = new StringBuilder();
        sb.append(text);
        IntStream.range(0, width - text.length()).forEach(i -> sb.append(' '));
        return sb.toString();
    }

    private static String rpad(int width, Object obj) {
        String text = String.valueOf(obj);
        StringBuilder sb = new StringBuilder();
        IntStream.range(0, width - text.length()).forEach(i -> sb.append(' '));
        sb.append(text);
        return sb.toString();
    }

    private static String colored(String text, ConsoleColors color) {
        return color + text + ConsoleColors.RESET;
    }

    private static String reportName(String name) {
        return colored(name, ConsoleColors.BLUE_BOLD_BRIGHT);
    }

    private static String reportTime(String time) {
        return colored(time, ConsoleColors.RED_BOLD_BRIGHT);
    }

    private static String reportPathName(String name) {
        return colored(name, ConsoleColors.BLUE);
    }

    private static String reportPathTime(String time) {
        return colored(time, ConsoleColors.RED);
    }

    public void print() {
        print(10, 3);
    }

    public void print(int maxSize, int maxPathCount) {
        final String report = generate(maxSize, maxPathCount);
        System.out.println(report);
    }

    public String generate() {
        return generate(10, 3);
    }

    // shared report column widths
    int nameWidth;
    int pathWidth;
    int rowNameWidth;
    int timeWidth;
    boolean includeCalls;

    public String generate(final int maxSize, final int maxPathCount) {
        List<TimerInfo> list = new ArrayList<>(this.timers.values());
        final List<TimerInfo> worstSelf = list.stream()
                .sorted(SELF_TIME.reversed())
                .limit(maxSize)
                .sorted(SELF_TIME)
                .toList();
        final List<TimerInfo> worstTime = list.stream()
                .sorted(ELAPSED_TIME.reversed())
                .limit(maxSize)
                .sorted(ELAPSED_TIME)
                .toList();
        final Map<TimerInfo, List<Entry<String, Stopwatch>>> worstSelfPaths = worstSelf.stream()
                .collect(Collectors.toMap(i -> i, collectTopPaths(maxPathCount, e -> e.getSelfElapsed(now))));
        final Map<TimerInfo, List<Entry<String, Stopwatch>>> worstTimePaths = worstTime.stream()
                .collect(Collectors.toMap(i -> i, collectTopPaths(maxPathCount, e -> e.getElapsed(now))));

        this.nameWidth = Stream.concat(worstSelf.stream(), worstTime.stream()).mapToInt(t -> t.name.length()).max().orElse(0);
        this.pathWidth = Stream.concat(worstSelfPaths.values().stream(), worstSelfPaths.values().stream())
                .flatMap(List::stream).map(Entry::getKey).mapToInt(String::length).max().orElse(0);
        this.rowNameWidth = Math.max(nameWidth, pathWidth);
        this.timeWidth = Stream.concat(worstSelf.stream(), worstTime.stream())
                .flatMapToLong(t -> LongStream.of(t.elapsedTime.total, t.selfTime.total, t.calls.total))
                .mapToInt(v -> Long.toString(v).length())
                .max().orElse(0);
        this.includeCalls = Stream.concat(worstSelf.stream(), worstTime.stream()).anyMatch(t -> t.calls.total > 1);


        final StringBuilder out = new StringBuilder();

        out.append(colored("Stopwatch Report for", ConsoleColors.GREEN_BRIGHT));
        out.append(": ").append(reportName(stopwatch.name)).append('\n');

        out.append(colored("worst self times", ConsoleColors.GREEN)).append('\n');
        worstSelf.forEach(info -> {
            StringBuilder sb = new StringBuilder();
            sb.append(reportName(lpad(rowNameWidth + PATH_NAME_PREFIX.length(), info.name)));
            sb.append(" : self: ");
            sb.append(reportTime(rpad(timeWidth, info.selfTime.total)));
            if (info.calls.total <= 1) {
                if (includeCalls) sb.append("   ");
                sb.append(reportTime(rpad(timeWidth, "")));
            } else {
                sb.append(" / ").append(reportTime(rpad(timeWidth, info.calls.total)));
            }
            if (info.selfTime.total < info.elapsedTime.total) {
                sb.append(" (total: ").append(reportTime(rpad(timeWidth, info.elapsedTime.total))).append(")");
            }
            out.append(sb);
            out.append('\n');

            listMaxUse(worstSelfPaths.get(info), now, rowNameWidth, timeWidth, out, info, true);
        });
        out.append('\n');

        out.append(colored("worst total times", ConsoleColors.GREEN)).append('\n');
        worstTime.forEach(info -> {
            StringBuilder sb = new StringBuilder();
            sb.append(reportName(lpad(rowNameWidth + PATH_NAME_PREFIX.length(), info.name)));
            sb.append(" : time: ");
            sb.append(reportTime(rpad(timeWidth, info.elapsedTime.total)));
            if (info.calls.total <= 1) {
                if (includeCalls) sb.append("   ");
                sb.append(reportTime(rpad(timeWidth, "")));
            } else {
                sb.append(" / ").append(reportTime(rpad(timeWidth, info.calls.total)));
            }
            if (info.selfTime.total < info.elapsedTime.total) {
                sb.append("  (self: ").append(reportTime(rpad(timeWidth, info.selfTime.total))).append(")");
            }
            out.append(sb);
            out.append('\n');

            listMaxUse(worstTimePaths.get(info), now, rowNameWidth, timeWidth, out, info, false);
        });

        return out.toString();
    }

    private static Function<? super TimerInfo, ? extends List<Entry<String, Stopwatch>>> collectTopPaths(int maxPathCount, ToLongFunction<Stopwatch> selfElapsedGetter) {
        final Comparator<Entry<String, Stopwatch>> order = Comparator.comparingLong(a -> selfElapsedGetter.applyAsLong(a.getValue()));
        return info -> info.contexts.entrySet()
                .stream().sorted(order.reversed())
                .limit(maxPathCount)
                .sorted(order)
                .toList();
    }

    private void listMaxUse(List<Entry<String, Stopwatch>> list, final long now, int pathWidth, int timeWidth, StringBuilder out, TimerInfo info, boolean self) {
        if (list.size() <= 1) return;
        list.forEach(entry -> {
            final Stopwatch sw = entry.getValue();
            out.append(PATH_NAME_PREFIX);
            out.append(reportPathName(lpad(pathWidth, entry.getKey())));
            out.append(" : ");
            out.append(self ? "self" : "time");
            out.append(": ");
            out.append(reportPathTime(rpad(timeWidth, self ? sw.getSelfElapsed(now) : sw.getElapsed(now))));
            if (info.calls.total > 1) {
                out.append(" / ").append(reportPathTime(rpad(timeWidth, sw.calls())));
            } else {
                out.append("   ").append(rpad(timeWidth, ""));
            }
            if (sw.getSelfElapsed(now) < sw.getElapsed(now)) {
                out.append(self ? " (total" : "  (self");
                out.append(": ").append(reportPathTime(rpad(timeWidth, self ? sw.getElapsed(now) : sw.getSelfElapsed(now)))).append(")");
            }
            out.append('\n');
        });
    }

    static class TimerValue {
        long min;
        long max;
        long total;
        long count;

        double average() {
            if (count == 0) return 0;
            return (double) total / (double) count;
        }

        void add(long value) {
            min = Math.min(min, value);
            max = Math.max(max, value);
            total += value;
            count++;
        }
    }

    @RequiredArgsConstructor
    static class TimerInfo {
        final String name;

        TimerValue elapsedTime = new TimerValue();

        TimerValue selfTime = new TimerValue();
        TimerValue calls = new TimerValue();

        Map<String, Stopwatch> contexts = new HashMap<>();

        void add(long now, Stopwatch stopwatch) {
            elapsedTime.add(stopwatch.getElapsed(now));
            selfTime.add(stopwatch.getSelfElapsed(now));
            calls.add(stopwatch.calls());

            String path = stopwatch.getPath();
            if (path.endsWith("/" + stopwatch.name + "/")) {
                path = path.substring(0, path.length() - stopwatch.name.length() - 2);
            }
            contexts.put(path, stopwatch);
        }
    }

    private final Stopwatch stopwatch;
    private final long now;

    private final Map<String, TimerInfo> timers = new HashMap<>();

    public StopwatchReport(final Stopwatch stopwatch, final long now) {
        this.stopwatch = stopwatch;
        this.now = now;
    }

    void add(Stopwatch stopwatch) {
        final TimerInfo timerInfo = timers.computeIfAbsent(stopwatch.name, TimerInfo::new);
        timerInfo.add(now, stopwatch);
    }
}
