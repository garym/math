package net.gcvs.math;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


class BigRationalTest {

    static final long[] longs = {
            -12345678901234L, -123456, -100,
            -12, -10, -9, -6, -4, -2, -1,
            0,
            1, 3, 5, 7, 8, 11,
            100, 123, 2468, 369121345674L
    };

    @Test
    void BigRational_long_whenZero_thenZero() {
        BigRational n = new BigRational(0);
        verifyEquals(0, 0, 1, n);
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 2, 4, 12345, 9876543210123L})
    void BigRational_long_whenPositive_thenSignumPositive(long value) {
        BigRational n = new BigRational(value);
        assertEquals(1, n.signum());
        assertEquals(value, n.getNumerator().longValueExact());
        assertEquals(1, n.getDenominator().intValueExact());
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, -2, -4, -12345, -9876543210123L})
    void BigRational_long_whenNegative_thenSignumNegative(long value) {
        BigRational n = new BigRational(value);
        assertEquals(-1, n.signum());
        assertEquals(Math.abs(value), n.getNumerator().longValueExact());
        assertEquals(1, n.getDenominator().intValueExact());
    }

    @Test
    void signum() {
        assertEquals(-1, BigRational.NEGATIVE_INFINITY.signum());
        assertEquals(0, BigRational.NaN.signum());
        assertEquals(1, BigRational.POSITIVE_INFINITY.signum());

        assertEquals(0, BigRational.ZERO.signum());
        assertEquals(1, BigRational.ONE.signum());
        assertEquals(-1, BigRational.ONE.negate().signum());
    }

    @Test
    void BigRational_long() {
        assertEquals(new BigRational(BigInteger.valueOf(Long.MAX_VALUE)), new BigRational(Long.MAX_VALUE));
        assertEquals(new BigRational(BigInteger.valueOf(Long.MIN_VALUE)), new BigRational(Long.MIN_VALUE));
    }

    @Test
    void BigRational_long_long() {
        assertThrows(ArithmeticException.class, () -> new BigRational(0, 0));
        assertThrows(ArithmeticException.class, () -> new BigRational(1, 0));
        assertThrows(ArithmeticException.class, () -> new BigRational(-1, 0));

        BigRational value = new BigRational(1, 2);
        verifyEquals(1, 1, 2, value);

        value = new BigRational(3, 9);
        verifyEquals(1, 3, 9, value);
        verifyEquals(1, 1, 3, value.reduce());

        value = new BigRational(-8, -12);
        verifyEquals(1, 8, 12, value);
        verifyEquals(1, 2, 3, value.reduce());

        value = new BigRational(5, -15);
        verifyEquals(-1, 5, 15, value);
        verifyEquals(-1, 1, 3, value.reduce());

        value = new BigRational(-14, 8);
        verifyEquals(-1, 14, 8, value);
        verifyEquals(-1, 7, 4, value.reduce());

    }

    @Test
    void BigRational_BigInteger() {
        BigRational value = new BigRational(new BigInteger("0"));
        verifyEquals(0, 0, 1, value);

        value = new BigRational(new BigInteger("123"));
        verifyEquals(1, 123, 1, value);

        value = new BigRational(new BigInteger("-234"));
        verifyEquals(-1, 234, 1, value);

        value = new BigRational(new BigInteger("1234567890123456789"));
        assertEquals(1, value.signum());
        assertEquals("1234567890123456789", value.getNumerator().toString());
        assertEquals(1, value.getDenominator().intValueExact());

        value = new BigRational(new BigInteger("-98765432109876543210"));
        assertEquals(-1, value.signum());
        assertEquals("98765432109876543210", value.getNumerator().toString());
        assertEquals(1, value.getDenominator().intValueExact());
    }

    @Test
    void BigRational_BigInteger_BigInteger() {
        assertThrows(ArithmeticException.class, () -> new BigRational(new BigInteger("0"), new BigInteger("0")));
        assertThrows(ArithmeticException.class, () -> new BigRational(new BigInteger("1"), new BigInteger("0")));
        assertThrows(ArithmeticException.class, () -> new BigRational(new BigInteger("-1"), new BigInteger("0")));

        BigRational value = new BigRational(new BigInteger("1"), new BigInteger("2")).reduce();
        verifyEquals(1, 1, 2, value);

        value = new BigRational(new BigInteger("3"), new BigInteger("9"));
        verifyEquals(1, 3, 9, value);
        verifyEquals(1, 1, 3, value.reduce());

        value = new BigRational(new BigInteger("-8"), new BigInteger("-12"));
        verifyEquals(1, 8, 12, value);
        verifyEquals(1, 2, 3, value.reduce());
        value = new BigRational(new BigInteger("5"), new BigInteger("-15"));
        verifyEquals(-1, 5, 15, value);
        verifyEquals(-1, 1, 3, value.reduce());

        value = new BigRational(new BigInteger("-14"), new BigInteger("8"));
        verifyEquals(-1, 14, 8, value);
        verifyEquals(-1, 7, 4, value.reduce());


        BigInteger num = new BigInteger("3");
        BigInteger den = new BigInteger("5");
        final BigRational rational = new BigRational(num, den);
        assertSame(num, rational.getNumerator());
        assertSame(den, rational.getDenominator());
    }

    @Test
    void BigRational_String() {
        BigRational value = new BigRational("0");
        verifyEquals(0, 0, 1, true, value);

        value = new BigRational("1");
        verifyEquals(1, 1, 1, true, value);

        value = new BigRational("-5");
        verifyEquals(-1, 5, 1, true, value);

        value = new BigRational("1.23");
        verifyEquals(1, 123, 100, false, value);

        value = new BigRational("-0.3");
        verifyEquals(-1, 3, 10, false, value);

        value = new BigRational("1/5");
        verifyEquals(1, 1, 5, true, value);

        value = new BigRational("-4/9");
        verifyEquals(-1, 4, 9, false, value);

        value = new BigRational("9.5/-3");
        verifyEquals(-1, 95, 30, false, value);

        value = new BigRational("-1/9.5");
        verifyEquals(-1, 10, 95, false, value);

        value = new BigRational("-1/9.50");
        verifyEquals(-1, 100, 950, false, value);

        value = new BigRational("1.23/4.56").reduce();
        verifyEquals(1, 41, 152, true, value);

        value = new BigRational("123000/0.00456").reduce();
        verifyEquals(1, 512500000, 19, true, value);

        value = new BigRational("0.0456/-4930200").reduce();
        verifyEquals(-1, 19, 2054250000, true, value);

        assertThrows(NumberFormatException.class, () -> new BigRational("x"));
        assertThrows(NumberFormatException.class, () -> new BigRational("1/x"));
        assertThrows(NumberFormatException.class, () -> new BigRational("x/2"));
        assertThrows(NumberFormatException.class, () -> new BigRational("1/2/3"));

        verifyEquals(1, 1, 0, true, new BigRational("Infinity"));
        verifyEquals(-1, 1, 0, true, new BigRational("-Infinity"));
        verifyEquals(0, 0, 0, true, new BigRational("NaN"));

        verifyEquals(0, 0, 0, true, new BigRational("0/0"));
        verifyEquals(1, 1, 0, true, new BigRational("1/0"));
        verifyEquals(-1, 1, 0, true, new BigRational("-1/0"));
        verifyEquals(1, 1, 0, true, new BigRational("10/0"));
        verifyEquals(-1, 1, 0, true, new BigRational("-10/0"));
    }

    @Test
    void BigRational_String_denominatorAndSimplestForm() {
        BigRational value = new BigRational("1");
        assertTrue(value.isSimplestForm());
        assertSame(BigInteger.ONE, value.getDenominator());

        value = new BigRational("10");
        assertTrue(value.isSimplestForm());
        assertSame(BigInteger.ONE, value.getDenominator());

        value = new BigRational("10");
        assertTrue(value.isSimplestForm());
        assertSame(BigInteger.ONE, value.getDenominator());

        value = new BigRational("10/1");
        assertTrue(value.isSimplestForm());
        assertSame(BigInteger.ONE, value.getDenominator());

        value = new BigRational("0.01/0.01");
        assertTrue(value.isSimplestForm());
        assertSame(BigInteger.ONE, value.getDenominator());
    }

    @Test
    void add_BigRational() {
        assertEquals(BigRational.NaN, BigRational.ZERO.add(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.ONE.add(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.add(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.add(BigRational.NaN));

        assertEquals(BigRational.NaN, BigRational.NaN.add(BigRational.ZERO));
        assertEquals(BigRational.NaN, BigRational.NaN.add(BigRational.ONE));
        assertEquals(BigRational.NaN, BigRational.NaN.add(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NaN.add(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.add(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.add(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.add(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.add(BigRational.NEGATIVE_INFINITY));

        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.add(BigRational.ZERO));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.add(BigRational.ONE));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ZERO.add(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ONE.add(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.add(BigRational.ZERO));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.add(BigRational.ONE));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ZERO.add(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ONE.add(BigRational.NEGATIVE_INFINITY));

        BigRational value = new BigRational(10, 1).add(BigRational.ZERO);
        verifyEquals(1, 10, 1, value);

        value = new BigRational(10, 1).add(new BigRational(-10, 1));
        verifyEquals(0, 0, 1, value);

        value = new BigRational(10, 1).add(new BigRational(15, 1));
        verifyEquals(1, 25, 1, value);

        value = new BigRational(2, 3).add(new BigRational(3, 4));
        verifyEquals(1, 17, 12, value);

        value = new BigRational(1, 6).add(new BigRational(1, 3)).reduce();
        verifyEquals(1, 1, 2, value);

        value = new BigRational(5, 1).add(new BigRational(-3, 1));
        verifyEquals(1, 2, 1, value);

        value = new BigRational(-3, 1).add(new BigRational(2, 1));
        verifyEquals(-1, 1, 1, value);

        value = new BigRational(-3, 2).add(new BigRational(5, 4)).reduce();
        verifyEquals(-1, 1, 4, value);

        value = new BigRational(25, 7).add(new BigRational(-5, 3));
        verifyEquals(1, 40, 21, value);

    }

    /**
     * adding a BigInteger is equivalent to adding a BigRational constructed from the BigInteger
     */
    @Test
    void add_BigInteger() {
        verifyBigIntegerSame(BigRational::add, BigRational::add);
    }

    @Test
    void add_BigInteger_whenNotReducedThenSumNotReduced() {
        BigRational value = new BigRational("5/7");
        assertFalse(value.isSimplestForm());

        final BigRational sum = value.add(BigInteger.ONE);

        assertFalse(sum.isSimplestForm());
        assertEquals(sum, new BigRational("12/7"));
    }

    @Test
    void add_BigInteger_whenReducedThenSumReduced() {
        BigRational value = new BigRational("5/7").reduce();
        final BigRational sum = value.add(BigInteger.ONE);

        assertEquals(sum, new BigRational("12/7"));
        assertTrue(sum.isSimplestForm());
    }

    @Test
    void isSimplestForm_examplesTrue() {
        assertTrue(BigRational.ZERO.isSimplestForm());
        assertTrue(BigRational.ONE.isSimplestForm());
        assertTrue(BigRational.NaN.isSimplestForm());
        assertTrue(BigRational.POSITIVE_INFINITY.isSimplestForm());
        assertTrue(BigRational.NEGATIVE_INFINITY.isSimplestForm());

        assertTrue(new BigRational("123").isSimplestForm());
        assertTrue(new BigRational("-321").isSimplestForm());

        assertTrue(new BigRational("2/1").isSimplestForm());
        assertTrue(new BigRational("-8/1").isSimplestForm());

        assertTrue(new BigRational("1/8").isSimplestForm());
        assertTrue(new BigRational("-1/2").isSimplestForm());

        assertTrue(new BigRational("1").isSimplestForm());

        assertTrue(new BigRational("4/10").simplify().isSimplestForm());
        assertTrue(new BigRational("-4/10").simplify().isSimplestForm());

        assertTrue(new BigRational("4").negate().isSimplestForm());
        assertTrue(new BigRational("-7").negate().isSimplestForm());

        assertTrue(new BigRational("1/4").negate().isSimplestForm());
        assertTrue(new BigRational("-1/7").negate().isSimplestForm());

        assertTrue(new BigRational("4").reciprocal().isSimplestForm());
        assertTrue(new BigRational("-7").reciprocal().isSimplestForm());

        assertTrue(new BigRational("6/10").simplify().reciprocal().isSimplestForm());
        assertTrue(new BigRational("-10/15").simplify().reciprocal().isSimplestForm());

        assertTrue(new BigRational("4").abs().isSimplestForm());
        assertTrue(new BigRational("-7").abs().isSimplestForm());

        assertTrue(new BigRational("4").multiply(new BigRational("6")).isSimplestForm());
        assertTrue(new BigRational("1/4").multiply(new BigRational("1/6")).isSimplestForm());


        assertTrue(new BigRational("3/4").multiplyReduce(new BigRational("2/6")).isSimplestForm());
        assertTrue(new BigRational("3/4").addReduce(new BigRational("2/6")).isSimplestForm());
        assertTrue(new BigRational("3/4").subtractReduce(new BigRational("2/6")).isSimplestForm());
        assertTrue(new BigRational("3/4").divideReduce(new BigRational("2/6")).isSimplestForm());

    }

    @Test
    void isSimplestForm_examplesFalse() {
        BigRational value = new BigRational("6/10");
        value.reduce();
        assertFalse(value.isSimplestForm());

        assertFalse(new BigRational("1/2").multiply(new BigRational("3/5")).isSimplestForm());

        assertFalse(new BigRational("2/3").isSimplestForm());
    }

    @Test
    void add_long() {
        verifyLongSame(BigRational::add, BigRational::add);
    }

    @Test
    void negate() {
        assertEquals(BigRational.NaN, BigRational.NaN.negate());
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.POSITIVE_INFINITY.negate());
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.NEGATIVE_INFINITY.negate());

        BigRational value = BigRational.ZERO.negate();
        assertEquals(BigRational.ZERO, value);
        verifyEquals(0, 0, 1, true, value);

        value = new BigRational("1/2").negate();
        verifyEquals(-1, 1, 2, true, value);

        value = new BigRational("-3/9").negate();
        verifyEquals(1, 3, 9, false, value);

        value = new BigRational("-3/4").simplify().negate();
        verifyEquals(1, 3, 4, true, value);

        assertTrue(new BigRational("2/1").negate().isSimplestForm());

    }

    @Test
    void isInfinity() {
        assertFalse(BigRational.ZERO.isInfinite());
        assertFalse(BigRational.ZERO.divide(BigRational.ZERO).isInfinite());
        assertTrue(BigRational.ZERO.reciprocal().isInfinite());

        assertFalse(BigRational.ONE.isInfinite());
        assertTrue(BigRational.ONE.divide(BigRational.ZERO).isInfinite());
        assertTrue(BigRational.ONE.negate().divide(BigRational.ZERO).isInfinite());
        assertFalse(BigRational.ONE.reciprocal().isInfinite());

        assertTrue(new BigRational("10/3").divide(BigRational.ZERO).isInfinite());
        assertTrue(new BigRational("-4/5").divide(BigRational.ZERO).isInfinite());

        assertFalse(BigRational.NaN.isInfinite());
        assertTrue(BigRational.POSITIVE_INFINITY.isInfinite());
        assertTrue(BigRational.NEGATIVE_INFINITY.isInfinite());
    }

    @Test
    void isNaN() {
        assertTrue(BigRational.NaN.isNaN());
        assertTrue(BigRational.ZERO.multiply(BigRational.POSITIVE_INFINITY).isNaN());
        assertTrue(BigRational.ZERO.multiply(BigRational.NEGATIVE_INFINITY).isNaN());
        assertTrue(BigRational.ZERO.divide(BigRational.ZERO).isNaN());
        assertTrue(BigRational.POSITIVE_INFINITY.add(BigRational.NEGATIVE_INFINITY).isNaN());
        assertTrue(BigRational.NEGATIVE_INFINITY.add(BigRational.POSITIVE_INFINITY).isNaN());
        assertTrue(BigRational.POSITIVE_INFINITY.subtract(BigRational.POSITIVE_INFINITY).isNaN());
        assertTrue(BigRational.NEGATIVE_INFINITY.subtract(BigRational.NEGATIVE_INFINITY).isNaN());

        assertFalse(BigRational.ZERO.isNaN());
        assertFalse(BigRational.ZERO.reciprocal().isNaN());

        assertFalse(BigRational.ONE.isNaN());
        assertFalse(BigRational.ONE.reciprocal().isNaN());

        assertFalse(BigRational.POSITIVE_INFINITY.isNaN());
        assertFalse(BigRational.NEGATIVE_INFINITY.isNaN());
    }

    @Test
    void reciprocal() {
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ZERO.reciprocal());

        assertEquals(BigRational.NaN, BigRational.NaN.reciprocal());
        assertEquals(BigRational.ZERO, BigRational.POSITIVE_INFINITY.reciprocal());
        assertEquals(BigRational.ZERO, BigRational.NEGATIVE_INFINITY.reciprocal());

        assertEquals(BigRational.ONE, BigRational.ONE.reciprocal());

        verifyEquals(1, 2, 1, true, new BigRational(1, 2).reciprocal());

        verifyEquals(-1, 4, 3, false, new BigRational(-3, 4).reciprocal());

        verifyEquals(-1, 4, 3, true, new BigRational(-3, 4).reduce().reciprocal());
    }

    @Test
    void subtract_BigRational() {
        assertEquals(BigRational.NaN, BigRational.ZERO.subtract(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.ONE.subtract(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.subtract(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.subtract(BigRational.NaN));

        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.ZERO));
        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.ONE));
        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.subtract(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.subtract(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.subtract(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.subtract(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.subtract(BigRational.ZERO));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.subtract(BigRational.ONE));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ZERO.subtract(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ONE.subtract(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.subtract(BigRational.ZERO));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.subtract(BigRational.ONE));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ZERO.subtract(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ONE.subtract(BigRational.NEGATIVE_INFINITY));

        BigRational value = new BigRational(1, 2).subtract(new BigRational(0, 1));
        verifyEquals(1, 1, 2, value);

        value = new BigRational(1, 2).subtract(new BigRational(1, 2)).reduce();
        verifyEquals(0, 0, 1, value);

        value = new BigRational(2, 3).subtract(new BigRational(1, 6)).reduce();
        verifyEquals(1, 1, 2, value);

        value = new BigRational(2, 3).subtract(new BigRational(-1, 6)).reduce();
        verifyEquals(1, 5, 6, value);

        value = new BigRational(-6, 7).subtract(new BigRational(-1, 3));
        verifyEquals(-1, 11, 21, value);

        value = new BigRational(-6, 7).subtract(new BigRational(1, 3));
        verifyEquals(-1, 25, 21, value);

        value = new BigRational(1, 3).subtract(new BigRational(-1, 6)).reduce();
        verifyEquals(1, 1, 2, value);

    }

    @Test
    void subtract_BigInteger() {

        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.ZERO));
        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.ONE));
        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NaN.subtract(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.subtract(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.subtract(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.subtract(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.subtract(BigRational.POSITIVE_INFINITY));

        verifyBigIntegerSame(BigRational::subtract, BigRational::subtract);
    }

    /**
     * assert a.add(long) == a.add(new BigRational(long));
     */
    @Test
    void subtract_long() {
        verifyLongSame(BigRational::subtract, BigRational::subtract);
    }

    @Test
    void multiply_BigRational() {
        verifyEquals(0, 0, 1, true, BigRational.ZERO.multiplyReduce(BigRational.ONE));
        verifyEquals(0, 0, 1, true, BigRational.ONE.multiplyReduce(BigRational.ZERO));

        BigRational value = new BigRational(1, 1).multiply(new BigRational(0, 1));
        verifyEquals(0, 0, 1, value);

        value = new BigRational(1, 1).multiply(new BigRational(1, 1));
        verifyEquals(1, 1, 1, value);

        value = new BigRational(1, 1).multiply(new BigRational(-1, 1));
        verifyEquals(-1, 1, 1, value);

        value = new BigRational(-2, 1).multiplyReduce(new BigRational(1, 2));
        verifyEquals(-1, 1, 1, value);

        value = new BigRational(-3, 10).multiplyReduce(new BigRational(-2, 3));
        verifyEquals(1, 1, 5, value);

        value = new BigRational(-3, 10).multiplyReduce(new BigRational(-2, 3));
        verifyEquals(1, 1, 5, value);


        verifyEquals(1, 21, 10, true, new BigRational("3/5").multiplyReduce(new BigRational("7/2")));
        verifyEquals(1, 21, 10, true, new BigRational("6/5").multiplyReduce(new BigRational("7/4")));
        verifyEquals(1, 21, 10, true, new BigRational("3/55").multiplyReduce(new BigRational("77/2")));
        verifyEquals(1, 21, 10, true, new BigRational("6/55").multiplyReduce(new BigRational("77/4")));

        verifyEquals(1, 21, 10, true, new BigRational("-3/5").multiplyReduce(new BigRational("-7/2")));
        verifyEquals(-1, 21, 10, true, new BigRational("-3/5").multiplyReduce(new BigRational("7/2")));
        verifyEquals(-1, 21, 10, true, new BigRational("3/5").multiplyReduce(new BigRational("-7/2")));

        for (BigRational r1 : rationals()) {
            for (BigRational r2 : rationals()) {
                BigRational r12 = r1.multiply(r2);
                BigRational r21 = r2.multiply(r1);

                assertEquals(r12, r21);
            }
        }
    }

    @Test
    void multiply_withNaNInfinity() {
        BigRational zero = BigRational.ZERO;
        BigRational positive = new BigRational("5/2");
        BigRational negative = new BigRational("-2/3");
        BigRational NaN = BigRational.NaN;
        BigRational PlusInfinity = BigRational.POSITIVE_INFINITY;
        BigRational MinusInfinity = BigRational.NEGATIVE_INFINITY;

        assertTrue(NaN.multiply(NaN).isNaN());
        assertTrue(NaN.multiply(zero).isNaN());
        assertTrue(NaN.multiply(positive).isNaN());
        assertTrue(NaN.multiply(negative).isNaN());
        assertTrue(zero.multiply(NaN).isNaN());
        assertTrue(positive.multiply(NaN).isNaN());
        assertTrue(negative.multiply(NaN).isNaN());

        assertEquals(PlusInfinity, PlusInfinity.multiply(PlusInfinity));
        assertEquals(NaN, PlusInfinity.multiply(zero));
        assertEquals(PlusInfinity, PlusInfinity.multiply(positive));
        assertEquals(MinusInfinity, PlusInfinity.multiply(negative));
        assertEquals(NaN, zero.multiply(PlusInfinity));
        assertEquals(PlusInfinity, positive.multiply(PlusInfinity));
        assertEquals(MinusInfinity, negative.multiply(PlusInfinity));

        assertEquals(PlusInfinity, MinusInfinity.multiply(MinusInfinity));
        assertEquals(NaN, MinusInfinity.multiply(zero));
        assertEquals(MinusInfinity, MinusInfinity.multiply(positive));
        assertEquals(PlusInfinity, MinusInfinity.multiply(negative));
        assertEquals(NaN, zero.multiply(MinusInfinity));
        assertEquals(MinusInfinity, positive.multiply(MinusInfinity));
        assertEquals(PlusInfinity, negative.multiply(MinusInfinity));

        assertEquals(MinusInfinity, MinusInfinity.multiply(PlusInfinity));
        assertEquals(MinusInfinity, PlusInfinity.multiply(MinusInfinity));

        assertEquals(NaN, NaN.multiply(PlusInfinity));
        assertEquals(NaN, PlusInfinity.multiply(NaN));

        assertEquals(NaN, NaN.multiply(MinusInfinity));
        assertEquals(NaN, MinusInfinity.multiply(NaN));
    }

    @Test
    void multiplyReduce_withNaNInfinity() {
        BigRational a = new BigRational("5/2");
        BigRational NaN = BigRational.NaN;

        assertTrue(NaN.multiplyReduce(NaN).isNaN());
        assertTrue(NaN.multiplyReduce(a).isNaN());
        assertTrue(a.multiplyReduce(NaN).isNaN());
    }

    @Test
    void multiply_BigInteger() {
        verifyBigIntegerSame(BigRational::multiply, BigRational::multiply);
    }

    @Test
    void multiply_long() {
        verifyLongSame(BigRational::multiply, BigRational::multiply);
    }

    @Test
    void divide_BigRational() {
        BigRational value = new BigRational(5, 1).divide(new BigRational(0, 1));
        verifyEquals(1, 1, 0, value);

        value = new BigRational(-5, 1).divide(new BigRational(0, 1));
        verifyEquals(-1, 1, 0, value);

        value = new BigRational(1, 1).divide(new BigRational(1, 1));
        verifyEquals(1, 1, 1, value);

        value = new BigRational(1, 1).divide(new BigRational(-1, 1));
        verifyEquals(-1, 1, 1, value);

        value = new BigRational(-2, 1).divide(new BigRational(1, 2));
        verifyEquals(-1, 4, 1, value);
    }

    @Test
    void divideReduce_BigRational() {
        verifyEquals(0, 0, 1, true, new BigRational("0").divideReduce(BigRational.ONE));
        verifyEquals(1, 1, 0, true, new BigRational("4").divideReduce(BigRational.ZERO));
        verifyEquals(-1, 1, 0, true, new BigRational("-4").divideReduce(BigRational.ZERO));

        verifyEquals(1, 2, 25, true, new BigRational("4/35").divideReduce(new BigRational("10/7")));

        verifyEquals(1, 33, 35, true, new BigRational("3/5").divideReduce(new BigRational("7/11")));

        verifyEquals(1, 1, 5, true, new BigRational("-3/10").divideReduce(new BigRational("-3/2")));

        verifyEquals(-1, 2, 5, true, new BigRational("4/35").divideReduce(new BigRational("-2/7")));
        verifyEquals(-1, 2, 5, true, new BigRational("-4/35").divideReduce(new BigRational("2/7")));
    }

    @Test
    void divideReduce_BigInteger_or_long() {
        verifyEquals(0, 0, 1, true, new BigRational("0").divideReduce(BigInteger.ONE));
        verifyEquals(1, 1, 0, true, new BigRational("1").divideReduce(BigInteger.ZERO));

        verifyEquals(1, 2, 55, true, new BigRational("4/11").divideReduce(10));

        verifyEquals(1, 3, 35, true, new BigRational("3/5").divideReduce(7));

        verifyEquals(1, 1, 10, true, new BigRational("-3/10").divideReduce(BigInteger.valueOf(-3)));

        verifyEquals(-1, 2, 3, true, new BigRational("14/3").divideReduce(BigInteger.valueOf(-7)));
        verifyEquals(-1, 2, 3, true, new BigRational("-14/3").divideReduce(BigInteger.valueOf(7)));

    }

    @Test
    void divide_BigInteger() {
        verifyBigIntegerSame(BigRational::divide, BigRational::divide);
    }

    @Test
    void divide_long() {
        verifyLongSame(BigRational::divide, BigRational::divide);
    }

    @Test
    void compareTo() {
        for (BigRational a : rationals()) {
            for (BigRational b : rationals()) {
                assertEquals(a.toBigDecimal().compareTo(b.toBigDecimal()), a.compareTo(b));
            }
        }
    }

    @Test
    void compareTo_specialNumbers() {
        List<BigRational> ordered = List.of(BigRational.NEGATIVE_INFINITY, BigRational.ZERO, BigRational.ONE, BigRational.POSITIVE_INFINITY, BigRational.NaN);

        for (int i = 0; i < ordered.size(); i++) {
            BigRational a = ordered.get(i);
            for (int j = 0; j < ordered.size(); j++) {
                BigRational b = ordered.get(j);
                assertEquals(Integer.compare(i, j), a.compareTo(b));
            }
        }
    }

    @Test
    void pow_int() {
        BigRational value = new BigRational("2").power(5);
        verifyEquals(1, 32, 1, value);

        value = new BigRational("1/3").power(4);
        verifyEquals(1, 1, 81, value);
    }

    @Test
    void compareTo_when() {
        for (int an = -5; an <= 5; an++) {
            for (int ad = 1; ad <= 5; ad++) {
                BigRational a = new BigRational(String.format("%d/%d", an, ad));
                for (int bn = -5; bn <= 5; bn++) {
                    for (int bd = 1; bd <= 5; bd++) {
                        BigRational b = new BigRational(String.format("%d/%d", bn, bd));

                        int cmp = a.compareTo(b);
                        int doubleCmp = Double.compare(a.doubleValue(), b.doubleValue());

                        assertEquals(doubleCmp, cmp);
                    }
                }
            }
        }
    }

    @Test
    void isZero() {
        assertTrue(BigRational.ZERO.isZero());
        assertFalse(BigRational.ONE.isZero());
        assertFalse(BigRational.ONE.negate().isZero());
        assertFalse(BigRational.NEGATIVE_INFINITY.isZero());
        assertFalse(BigRational.POSITIVE_INFINITY.isZero());
        assertFalse(BigRational.NaN.isZero());
    }

    @Test
    void abs() {
        assertEquals(BigRational.NaN, BigRational.NaN.abs());
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.abs());
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.NEGATIVE_INFINITY.abs());

        assertSame(BigRational.ONE, BigRational.ONE.abs());
        assertEquals(1, new BigRational("-1/5").abs().signum());
    }

    @Test
    void min() {
        assertSame(BigRational.ZERO, BigRational.ONE.min(BigRational.ZERO));
        assertSame(BigRational.ZERO, BigRational.ZERO.min(BigRational.ONE));
        assertNotSame(BigRational.ONE, new BigRational("1").min(BigRational.ONE));

        assertSame(BigRational.ZERO, BigRational.min(BigRational.ONE, BigRational.ZERO));
        assertSame(BigRational.ZERO, BigRational.min(BigRational.ZERO, BigRational.ONE));

        assertSame(BigRational.ZERO, BigRational.min(BigRational.ZERO, new BigRational("0")));
        assertNotSame(BigRational.ZERO, BigRational.min(new BigRational("0"), BigRational.ZERO));
    }

    @Test
    void max() {
        assertSame(BigRational.ONE, BigRational.ONE.max(BigRational.ZERO));
        assertSame(BigRational.ONE, BigRational.ZERO.max(BigRational.ONE));
        assertNotSame(BigRational.ZERO, new BigRational("0").max(BigRational.ZERO));

        assertSame(BigRational.ONE, BigRational.max(BigRational.ONE, BigRational.ZERO));
        assertSame(BigRational.ONE, BigRational.max(BigRational.ZERO, BigRational.ONE));

        assertSame(BigRational.ZERO, BigRational.max(BigRational.ZERO, new BigRational("0")));
        assertNotSame(BigRational.ZERO, BigRational.max(new BigRational("0"), BigRational.ZERO));
    }

    @Test
    void floor() {
        assertThrows(ArithmeticException.class, BigRational.NaN::floor);
        assertThrows(ArithmeticException.class, BigRational.POSITIVE_INFINITY::floor);
        assertThrows(ArithmeticException.class, BigRational.NEGATIVE_INFINITY::floor);

        assertEquals(BigInteger.ZERO, BigRational.ZERO.floor());
        assertEquals(BigInteger.ONE, BigRational.ONE.floor());

        assertEquals(BigInteger.valueOf(5), new BigRational("5").floor());
        assertEquals(BigInteger.valueOf(3), new BigRational("6/2").floor());
        assertEquals(BigInteger.valueOf(-4), new BigRational("-4").floor());
        assertEquals(BigInteger.valueOf(-2), new BigRational("-10/5").floor());

        assertEquals(BigInteger.valueOf(10), new BigRational("21/2").floor());
        assertEquals(BigInteger.valueOf(2), new BigRational("5/2").floor());
        assertEquals(BigInteger.valueOf(3), new BigRational("11/3").floor());

        assertEquals(BigInteger.valueOf(-3), new BigRational("-5/2").floor());

        assertEquals(0, new BigRational("0.5").floor().signum());
        assertEquals(-1, new BigRational("-0.5").floor().signum());

        assertEquals(BigInteger.valueOf(-10), new BigRational("-19/2").floor());
    }

    @Test
    void ceil() {
        assertThrows(ArithmeticException.class, BigRational.NaN::ceil);
        assertThrows(ArithmeticException.class, BigRational.POSITIVE_INFINITY::ceil);
        assertThrows(ArithmeticException.class, BigRational.NEGATIVE_INFINITY::ceil);

        assertEquals(BigInteger.ZERO, BigRational.ZERO.ceil());
        assertEquals(BigInteger.ONE, BigRational.ONE.ceil());

        assertEquals(BigInteger.valueOf(5), new BigRational("5").floor());
        assertEquals(BigInteger.valueOf(3), new BigRational("6/2").floor());
        assertEquals(BigInteger.valueOf(-4), new BigRational("-4").floor());
        assertEquals(BigInteger.valueOf(-2), new BigRational("-10/5").floor());

        assertEquals(BigInteger.valueOf(11), new BigRational("21/2").ceil());
        assertEquals(BigInteger.valueOf(3), new BigRational("5/2").ceil());
        assertEquals(BigInteger.valueOf(4), new BigRational("11/3").ceil());

        assertEquals(BigInteger.valueOf(-2), new BigRational("-5/2").ceil());

        assertEquals(1, new BigRational("0.5").ceil().signum());
        assertEquals(0, new BigRational("-0.5").ceil().signum());

        assertEquals(BigInteger.valueOf(-9), new BigRational("-19/2").ceil());
    }

    @Test
    void frac() {
        assertThrows(ArithmeticException.class, BigRational.NaN::frac);
        assertThrows(ArithmeticException.class, BigRational.POSITIVE_INFINITY::frac);
        assertThrows(ArithmeticException.class, BigRational.NEGATIVE_INFINITY::frac);

        verifyEquals(0, 0, 1, true, BigRational.ONE.frac());

        verifyEquals(1, 6, 10, false, new BigRational("19.6").frac());

        verifyEquals(1, 4, 5, false, new BigRational("-21/5").frac());

        verifyEquals(1, 1, 2, false, new BigRational("25/2").frac());
        verifyEquals(1, 1, 2, true, new BigRational("25/2").simplify().frac());
    }

    @Test
    void mediant() {
        BigRational a = new BigRational("4/7");
        BigRational b = new BigRational("5/11");

        verifyEquals(1, 1, 2, true, BigRationals.mediant(a, b));
        verifyEquals(1, 1, 2, true, BigRationals.mediant(b, a));
    }

    @Test
    void equals() {
        assertEquals(BigRational.ZERO, BigRational.ZERO);
        assertEquals(BigRational.ONE, BigRational.ONE);
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY);
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY);
        assertEquals(BigRational.NaN, BigRational.NaN);

        assertNotEquals(BigRational.POSITIVE_INFINITY, BigRational.NEGATIVE_INFINITY);
        assertNotEquals(BigRational.POSITIVE_INFINITY, BigRational.NaN);
        assertNotEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY);

        assertNotEquals(null, BigRational.ONE);
        assertFalse(BigRational.ONE.equals("string")); // do not simplify assertion, code coverage for not instanceof

        assertNotEquals(BigRational.ONE, new BigRational("-4/-4"));

        assertEquals(new BigRational("9"), new BigRational("9"));
        assertEquals(new BigRational("-7"), new BigRational("-7"));

        assertEquals(new BigRational("-7/3"), new BigRational("7/-3"));

        assertNotEquals(new BigRational("5"), new BigRational("10/2"));
        assertNotEquals(new BigRational("3/6"), new BigRational("1/2"));

        assertNotEquals(new BigRational("15/7"), new BigRational("30/14"));

        assertNotEquals(new BigRational("5"), new BigRational("-5"));

        assertNotEquals(new BigRational("5/2"), new BigRational("5/3"));
        assertNotEquals(new BigRational("3/2"), new BigRational("5/2"));

        assertNotEquals(new BigRational("2/5"), new BigRational("3/4"));
        assertNotEquals(new BigRational("-2/5"), new BigRational("-22/55"));
    }

    @Test
    void intValue() {
        assertThrows(ArithmeticException.class, BigRational.NaN::intValue);
        assertThrows(ArithmeticException.class, BigRational.POSITIVE_INFINITY::intValue);
        assertThrows(ArithmeticException.class, BigRational.NEGATIVE_INFINITY::intValue);

        assertEquals(0, BigRational.ZERO.intValue());
        assertEquals(1, BigRational.ONE.intValue());
        assertEquals(-1, BigRational.ONE.negate().intValue());

        assertEquals(Integer.MAX_VALUE, BigRational.valueOf(Integer.MAX_VALUE).intValue());
        assertEquals(Integer.MIN_VALUE, BigRational.valueOf(Integer.MIN_VALUE).intValue());
    }

    @Test
    void intValueExact() {
        assertThrows(ArithmeticException.class, BigRational.NaN::intValueExact);
        assertThrows(ArithmeticException.class, BigRational.POSITIVE_INFINITY::intValueExact);
        assertThrows(ArithmeticException.class, BigRational.NEGATIVE_INFINITY::intValueExact);

        assertEquals(0, BigRational.ZERO.intValueExact());
        assertEquals(1, BigRational.ONE.intValueExact());
        assertEquals(-1, BigRational.ONE.negate().intValueExact());

        assertEquals(Integer.MAX_VALUE, BigRational.valueOf(Integer.MAX_VALUE).intValueExact());
        assertEquals(Integer.MIN_VALUE, BigRational.valueOf(Integer.MIN_VALUE).intValueExact());

        assertThrows(Exception.class, () -> BigRational.valueOf(Integer.MAX_VALUE + 1L).intValueExact());
        assertThrows(Exception.class, () -> BigRational.valueOf(Integer.MIN_VALUE - 1L).intValueExact());
    }

    @Test
    void longValue() {
        assertThrows(ArithmeticException.class, BigRational.NaN::longValue);
        assertThrows(ArithmeticException.class, BigRational.POSITIVE_INFINITY::longValue);
        assertThrows(ArithmeticException.class, BigRational.NEGATIVE_INFINITY::longValue);

        assertEquals(0L, BigRational.ZERO.longValue());
        assertEquals(1L, BigRational.ONE.longValue());
        assertEquals(-1L, BigRational.ONE.negate().longValue());

        assertEquals(Long.MAX_VALUE, BigRational.valueOf(Long.MAX_VALUE).longValue());
        assertEquals(Long.MIN_VALUE, BigRational.valueOf(Long.MIN_VALUE).longValue());
    }

    @Test
    void longValueExact() {
        assertThrows(ArithmeticException.class, BigRational.NaN::longValueExact);
        assertThrows(ArithmeticException.class, BigRational.POSITIVE_INFINITY::longValueExact);
        assertThrows(ArithmeticException.class, BigRational.NEGATIVE_INFINITY::longValueExact);

        assertEquals(0, BigRational.ZERO.longValueExact());
        assertEquals(1, BigRational.ONE.longValueExact());
        assertEquals(-1, BigRational.ONE.negate().longValueExact());

        assertEquals(Long.MAX_VALUE, BigRational.valueOf(Long.MAX_VALUE).longValueExact());
        assertEquals(Long.MIN_VALUE, BigRational.valueOf(Long.MIN_VALUE).longValueExact());

        assertThrows(Exception.class, () -> BigRational.valueOf(Long.MAX_VALUE).add(BigRational.ONE).longValueExact());
        assertThrows(Exception.class, () -> BigRational.valueOf(Long.MIN_VALUE).subtract(BigRational.ONE).longValueExact());
    }

    @Test
    void floatValue() {
        assertEquals(Float.NaN, BigRational.NaN.floatValue());
        assertEquals(Float.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.floatValue());
        assertEquals(Float.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.floatValue());

        assertEquals(0, BigRational.ZERO.floatValue());
        assertEquals(1, BigRational.ONE.floatValue());
        assertEquals(-1, BigRational.ONE.negate().floatValue());

        assertEquals(Short.MAX_VALUE, BigRational.valueOf(Short.MAX_VALUE).floatValue());
        assertEquals(Short.MIN_VALUE, BigRational.valueOf(Short.MIN_VALUE).floatValue());
    }

    @Test
    void doubleValue() {
        assertEquals(Double.NaN, BigRational.NaN.doubleValue());
        assertEquals(Double.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.doubleValue());
        assertEquals(Double.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.doubleValue());

        assertEquals(0, BigRational.ZERO.doubleValue());
        assertEquals(1, BigRational.ONE.doubleValue());
        assertEquals(-1, BigRational.ONE.negate().doubleValue());

        assertEquals(Short.MAX_VALUE, BigRational.valueOf(Short.MAX_VALUE).doubleValue());
        assertEquals(Short.MIN_VALUE, BigRational.valueOf(Short.MIN_VALUE).doubleValue());
    }

    @Test
    void valueOf_BigDecimal() {
        assertEquals(new BigDecimal("0"), BigRational.valueOf(new BigDecimal("0")).toBigDecimal());
        assertEquals(new BigDecimal("1"), BigRational.valueOf(new BigDecimal("1")).toBigDecimal());
        assertEquals(new BigDecimal("-1"), BigRational.valueOf(new BigDecimal("-1")).toBigDecimal());

        assertEquals(new BigDecimal("123"), BigRational.valueOf(new BigDecimal("123")).toBigDecimal());
        assertEquals(new BigDecimal("-123"), BigRational.valueOf(new BigDecimal("-123")).toBigDecimal());

        assertEquals(new BigDecimal("5.5"), BigRational.valueOf(5.5).toBigDecimal());

        assertEquals(new BigDecimal("1234567890123456789"), BigRational.valueOf(new BigDecimal("1234567890123456789")).toBigDecimal());
        assertEquals(new BigDecimal("-0.1234567890123456789"), BigRational.valueOf(new BigDecimal("-0.1234567890123456789")).toBigDecimal());

        assertEquals(new BigDecimal("12300000"), BigRational.valueOf(new BigDecimal("123").scaleByPowerOfTen(5)).toBigDecimal());
    }

    @Test
    void power() {
        assertEquals(BigRational.ZERO.power(0), BigRational.NaN);

        verifyEquals(0, 0, 1, true, BigRational.ZERO.power(1));
        verifyEquals(1, 1, 1, true, BigRational.ONE.power(0));
        verifyEquals(1, 1, 1, true, BigRational.ONE.power(1));
        verifyEquals(-1, 1, 1, true, new BigRational("-1").power(1));

        verifyEquals(1, 1, 1, true, new BigRational("2/5").power(0));
        verifyEquals(1, 1, 1, true, new BigRational("-7/4").power(0));

        verifyEquals(1, 2, 5, false, new BigRational("2/5").power(1));
        verifyEquals(-1, 2, 5, false, new BigRational("-2/5").power(1));

        verifyEquals(1, 9, 49, false, new BigRational("-3/7").power(2));
        verifyEquals(1, 9, 49, true, new BigRational("-3/7").simplify().power(2));

        verifyEquals(-1, 27, 8, false, new BigRational("-3/2").power(3));

        verifyEquals(1, 2, 3, false, new BigRational("3/2").power(-1));
        verifyEquals(1, 4, 9, false, new BigRational("-3/2").power(-2));

        BigRational reducable = new BigRational("2/4");
        reducable.reduce();
        verifyEquals(1, 1, 4, true, reducable.power(2));

    }

    @Test
    void subtractReduce() {
        assertEquals(BigRational.NaN, BigRational.ZERO.subtractReduce(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.ONE.subtractReduce(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.subtractReduce(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.subtractReduce(BigRational.NaN));

        assertEquals(BigRational.NaN, BigRational.NaN.subtractReduce(BigRational.ZERO));
        assertEquals(BigRational.NaN, BigRational.NaN.subtractReduce(BigRational.ONE));
        assertEquals(BigRational.NaN, BigRational.NaN.subtractReduce(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NaN.subtractReduce(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.subtractReduce(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.subtractReduce(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.subtractReduce(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.subtractReduce(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.subtractReduce(BigRational.ZERO));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.subtractReduce(BigRational.ONE));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ZERO.subtractReduce(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ONE.subtractReduce(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.subtractReduce(BigRational.ZERO));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.subtractReduce(BigRational.ONE));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ZERO.subtractReduce(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ONE.subtractReduce(BigRational.NEGATIVE_INFINITY));

        // use of Zero
        verifyEquals(0, 0, 1, true, BigRational.ZERO.subtractReduce(BigRational.ZERO));
        verifyEquals(1, 1, 2, true, new BigRational("1/2").subtractReduce(BigRational.ZERO));
        verifyEquals(-1, 5, 2, true, BigRational.ZERO.subtractReduce(new BigRational("5/2")));

        // use of Integers
        verifyEquals(1, 9, 1, true, new BigRational("5").subtractReduce(new BigRational("-4")));
        verifyEquals(-1, 3, 1, true, new BigRational("-1").subtractReduce(new BigRational("2")));
        verifyEquals(1, 2, 1, true, new BigRational("3").subtractReduce(new BigRational("1")));
        verifyEquals(-1, 3, 1, true, new BigRational("1").subtractReduce(new BigRational("4")));
        verifyEquals(-1, 2, 1, true, new BigRational("-3").subtractReduce(new BigRational("-1")));
        verifyEquals(1, 3, 1, true, new BigRational("-1").subtractReduce(new BigRational("-4")));

        verifyEquals(1, 1, 6, true,
                new BigRational("1/2").subtractReduce(new BigRational("1/3")));

        verifyEquals(0, 0, 1, true,
                new BigRational("3/6").subtractReduce(new BigRational("5/10")));

        verifyEquals(0, 0, 1, true,
                new BigRational("-3/6").subtractReduce(new BigRational("-5/10")));

        verifyEquals(1, 1, 1, true,
                new BigRational("3/6").subtractReduce(new BigRational("-5/10")));

        verifyEquals(-1, 1, 1, true,
                new BigRational("-3/6").subtractReduce(new BigRational("5/10")));
    }

    @Test
    void addReduce() {

        assertEquals(BigRational.NaN, BigRational.ZERO.addReduce(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.ONE.addReduce(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.addReduce(BigRational.NaN));
        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.addReduce(BigRational.NaN));

        assertEquals(BigRational.NaN, BigRational.NaN.addReduce(BigRational.ZERO));
        assertEquals(BigRational.NaN, BigRational.NaN.addReduce(BigRational.ONE));
        assertEquals(BigRational.NaN, BigRational.NaN.addReduce(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NaN.addReduce(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NaN, BigRational.POSITIVE_INFINITY.addReduce(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NaN, BigRational.NEGATIVE_INFINITY.addReduce(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.addReduce(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.addReduce(BigRational.NEGATIVE_INFINITY));

        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.addReduce(BigRational.ZERO));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.POSITIVE_INFINITY.addReduce(BigRational.ONE));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ZERO.addReduce(BigRational.POSITIVE_INFINITY));
        assertEquals(BigRational.POSITIVE_INFINITY, BigRational.ONE.addReduce(BigRational.POSITIVE_INFINITY));

        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.addReduce(BigRational.ZERO));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.NEGATIVE_INFINITY.addReduce(BigRational.ONE));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ZERO.addReduce(BigRational.NEGATIVE_INFINITY));
        assertEquals(BigRational.NEGATIVE_INFINITY, BigRational.ONE.addReduce(BigRational.NEGATIVE_INFINITY));

        // use of ZERO
        verifyEquals(0, 0, 1, true, BigRational.ZERO.addReduce(BigRational.ZERO));
        verifyEquals(1, 1, 2, true, new BigRational("1/2").addReduce(BigRational.ZERO));
        verifyEquals(-1, 1, 2, true, new BigRational("-1/2").addReduce(BigRational.ZERO));
        verifyEquals(1, 5, 2, true, BigRational.ZERO.addReduce(new BigRational("5/2")));
        verifyEquals(-1, 5, 2, true, BigRational.ZERO.addReduce(new BigRational("-5/2")));

        // use of Integers
        verifyEquals(1, 9, 1, true, new BigRational("5").addReduce(new BigRational("4")));
        verifyEquals(-1, 3, 1, true, new BigRational("-1").addReduce(new BigRational("-2")));
        verifyEquals(1, 2, 1, true, new BigRational("3").addReduce(new BigRational("-1")));
        verifyEquals(-1, 3, 1, true, new BigRational("1").addReduce(new BigRational("-4")));
        verifyEquals(-1, 2, 1, true, new BigRational("-3").addReduce(new BigRational("1")));
        verifyEquals(1, 3, 1, true, new BigRational("-1").addReduce(new BigRational("4")));

        // reduced
        verifyEquals(1, 1, 6, true,
                new BigRational("1/2").addReduce(new BigRational("-1/3")));

        verifyEquals(1, 1, 1, true,
                new BigRational("3/6").addReduce(new BigRational("5/10")));

        verifyEquals(-1, 1, 1, true,
                new BigRational("-3/6").addReduce(new BigRational("-5/10")));

        verifyEquals(0, 0, 1, true,
                new BigRational("3/6").addReduce(new BigRational("-5/10")));

        verifyEquals(0, 0, 1, true,
                new BigRational("-3/6").addReduce(new BigRational("5/10")));
    }

    @Test
    void BigRational_toString() {
        assertEquals("0", BigRational.ZERO.toString());
        assertEquals("1", BigRational.ONE.toString());

        assertEquals("Infinity", BigRational.POSITIVE_INFINITY.toString());
        assertEquals("-Infinity", BigRational.NEGATIVE_INFINITY.toString());
        assertEquals("NaN", BigRational.NaN.toString());

        assertEquals("-1", new BigRational(-1).toString());
        assertEquals("1/2", new BigRational("1/2").toString());
        assertEquals("-3/5", new BigRational("-3/5").toString());
        assertEquals("-3/5", new BigRational("3/-5").toString());
        assertEquals("25/10", new BigRational("2.5").toString());
    }

    @Test
    void of_BigInteger_BigInteger() {
        verifyEquals(1, 5, 2, false, BigRational.of(BigInteger.valueOf(5), BigInteger.valueOf(2)));
        verifyEquals(1, 1, 1, true, BigRational.of(BigInteger.ONE, BigInteger.ONE));
        verifyEquals(0, 0, 1, true, BigRational.of(BigInteger.ZERO, BigInteger.ONE));
        verifyEquals(1, 1, 0, true, BigRational.of(new BigInteger("123"), BigInteger.ZERO));
        verifyEquals(-1, 1, 0, true, BigRational.of(new BigInteger("-456"), BigInteger.ZERO));
        verifyEquals(0, 0, 0, true, BigRational.of(BigInteger.ZERO, BigInteger.ZERO));

        verifyEquals(0, 0, 1, true, BigRational.of(BigInteger.ZERO, BigInteger.TEN));

        verifyEquals(1, 1, 2, true, BigRational.of(BigInteger.ONE, BigInteger.TWO));
        verifyEquals(1, 2, 1, true, BigRational.of(BigInteger.TWO, BigInteger.ONE));

    }

    @Test
    void reduce_BigInteger_BigInteger() {
        verifyEquals(1, 1, 1, true, BigRationals.reduce(BigInteger.ONE, BigInteger.ONE));
        verifyEquals(0, 0, 1, true, BigRationals.reduce(BigInteger.ZERO, BigInteger.ONE));
        verifyEquals(1, 1, 0, true, BigRationals.reduce(new BigInteger("123"), BigInteger.ZERO));
        verifyEquals(-1, 1, 0, true, BigRationals.reduce(new BigInteger("-456"), BigInteger.ZERO));
        verifyEquals(0, 0, 0, true, BigRationals.reduce(BigInteger.ZERO, BigInteger.ZERO));

        verifyEquals(0, 0, 1, true, BigRationals.reduce(BigInteger.ZERO, BigInteger.TEN));

        verifyEquals(1, 1, 2, true, BigRationals.reduce(BigInteger.ONE, BigInteger.TWO));

    }

    @Test
    void isInteger() {
        assertTrue(BigRational.ONE.isInteger());
        assertTrue(BigRational.ZERO.isInteger());
        assertTrue(BigRational.ONE.negate().isInteger());
        assertTrue(new BigRational("12345").isInteger());
        assertTrue(new BigRational("4/2").isInteger());
        assertTrue(new BigRational("-6/3").isInteger());

        assertFalse(BigRational.POSITIVE_INFINITY.isInteger());
        assertFalse(BigRational.NEGATIVE_INFINITY.isInteger());
        assertFalse(BigRational.NaN.isInteger());

        assertFalse(new BigRational("5/2").isInteger());
        assertFalse(new BigRational("-4.7").isInteger());
    }

    @Test
    void hashCode_map() {
        Set<BigRational> set = new HashSet<>();
        set.addAll(rationals());
        int size = set.size();

        set.addAll(rationals());
        assertEquals(size, set.size());

        for (BigRational r : rationals()) {
            assertTrue(set.contains(r));
        }
    }

    String report(BigRational expected, BigRational actual) {
        return "Expected: " + expected + ", Actual: " + actual;
    }


    /**
     * assert integerFun(BigRational, BigInteger) == rationalFun(BigRational, new BigRational(BigInteger));
     */
    void verifyBigIntegerSame(BiFunction<BigRational, BigRational, BigRational> rationalFun, BiFunction<BigRational, BigInteger, BigRational> integerFun) {
        for (BigRational r : rationals()) {
            for (long c : longs) {
                BigInteger ci = BigInteger.valueOf(c);

                BigRational expected = rationalFun.apply(r, new BigRational(ci));
                BigRational actual = integerFun.apply(r, ci);

                assertEquals(0, expected.compareTo(actual), () -> report(expected, actual));
                assertEquals(0, actual.compareTo(expected));
            }
        }
    }

    /**
     * assert integerFun(BigRational, long) == ¬rationalFun(BigRational, new BigRational(long));
     */
    void verifyLongSame(BiFunction<BigRational, BigRational, BigRational> rationalFun, BiFunction<BigRational, Long, BigRational> integerFun) {
        for (BigRational r : rationals()) {
            for (long c : longs) {
                BigRational expected = rationalFun.apply(r, new BigRational(c));
                BigRational value = integerFun.apply(r, c);

                assertEquals(0, expected.compareTo(value));
                assertEquals(0, value.compareTo(expected));
            }
        }
    }

    static void verifyEquals(BigRational expected, BigRational value) {
        assertEquals(expected, value);
    }

    static void verifyEquals(int signum, long num, long den, BigRational value) {
        assertEquals(signum, value.signum());
        assertEquals(num, value.getNumerator().longValueExact());
        assertEquals(den, value.getDenominator().longValueExact());
    }

    static void verifyEquals(int signum, long num, long den, boolean simplestForm, BigRational value) {
        verifyEquals(signum, num, den, value);
        assertEquals(simplestForm, value.isSimplestForm());
    }

    static List<BigRational> rationals;

    static List<BigRational> rationals() {
        if (rationals != null) {
            return rationals;
        }
        List<BigRational> list = new ArrayList<>();
        for (long a : longs) {
            BigInteger ai = BigInteger.valueOf(a);
            for (long b : longs) {
                if (b == 0) {
                    continue;
                }
                BigInteger bi = BigInteger.valueOf(b);
                BigRational r = BigRationals.reduce(ai, bi);
                list.add(r);
            }
        }
        rationals = list;
        return list;
    }
}
