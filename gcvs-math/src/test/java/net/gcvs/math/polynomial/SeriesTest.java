package net.gcvs.math.polynomial;

import net.gcvs.math.BigRational;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SeriesTest {

    @Test
    void truncate_terminateInfiniteSeries() {

        TermGenerator<BigRational> tg = BigRational::valueOf;

        List<Term<BigRational>> terms = new ArrayList<>();
        final TruncatedSeries<BigRational> truncated = tg.truncate(2);
        truncated.forEach(terms::add);

        assertSame(tg, truncated.getSeries());
        assertEquals(2, truncated.getMaxDegree());

        assertEquals(3, terms.size());
        assertEquals(0, terms.get(0).degree());
        assertEquals(1, terms.get(1).degree());
        assertEquals(2, terms.get(2).degree());

        assertEquals(BigRational.valueOf(0), terms.get(0).coefficient());
        assertEquals(BigRational.valueOf(1), terms.get(1).coefficient());
        assertEquals(BigRational.valueOf(2), terms.get(2).coefficient());
    }

    @Test
    void truncate_whenTruncatedSeriesIsLonger_thenTruncateRepackages() {

        TermGenerator<BigRational> tg = BigRational::valueOf;

        final TruncatedSeries<BigRational> truncated = tg.truncate(5);
        final TruncatedSeries<BigRational> truncatedSmaller = truncated.truncate(2);

        assertNotSame(truncated, truncatedSmaller);
        assertSame(truncated.getSeries(), truncatedSmaller.getSeries());
        assertEquals(2, truncatedSmaller.getMaxDegree());
    }


    @Test
    void truncate_whenTruncatedSeriesIsSmaller_thenTruncateHasNoEffect() {

        TermGenerator<BigRational> tg = BigRational::valueOf;

        final Series<BigRational> truncated = tg.truncate(2);
        final Series<BigRational> truncatedSame = truncated.truncate(5);
        final Series<BigRational> truncatedLonger = truncated.truncate(5);

        assertSame(truncated, truncatedSame);
        assertSame(truncated, truncatedLonger);
    }

}