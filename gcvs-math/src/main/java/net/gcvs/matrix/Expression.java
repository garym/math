package net.gcvs.matrix;

import java.util.Arrays;

public class Expression {
	protected Terms terms;
	final int[] factors;

	private final int cardinality;

	private final int[] term;
	private final int[] termFactors;

	public Expression(final Terms terms, final int[] factors) {
		this.terms = terms;
		this.factors = factors.clone();

		int cardinality = 0;
		for (int factor : factors) {
			if (factor != 0) {
				cardinality++;
			}
		}
		this.cardinality = cardinality;

		term = new int[cardinality];
		termFactors = new int[cardinality];
		int idx = 0;
		for (int i = 0, len = factors.length; i < len; i++) {
			final int factor = factors[i];
			if (factor != 0) {
				term[idx] = i;
				termFactors[idx] = factor;
				idx++;
			}
		}
	}

	public static String toString(final int[] termFactors, final Object... terms) {
		final StringBuilder sb = new StringBuilder();
		String sep = "";
		for (int i = 0, len = termFactors.length; i < len; i++) {
			final int factor = termFactors[i];
			if (factor >= 1) {
				sb.append(sep);
				if (factor > 1) {
					sb.append(factor);
				}
			} else {
				sb.append(" - ");
				if (factor < -1) {
					sb.append(-factor);
				}
			}
			sb.append(terms[i]);
			sep = " + ";
		}
		return sb.toString();
	}

	public String toString() {
		final StringBuilder sb = new StringBuilder();
		String sep = "";
		for (int i = 0, len = cardinality; i < len; i++) {
			final int t = term[i];
			final int factor = termFactors[i];
			if (factor >= 1) {
				sb.append(sep);
				if (factor > 1) {
					sb.append(factor);
				}
			} else {
				sb.append(" - ");
				if (factor < -1) {
					sb.append(-factor);
				}
			}
			sb.append(this.terms.name(t));
			sep = " + ";
		}
		return sb.toString();
	}

	public final int length() {
		return this.cardinality;
	}

	public final int factor(final int term) {
		return factors[term];
	}

	public final int term(final int idx) {
		return term[idx];
	}

	public final int termFactor(final int idx) {
		return termFactors[idx];
	}

	public boolean containsTerm(final int idx) {
		return factors[idx] != 0;
	}

	public Expression add(final int factor, final Expression e) {
		final int[] factors = this.factors.clone();
		for (int i = 0, len = factors.length; i < len; i++) {
			factors[i] += factor * e.factors[i];
		}
		return new Expression(terms, factors);
	}

	public Expression simplify() {
		return new Expression(terms, factors);
	}

	public void addTo(final int factor, final int[] sum) {
		for (int i = 0, len = term.length; i < len; i++) {
			final int t = term[i];
			sum[t] += factor * this.termFactors[i];
		}
	}

	public boolean matches(final int[] factors) {
		return Arrays.equals(this.factors, factors);
	}
}