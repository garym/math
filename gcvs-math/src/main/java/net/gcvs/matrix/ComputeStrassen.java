package net.gcvs.matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import net.gcvs.matrix.storage.Store;

public class ComputeStrassen {

    public static boolean equals(final int[] a, final int[] b) {
        if (a == b) {
            return true;
        }
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0, len = a.length; i < len; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    public static class LC {
        final int[] list;

        public LC(final int[] list) {
            this.list = list;
        }

        public int hashCode() {
            return Arrays.hashCode(list);
        }

        public boolean equals(final Object o) {
            if (o instanceof LC lc) {
                return Arrays.equals(lc.list, list);
            }
            return false;
        }

    }

    static long fc;
    static long fc0;
    static long fc1;
    static long fcseq;

    public static int[] findCombination(final Expression target, final Expression... list) {
        fc++;
        // assert list must contain all of target's terms
        TERM:
        for (int x = 0, xlen = target.length(); x < xlen; x++) {
            final int t = target.term(x);
            E:
            for (final Expression e : list) {
                for (int y = 0, ylen = e.length(); y < ylen; y++) {
                    final int et = e.term(y);
                    if (et > t) { // we can do this because terms are in ascending order
                        continue E;
                    }
                    if (et == t) {
                        continue TERM;
                    }
                }
            }
            fc0++;
            return null;
        }
        // assert terms in list, not in target, must appear at least twice in list so they can be cancelled out in the linear
        // combination
        EXPRESSION:
        for (final Expression e : list) {
            for (int y = 0, ylen = e.length(); y < ylen; y++) {
                final int et = e.term(y);
                if (target.containsTerm(et)) {
                    continue;
                }
                // et not in target, look for et in expression list
                for (final Expression e2 : list) {
                    if (e == e2) {
                        continue;
                    }
                    if (e2.containsTerm(et)) {
                        continue EXPRESSION;
                    }
                }
                fc1++;
                return null;
            }
        }

        fcseq++;
        final int listLength = list.length;
        final Sequence sequence = new Sequence(-1, 1, list.length);
        final int[] seq = sequence.create();
        SEQ:
        do {
            for (int x = 0, xlen = target.length(); x < xlen; x++) {
                final int t = target.term(x);
                final int tfactor = target.termFactor(x);
                int sum = 0;
                for (int i = 0; i < list.length; i++) {
                    final int factor = seq[i];
                    if (factor == 0) {
                        // combination must not ignore one of the list Expressions
                        continue SEQ;
                    }
                    final Expression e = list[i];
                    sum += e.factor(t) * factor;
                }
                if (sum != tfactor) {
                    continue SEQ;
                }
            }

            // all other terms must have factor zero
            for (final Expression e : list) {
                for (int y = 0, ylen = e.length(); y < ylen; y++) {
                    final int t = e.term(y);
                    if (target.containsTerm(t)) {
                        continue;
                    }

                    int sum = 0;
                    for (int i = 0; i < list.length; i++) {
                        final int factor = seq[i];
                        if (factor == 0) {
                            continue;
                        }
                        final Expression e2 = list[i];
                        sum += e2.factor(t) * factor;
                    }
                    if (sum != 0) {
                        continue SEQ;
                    }
                }
            }
            return seq;

        } while (sequence.next(seq));
        return null;
    }

    public static int[] findCombination(final int[] factors, final Sequence sequence, final int[]... list) {
        // assert sequence.length == list.length
        final int flen = factors.length;
        final int lilen = list.length;
        if (fc % 10000000 == 0) {
//			System.out.println("fc: " + fc + " fc0: " + fc0 + ", fc1: " + fc1 + ", fcseq: " + fcseq);
        }
        fc++;
        FACTOR:
        for (int i = 0; i < flen; i++) {
            if (factors[i] != 0) {
                for (int j = 0; j < lilen; j++) {
                    if (list[j][i] != 0) {
                        continue FACTOR;
                    }
                }
                fc0++;
                return null;
            }
            int c = 0;
            for (int j = 0; j < lilen; j++) {
                if (list[j][i] != 0) {
                    c++;
                }
            }
            if (c == 1) {
                fc1++;
                return null;
            }
        }

        fcseq++;
        final int[] seq = sequence.create();
        SEQ:
        do {
            for (int i = 0; i < flen; i++) {
                int sum = 0;
                for (int m = 0; m < lilen; m++) {
                    final int mul = seq[m];
                    if (mul == 0) {
                        continue SEQ;
                    }
                    sum += list[m][i] * mul;
                }
                if (sum != factors[i]) {
                    continue SEQ;
                }
            }
            return seq;

        } while (sequence.next(seq));

        return null;
    }

    void cross() {
        System.out.println("crossing");
        for (final Product p : products) {

//			for (final Product q : products) {
//				(a+b)(c+d) +4
//				(a+b)(c-d) +2-2
//				(a-b)(c-d) +2-2
//				a(c+d) +2
//				a(c-d) -2
//				a(c+d+e) +3

//			}
        }
    }

    /**
     *
     */
    public void target(final Expression e, final int n) {
        System.out.println("Expression: " + e);
        final int t = e.term(0);

        System.out.println("term: " + terms.name(t));
        final BitSet t0 = products.withTerm(t);
        for (int i = t0.nextSetBit(0); i != -1; i = t0.nextSetBit(i + 1)) {
            final Product p = products.get(i);
            // System.out.println("term " + p);
        }
        System.out.println("found " + t0.cardinality() + " products containing term " + terms.name(t));
        int[] sizes = new int[10];
        for (Product p : products) {
            sizes[p.length()]++;
        }
        // sizes: 1, 2, 3, 4, 6, 9
//		System.out.println(Arrays.toString(sizes));

        BitSet eligibleNines = new BitSet();
        eligibleNines.or(products.withCardinality(9));
        for (int i = 0; i < e.length(); i++) {
            eligibleNines.and(products.withTerm(e.term(i)));
        }

        System.out.println("eligible nines " + eligibleNines.cardinality());
        for (int i = eligibleNines.nextSetBit(0); i != -1; i = eligibleNines.nextSetBit(i + 1)) {
            Product p = products.get(i);
//			System.out.println(p);

            Expression six = p.add(-p.factor(t), e);
            if (six.length() == 6) {
                System.out.println(p + ", " + six);

                BitSet set = products.all();
                for (int j = 0; j < six.length(); j++) {
                    set.and(products.withTerm(six.term(j)));
                }
                set.and(products.withCardinality(6));
                System.out.println("set len = " + set.length());

            }

        }

    }

    long start;
    final AtomicLong target2Calls = new AtomicLong();

    public int[] target2(final String prefix, final Expression target) {
        if (start == 0) {
            start = System.currentTimeMillis();
        }

        target2Calls.incrementAndGet();

        if (target2Calls.get() % 1000 == 0) {
            System.out.println("calls: " + target2Calls + ", millis per call: " + ((double) (System.currentTimeMillis() - start) / target2Calls.get()));
        }

        try {
            // iterate all possible target term partitioning between the two expressions
            final Sequence sequence = new Sequence(0, 1, target.length());
            final int[] seq = sequence.create();
            // without loss of generality, last term is always on 2nd expression
            seq[seq.length - 1] = 1;
            do {
                BitSet left = products.all();
                BitSet right = products.all();
//			left.andNot(products.withCardinality(9));
//			right.andNot(products.withCardinality(9));
                for (int i = 0; i < seq.length; i++) {
                    (seq[i] == 0 ? left : right).and(products.withTerm(target.term(i)));
                }

//			System.out.println("target2: " + Arrays.toString(seq) + ", " + left.cardinality() + " * " + right.cardinality());

                for (int i = left.nextSetBit(0); i != -1; i = left.nextSetBit(i + 1)) {
                    final Product p = products.get(i);
                    for (int j = right.nextSetBit(0); j != -1; j = right.nextSetBit(j + 1)) {
                        final Product q = products.get(j);
                        int[] comb = findCombination(target, p, q);
                        if (comb != null) {
//							System.out.println(prefix + ", " + p + ", " + q + " = " + target);
                            return new int[]{i, j};
                        }
                    }
                }

            } while (sequence.next(seq));
        } finally {
//			target2Millis.addAndGet(System.currentTimeMillis() - start);
        }
        return null;
    }

    void findThrees(final Expression target) {
        System.out.println("findThrees from " + products.size() + " products");
        products.parallelStream().forEach(a -> {
            final Expression b = target.add(-1, a);
            // System.out.println(b);
            int[] comb = target2(a.toString(), b);
            if (comb != null) {
                final Product p = products.get(comb[0]);
                final Product q = products.get(comb[1]);
                int[] combs = findCombination(target, a, p, q);

                System.out.println(Expression.toString(combs, a, p, q) + " = " + target);
            }
        });
    }

    final Store store;

    // list of valid M(i, j) * N(k, l) combinations
    final Terms mterms;
    final Terms nterms;
    final Terms terms;
    final Expression[] targets;
    final Products products;

    public ComputeStrassen(final int a, final int k, final int b) {
        this.store = new Store(a, k, b);

        this.mterms = store.m.terms();
        this.nterms = store.n.terms();

        System.out.println(store.m);
        System.out.println(store.n);
        System.out.println(store.m.mul(store.n));

        this.terms = store.terms;
        System.out.println(terms);

        this.targets = store.targets;

        for (int i = 0; i < targets.length; i++) {
            System.out.println("target " + i + " = " + targets[i]);
        }

        this.products = createProducts(store.m, store.n);
        System.out.println("found " + products.size() + " products");
    }

    static int[] readIntArray(final BufferedReader in) throws IOException {
        String line = in.readLine();
        String[] values = line.split("[, ]+");
        int[] array = new int[values.length];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(values[i]);
        }
        return array;
    }

    static String[] readStringArray(final BufferedReader in) throws IOException {
        String line = in.readLine();
        String[] values = line.split("[, ]+");
        return values;
    }

    static void writeIntArray(final Writer out, int... ints) throws IOException {
        String sep = "";
        for (int i : ints) {
            out.write(sep);
            out.write(Integer.toString(i));
            sep = ",";

        }
        out.write("\n");
    }

    static void writeStringArray(final Writer out, String... ints) throws IOException {
        String sep = "";
        for (String i : ints) {
            out.write(sep);
            out.write(i);
            sep = ",";

        }
        out.write("\n");
    }

    static Mat readMatrix(final BufferedReader in) throws IOException {
        int[] dims = readIntArray(in);
        String[][] values = new String[dims[0]][];
        for (int i = 0; i < values.length; i++) {
            values[i] = readStringArray(in);
        }
        return new Mat(values);
    }

    static void writeMatrix(final Writer out, final Mat mat) throws IOException {
        writeIntArray(out, mat.w(), mat.h());
        for (int i = 0; i < mat.h(); i++) {
            writeStringArray(out, mat.ids[i]);
        }
    }

    static void writeExpression(final Writer out, final Expression e) throws IOException {
        List<String> line = new ArrayList<>();
        for (int i = 0; i < e.factors.length; i++) {
            if (e.factors[i] != 0) {
                line.add(Integer.toString(e.factors[i]));
                line.add(e.terms.name(i));
            }
        }
        writeStringArray(out, line.toArray(new String[line.size()]));
    }

    static void writeProduct(final Writer out, final Product p) throws IOException {
        writeExpression(out, p.a);
        writeExpression(out, p.b);

    }

    Expression readExpression(final BufferedReader in, final Terms termNames) throws IOException {
        String[] line = readStringArray(in);
        int[] terms = termNames.newInstance();
        for (int i = 0; i < line.length; i += 2) {
            int factor = Integer.parseInt(line[i]);
            String term = line[i + 1];
            int termIdx = termNames.findTerm(term);
            terms[termIdx] = factor;

        }
        return new Expression(this.terms, terms);
    }

    Product readProduct(final BufferedReader in) throws IOException {
        Expression a = readExpression(in, mterms);
        Expression b = readExpression(in, nterms);

        return Product.multiply(this.terms, a, b);
    }

    public void compute() {
        BigInteger bi = BigInteger.valueOf(products.size());
        BigInteger bip = BigInteger.ONE;
        for (int i = 1; i <= 7; i++) {
            bip = bip.multiply(bi);
            bi = bi.subtract(BigInteger.ONE);
            bip = bip.divide(BigInteger.valueOf(i));
        }
        System.out.println("combinations: " + bip);

        for (int i = 0; i < targets.length; i++) {
            System.out.println("target" + i + ": " + targets[i]);
        }

        long start = System.currentTimeMillis();
        BitSet useful = new BitSet(products.size());

        Combinations[] combs = new Combinations[targets.length];
        Arrays.setAll(combs, i -> new Combinations(targets[i], products.size()));

        boolean loud = false;
        int[] counts = new int[4];
        int cidx = 0;

        final Sequence sequence2 = new Sequence(-1, 1, 2);
        final Sequence sequence3 = new Sequence(-1, 1, 3);
        final Sequence sequence4 = new Sequence(-1, 1, 4);
        final int[][] abs = new int[2][];
        final int[][] abcs = new int[3][];
        final int[][] abcds = new int[4][];

        for (final Combinations comb : combs) {
            System.out.println("calculating linear combination of products to match target " + cidx + ": " + comb.target);
            int sum2 = 0, sum3 = 0, sum4 = 0;
            final Expression e = comb.target;
            for (int i = 0; i < products.size(); i++) {
                final Product a = products.get(i);
                abs[0] = abcs[0] = abcds[0] = a.factors;
                for (int j = i + 1; j < products.size(); j++) {
                    final Product b = products.get(j);
                    abs[1] = abcs[1] = abcds[1] = b.factors;
                    // int[] ab = findCombination(e.factors, sequence2, abs);
                    int[] ab = findCombination(e, a, b);
                    if (ab != null) {
                        counts[cidx]++;
                        if (loud)
                            System.out.println(sign("", ab[0]) + "" + a + ""
                                    + sign(" + ", ab[1]) + "" + b + ""
                                    + " = " + e);
                        useful.set(i);
                        useful.set(j);
                        comb.add(ab, i, j);
                        sum2++;

                    } else {
                        for (int k = j + 1; k < products.size(); k++) {
                            final Product c = products.get(k);
                            abcs[2] = abcds[2] = c.factors;
//							int[] abc = findCombination(e.factors, sequence3, abcs);
                            int[] abc = findCombination(e, a, b, c);
                            if (abc != null) {
                                counts[cidx]++;
                                if (loud)
                                    System.out.println(sign("", abc[0]) + "" + a + ""
                                            + sign(" + ", abc[1]) + "" + b + ""
                                            + sign(" + ", abc[2]) + "" + c + ""
                                            + " = " + e);
                                useful.set(i);
                                useful.set(j);
                                useful.set(k);
                                comb.add(abc, i, j, k);
                                sum3++;

                            } else {
                                for (int l = k + 1; l < products.size(); l++) {
                                    Product d = products.get(l);
                                    abcds[3] = d.factors;
//									int[] abcd = findCombination(e.factors, sequence4, abcds);
                                    int[] abcd = findCombination(e, a, b, c, d);
                                    if (abcd != null) {
                                        counts[cidx]++;
                                        if (loud)
                                            System.out.println(sign("", abcd[0]) + "" + a + ""
                                                    + sign(" + ", abcd[1]) + "" + b + ""
                                                    + sign(" + ", abcd[2]) + "" + c + ""
                                                    + sign(" + ", abcd[3]) + "" + d + ""
                                                    + " = " + e);
                                        useful.set(i);
                                        useful.set(j);
                                        useful.set(k);
                                        useful.set(l);
                                        comb.add(abcd, i, j, k, l);
                                        sum4++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            System.out.println("found " + sum2 + " sum of 2, " + sum3 + " sum of 3, " + sum4 + " sum of 4 products");

            cidx++;
        }

        Iterator<Product> it = products.iterator();
        int idx1 = 0;
        while (it.hasNext()) {
            Product prod = it.next();

            if (!useful.get(idx1)) {
                System.out.println(prod + " not useful");
                it.remove();
            }
            idx1++;
        }
        System.out.println("" + products.size() + " useful terms");

        System.out.println("counts: " + Arrays.toString(counts));

        long end = System.currentTimeMillis();

        System.out.println("took " + (end - start) + " millis");

        final Combinations c0 = combs[0];
        final Combinations c1 = combs[1];
        final Combinations c2 = combs[2];
        final Combinations c3 = combs[3];

        int solution = 0;

        final int maxProducts = 7;

        final BitSet mp01 = new BitSet();
        final BitSet mp012 = new BitSet();
        final BitSet mp0123 = new BitSet();
        System.out.println("c0.size = " + c0.size);
        for (int m0 = 0; m0 < c0.size; m0++) {
            if (m0 % 100 == 0)
                System.out.println("c0: " + m0);
            final BitSet mp0 = c0.methodProducts.get(m0);
            M1:
            for (int m1 = 0; m1 < c1.size; m1++) {
                final BitSet mp1 = c1.methodProducts.get(m1);
                mp01.clear();
                mp01.or(mp0);
                mp01.or(mp1);
                if (mp01.cardinality() > maxProducts) {
                    continue M1;
                }

                M2:
                for (int m2 = 0; m2 < c2.size; m2++) {
                    final BitSet mp2 = c2.methodProducts.get(m2);
                    mp012.clear();
                    mp012.or(mp01);
                    mp012.or(mp2);
                    if (mp012.cardinality() > maxProducts) {
                        continue M2;
                    }

                    M3:
                    for (int m3 = 0; m3 < c3.size; m3++) {
                        final BitSet mp3 = c3.methodProducts.get(m3);
                        mp0123.clear();
                        mp0123.or(mp012);
                        mp0123.or(mp3);
                        if (mp0123.cardinality() > maxProducts) {
                            continue M3;
                        }
                        // found

//						System.out.println("found: " + display(products, c0.methods.get(m0))
//								+ ", " + display(products, c1.methods.get(m1))
//								+ ", " + display(products, c2.methods.get(m2))
//								+ ", " + display(products, c3.methods.get(m3)));

                        int sum = c0.methods.get(m0).length
                                + c1.methods.get(m1).length
                                + c2.methods.get(m2).length
                                + c3.methods.get(m3).length;
                        if (sum <= 12) {
                            System.out.println("found[" + mp0123.cardinality() + "], sum: " + sum
                                    + ", " + c0.methods.get(m0).length
                                    + ", " + c1.methods.get(m1).length
                                    + ", " + c2.methods.get(m2).length
                                    + ", " + c3.methods.get(m3).length);
                            int[] ids = new int[mp0123.cardinality()];
                            int ididx = 0;
                            for (int p = mp0123.nextSetBit(0); p != -1; p = mp0123.nextSetBit(p + 1)) {
                                Product prod = products.get(p);
                                ids[ididx++] = p;
                                System.out.println("S" + ididx + " = " + p + " = " + prod);
                            }
                            c0.printMethod(ids, m0);
                            c1.printMethod(ids, m1);
                            c2.printMethod(ids, m2);
                            c3.printMethod(ids, m3);
                            solution++;
                        }
                    }
                }
            }

        }

        System.out.println("found " + solution + " solutions");
        // consider one target

        // choose one method to get to target a
        // which methods are compatible to get to target b

    }

    public static String display(final List<Product> products, final int[] list) {
        final StringBuilder sb = new StringBuilder();
        String sep = "";
        for (int i = 0; i < list.length; i++) {
            sb.append(sep);
            sb.append(products.get(list[i]));
            sep = ", ";
        }
        return sb.toString();
    }

    private Products createProducts(final Mat m, final Mat n) {
        final Terms mterms = this.mterms;
        final Terms nterms = this.nterms;
        final List<Product> prods = new ArrayList<>();
        final int maxCardinality = 3;

        final Sequence msequence = new Sequence(-1, 1, mterms.length);
        final Sequence nsequence = new Sequence(-1, 1, nterms.length);
        final int[] mseq = msequence.create();
        do {
            if (!Sequence.isSmallestPositive(mseq)
                    || Sequence.cardinality(mseq) > maxCardinality
                    || !Sequence.gcdIsOne(mseq) || Sequence.isEmpty(mseq)) {
                continue;
            }
            final Expression a = mterms.expression(mseq);
            final int[] nseq = nsequence.create();
            do {
                if (!Sequence.isSmallestPositive(nseq)
                        || Sequence.cardinality(nseq) > maxCardinality
                        || !Sequence.gcdIsOne(nseq) || Sequence.isEmpty(nseq)) {
                    continue;
                }
                final Expression b = nterms.expression(nseq);

                Product product = Product.multiply(this.terms, a, b);
                // System.out.println(product);
                prods.add(product);

            } while (nsequence.next(nseq));
        } while (msequence.next(mseq));

        return new Products(this.terms, prods);
    }

    public static String sign(final String sep, final int a) {
        if (a == 0 || a > 1) {
            return sep + a + " * ";
        }
        if (a == 1) {
            return sep;
        }
        if (a == -1) {
            return " - ";
        }
        return " - " + a + " * ";
    }

    public static String[] splitOn(final char s0, final char s1, final String line) {
        final List<String> terms = new ArrayList<>();
        final StringBuilder sb = new StringBuilder();

        boolean brackets = false;
        C:
        for (int i = 0; i < line.length(); i++) {
            final char c = line.charAt(i);
            if (c == s0 || c == s1) {
                if (!brackets) {
                    terms.add(sb.toString().trim());
                    sb.setLength(0);
                    if (c == '-') {
                        sb.append('-');
                    }
                    continue C;
                }
            }
            switch (c) {
                case '(':
                    if (brackets)
                        throw new RuntimeException();
                    brackets = true;
                    break;
                case ')':
                    if (!brackets)
                        throw new RuntimeException();
                    brackets = false;
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    break;
                case ' ':
                    break;
                case '+':
                case '-':
                case '*':
                case '/':
                    break;
                default:
                    throw new RuntimeException();

            }
            sb.append(c);
        }
        if (sb.length() > 0) {
            terms.add(sb.toString().trim());
        }

        return terms.toArray(new String[terms.size()]);
    }

    public static String unbracket(String s) {
        s = s.trim();
        if (s.startsWith("(") && s.endsWith(")")) {
            return s.substring(1, s.length() - 1);
        }
        return s;
    }

    public Expression decode(final String line) {
        String[] products = splitOn('+', '-', line);
        int[] productFactors = new int[products.length];
        Product[] ps = new Product[products.length];
        for (int termIdx = 0; termIdx < products.length; termIdx++) {
            String term = products[termIdx].trim();
            if (term.startsWith("-")) {
                term = term.substring(1).trim();
                productFactors[termIdx] = -1;
            } else {
                productFactors[termIdx] = 1;
            }
            String[] factors = splitOn('*', '*', term);
            Expression[] es = new Expression[factors.length];
            Terms tms = this.mterms;
            for (int fidx = 0; fidx < factors.length; fidx++) {
                String factor = factors[fidx];
                factor = unbracket(factor);
                String[] ts = splitOn('+', '-', factor);
                int[] f = new int[ts.length];
                for (int i = 0; i < ts.length; i++) {
                    String t = ts[i];
                    f[i] = 1;
                    if (t.startsWith("-")) {
                        t = t.substring(1).trim();
                        ts[i] = t;
                        f[i] = -1;
                    }
                }
                es[fidx] = tms.generate(f, ts);
                tms = this.nterms;
            }
            ps[termIdx] = Product.multiply(this.terms, es[0], es[1]);
//			System.out.println(p);
        }
        return ProductSum.add(this.terms, productFactors, ps);
    }

    public static void main(String[] args) throws IOException {

        ComputeStrassen computeStrassen = new ComputeStrassen(3, 3, 3);

        try (BufferedReader in = new BufferedReader(new FileReader(new File("AJ+BM+CP.txt")))) {
            for (String line = in.readLine(); line != null; line = in.readLine()) {

                System.out.println(computeStrassen.decode(line));
                System.out.println(computeStrassen.decode(line).simplify());
            }

        }

//		computeStrassen.cross();

        for (Expression target : computeStrassen.targets) {
            computeStrassen.target(target, 2);
        }
        computeStrassen.target2("", computeStrassen.targets[1]);

        computeStrassen.findThrees(computeStrassen.targets[1]);

        if (true) {
//			return;
        }
        // computeStrassen.compute();
    }
}
