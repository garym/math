package net.gcvs.math.polynomial;

import net.gcvs.math.BigRational;
import net.gcvs.math.algebra.Algebra;
import net.gcvs.math.algebra.BigRationalAlgebra;
import net.gcvs.math.cf.SimpleContinuedFraction;
import org.junit.jupiter.api.Test;

import java.math.MathContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StandardSeriesTest {

    public static final BigRational contfrac_e = new BigRational("410105312/150869313");
    public static final BigRational series_e = new BigRational("7437374403113/2736057139200");

    Algebra<BigRational> algebra = new BigRationalAlgebra(true);

    SeriesAlgebra<BigRational> seriesAlgebra = new SeriesAlgebra<>(algebra);

    StandardSeries<BigRational> standardSeries = new StandardSeries<>(algebra);

    @Test
    void exp() {
        final Series<BigRational> e = standardSeries.e();

        System.out.println(seriesAlgebra.toString(e, 8));

        BigRational value = BigRational.ZERO;
        for (int degree = 0; degree <= 17; degree++) {
            value = seriesAlgebra.eval(e, algebra.one(), degree).simplify();
            System.out.println(value);
        }
        System.out.println("v = " + value.toBigDecimal().round(MathContext.DECIMAL64));
        System.out.println("d = " + value.doubleValue());
        System.out.println("e = " + Math.exp(1));

        assertEquals(Math.exp(1), value.toBigDecimal().doubleValue());

    }

    @Test
    void contFrac() {
        BigRational value = BigRational.ZERO;
        for (int i = 0; i <= 20; i++) {
            final SimpleContinuedFraction prefix = SimpleContinuedFraction.E.prefix(i);
            value = prefix.toBigRational();
            System.out.println(value);
//            System.out.println("e[" + i + "] = " + value.toBigDecimal().round(MathContext.DECIMAL64));
        }
        System.out.println("d = " + value.doubleValue());
        System.out.println("e     = " + Math.exp(1));
        assertEquals(Math.exp(1), value.toBigDecimal().doubleValue());
    }

    @Test
    void e_e() {
        System.out.println(contfrac_e.doubleValue());
        System.out.println(series_e.doubleValue());

        System.out.println(contfrac_e.subtract(series_e).simplify());


        assertEquals(contfrac_e.doubleValue(), series_e.doubleValue());
    }

    @Test
    void sin() {

        final Series<BigRational> sin = standardSeries.sin();

        System.out.println(seriesAlgebra.toString(sin, 5));

        BigRational value = BigRational.ZERO;
        for (int degree = 0; degree <= 17; degree++) {
            value = seriesAlgebra.eval(sin, algebra.one(), degree).simplify();
            System.out.println(value);
        }
        System.out.println("v = " + value.toBigDecimal().round(MathContext.DECIMAL64));
        System.out.println("d = " + value.doubleValue());
        System.out.println("e = " + Math.sin(1));

        assertEquals(Math.sin(1), value.toBigDecimal().doubleValue());


    }

    @Test
    void cos() {

        final Series<BigRational> cos = standardSeries.cos();

        System.out.println(seriesAlgebra.toString(cos, 5));

        BigRational value = BigRational.ZERO;
        for (int degree = 0; degree <= 18; degree++) {
            value = seriesAlgebra.eval(cos, algebra.one(), degree).simplify();
            System.out.println(value);
        }
        System.out.println("v = " + value.toBigDecimal().round(MathContext.DECIMAL64));
        System.out.println("d = " + value.doubleValue());
        System.out.println("e = " + Math.cos(1));

        assertEquals(Math.cos(1), value.toBigDecimal().doubleValue());


    }

    @Test
    void sin2_cos2() {

        final Series<BigRational> sin = standardSeries.sin();
        final Series<BigRational> cos = standardSeries.cos();

        System.out.println("sin = " + seriesAlgebra.toString(sin, 10));
        System.out.println("cos = " + seriesAlgebra.toString(cos, 10));

        final Series<BigRational> sin2 = seriesAlgebra.multiply(sin, sin);
        final Series<BigRational> cos2 = seriesAlgebra.multiply(cos, cos);

        System.out.println("sin2 = " + seriesAlgebra.toString(sin2, 10));
        System.out.println("cos2 = " + seriesAlgebra.toString(cos2, 10));

        final Series<BigRational> sin2cos2 = seriesAlgebra.add(sin2, cos2);

        System.out.println("sin2cos2 = " + seriesAlgebra.toString(sin2cos2, 10));

        System.out.println(seriesAlgebra.eval(sin2cos2, algebra.one(), 10));

        assertEquals(algebra.one(), seriesAlgebra.eval(sin2cos2, algebra.one(), 10));
    }

    @Test
    void sinh() {
        final Series<BigRational> sinh = standardSeries.sinh();

        System.out.println(seriesAlgebra.toString(sinh, 10));


        final Series<BigRational> sinhcosh = seriesAlgebra.add(standardSeries.sinh(), standardSeries.cosh());

        assertEquals(seriesAlgebra.toString(standardSeries.e(), 20), seriesAlgebra.toString(sinhcosh, 20));
    }

    @Test
    void cosh() {
        final Series<BigRational> cosh = standardSeries.cosh();

        System.out.println(seriesAlgebra.toString(cosh, 10));
    }

    @Test
    void arctan() {
        final Series<BigRational> arctan = standardSeries.arctan();

        System.out.println(seriesAlgebra.toString(arctan, 20));
    }

    @Test
    void ln1PlusX() {
        final Series<BigRational> ln = standardSeries.ln1PlusX();

        System.out.println(seriesAlgebra.toString(ln, 20));
    }

    @Test
    void ln1MinusX() {
        final Series<BigRational> ln = standardSeries.ln1MinusX();

        System.out.println(seriesAlgebra.toString(ln, 20));
    }

    @Test
    void reciprocal() {
        final Series<BigRational> reciprocal = standardSeries.reciprocal();

        System.out.println(seriesAlgebra.toString(reciprocal, 10));

        System.out.println(seriesAlgebra.eval(reciprocal, new BigRational("5/3"), 10));
    }

}