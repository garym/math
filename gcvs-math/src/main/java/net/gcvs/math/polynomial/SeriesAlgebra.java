package net.gcvs.math.polynomial;

import net.gcvs.math.algebra.Algebra;
import net.gcvs.math.algebra.DivMod;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SeriesAlgebra<V> implements Algebra<Series<V>> {
    private final Series<V> zero;
    private final Series<V> one;
    private final Algebra<V> algebra;

    public SeriesAlgebra(Algebra<V> algebra) {
        this.algebra = algebra;
        this.zero = constant(0);
        this.one = constant(1);
    }

    public Algebra<V> getAlgebra() {
        return algebra;
    }

    @Override
    public Series<V> zero() {
        return zero;
    }

    @Override
    public Series<V> one() {
        return one;
    }

    @Override
    public Series<V> constant(long value) {
        return constant(algebra.constant(value));
    }

    public Series<V> constant(final V value) {
        final Term<V> term = new Term<>(0, value);
        return () -> new Iterator<>() {
            boolean hasNext = true;

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public Term<V> next() {
                if (!hasNext) throw new NoSuchElementException();
                hasNext = false;
                return term;
            }
        };
    }

    @Override
    public Series<V> add(final Series<V> a, final Series<V> b) {
        if (zero.equals(a)) return b;
        if (zero.equals(b)) return a;
        return new TermBinaryOpSeries<>(a, b, algebra::add);
    }

    @Override
    public Series<V> subtract(Series<V> a, Series<V> b) {
        if (zero.equals(b)) return a;
        return new TermBinaryOpSeries<>(a, b, algebra::subtract);
    }

    @Override
    public Series<V> multiply(Series<V> a, Series<V> b) {
        if (one.equals(a)) return b;
        if (one.equals(b)) return a;
        return new MultiplySeries<>(this, a, b);
    }

    public Series<V> multiply(final Series<V> series, final Term<V> term) {
        if (zero.equals(series)) return zero;
        if (algebra.zero().equals(term.coefficient())) return zero;
        return new MultiplyTermSeries<>(algebra, series, term);
    }

    @Override
    public Series<V> divide(Series<V> a, Series<V> b) {
        return null;
    }

    @Override
    public DivMod<Series<V>> divideAndRemainder(Series<V> a, Series<V> b) {
        return null;
    }

    @Override
    public Series<V>[] newArray(int length) {
        return new Series[length];
    }

    @Override
    public String toString(Series<V> a) {
        return toString(a, 10);
    }

    public Series<V> composite(final Series<V> g, final Series<V> f) {
        return new CompositeSeries<>(this, g, f);
    }

    public V eval(Series<V> series, final V x, final long degree) {
        V sum = algebra.zero();
        Iterator<Term<V>> it = series.iterator();
        V xn = algebra.one();
        long pow = 0;
        while (it.hasNext()) {
            Term<V> term = it.next();
            if (term.degree() > degree) break;

            while (term.degree() > pow) {
                xn = algebra.multiply(xn, x);
                pow++;
            }

            sum = algebra.add(sum, algebra.multiply(xn, term.coefficient()));

            if (term.degree() == degree) break;
        }
        return sum;
    }

    public Series<V> differentiate(final Series<V> series) {
        return () -> new Iterator<>() {
            final Peekerator<Term<V>> it = Peekerator.of(series.iterator());

            @Override
            public boolean hasNext() {
                // skip constant term
                if (it.hasNext() && it.peek().degree() == 0) {
                    it.next();
                }
                return it.hasNext();
            }

            @Override
            public Term<V> next() {
                if (!hasNext()) throw new NoSuchElementException();
                final Term<V> term = it.next();
                return new Term<>(term.degree() - 1, algebra.multiply(term.coefficient(), algebra.constant(term.degree())));
            }
        };
    }

    public Series<V> integrate(final Series<V> series) {
        return () -> new Iterator<>() {
            final Iterator<Term<V>> it = series.iterator();

            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public Term<V> next() {
                if (!hasNext()) throw new NoSuchElementException();
                final Term<V> term = it.next();
                return new Term<>(term.degree() + 1, algebra.divide(term.coefficient(), algebra.constant(term.degree() + 1)));
            }
        };
    }

    public String toString(Series<V> series, final long maxDegree) {
        final V zero1 = algebra.zero();
        final V one1 = algebra.one();
        boolean ellipsis = false;

        final StringBuilder sb = new StringBuilder();
        for (Term<V> term : series) {
            if (term.degree() > maxDegree) {
                ellipsis = true;
                break;
            }
            final long i = term.degree();
            final V co = term.coefficient();
            if (zero1.equals(co)) {
                continue;
            }
            if (sb.length() > 0) {
                sb.append(" + ");
            }

            if (!one1.equals(co) || i == 0) {
                String coeff = algebra.toString(co);
                if (coeff.contains(" ") && !coeff.contains("(")) {
                    sb.append("(").append(coeff).append(")");
                } else {
                    sb.append(coeff);
                }
                if (i > 0) {
                    sb.append(" * ");
                }
            }
            if (i > 0) {
                sb.append("x");
                if (i > 1) {
                    sb.append("^").append(i);
                }
            }
        }
        if (sb.length() == 0) {
            sb.append(zero1);
        }
        if (ellipsis) {
            sb.append(" + ...");
        }
        return sb.toString();
    }
}
