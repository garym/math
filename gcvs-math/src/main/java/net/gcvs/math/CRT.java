package net.gcvs.math;

import java.math.BigInteger;

public class CRT {

    /**
     * if ax + by = gcd(a, b)
     * then eea(a, b) = [x, y]
     */
    public static long[] eea(long a, long b) {
        long r0 = a, r1 = b;
        long s0 = 1, s1 = 0;
        long t0 = 0, t1 = 1;

        for (; ; ) {
            long qi = r0 / r1;
            long ri = r0 - qi * r1;

            if (ri == 0) {
                return new long[]{s1, t1};
            }
            r0 = r1;
            r1 = ri;

            long si = s0 - qi * s1;
            s0 = s1;
            s1 = si;

            long ti = t0 - qi * t1;
            t0 = t1;
            t1 = ti;
        }
    }

    /**
     * if xy = 1 (mod n)
     * inverse(x, n) = y
     * <p>
     * 0 < x < n
     */
    public static long inverse(long x, long n) {
        long r0 = n, r1 = x;
        long t0 = 0, t1 = 1;
        for (; ; ) {
            long qi = r0 / r1;
            long ri = r0 - qi * r1;

            if (ri == 0) {
                long inverse = t1;
                if (inverse < 0) inverse += n;
                return inverse;
            }
            r0 = r1;
            r1 = ri;

            long ti = t0 - qi * t1;
            t0 = t1;
            t1 = ti;
        }
    }


}
