package net.gcvs.primes;

import java.util.Arrays;
import java.util.BitSet;

public class AtkinPrimeGenerator implements IntPrimeGenerator {

    int[] ss = {1, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 49, 53, 59};
    int[] m1 = {1, 13, 17, 29, 37, 41, 49, 53};
    int[] m2 = {7,19,31,43};
    int[] m3 = {11,23,47,59};
    BitSet S = new BitSet(60);
    BitSet M1 = new BitSet(60);
    BitSet M2 = new BitSet(60);
    BitSet M3 = new BitSet(60);

    {
        Arrays.stream(ss).forEach(S::set);
        Arrays.stream(m1).forEach(M1::set);
        Arrays.stream(m2).forEach(M2::set);
        Arrays.stream(m3).forEach(M3::set);
    }


    @Override
    public int[] intArray(final int n) {

        BitSet set = new BitSet(n);
        set.set(2);
        set.set(3);
        set.set(5);

        final int root = (int) Math.sqrt(n);

        for (int x = 1; x <= root; x++) {
            for (int y = 1; y <= root; y += 2) {
                int m = 4 * x * x + y * y;
                if (M1.get(m % 60) && m <= n) {
                    set.flip(m);
                }
            }
        }
        for (int x = 1; x <= root; x += 2) {
            for (int y = 2; y <= root; y += 2) {
                int m = 3 * x * x + y * y;
                if (M2.get(m % 60) && m <= n) {
                    set.flip(m);
                }
            }
        }
        for (int x = 2; x <= root; x += 2) {
            for (int y = x - 1; y >= 1; y -= 2) {
                int m = 3 * x * x - y * y;
                if (M3.get(m % 60) && m <= n) {
                    set.flip(m);
                }
            }
        }




        return PrimesHelper.toArray(set);
    }
}
