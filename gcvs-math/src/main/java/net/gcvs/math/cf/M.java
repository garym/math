package net.gcvs.math.cf;

import java.math.BigInteger;

public record M(BigInteger a, BigInteger b, BigInteger c, BigInteger d) {
    public M(long a, long b, long c, long d) {
        this(BigInteger.valueOf(a), BigInteger.valueOf(b), BigInteger.valueOf(c), BigInteger.valueOf(d));
    }
}

