package net.gcvs.math.algebra;

import java.math.BigDecimal;
import java.math.MathContext;

public class BigDecimalAlgebra implements Algebra<BigDecimal> {
    @Override
    public BigDecimal zero() {
        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal one() {
        return BigDecimal.ONE;
    }

    @Override
    public BigDecimal constant(long value) {
        return BigDecimal.valueOf(value);
    }

    @Override
    public BigDecimal add(BigDecimal a, BigDecimal b) {
        return a.add(b);
    }

    @Override
    public BigDecimal subtract(BigDecimal a, BigDecimal b) {
        return a.subtract(b);
    }

    @Override
    public BigDecimal multiply(BigDecimal a, BigDecimal b) {
        return a.multiply(b);
    }

    @Override
    public BigDecimal divide(BigDecimal a, BigDecimal b) {
        return a.divide(b, MathContext.DECIMAL128);
    }

    @Override
    public DivMod<BigDecimal> divideAndRemainder(BigDecimal a, BigDecimal b) {
        final BigDecimal[] bigDecimals = a.divideAndRemainder(b);
        return new DivMod<>(bigDecimals[0], bigDecimals[1]);
    }

    @Override
    public BigDecimal[] newArray(int length) {
        return new BigDecimal[length];
    }

    @Override
    public String toString(final BigDecimal a) {
        return a.toString();
    }
}
