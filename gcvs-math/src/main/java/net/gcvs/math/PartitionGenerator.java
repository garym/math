package net.gcvs.math;

import java.util.Arrays;

public class PartitionGenerator {
    private final int givenMinPartSize;
    private final int givenMaxPartSize;
    private final int givenMinPartCount;
    private final int givenMaxPartCount;


    private final int n;
    private final int minPartSize;
    private final int maxPartSize;
    private final int minPartCount;
    private final int maxPartCount;

    private int[] parts;
    private int lastIndex;
    private int nextDecreaseIndex;

    public int getPart(int idx) {
        return parts[idx];
    }

    public PartitionGenerator(int n) {
        this(n, 1, n, 1, n);
    }

    public PartitionGenerator(int n, int maxPartSize, int maxPartCount) {
        this(n, 1, maxPartSize, 1, maxPartCount);
    }

    public PartitionGenerator(final int n, final int minPartSize, int maxPartSize, int minPartCount, int maxPartCount) {
        this.givenMinPartSize = minPartSize;
        this.givenMaxPartSize = maxPartSize;
        this.givenMinPartCount = minPartCount;
        this.givenMaxPartCount = maxPartCount;

        if (minPartCount * maxPartSize < n) {
            // minPartCount too low: increase the minPartCount
            minPartCount = (n + maxPartSize - 1) / maxPartSize;
        }
        if (maxPartCount * minPartSize > n) {
            // maxPartCount too high: decrease maxPartCount
            maxPartCount = n / minPartSize;
        }
        if (maxPartSize + minPartCount - 1 > n) {
            maxPartSize = n - minPartCount + 1;
        }

        if (maxPartSize < minPartSize) {
            throw new IllegalStateException();
        }
        if (maxPartCount < minPartCount) {
            throw new IllegalStateException();
        }
        if (Math.multiplyFull(minPartCount, minPartSize) > n) {
            throw new IllegalStateException();
        }
        if (Math.multiplyFull(maxPartCount, maxPartSize) < n) {
            throw new IllegalStateException();
        }
        this.n = n;
        this.minPartSize = minPartSize;
        this.maxPartSize = maxPartSize;
        this.minPartCount = minPartCount;
        this.maxPartCount = maxPartCount;
    }

    public int getGivenMinPartSize() {
        return givenMinPartSize;
    }

    public int getGivenMaxPartSize() {
        return givenMaxPartSize;
    }

    public int getGivenMinPartCount() {
        return givenMinPartCount;
    }

    public int getGivenMaxPartCount() {
        return givenMaxPartCount;
    }

    public int getN() {
        return n;
    }

    public int getMinPartSize() {
        return minPartSize;
    }

    public int getMaxPartSize() {
        return maxPartSize;
    }

    public int getMinPartCount() {
        return minPartCount;
    }

    public int getMaxPartCount() {
        return maxPartCount;
    }

    /**
     * creates the initial configuration.
     * The trick is, that minPartCount has been properly computed: (n + max - 1) / max;
     * so the initial configuration ALWAYS contains exactly minPartCount parts.
     */
    private void reset() {
        this.parts = new int[maxPartCount];
        final int maxPart = Math.min(n, maxPartSize);
        int sum = n;
        for (int i = 0; i < minPartCount; i++) {
            parts[i] = minPartSize;
            sum -= minPartSize;
        }
        lastIndex = minPartCount - 1;
        for (int i = 0; i < parts.length; i++) {
            int add = Math.min(sum, maxPart - parts[i]);
            parts[i] += add;
            sum -= add;
            if (sum == 0) {
                nextDecreaseIndex = i;
                break;
            }
        }
    }

    public boolean next() {
        if (parts == null) {
            reset();
            return true;
        }
        return inc();
    }

    static long loops;
    static long tests;
    static long testParts;

    /**
     * find and decrease the right most part greater than minPartSize
     *
     * @return true iff a next part was found
     */
    private boolean inc() {
        final int[] parts = this.parts;
        final int min = this.minPartSize;
        final int oldLastIndex = this.lastIndex;
        final int lastPart = parts.length - 1;

        // accumulate the sequence of minPartSize parts
        int sum = min * (lastIndex - nextDecreaseIndex);

        // gather up the parts that cannot be split further, accumulate their total value into variable sum
        for (int i = nextDecreaseIndex; i >= 0; i--) {
            loops++;
            // note: if we adjust i in this loop, we must not continue the loop.

            final int part = parts[i];
            // can we reduce the value of this part?
            // we cant if it's already the minimum size
            // or our accumulated sum isn't big enough to create a new part
            // or we are already at the last part
            if (part <= min || sum + part < 2 * min || i == lastPart) {
                sum += part;
            } else {
                int tx = Math.max(1, min - sum);
                for (int max = part - tx; max >= min; max -= tx) {
                    // take tx off the current part, and add it to sum
                    // transfer enough from the current part to make another part of size min
                    parts[i] -= tx;
                    if (parts[i] > min) {
                        nextDecreaseIndex = i;
                    } else {
                        nextDecreaseIndex = i - 1;
                    }
                    sum += tx; // assert sum >= min
                    tx = 1; // subsequently transfer 1 to sum

                    final int missingParts = minPartCount - (i + 1);

                    // shortcut when the accumulated sum can fit in a single part
                    if (sum <= max && missingParts <= 1) {
                        parts[++i] = sum; // next slot guaranteed to be available since i is not at the last part
                        if (sum > min) {
                            nextDecreaseIndex = i;
                        }
                        this.lastIndex = i;
                        while (i < oldLastIndex) {
                            parts[++i] = 0;
                        }
                        return true;
                    }

                    // we must spread sum over multiple parts; might not be possible, lets try
                    // if there's not room for two parts, sum is not big enough to split into two parts, roll up sum
                    final int partsLeft = lastPart - i;
                    if (partsLeft <= 1) {
                        break;
                    }

                    // two possible numerical orderings:
                    // min, max, 2min, 2max, 3min, 3max, ...
                    // or
                    // min, 2min, max, 3min, 2max, 4min, 3max, ...
                    final int minRemainingParts = Math.max(missingParts, (sum + max - 1) / max);

                    // if sum cannot cover the minimum remaining parts
                    final int sumMinRemainingParts = min * minRemainingParts;
                    if (sum < sumMinRemainingParts) {
                        // request the difference from the current part
                        tx = sumMinRemainingParts - sum;
                        continue;
                    }

                    final int maxRemainingParts = Math.min(partsLeft, (sum + min - 1) / min);
                    if (sum <= maxRemainingParts * max) {
                        boolean fits = false;
                        tests++;
                        // TODO this test should not require a loop, fix
                        for (int remainingParts = minRemainingParts; remainingParts <= maxRemainingParts; remainingParts++) {
                            testParts++;
                            if (min * remainingParts <= sum && sum <= max * remainingParts) {
                                // can be fit in remaining parts
                                fits = true;
                                break;
                            }
                        }
                        if (!fits) {
                            // since there were gaps in the range, we must decrement current part to re-test
                            continue;
                        }
                    } else {
                        // sum too big, rollup this part
                        // or too small
                        break;
                    }

                    // apply the solution, that we know exists
                    // apply min count rules when distributing
                    // minRemainingParts >= missingParts
                    final int newLastIndex = i + minRemainingParts;
                    int m = i;
                    while (m < newLastIndex) {
                        parts[++m] = min;
                    }
                    while (m < oldLastIndex) {
                        parts[++m] = 0;
                    }
                    this.lastIndex = newLastIndex;

                    sum -= min * minRemainingParts;
                    if (sum > 0) {
                        final int diff = max - min;
                        while (sum > diff) {
                            parts[++i] += diff;
                            sum -= diff;
                        }
                        parts[++i] += sum;
                        nextDecreaseIndex = i;
                    }
                    return true;
                }

                // roll up this part
                sum += parts[i];
            }
        }
        return false;
    }

    public int[] parts() {
        return parts;
    }

    public int[] toArray() {
        return Arrays.copyOf(parts, lastIndex + 1);
    }

    public int getPartCount() {
        return lastIndex + 1;
    }

    public boolean isPartitionValid(final int[] parts) {
        final int max = parts[0];
        if (max > getMaxPartSize()) {
            return false;
        }

        int len = 0;
        int sum = 0;

        for (int part : parts) {
            if (part == 0) {
                break;
            }
            len++;
            if (len > getMaxPartCount()) {
                return false;
            }
            sum += part;
            if (sum > getN()) {
                return false;
            }
            if (part < getMinPartSize() || part > getMaxPartSize()) {
                return false;
            }
        }
        return len >= getMinPartCount() && sum == getN();
    }

    public String toString() {
        if (parts == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        String sep = "";
        for (int part : parts) {
            if (part == 0) {
                break;
            }
            sb.append(sep);
            sep = ", ";
            sb.append(part);
        }
        sb.append(']');
        return sb.toString();
    }
}
