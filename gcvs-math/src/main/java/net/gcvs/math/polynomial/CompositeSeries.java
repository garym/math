package net.gcvs.math.polynomial;

import java.util.*;

/**
 * function composition: (g º f)(x) = g(f(x))
 *
 * 2022/11/10 - this doesn't work yet, in general, coefficients are infinite sums
 */
public class CompositeSeries<V> implements Series<V> {

    private final SeriesAlgebra<V> seriesAlgebra;
    private final Series<V> g;
    private final Series<V> f;
    private final long lowestDegreeInF;

    public CompositeSeries(final SeriesAlgebra<V> seriesAlgebra, final Series<V> g, final Series<V> f) {
        this.seriesAlgebra = seriesAlgebra;
        this.g = g;
        this.f = f;
        final Iterator<Term<V>> it = f.iterator();
        this.lowestDegreeInF = it.hasNext() ? it.next().degree() : 0;
    }

    @Override
    public Iterator<Term<V>> iterator() {
        return new Iterator<Term<V>>() {

            final PowerSeriesGenerator<V> fpow = new PowerSeriesGenerator<>(seriesAlgebra, f);

            // g(f(x)) = g0 + g1 f(x) + g2 f(x2) + g3 f(x3) + ...

            final Peekerator<Term<V>> git = Peekerator.of(g.iterator());

            final PriorityQueue<Peekerator<Term<V>>> sums = new PriorityQueue<>(Comparator.comparingLong(a -> a.peek().degree()));

            long nextG() {
                return git.peek().degree() + lowestDegreeInF;
            }

            void fillTo(final long degree) {
                while (git.hasNext() && nextG() <= degree) {
                    addG();
                }
            }

            void addG() {
                final Term<V> next = git.next();
                final Series<V> fdegree = fpow.power(next.degree());
                sums.add(Peekerator.of(seriesAlgebra.multiply(fdegree, new Term<>(0, next.coefficient())).iterator()));
                // assert peekerator is not empty, all Series have at least one term, even if it's the symbolic zero
            }

            boolean hasAnyNext() {
                return (git.hasNext() || !sums.isEmpty());
            }

            @Override
            public boolean hasNext() {
                return hasAnyNext();
            }

            @Override
            public Term<V> next() {
                if (sums.isEmpty()) {
                    if (!git.hasNext()) {
                        throw new NoSuchElementException();
                    }
                    addG();
                }
                // sums.size() > 0
                long nextDegree = sums.peek().peek().degree();
                if (git.hasNext() && nextG() <= nextDegree) {
                    addG();
                    nextDegree = sums.peek().peek().degree();
                }

                V coeff = null;
                while (!sums.isEmpty() && sums.peek().peek().degree() == nextDegree) {
                    // note: implementation would be tiny bit faster if we peek and then heapify modified value
                    // instead of poll/add
                    final Peekerator<Term<V>> peek = sums.poll();
                    final V co = peek.next().coefficient();
                    if (coeff == null) {
                        coeff = co;
                    } else {
                        coeff = seriesAlgebra.getAlgebra().add(coeff, co);
                    }
                    if (peek.hasNext()) sums.add(peek);
                }
                return new Term<>(nextDegree, coeff);
            }
        };
    }
}
