package net.gcvs.math;

import java.math.BigInteger;

import static java.lang.Math.*;

public class LongFraction implements Comparable<LongFraction> {
    public static final LongFraction ZERO = new LongFraction(0, 1);
    public static final LongFraction ONE = new LongFraction(1, 1);
    private final long num;
    private final long den;

    /**
     * lazy initialization of simplestForm flag<br>
     * simplestForm iff gcd(numerator, denominator) == 1
     */
    private Boolean simplestForm;

    public LongFraction(long num, long den) {
        this.num = num;
        this.den = den;
    }

    private LongFraction(long num, long den, Boolean simplestForm) {
        this.num = num;
        this.den = den;
        this.simplestForm = simplestForm;
    }

    public LongFraction power(final int p) {
        if (p == 0) {
            return LongFraction.ONE;
        }
        int i;
        final long num, den;
        if (p > 0) {
            i = p;
            num = this.num;
            den = this.den;
        } else {
            i = -p;
            num = this.den;
            den = this.num;
        }

        long n = num, d = den;
        for (; i > 1; i--) {
            n *= num;
            d *= den;
        }

        return new LongFraction(n, d);
    }

    public LongFraction add(LongFraction f) {
        if (num == 0) {
            return f;
        }
        if (f.num == 0) {
            return this;
        }
        if (den == f.den) {
            return new LongFraction(addExact(num, f.num), den);
        }
        // TODO force gcd simplification if arithmetic exception thrown here
        return new LongFraction(
                addExact(multiplyExact(num, f.den), multiplyExact(f.num, den)),
                multiplyExact(den, f.den));
    }

    public LongFraction subtract(LongFraction f) {
        if (num == 0) {
            return f.negate();
        }
        if (f.num == 0) {
            return this;
        }
        if (den == f.den) {
            return new LongFraction(subtractExact(num, f.num), den);
        }
        // TODO force gcd simplification if arithmetic exception thrown here
        return new LongFraction(
                subtractExact(multiplyExact(num, f.den), multiplyExact(f.num, den)),
                multiplyExact(den, f.den));
    }

    public static int simplifyCount = 0;
    public static int usefulSimplify = 0;

    public LongFraction simplify() {
        simplifyCount++;
        if (Boolean.TRUE.equals(simplestForm)) {
            return this;
        }
        long gcd = GCD.gcd(num, den);
        if (gcd == 1) {
            simplestForm = Boolean.TRUE;
            return this;
        }
        usefulSimplify++;
        return new LongFraction(num / gcd, den / gcd, Boolean.TRUE);
    }

    @Override
    public int compareTo(LongFraction o) {
        // better than overflow problems

        // return Double.compare(doubleValue(), o.doubleValue());
        try {
            return Long.compare(Math.multiplyExact(num, o.den), Math.multiplyExact(o.num, den));
        } catch (ArithmeticException e) {
//			System.err.println(e.getMessage());
            return BigInteger.valueOf(num).multiply(BigInteger.valueOf(o.den))
                    .compareTo(BigInteger.valueOf(o.num).multiply(BigInteger.valueOf(den)));
        }
    }

    public int hashCode() {
        return (int) (num * den);
    }

    public boolean equals(Object o) {
        LongFraction f = (LongFraction) o;
        return compareTo(f) == 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(num).append('/').append(den);
        return sb.toString();
    }

    public LongFraction mul(LongFraction f) {
        return new LongFraction(multiplyExact(num, f.num), multiplyExact(den, f.den));
    }

    public LongFraction multiply(LongFraction f) {
        return new LongFraction(multiplyExact(num, f.num), multiplyExact(den, f.den));
    }

    public int signum() {
        return Long.signum(num) * Long.signum(den);
    }

    public double doubleValue() {
        return (double) num / (double) den;
    }

    public long getNumerator() {
        return num;
    }

    public long getDenominator() {
        return den;
    }

    public BigRational toBigRational() {
        return new BigRational(num, den);
    }

    public LongFraction negate() {
        return new LongFraction(-num, den);
    }
}
