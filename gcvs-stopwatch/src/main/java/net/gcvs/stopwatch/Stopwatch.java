package net.gcvs.stopwatch;

import lombok.Getter;

import java.util.*;

/**
 * Gary's adhoc performance analysis tool.
 * <p>
 * Nested hierarchy of Stopwatch timers.
 */
public class Stopwatch implements AutoCloseable {
    private static final String NO_MARK = "nomark";

    private static final ThreadLocal<Stopwatch> LOCAL = new ThreadLocal<>();
    private static final Stopwatch main = new Stopwatch(null, "main");
    private static final List<Stopwatch> threadHistory = Collections.synchronizedList(new ArrayList<>());

    private static boolean reportOnExit = true;
    private static int exitMaxSize = 10;
    private static int exitMaxPathCount = 5;


    static {
        // initialize the main thread
        LOCAL.set(main);
        main.start(System.currentTimeMillis());
        Runtime.getRuntime().addShutdownHook(new Thread(Stopwatch::exitReport));
    }

    @Getter
    final String name;
    private final Stopwatch parent;
    private long elapsed;
    private long calls;
    private long start;
    private boolean running;

    private Stopwatch mark;
    private boolean isMark;

    private final Map<String, Stopwatch> map = new LinkedHashMap<>();

    private Stopwatch(final Stopwatch parent, final String name) {
        this.parent = parent;
        this.name = name;
    }

    long calls() {
        return calls;
    }

    public static Stopwatch start(final String name) {
        final long now = System.currentTimeMillis();
        final Stopwatch w = get(now).get(name);
        w.start(now);
        LOCAL.set(w);
        return w;
    }

    public static void mark(String name) {
        final Stopwatch w = LOCAL.get();
        if (w == null) return;
        final long now = System.currentTimeMillis();
        w.mark(now, name);
    }

    public static long stop() {
        final long now = System.currentTimeMillis();
        final Stopwatch sw = get(now);
        long time = sw.end(now);
        if (sw.parent == null) {
            LOCAL.remove();
        } else {
            LOCAL.set(sw.parent);
        }
        return time;
    }

    private static Stopwatch get(final long now) {
        Stopwatch sw = LOCAL.get();
        if (sw == null) {
            sw = new Stopwatch(null, Thread.currentThread().getName());
            sw.start(now);
            threadHistory.add(sw);
        }

        return sw;
    }

    private Stopwatch get(final String name) {
        return map.computeIfAbsent(name, this::create);
    }

    private Stopwatch create(String key) {
        return new Stopwatch(this, key);
    }

    private void start(final long now) {
        if (running) {
            throw new IllegalStateException("impossible");
        }
        start = now;
        calls++;
        running = true;
    }

    private void mark(long now, String name) {
        (mark == null ? newMark(NO_MARK, start) : mark).end(now);
        mark = newMark(name, now);
    }

    private Stopwatch newMark(String name, long start) {
        final Stopwatch stopwatch = get(name);
        stopwatch.start(start);
        stopwatch.isMark = true;
        return stopwatch;
    }

    private long end(final long now) {
        if (!running) {
            throw new IllegalStateException("impossible");
        }
        if (mark != null) {
            mark.end(now);
            mark = null;
        }
        long time = now - start;
        elapsed += time;
        running = false;
        return time;
    }

    public StopwatchReport report() {
        final long now = System.currentTimeMillis();

        StopwatchReport report = new StopwatchReport(this, now);

        collectReport(report);

        return report;
    }

    public static void exitReport(int maxSize, int maxPathCount) {
        reportOnExit = true;
        exitMaxSize = maxSize;
        exitMaxPathCount = maxPathCount;
    }

    private void collectReport(StopwatchReport report) {
        report.add(this);
        map.values().forEach(sw -> sw.collectReport(report));
    }

    private static void exitReport() {
        if (!reportOnExit) return;
        final long now = System.currentTimeMillis();
        for (final Stopwatch sw : threadHistory) {
            if (sw.running) {
                sw.end(now);
            }
            merge(main, sw);
        }
        // reset elapsed, all is global time
        main.elapsed = 0;
        main.end(now);

        main.report().print(exitMaxSize, exitMaxPathCount);
    }

    private static Stopwatch merge(final Stopwatch a, final Stopwatch b) {
        a.calls += b.calls;
        a.elapsed += b.elapsed;

        for (final String name : b.map.keySet()) {
            final Stopwatch aw = a.map.get(name);
            final Stopwatch bw = b.map.get(name);
            if (aw == null) {
                a.map.put(name, bw);
            } else {
                a.map.put(name, merge(aw, bw));
            }
        }
        return a;
    }

    long getElapsed(final long now) {
        long e = elapsed;
        if (running) {
            e += now - start;
        }
        return e;
    }

    long getChildElapsed(final long now) {
        long e = 0;
        for (final Stopwatch sw : map.values()) {
            if (!sw.isMark) e += sw.getElapsed(now);
        }
        return e;
    }

    long getSelfElapsed(final long now) {
        return getElapsed(now) - getChildElapsed(now);
    }

    String getPath() {
        if (parent == null) {
            return "/";
        }
        return parent.getPath() + name + '/';
    }

    @Override
    public void close() {
        stop();
    }
}
