package net.gcvs.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JoinTest {

    @Test
    void join() {
        assertEquals("", Join.join(""));
        assertEquals("", Join.join(","));
        assertEquals("1", Join.join("", 1));
        assertEquals("gary", Join.join(",", "gary"));
        assertEquals("abc", Join.join("", "a", "b", "c"));
        assertEquals("a,b,c", Join.join(",", "a", "b", "c"));
        assertEquals("123", Join.join("", 1, 2, 3));
    }
}