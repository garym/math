package net.gcvs.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongArrayTest {

    @Test
    void whenAddToSharedArray_thenExceptionThrown() {
        final LongArray array = LongArray.of(new long[10]);
        assertThrows(UnsupportedOperationException.class, () -> array.add(1));
    }

    @Test
    void noargs_constructor() {
        final LongArray array = new LongArray();
        assertEquals(0, array.size());
    }

    @Test
    void capacity_constructor() {
        final LongArray array = new LongArray(2);
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        array.add(5);
        assertEquals(5, array.size());
        assertEquals(1, array.get(0));
        assertEquals(2, array.get(1));
        assertEquals(3, array.get(2));
        assertEquals(4, array.get(3));
        assertEquals(5, array.get(4));
    }

    @Test
    void array_constructor() {
        final LongArray array = new LongArray(new long[]{4, 6, 8});
        assertEquals(3, array.size());
        assertEquals(4, array.get(0));
        assertEquals(6, array.get(1));
        assertEquals(8, array.get(2));
    }

    @Test
    void add() {
        final LongArray array = new LongArray();
        array.add(1);
        assertEquals(1, array.get(0));
        array.add(345);
        assertEquals(345, array.get(1));
    }

    @Test
    void toArray() {
        final LongArray array = new LongArray();
        assertArrayEquals(new long[0], array.toArray());

        array.add(7);
        assertArrayEquals(new long[]{7}, array.toArray());

        array.add(9);
        assertArrayEquals(new long[]{7, 9}, array.toArray());
    }

    @Test
    void get() {
        final LongArray array = new LongArray(new long[]{8, 4, 2});
        assertEquals(8, array.get(0));
        assertEquals(4, array.get(1));
        assertEquals(2, array.get(2));
        array.add(9);
        assertEquals(9, array.get(3));

        assertThrows(IndexOutOfBoundsException.class, () -> array.get(4));
    }

    @Test
    void size() {
        final LongArray array = new LongArray();
        assertEquals(0, array.size());
        for (int i = 1; i < 10; i++) {
            array.add(i);
            assertEquals(i, array.size());
        }
    }

    @Test
    void remove() {
        final LongArray array = new LongArray(new long[]{3, 5, 9});
        long removed = array.remove(1);
        assertEquals(5L, removed);
        assertEquals(2, array.size());
        assertEquals(3, array.get(0));
        assertEquals(9, array.get(1));

        assertThrows(IndexOutOfBoundsException.class, () -> array.remove(2));
    }

    @Test
    void remove_2() {
        final LongArray array = new LongArray(new long[]{1, 2, 3, 4, 5, 6});
        assertArrayEquals(array.toArray(), new long[]{1, 2, 3, 4, 5, 6});
        array.remove(5);
        assertArrayEquals(array.toArray(), new long[]{1, 2, 3, 4, 5});
        array.remove(0);
        assertArrayEquals(array.toArray(), new long[]{2, 3, 4, 5});
        array.remove(2);
        assertArrayEquals(array.toArray(), new long[]{2, 3, 5});
        array.remove(2);
        assertArrayEquals(array.toArray(), new long[]{2, 3});
        array.remove(0);
        assertArrayEquals(array.toArray(), new long[]{3});
        array.remove(0);
        assertArrayEquals(array.toArray(), new long[]{});
    }

    @Test
    void stream() {
        final LongArray array = new LongArray(new long[]{1, 2, 3, 4, 5, 6});
        assertArrayEquals(new long[]{1, 2, 3, 4, 5, 6}, array.stream().toArray());
    }

    @Test
    void testToString() {
        assertEquals("[]", new LongArray().toString());
        assertEquals("[5]", new LongArray(new long[]{5}).toString());
        assertEquals("[1, 2]", new LongArray(new long[]{1, 2}).toString());
        assertEquals("[2, 4, 6]", new LongArray(new long[]{2, 4, 6}).toString());
    }

    @Test
    void contains() {
        final LongArray array = new LongArray(new long[]{1, 2, 3, 5, 6});
        assertEquals(true, array.contains(1));
        assertEquals(true, array.contains(3));
        assertEquals(true, array.contains(5));
        assertEquals(true, array.contains(6));

        assertEquals(false, array.contains(4));
        assertEquals(false, array.contains(9));
    }
}