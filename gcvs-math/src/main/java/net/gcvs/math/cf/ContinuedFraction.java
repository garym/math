package net.gcvs.math.cf;

import net.gcvs.math.BigRational;
import net.gcvs.math.Rational;
import net.gcvs.util.IntArray;
import net.gcvs.util.LongArray;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class ContinuedFraction {

    private BigInteger[] sequence;

    public ContinuedFraction(final IntArray ints) {
        this.sequence = ints.stream().mapToObj(BigInteger::valueOf).toArray(BigInteger[]::new);
    }

    public ContinuedFraction(final LongArray longs) {
        this.sequence = longs.stream().mapToObj(BigInteger::valueOf).toArray(BigInteger[]::new);
    }

    public ContinuedFraction(final BigInteger[] ints) {
        this.sequence = ints.clone();
    }

    public ContinuedFraction(final List<BigInteger> ints) {
        this.sequence = ints.toArray(new BigInteger[ints.size()]);
    }

    public static Supplier<BigInteger> supplierOf(final int[] ints) {
        return new Supplier<BigInteger>() {
            int idx = 0;

            @Override
            public BigInteger get() {
                return BigInteger.valueOf(ints[idx++]);
            }
        };
    }

    public static Supplier<BigInteger> supplierOf(final BigInteger[] ints) {
        return new Supplier<BigInteger>() {
            int idx = 0;

            @Override
            public BigInteger get() {
                return ints[idx++];
            }
        };
    }

    public BigRational toBigRational() {
        return expand(supplierOf(sequence), sequence.length - 1);
    }

    public static BigRational expand(final Supplier<BigInteger> sequence, final int terms) {

        // a0 + 1/(a1 + 1/(a2 + 1/(a3 + ... + 1/(an + 1/x)) = (ax + b) / (cx + d)

        BigInteger a = BigInteger.ONE;
        BigInteger b = BigInteger.ZERO;
        BigInteger c = BigInteger.ZERO;
        BigInteger d = BigInteger.ONE;

        for (int i = 0; i < terms; i++) {
            BigInteger ai = sequence.get();

            BigInteger na = ai.multiply(a).add(b);
            b = a;
            a = na;

            BigInteger nc = ai.multiply(c).add(d);
            d = c;
            c = nc;
        }

        BigInteger an = sequence.get();
        return new BigRational(a.multiply(an).add(b), c.multiply(an).add(d));

    }

    public ContinuedFraction prefix(int terms) {
        return new ContinuedFraction(Arrays.copyOf(sequence, Math.min(terms, sequence.length)));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[').append(sequence[0]);
        String sep = "; ";
        for (int i = 1; i < sequence.length; i++) {
            sb.append(sep).append(sequence[i]);
            sep = ", ";
        }
        sb.append(']');
        return sb.toString();
    }

    public static ContinuedFraction of(final int... ints) {
        return new ContinuedFraction(new IntArray(ints));
    }

    public static ContinuedFraction of(Rational r) {
        List<BigInteger> ints = new ArrayList<>();
        var a = r.getNumerator();
        var b = r.getDenominator();
        while (b.signum() > 0) {
            var divmod = a.divideAndRemainder(b);
            ints.add(divmod[0]);
            a = b;
            b = divmod[1];
        }
        return new ContinuedFraction(ints);
    }

    public BigInteger get(int idx) {
        return sequence[idx];
    }

    public int length() {
        return sequence.length;
    }
}
