package net.gcvs.primes;

import net.gcvs.util.LongArray;

import java.util.BitSet;


/**
 * Sieve of Atkin might be worth looking at, for prime number generation
 * <p>
 * https://www.baeldung.com/cs/prime-number-algorithms
 */
public interface PrimeGenerator extends IntPrimeGenerator {
    LongArray longArray(long n);

    BitSet primes(int n);

    default long[] array(long n) {
        return longArray(n).toArray();
    }

    default int[] intArray(int n) {
        return PrimesHelper.toArray(primes(n));
    }
}