package net.gcvs.math.algebra;

import net.gcvs.math.BigRational;
import net.gcvs.math.cf.SimpleContinuedFraction;
import net.gcvs.math.polynomial.Series;
import net.gcvs.math.polynomial.SeriesAlgebra;
import net.gcvs.math.polynomial.StandardSeries;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ComplexAlgebraTest {

    private static final double DELTA = 0.0000001;
    Algebra<BigRational> algebra = new BigRationalAlgebra();
    ComplexAlgebra<BigRational> complexAlgebra = new ComplexAlgebra<>(algebra);

    SeriesAlgebra<Complex<BigRational>> seriesAlgebra = new SeriesAlgebra<>(complexAlgebra);
    StandardSeries<Complex<BigRational>> standardSeries = new StandardSeries<>(complexAlgebra);

    /**
     * e ^ i.pi = -1
     */
    @Test
    void euler() {

        final Series<Complex<BigRational>> e = standardSeries.e();

        final BigRational pi = SimpleContinuedFraction.PI.toBigRational().simplify();
        System.out.println(pi);
        Complex<BigRational> ipi = new Complex<>(algebra.zero(), pi);

        final Complex<BigRational> value = seriesAlgebra.eval(e, ipi, 30);

//        System.out.println(value);
        System.out.println(value.real().doubleValue());
        System.out.println(value.imaginary().doubleValue());

        assertEquals(-1, value.real().doubleValue(), DELTA);
        assertEquals(0, value.imaginary().doubleValue(), DELTA);
    }

    @Test
    void sincos() {

        final Series<Complex<BigRational>> cos = standardSeries.cos();
        final Series<Complex<BigRational>> sin = standardSeries.sin();

        final Series<Complex<BigRational>> isin = seriesAlgebra.multiply(sin, seriesAlgebra.constant(new Complex<>(algebra.zero(), algebra.one())));

        final Series<Complex<BigRational>> cosisin = seriesAlgebra.add(cos, isin);

        System.out.println(seriesAlgebra.toString(cosisin, 10));

    }

}