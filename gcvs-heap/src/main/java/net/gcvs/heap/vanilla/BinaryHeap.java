package net.gcvs.heap.vanilla;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Stack;

import net.gcvs.heap.Heap;

/**
 * Implicit Binary Heap, i.e. array backed.
 * 
 * @author gary
 *
 * @param <T>
 */
public class BinaryHeap<T> implements Heap<T> {
	/**
	 * Default initial capacity.
	 */
	private static final int DEFAULT_CAPACITY = 10;

	/**
	 * The maximum size of array to allocate (unless necessary). Some VMs reserve some header words in an array. Attempts to
	 * allocate larger arrays may result in OutOfMemoryError: Requested array size exceeds VM limit
	 */
	private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
	/**
	 * Shared empty array instance used for empty instances.
	 */
	private static final Object[] EMPTY_ELEMENTDATA = {};

	/**
	 * Shared empty array instance used for default sized empty instances. We distinguish this from EMPTY_ELEMENTDATA to
	 * know how much to inflate when first element is added.
	 */
	private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};

	protected final Comparator<T> order;
	protected Object[] data;
	protected int size;

	@SuppressWarnings("unchecked")
	public BinaryHeap() {
		this(DEFAULT_CAPACITY, (a, b) -> ((Comparable<T>) a).compareTo(b));
	}

	public BinaryHeap(final Comparator<T> order) {
		this(DEFAULT_CAPACITY, order);
	}

	public BinaryHeap(final int initialCapacity, final Comparator<T> order) {
		if (initialCapacity > 0) {
			this.data = new Object[initialCapacity];
		} else if (initialCapacity == 0) {
			this.data = EMPTY_ELEMENTDATA;
		} else {
			throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
		}
		this.order = order;
	}

	@SuppressWarnings("unchecked")
	protected final T get(final int idx) {
		return (T) data[idx];
	}

	/**
	 * mapping from node to parent.
	 */
	protected static final int parent(final int index) {
		return ((index + 1) >> 1) - 1;
	}

	/**
	 * mapping from node to left child.
	 */
	protected static final int left(final int index) {
		return (index << 1) + 1;
	}

	/**
	 * mapping from node to right child.
	 */
	protected static final int right(final int index) {
		return (index << 1) + 2;
	}

	public void insert(final T t) {
		Objects.requireNonNull(t);
		if (size == data.length) {
			data = grow();
		}

		final int index = size;
		size++;
		if (data[0] == null) {
			data[0] = t;
			siftDown(0);
		} else {
			data[index] = t;
			if (index > 0) {
				siftUp(index);
			}
		}
	}

	/**
	 * Increases the capacity of this {@code ArrayList} instance, if necessary, to ensure that it can hold at least the
	 * number of elements specified by the minimum capacity argument.
	 *
	 * @param minCapacity the desired minimum capacity
	 */
	public void ensureCapacity(final int minCapacity) {
		if (minCapacity > data.length
				&& !(data == DEFAULTCAPACITY_EMPTY_ELEMENTDATA
						&& minCapacity <= DEFAULT_CAPACITY)) {
			grow(minCapacity);
		}
	}

	/**
	 * Increases the capacity to ensure that it can hold at least the number of elements specified by the minimum capacity
	 * argument.
	 *
	 * @param minCapacity the desired minimum capacity
	 * @throws OutOfMemoryError if minCapacity is less than zero
	 */
	protected Object[] grow(final int minCapacity) {
		return data = Arrays.copyOf(data, newCapacity(minCapacity));
	}

	protected Object[] grow() {
		return grow(size + 1);
	}

	/**
	 * Returns a capacity at least as large as the given minimum capacity. Returns the current capacity increased by 50% if
	 * that suffices. Will not return a capacity greater than MAX_ARRAY_SIZE unless the given minimum capacity is greater
	 * than MAX_ARRAY_SIZE.
	 *
	 * @param minCapacity the desired minimum capacity
	 * @throws OutOfMemoryError if minCapacity is less than zero
	 */
	private int newCapacity(int minCapacity) {
		// overflow-conscious code
		int oldCapacity = data.length;
		int newCapacity = oldCapacity + (oldCapacity >> 1);
		if (newCapacity - minCapacity <= 0) {
			if (data == DEFAULTCAPACITY_EMPTY_ELEMENTDATA)
				return Math.max(DEFAULT_CAPACITY, minCapacity);
			if (minCapacity < 0) // overflow
				throw new OutOfMemoryError();
			return minCapacity;
		}
		return (newCapacity - MAX_ARRAY_SIZE <= 0)
				? newCapacity
				: hugeCapacity(minCapacity);
	}

	private static int hugeCapacity(int minCapacity) {
		if (minCapacity < 0) // overflow
			throw new OutOfMemoryError();
		return (minCapacity > MAX_ARRAY_SIZE)
				? Integer.MAX_VALUE
				: MAX_ARRAY_SIZE;
	}

	private void siftDown(final int index) {
		final int size = this.size;
		final Object[] data = this.data;
		final int left = index * 2 + 1;
		if (left >= size) {
			return;
		}
		final T parent = get(index);
		int smallest = index;
		T smallestValue = parent;
		{
			final T a = get(left);
			final int cmp = order.compare(smallestValue, a);
			if (cmp > 0) {
				smallest = left;
				smallestValue = a;
			}
		}
		// a > node
		final int right = left + 1;
		if (right < size) {
			final T b = get(right);
			final int cmp = order.compare(smallestValue, b);
			if (cmp > 0) {
				smallest = right;
				smallestValue = b;
			}

		}
		if (smallest != index) {
			data[index] = smallestValue;
			data[smallest] = parent;
			siftDown(smallest);
		}
	}

	private void siftUp(final int startIndex) {
		if (startIndex <= 0) {
			return;
		}
		int index = startIndex;
		T value = get(index);
		do {
			int parentIndex = parent(index);
			T parent = get(parentIndex);
			if (order.compare(parent, value) <= 0) {
				break;
			}
			data[index] = parent;
			data[parentIndex] = value;
			index = parentIndex;
		} while (index > 0);
	}

	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public T extractMin() {
		if (size == 0) {
			throw new IllegalStateException();
		}
		final int lastIndex = --size;
		final T head = get(0);
		if (lastIndex > 0) {
			data[0] = data[lastIndex];
			data[lastIndex] = null;
			siftDown(0);
		} else {
			data[0] = null;
		}

		return head;
	}

	@Override
	public T findMin() {
		if (size == 0) {
			return null;
		}
		return get(0);
	}

	protected void isHeap() {
		for (int i = 1; i < size; i++) {
			T v = get(i);
			T p = get(parent(i));
			if (order.compare(p, v) > 0) {
				throw new IllegalStateException("heap property broken");
			}
		}
	}

	public void report() {
	}

	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		String sep = "";
		for (int i = 0; i < size; i++) {
			final Object t = data[i];
			sb.append(sep);
			sb.append(t == null ? "_" : t);
			sep = ", ";
		}
		sb.append("]");
		return sb.toString();
	}
}
