package net.gcvs.heap;

import java.util.Comparator;
import java.util.Stack;

public class PairingHeap<T> implements Heap<T> {

    static class PairingTree<T> {
        // TODO back is apparently required for decreaseKey
        // PairingTree<T> back;
        PairingTree<T> next; // from wiki: possibly eliminate back by setting last child.next to parent and boolean "end of list" flag
        PairingTree<T> child;

        T elem;

        public PairingTree(final T elem) {
            this.elem = elem;
        }
    }

    final Comparator<T> order;

    PairingTree<T> root;

    public PairingHeap(final Comparator<T> order) {
        this.order = order;
    }

    /**
     * create a new heap for the inserted element and meld into the original heap.
     *
     * @param t
     */
    @Override
    public void insert(final T t) {
        root = merge(new PairingTree<T>(t), root);
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public T findMin() {
        return root == null ? null : root.elem;
    }

    @Override
    public T extractMin() {
        if (isEmpty()) {
            return null;
        }
        T min = root.elem;
        root = mergePairs(root.child);
        return min;
    }

    /**
     * mergePairs recursion has been unrolled to use the Collections Stack.<br>
     * This avoids stack overflow errors for large heaps.
     *
     * @param list
     * @return
     */
    PairingTree<T> mergePairs(final PairingTree<T> list) {
        if (list == null) {
            return null;
        }
        if (list.next == null) {
            return list;
        }
        final Stack<PairingTree<T>> stack = new Stack<>();

        PairingTree<T> cursor = list;
        while (cursor != null) {
            if (cursor.next == null) {
                stack.push(cursor);
                break;
            }
            PairingTree<T> head0 = cursor;
            PairingTree<T> head1 = cursor.next;
            PairingTree<T> tail = head1.next;
            head0.next = null; // detach from the list
            head1.next = null; // detach from the list
            stack.push(merge(head0, head1));
            cursor = tail;
        }
        PairingTree<T> tail = stack.pop();
        while (stack.size() > 0) {
            PairingTree<T> t2 = stack.pop();
            tail = merge(t2, tail);
        }
        return tail;
    }

    PairingTree<T> merge(final PairingTree<T> a, final PairingTree<T> b) {
        if (a == null) {
            return b;
        }
        if (b == null) {
            return a;
        }
        if (order.compare(a.elem, b.elem) <= 0) {
            b.next = a.child;
            a.child = b;
            return a;
        }
        a.next = b.child;
        b.child = a;
        return b;
    }
}
