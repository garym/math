package net.gcvs.heap;

import net.gcvs.heap.vanilla.BinaryHeap;
import net.gcvs.heap.vanilla.BinomialHeap;
import net.gcvs.heap.vanilla.PairingHeap;
import net.gcvs.util.Sample;
import net.gcvs.util.Shuffle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Comparator;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class HeapTest {

    Random random = new Random(123);
    Shuffle shuffle = new Shuffle(random);
    TotalOrderOracle<Integer> order;

    @BeforeEach
    public void setSeed() {
        order = new TotalOrderOracle<>(Integer::compareTo);
        order.countOnly();
    }

    @ParameterizedTest(name = "heap: {arguments}")
    @ValueSource(classes = {
            BinaryHeap.class,
            net.gcvs.heap.BinaryHeap.class,
            BinomialHeap.class,
            PairingHeap.class,
            net.gcvs.heap.PairingHeap.class,
    })
    public void insertThenExtractAll(final Class<Heap<Integer>> heapClass) throws Exception {
        final Heap<Integer> heap = heapClass.getConstructor(Comparator.class).newInstance(order);

        assertTrue(heap.isEmpty());

        int[] ints = Sample.naturals(10000); // Sample.cross(Sample.naturals(100), Sample.naturals(100), (a, b) -> a + b);
        shuffle.shuffle(ints);

        for (int i : ints) {
            heap.insert(i);
        }

        int last = heap.extractMin();
        for (int i = 1; i < ints.length; i++) {
            int value = heap.extractMin();
            assertTrue(last <= value);
            last = value;
        }

        assertTrue(heap.isEmpty());

        System.out.println("\n"
                + "insertThenExtractAll Report: " + heapClass.getName());
        order.report();
    }

    @ParameterizedTest(name = "heap: {arguments}")
    @ValueSource(classes = {
            BinaryHeap.class,
            net.gcvs.heap.BinaryHeap.class,
            BinomialHeap.class,
            PairingHeap.class,
            net.gcvs.heap.PairingHeap.class,
    })
    public void insertDeleteMix(final Class<Heap<Integer>> heapClass) throws Exception {
        final Heap<Integer> heap = heapClass.getConstructor(Comparator.class).newInstance(order);

        assertTrue(heap.isEmpty());

        int[] ints = Sample.naturals(25000); // Sample.cross(Sample.naturals(100), Sample.naturals(100), (a, b) -> a + b);
        shuffle.shuffle(ints);

        int min = Integer.MIN_VALUE;
        int size = 0;
        for (int idx = 0; idx < ints.length || !heap.isEmpty(); ) {
//			System.out.println("heap: " + heap);
            final boolean insert = Sample.weightedChoice(random, size, ints.length - idx);
            if (insert) {
                int value = ints[idx++];
                heap.insert(value);
                min = Math.min(min, value);
//				System.out.println("add " + value);
                size++;
            } else {
                int value = heap.extractMin();
//				System.out.println("extract " + value);
                size--;
                assertTrue(min <= value);
                min = value;
            }
        }

        assertTrue(heap.isEmpty());

        System.out.println("\n" + "insertDeleteMix Report: " + heapClass.getName());
        order.report();
    }

}
