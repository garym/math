package net.gcvs.primes;

import net.gcvs.util.Timing;
import org.junit.jupiter.api.Assertions;

import static net.gcvs.util.Timing.time;

public class PrimeGeneratorATest {

    PrimeGeneratorA primeGenerator = new PrimeGeneratorA();

    //    @Test
    public void test() {
        int[] primes = time("ints", () -> primeGenerator.intArray(Integer.MAX_VALUE));
        long[] p2 = time("longs", () -> primeGenerator.array(10_000_000_000L));
        System.out.println(primes.length + ", " + p2.length);
        for (int i = 0; i < Math.min(primes.length, p2.length); i++) {
            Assertions.assertEquals(primes[i], p2[i]);
        }
        Assertions.assertEquals(primes.length, p2.length);

    }

    //    @Test
    public void benchmark() {
        int max = 1_000_000;
        int[] p2 = time("experimental", () -> primeGenerator.intArray(max));
        int[] ps = Timing.time("stable", () -> primeGenerator.intArray(max));

        for (int i = 0; i < Math.min(p2.length, ps.length); i++) {
            Assertions.assertEquals(p2[i], ps[i]);
        }
        Assertions.assertEquals(p2.length, ps.length);
    }

    public static void main(String[] args) {
        int[] ps = {2, 3, 5, 7};
        int prod = 1;
        for (int p : ps) prod *= p;
        boolean[] composites = new boolean[prod];
        composites[0] = true;
        for (int p : ps) {
            for (int i = 0; i < composites.length; i += p) {
                composites[i] = true;
            }
        }
        int iter = 1;
        int n = 0;
        for (int i = 0; i < iter; i++) {
            for (boolean c : composites) {
                System.out.print(" ");
                if (n < 10) {
                    System.out.print(" ");
                }
                System.out.print(n);
                n++;
            }
        }
        System.out.println();
        for (int i = 0; i < iter; i++) {
            for (boolean c : composites) {
                System.out.print("  ");
                System.out.print(c ? "C" : "P");
            }
        }
        System.out.println();
        int lastp = 1;
        for (int i = 1; i < composites.length; i++) {
            if (!composites[i]) {
                if (i - lastp > 0) {
                    System.out.print((i - lastp) + " ");
                    lastp = i;
                }
            }
        }
        System.out.println("2");
        System.out.println(prod);
    }
}
