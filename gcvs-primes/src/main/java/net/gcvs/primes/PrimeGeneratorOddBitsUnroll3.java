package net.gcvs.primes;

import static net.gcvs.primes.PrimeSet.*;

/**
 * experimental
 */
public class PrimeGeneratorOddBitsUnroll3 implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        // primes = 0..n
        long[] primes = new long[((n - 1) >> 7) + 1];
        final int nextPrime = Stripes.sieveStripe2(primes);

        final int nmod = ((n - 1) >> 1) & 0x3f;
        if (nmod != 0x3f) {
            primes[primes.length - 1] &= (1L << (nmod + 1)) - 1;
        }
        int sqrt = (int) Math.ceil(Math.sqrt(n));
        int minsqrt = Math.min(63, sqrt);
        int p;
        for (p = nextSetBit(primes, nextPrime); p != -1 && p <= minsqrt; p = nextSetBit(primes, p + 2)) {
            final int step = p << 1;
            for (int m = p * p; m >= 0 && m <= n; m += step) {
                clear(primes, m);
            }
        }
        final int max = n >> 1;
        if (p != -1) {
            int q = nextSetBit(primes, p + 2);

            if (q != -1 && q <= sqrt) {
                int r = nextSetBit(primes, q + 2);

                while (r != -1 && r <= sqrt) {
                    int mp = (p * p) >> 1;
                    int mq = (q * q) >> 1;
                    int mr = (r * r) >> 1;
                    for (; mr >= 0 && mr <= max; mp += p, mq += q, mr += r) {
                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                        primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                    }
                    for (; mq >= 0 && mq <= max; mp += p, mq += q) {
                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                    }
                    for (; mp >= 0 && mp <= max; mp += p) {
                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                    }
                    p = nextSetBit(primes, r + 2);
                    q = nextSetBit(primes, p + 2);
                    r = nextSetBit(primes, q + 2);
                }
                if (q != -1 && q <= sqrt) {
                    for (int mq = (q * q) >> 1; mq >= 0 && mq <= max; mq += q) {
                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                    }
                }
            }
            if (p != -1 && p <= sqrt) {
                for (int mp = (p * p) >> 1; mp <= max; mp += p) {
                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                }
            }
        }


        return expandToArray(primes);
    }
}
