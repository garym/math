package net.gcvs.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ConsoleColorsTest {
    @Test
    void colors() {
        for (ConsoleColors value : ConsoleColors.values()) {
            assertTrue(value.toString().startsWith("\033["));
            assertTrue(value.toString().endsWith("m"));
        }
    }
}