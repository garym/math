package net.gcvs.heap;

import java.util.Comparator;

public class ExplicitBinaryHeap<T> implements Heap<T> {

	private static class Node<T> {
		Node<T> parent;
		Node<T> left;
		Node<T> right;

		T value;
	}

	private final Comparator<T> order;

	Node<T> root;

	public ExplicitBinaryHeap(final Comparator<T> order) {
		this.order = order;
	}

	@Override
	public void insert(final T t) {
		// TODO Auto-generated method stub

	}

	@Override
	public T extractMin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T findMin() {
		// TODO Auto-generated method stub
		return null;
	}

}
