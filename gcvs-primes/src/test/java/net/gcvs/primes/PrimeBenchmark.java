package net.gcvs.primes;

import org.openjdk.jmh.annotations.*;

@BenchmarkMode(Mode.AverageTime)
@Fork(value = 1)
@Warmup(iterations = 3)
@Measurement(iterations = 3)
public class PrimeBenchmark {

    @State(Scope.Thread)
    public static class MyState {
        int p = Integer.MAX_VALUE;
    }

    @Benchmark
    public int[] PrimeGeneratorA_intArray(MyState state) {
        return new PrimeGeneratorA().intArray(state.p);
    }

    @Benchmark
    public int[] PrimeGeneratorOddBitsUnroll5CacheLine(MyState state) {
        return new PrimeGeneratorOddBitsUnroll5CacheLine().intArray(state.p);
    }

    /**
     * Benchmark                                             Mode  Cnt  Score   Error  Units
     * PrimeBenchmark.PrimeGeneratorOddBitsUnroll6CacheLine  avgt    3  5.335 ± 0.161   s/op
     * PrimeBenchmark.PrimeGeneratorOddBitsUnroll6CacheLine  avgt    3  5.332 ± 2.146   s/op
     */
    @Benchmark
    public int[] PrimeGeneratorOddBitsUnroll6CacheLine(MyState state) {
        return new PrimeGeneratorOddBitsUnroll6CacheLine().intArray(state.p);
    }

    /**
     * Benchmark                                                             Mode  Cnt  Score   Error  Units
     * PrimeBenchmark.PrimeGeneratorOddBitsUnroll5CacheLineReorderMemWrites  avgt    3  4.848 ± 0.883   s/op
     * PrimeBenchmark.PrimeGeneratorOddBitsUnroll5CacheLineReorderMemWrites  avgt    3  4.773 ± 0.193   s/op
     * PrimeBenchmark.PrimeGeneratorOddBitsUnroll5CacheLineReorderMemWrites  avgt    3  4.765 ± 1.108   s/op
     */
    @Benchmark
    public int[] PrimeGeneratorOddBitsUnroll5CacheLineReorderMemWrites(MyState state) {
        return new PrimeGeneratorOddBitsUnroll6CacheLineReorderMemWrites().intArray(state.p);
    }

    /**
     * Benchmark                                    Mode  Cnt  Score   Error  Units
     * PrimeBenchmark.PrimeGeneratorOddBitsUnrollN  avgt    3  5.886 ± 1.518   s/op
     */
    @Benchmark
    public int[] PrimeGeneratorOddBitsUnrollN(MyState state) {
        return new PrimeGeneratorOddBitsUnrollN().intArray(state.p);
    }

    @Benchmark
    public int[] primes(MyState state) {
        return new PrimeGeneratorOddBitsUnrollN().intArray(state.p);
    }

}
