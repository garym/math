package net.gcvs.primes;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

class MemTest {
    @Test
    @Tag("ignore")
    void alloc() {
        System.out.println(Runtime.getRuntime().maxMemory());
        long[] longs = new long[Integer.MAX_VALUE - 2];
        System.out.println(longs.length);
        long maxlen = 2147483645;
        System.out.println(maxlen * 64);
        // theoretical max prime from long sieve: 137_438_953_280
    }
}
