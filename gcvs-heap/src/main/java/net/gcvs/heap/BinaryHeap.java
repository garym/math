package net.gcvs.heap;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

/**
 * Implicit Binary Heap, i.e. array backed.
 * 
 * @author gary
 *
 * @param <T>
 */
public class BinaryHeap<T> extends net.gcvs.heap.vanilla.BinaryHeap<T> implements Heap<T> {
	private static final int DUNNO = 0;
	private static final int LEFT = 1;
	private static final int RIGHT = 2;

	int[] minChilds;
	public boolean delayExtract = true;
	public boolean rememberChilds = true;

	public BinaryHeap() {
	}

	public BinaryHeap(final Comparator<T> order) {
		super(order);
		this.minChilds = new int[data.length];
	}

	public BinaryHeap(final int initialCapacity, final Comparator<T> order) {
		super(initialCapacity, order);
		this.minChilds = new int[initialCapacity];
	}

	protected Object[] grow(final int minCapacity) {
		data = super.grow(minCapacity);
		minChilds = Arrays.copyOf(minChilds, data.length);
		return data;
	}

	public void insert(final T t) {
		Objects.requireNonNull(t);
		if (size == data.length) {
			data = grow();
		}

		final int index = size;
		size++;
		if (data[0] == null) {
			data[0] = t;
			siftDown(0);
		} else {
			data[index] = t;
			if (index > 0) {
				siftUp(index);
			}
		}

		//isHeap();
	}

	// 0 = root
	// 1 = child
	// 2 = child

	// left(0) = 2 * 0 + 1
	// right(0) = 2 * 0 + 2

	private void siftDown(final int index) {
		final int size = this.size;
		final Object[] data = this.data;
		final int left = index * 2 + 1;
		if (left >= size) {
			return;
		}
		final T parent = get(index);
		int smallest = index;
		T smallestValue = parent;
		switch (rememberChilds ? minChilds[index] : DUNNO) {
		case DUNNO: {
			{
				final T a = get(left);
				final int cmp = order.compare(smallestValue, a);
				if (cmp > 0) {
					smallest = left;
					smallestValue = a;
				}
			}
			// a > node
			final int right = left + 1;
			if (right < size) {
				final T b = get(right);
				final int cmp = order.compare(smallestValue, b);
				if (cmp > 0) {
					smallest = right;
					smallestValue = b;
				}

			}
			break;
		}
		case LEFT: { // if its greater than LEFT, swap with LEFT, minChilds[index] = DUNNO
			{
				final T a = get(left);
				final int cmp = order.compare(smallestValue, a);
				if (cmp > 0) {
					smallest = left;
					smallestValue = a;
					minChilds[index] = DUNNO;
				}
			}
			break;
		}
		case RIGHT: { // if its greater than RIGHT, swap with RIGHT
			// a > node
			final int right = left + 1;
			if (right < size) {
				final T b = get(right);
				final int cmp = order.compare(smallestValue, b);
				if (cmp > 0) {
					smallest = right;
					smallestValue = b;
					minChilds[index] = DUNNO;
				}
			}
			break;
		}
		}
		if (smallest != index) {
			data[index] = smallestValue;
			data[smallest] = parent;
			siftDown(smallest);
		}
	}

	private void siftUp(final int startIndex) {
		if (startIndex <= 0) {
			return;
		}
		int index = startIndex;
		T value = get(index);
		do {
			int parentIndex = parent(index);
			T parent = get(parentIndex);
			if (order.compare(parent, value) <= 0) {
				// TODO index < startIndex is an incorrect assertion
				if (true || index < startIndex) {
					// actually if this is part of insert algorithm,
					// and we are the only child, we do know the min child.
					minChilds[parentIndex] = DUNNO; // could be LEFT if we are only child
				}
				break;
			}
			data[index] = parent;
			data[parentIndex] = value;
			// we just swapped this index with its parent, so it's less than it's sibling
			if (rememberChilds) {
				minChilds[parentIndex] = index % 2 == 0 ? RIGHT : LEFT;
			}
			index = parentIndex;
		} while (index > 0);
	}

	public int size() {
		return size;
	}

	@Override
	public T extractMin() {
		if (size == 0) {
			throw new RuntimeException();
		}
		T head = get(0);
		if (head == null) {
			finalizeExtract();
			head = get(0);
		}
		data[0] = null;
		size--;
		if (!delayExtract) {
			finalizeExtract();
		}

		//isHeap();

		// delay siftDown
		return head;
	}

	private final void finalizeExtract() {
		assert data[0] == null;
		if (size > 0) {
			data[0] = data[size];
			data[size] = null;
			minChilds[parent(size)] = DUNNO;
			siftDown(0);
		}
	}

	@Override
	public T findMin() {
		if (size == 0) {
			return null;
		}
		T head = get(0);
		if (head == null) {
			if (false) {
				if (minChilds[0] == DUNNO) {
					if (size == 1) {
						minChilds[0] = LEFT;
					} else {
						minChilds[0] = order.compare(get(1), get(2)) <= 0 ? LEFT : RIGHT;
					}
				}
				return get(minChilds[0]);
			} else {

				finalizeExtract();
				head = get(0);
			}
		}
		return head;
	}

	protected void isHeap() {
		if (data[0] == null) {
			finalizeExtract();
		}
		super.isHeap();
		for (int i = 0; i < size; i++) {
			T p = get(i);
			if (left(i) >= size) {
				break;
			}
			if (right(i) >= size) {
				if (minChilds[i] == RIGHT) {
					throw new RuntimeException();
				}
				break;
			}
			T l = left(i) < size ? get(left(i)) : null;
			T r = right(i) < size ? get(right(i)) : null;

			if (minChilds[i] == LEFT && r != null) {
				if (order.compare(l, r) > 0) {
					throw new RuntimeException();
				}
			}

			if (minChilds[i] == RIGHT) {
				if (order.compare(r, l) > 0) {
					throw new RuntimeException();
				}
			}
		}
	}

	public void report() {
	}
}
