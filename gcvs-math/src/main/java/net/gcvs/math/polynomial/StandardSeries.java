package net.gcvs.math.polynomial;

import net.gcvs.math.algebra.Algebra;

import java.util.Iterator;

public class StandardSeries<V> {
    private final Algebra<V> algebra;

    public StandardSeries(final Algebra<V> algebra) {
        this.algebra = algebra;
    }

    public Series<V> e() {
        return () -> new Iterator<>() {
            long nextTerm = 0;
            V coefficient = algebra.one();

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant(nextTerm));
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm++;
                return term;
            }
        };
    }

    public Series<V> sin() {
        return () -> new Iterator<>() {
            long nextTerm = 1;
            V coefficient = algebra.one();

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant(nextTerm * (nextTerm - 1)));
                    coefficient = algebra.subtract(algebra.zero(), coefficient);
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm += 2;
                return term;
            }
        };
    }

    public Series<V> cos() {
        return () -> new Iterator<>() {
            long nextTerm = 0;
            V coefficient = algebra.one();

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant(nextTerm * (nextTerm - 1)));
                    coefficient = algebra.subtract(algebra.zero(), coefficient);
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm += 2;
                return term;
            }
        };
    }

    public Series<V> sinh() {
        return () -> new Iterator<>() {
            long nextTerm = 1;
            V coefficient = algebra.one();

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant(nextTerm * (nextTerm - 1)));
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm += 2;
                return term;
            }
        };
    }

    public Series<V> cosh() {
        return () -> new Iterator<>() {
            long nextTerm = 0;
            V coefficient = algebra.one();

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant(nextTerm * (nextTerm - 1)));
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm += 2;
                return term;
            }
        };
    }

    public Series<V> arctan() {
        return () -> new Iterator<>() {
            long nextTerm = 1;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                V coefficient = algebra.one();
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant((nextTerm & 2) == 0 ? nextTerm : -nextTerm));
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm += 2;
                return term;
            }
        };
    }

    /**
     * ln(1 + x)
     */
    public Series<V> ln1PlusX() {
        return () -> new Iterator<>() {
            long nextTerm = 1;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                V coefficient = algebra.one();
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant((nextTerm & 1) == 0 ? -nextTerm : nextTerm));
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm++;
                return term;
            }
        };
    }

    /**
     * ln(1-x)
     */
    public Series<V> ln1MinusX() {
        return () -> new Iterator<>() {
            long nextTerm = 1;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                V coefficient = algebra.one();
                if (nextTerm > 1) {
                    coefficient = algebra.divide(coefficient, algebra.constant(-nextTerm));
                }
                final Term<V> term = new Term<>(nextTerm, coefficient);
                nextTerm++;
                return term;
            }
        };
    }

    /**
     * geometric series
     * sum(x^n) = 1 / (1 - x)
     */
    public Series<V> geometric() {
        final V one = algebra.one();
        return () -> new Iterator<>() {
            private long degree;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Term<V> next() {
                return new Term<>(degree++, one);
            }
        };
    }

    public Series<V> reciprocal() {
        SeriesAlgebra<V> seriesAlgebra = new SeriesAlgebra<>(algebra);

        Series<V> oneMinusX = Series.<V>builder().algebra(algebra)
                .constant(0, 1)
                .constant(1, -1)
                .build();
        return seriesAlgebra.composite(geometric(), oneMinusX);
    }
}
