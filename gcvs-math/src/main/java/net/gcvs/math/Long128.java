package net.gcvs.math;

import java.math.BigInteger;

/**
 * incomplete, draft version
 */
public class Long128 {
    public static final Long128 MAX_VALUE = new Long128(-1L, Long.MAX_VALUE);
    public static final Long128 MIN_VALUE = new Long128(0L, Long.MIN_VALUE);

    private final long low;
    private final long high;

    public Long128(long low) {
        this.low = low;
        this.high = low >> 63;
    }

    public Long128(long low, long high) {
        this.low = low;
        this.high = high;
    }

    public Long128(String s) {
        BigInteger i = new BigInteger(s);
        if (i.bitLength() > 128) {
            throw new ArithmeticException(s);
        }
        final byte[] bytes = i.toByteArray();
        final int count = bytes.length;
        long high = 0;
        long low = 0;
        if (count > 8) {
            int updownshift = (16 - count) << 3;
            high |= (bytes[0] & 0xff);
            for (int b = 1; b < count - 8; b++) {
                high <<= 8;
                high |= (bytes[b] & 0xff);
            }
            high <<= updownshift;
            high >>= updownshift;

            for (int b = count - 8; b < count; b++) {
                low <<= 8;
                low |= (bytes[b] & 0xff);
            }
        } else {
            int updownshift = (8 - count) << 3;
            low |= (bytes[0] & 0xff);
            for (int b = 1; b < count; b++) {
                low <<= 8;
                low |= (bytes[b] & 0xff);
            }
            low <<= updownshift;
            high = low >> 63;
            low >>= updownshift;
        }

        this.high = high;
        this.low = low;
    }

    public String toString() {
        byte[] bytes = new byte[16];
        bytes[0] = (byte) (high >>> 56);
        bytes[1] = (byte) (high >>> 48);
        bytes[2] = (byte) (high >>> 40);
        bytes[3] = (byte) (high >>> 32);
        bytes[4] = (byte) (high >>> 24);
        bytes[5] = (byte) (high >>> 16);
        bytes[6] = (byte) (high >>> 8);
        bytes[7] = (byte) (high >>> 0);
        bytes[8] = (byte) (low >>> 56);
        bytes[9] = (byte) (low >>> 48);
        bytes[10] = (byte) (low >>> 40);
        bytes[11] = (byte) (low >>> 32);
        bytes[12] = (byte) (low >>> 24);
        bytes[13] = (byte) (low >>> 16);
        bytes[14] = (byte) (low >>> 8);
        bytes[15] = (byte) (low >>> 0);
        return new BigInteger(bytes).toString();
    }

    public Long128 multiply(Long128 v) {
        long lll = this.low * v.low;
        long llh = Math.multiplyHigh(this.low, v.low);

        long lhl = this.low * v.high;
        long hll = this.high * v.low;

        return new Long128(lll, llh + lhl + hll);
    }

    public Long128[] multiplyFull(Long128 v) {
        long lll = this.low * v.low;
        long llh = Math.multiplyHigh(this.low, v.low);

        long lhl = this.low * v.high;
        long lhh = Math.multiplyHigh(this.low, v.high);

        long hll = this.high * v.low;
        long hlh = Math.multiplyHigh(this.high, v.low);

        long hhl = this.high * v.high;
        long hhh = Math.multiplyHigh(this.high, v.high);


        return new Long128[]{new Long128(lll, llh + lhl + hll), new Long128(hhl + lhh + hlh, hhh)};
    }
}
