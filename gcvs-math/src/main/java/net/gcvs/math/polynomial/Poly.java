package net.gcvs.math.polynomial;

import net.gcvs.math.algebra.Algebra;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Poly<T> implements Series<T> {

    private final Algebra<T> algebra;
    private final T[] coeffs;

    /**
     * @param algebra
     * @param coeffs  - internal list, encapsulation paired with {@ PolyAlgebra.create())
     */
    Poly(final Algebra<T> algebra, T[] coeffs) {
        this.algebra = algebra;
        int len = coeffs.length, l = len;
        final T zero = algebra.zero();
        while (len > 0) {
            final T coeff = coeffs[len - 1];
            if (coeff != null && !zero.equals(coeff)) {
                break;
            }
            len--;
        }

        if (l != len) {
            coeffs = Arrays.copyOf(coeffs, len);
        }
        for (int i = 0; i < len; i++) {
            if (coeffs[i] == null) {
                coeffs[i] = zero;
            }
        }
        this.coeffs = coeffs;
    }

    /**
     * @return the coefficient algebra
     */
    public Algebra<T> getAlgebra() {
        return algebra;
    }

    public T get(int power) {
        return coeffs[power];
    }

    public final T coefficient(int power) {
        return coeffs[power];
    }

    public T eval(T x) {
        T px = algebra.one();
        T sum = algebra.zero();
        for (T c : coeffs) {
            sum = algebra.add(sum, algebra.multiply(px, c));
            px = algebra.multiply(px, x);
        }
//        System.out.println("eval(" + this + ")(" + x + ") = " + sum);
        return sum;
    }

    public String toString() {
        if (coeffs.length == 0) {
            return algebra.zero().toString();
        }
        StringBuilder sb = new StringBuilder();
        for (int i = coeffs.length - 1; i >= 0; i--) {
            final T co = coeffs[i];
            if (algebra.zero().equals(co)) {
                continue;
            }
            if (sb.length() > 0) {
                sb.append(" + ");
            }

            if (!algebra.one().equals(co) || i == 0) {
                String coeff = co.toString();
                if (coeff.contains(" ")) {
                    sb.append("(").append(coeff).append(")");
                } else {
                    sb.append(coeff);
                }
                if (i > 0) {
                    sb.append(" * ");
                }
            }
            if (i > 0) {
                sb.append("x");
                if (i > 1) {
                    sb.append("^").append(i);
                }
            }
        }
        if (sb.length() == 0) {
            sb.append(algebra.zero());
        }
        return sb.toString();
    }

    public int degree() {
        return this.coeffs.length - 1;
    }

    public int length() {
        return this.coeffs.length;
    }

    @Override
    public Iterator<Term<T>> iterator() {
        return new Iterator<>() {
            int nextIndex = nextIndex(0);

            private int nextIndex(int fromIndex) {
                final T zero = algebra.zero();
                for (int i = fromIndex; i < coeffs.length; i++) {
                    if (zero.equals(coeffs[i])) {
                        continue;
                    }
                    return i;
                }
                return -1;
            }

            @Override
            public boolean hasNext() {
                return nextIndex != -1;
            }

            @Override
            public Term<T> next() {
                final int index = this.nextIndex;
                if (index == -1) throw new NoSuchElementException();
                this.nextIndex = nextIndex(index + 1);
                return new Term<>(index, coeffs[index]);
            }
        };
    }
}
