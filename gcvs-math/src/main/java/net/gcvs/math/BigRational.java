package net.gcvs.math;

import java.io.Serial;
import java.math.BigDecimal;
import java.math.BigInteger;

import static net.gcvs.math.GCD.gcd;

/**
 * rational number: integer / integer<br>
 * <p>
 * regular operations do not call any gcd methods to reduce the result into simplest form.
 * There are equivalent opReduce methods which also reduce the result of the operation.
 * <p>
 *
 * @author gary
 */
public class BigRational extends Rational implements BigRationals {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final BigRational POSITIVE_INFINITY = new BigRational(1, BigInteger.ONE, BigInteger.ZERO, true);
    public static final BigRational NEGATIVE_INFINITY = new BigRational(-1, BigInteger.ONE, BigInteger.ZERO, true);
    public static final BigRational NaN = new BigRational(0, BigInteger.ZERO, BigInteger.ZERO, true);

    public static final BigRational ZERO = new BigRational(0, BigInteger.ZERO, BigInteger.ONE, true);
    public static final BigRational ONE = new BigRational(1, BigInteger.ONE, BigInteger.ONE, true);

    private static final String POSITIVE_INFINITY_STR = "Infinity";
    private static final String NEGATIVE_INFINITY_STR = "-Infinity";
    private static final String NaN_STR = "NaN";

    /**
     * assert: num >= 0, den > 0
     */
    private final BigInteger num;
    private final BigInteger den;
    /**
     * signum = -1, 0, 1
     */
    private final int signum;

    /**
     * lazy evaluation of simplestForm flag<br>
     * gcd(simplestForm.numerator, simplestForm.denominator) == 1
     */
    private transient BigRational simplestForm;

    public BigRational(final long num) {
        this(Long.signum(num), num == Long.MIN_VALUE ? BigInteger.valueOf(num).abs() : BigInteger.valueOf(Math.abs(num)), BigInteger.ONE, true);
    }

    public BigRational(final long num, final long den) {
        this(BigInteger.valueOf(num), BigInteger.valueOf(den));
    }

    public BigRational(final BigInteger num) {
        this(num.signum(), num.abs(), BigInteger.ONE, true);
    }

    /**
     * constructs a new BigRational but doesn't reduce to simplest form, lazy constructor
     * <p>
     * Use {@link #of(BigInteger, BigInteger)} instead, which recognises +- Infinity and NaN.
     */
    @Deprecated

    public BigRational(final BigInteger num, final BigInteger den) {
        if (den.signum() == 0) {
            throw new ArithmeticException();
        }
        this.signum = den.signum() * num.signum();
        BigInteger n = num.abs();
        BigInteger d = den.abs();
        this.num = n;
        this.den = d;
        if (BigInteger.ONE.equals(n) || BigInteger.ONE.equals(d)) {
            this.simplestForm = this;
        }
    }

    public BigRational(final String value) {
        final BigDecimal decimalNum;
        final BigDecimal decimalDen;
        int divisor = value.indexOf('/');
        if (divisor == -1) {
            if (POSITIVE_INFINITY_STR.equals(value)) {
                this.signum = 1;
                this.num = BigInteger.ONE;
                this.den = BigInteger.ZERO;
                this.simplestForm = this;
                return;
            }
            if (NEGATIVE_INFINITY_STR.equals(value)) {
                this.signum = -1;
                this.num = BigInteger.ONE;
                this.den = BigInteger.ZERO;
                this.simplestForm = this;
                return;
            }
            if (NaN_STR.equals(value)) {
                this.signum = 0;
                this.num = BigInteger.ZERO;
                this.den = BigInteger.ZERO;
                this.simplestForm = this;
                return;
            }
            decimalNum = new BigDecimal(value);
            decimalDen = BigDecimal.ONE;
        } else if (value.indexOf('/', divisor + 1) == -1) {
            decimalNum = new BigDecimal(value.substring(0, divisor));
            decimalDen = new BigDecimal(value.substring(divisor + 1));
        } else {
            throw new NumberFormatException("illegal value: " + value);
        }

        if (decimalDen.signum() == 0) {
            this.signum = decimalNum.signum();

            this.num = decimalNum.signum() == 0 ? BigInteger.ZERO : BigInteger.ONE;
            this.den = BigInteger.ZERO;

            this.simplestForm = this;
        } else {
            this.signum = decimalDen.signum() * decimalNum.signum();

            final int scale = decimalNum.scale() - decimalDen.scale();

            BigInteger intNum = decimalNum.unscaledValue().abs();
            BigInteger intDen = decimalDen.unscaledValue().abs();

            if (scale == 0) {
                this.num = intNum;
                this.den = intDen;
            } else {
                final var powerOfTen = BigInteger.TEN.pow(Math.abs(scale));
                if (scale >= 0) {
                    this.num = intNum;
                    this.den = BigInteger.ONE.equals(intDen) ? powerOfTen : intDen.multiply(powerOfTen);
                } else {
                    this.num = BigInteger.ONE.equals(intNum) ? powerOfTen : intNum.multiply(powerOfTen);
                    this.den = intDen;
                }
            }
            this.simplestForm = BigInteger.ONE.equals(this.num) || BigInteger.ONE.equals(this.den) ? this : null;
        }
    }

    /**
     * simplest constructor, just set the fields, no transformations
     * <p>
     * its a private constructor so we make "assert" statements, which can ultimately be switched off,
     * once the validity of the class has been proven by tests
     */
    BigRational(final int signum, final BigInteger num, final BigInteger den, final boolean simplestForm) {
        assert num.signum() >= 0;
        assert den.signum() >= 0;
        assert (signum == 0) == (num.signum() == 0);
        this.signum = signum;
        this.num = num;
        this.den = den;
        if (simplestForm) {
            this.simplestForm = this;
        }
    }

    /**
     * extracts the signum from num and den
     *
     * @param num - the numerator, a signed BigInteger
     * @param den - the denominator, a signed BigInteger
     * @return a new BigRational representing num / den
     */
    public static BigRational of(final BigInteger num, final BigInteger den) {
        if (den.signum() == 0) {
            if (num.signum() > 0) {
                return POSITIVE_INFINITY;
            }
            if (num.signum() < 0) {
                return NEGATIVE_INFINITY;
            }
            return NaN;
        }
        if (num.signum() == 0) {
            return ZERO;
        }
        final BigInteger n = num.abs();
        final BigInteger d = den.abs();
        return new BigRational(num.signum() * den.signum(), n, d, BigInteger.ONE.equals(n) || BigInteger.ONE.equals(d));
    }

    public String toString() {
        if (den.signum() == 0) {
            if (signum < 0) {
                return NEGATIVE_INFINITY_STR;
            }
            if (signum > 0) {
                return POSITIVE_INFINITY_STR;
            }
            return NaN_STR;
        }
        if (signum == 0) {
            return "0";
        }
        final StringBuilder sb = new StringBuilder();
        if (signum < 0) {
            sb.append('-');
        }
        sb.append(num);
        if (den.compareTo(BigInteger.ONE) != 0) {
            sb.append('/').append(den);
        }
        return sb.toString();
    }

    public BigRational add(final long i) {
        return add(new BigRational(i));
    }

    public BigRational add(final BigInteger i) {
        return linear(1, i);
    }

    public BigRational subtract(final long i) {
        return subtract(BigInteger.valueOf(i));
    }

    public BigRational subtract(final BigInteger i) {
        return linear(-1, i);
    }

    private BigRational linear(int sign, final BigInteger i) {
        sign = Integer.compare(sign, 0);
        if (i.signum() == 0) {
            return this;
        }
        if (this.signum == 0) {
            return BigRationals.reduce(sign, i, BigInteger.ONE);
        }
        BigInteger sub = i.multiply(den);
        final BigInteger n;
        if (signum == sign) {
            n = this.num.add(sub);
        } else {
            n = this.num.subtract(sub);
        }
        return new BigRational(signum * n.signum(), n.abs(), den, simplestForm == this);
    }

    public BigRational add(final BigRational a) {
        return linear(1, a, false);
    }

    public BigRational addReduce(final BigRational _b) {
        return linear(1, _b, true);
    }

    public BigRational subtract(final BigRational a) {
        return linear(-1, a, false);
    }

    public BigRational subtractReduce(final BigRational _b) {
        return linear(-1, _b, true);
    }

    private BigRational linear(int bsign, BigRational _b, boolean reduce) {
        bsign = Integer.compare(bsign, 0);

        final boolean notRational = den.signum() == 0 || _b.den.signum() == 0;
        if (notRational) {
            if (isNaN() || _b.isNaN()) return NaN;
            if (isInfinite()) {
                if (_b.isInfinite() && signum != bsign * _b.signum) {
                    return NaN;
                }
                return this;
            }
            return _b;
        }

        BigRational a = this;
        BigRational b = _b;
        if (a.signum == 0) {
            return (bsign >= 0 ? b : b.negate()).reduce(reduce);
        }
        if (b.signum == 0) {
            return a.reduce(reduce);
        }

        if (reduce) {
            a = a.useReducedIfPossible();
            b = b.useReducedIfPossible();
        }

        final BigInteger den = a.den.multiply(b.den);
        final BigInteger numden = a.num.multiply(b.den);
        final BigInteger dennum = a.den.multiply(b.num);
        final BigInteger n;
        if (a.signum == bsign * b.signum) {
            n = numden.add(dennum);
        } else {
            n = numden.subtract(dennum);
            if (n.signum() == 0) {
                return ZERO;
            }
        }
        return reduce ? BigRationals.reduce(a.signum, n, den)
                : new BigRational(this.signum * n.signum(), n.abs(), den, false);
    }

    public BigRational multiply(final BigRational b) {
        return multiply(b, false);
    }

    public BigRational multiplyReduce(final BigRational b) {
        return multiply(b, true);
    }

    BigRational multiply(BigRational b, boolean reduce) {
        b = b.reduce(reduce);
        return multiply(b.signum, b.num, b.den, reduce);
    }


    /**
     * (0, 0, 1) = * ZERO
     * (1, 1, 0) = * +Infinity
     * (-1, 1, 0) = * -Infinity
     * (0, 0, 0) = * NaN
     * (0, 1, 0) = / ZERO
     * (1, 0, 1) = / +Infinity
     * (-1, 0, 1) = / -Infinity
     * (0, 0, 0) = / NaN
     */
    BigRational multiply(final int bsignum, BigInteger bnum, BigInteger bden, final boolean reduce) {
        var a = this;

        final var signum = a.signum * bsignum;
        final boolean notRational = a.den.signum() == 0 || bden.signum() == 0;

        if (notRational) {
            if (a.isNaN() || (bnum.signum() == 0 && bden.signum() == 0)) return NaN;
            int bsign = a.signum * (bsignum == 0 ? bnum.signum() : bsignum);
            return bsign > 0 ? POSITIVE_INFINITY : bsign < 0 ? NEGATIVE_INFINITY : NaN;
        }
        if (signum == 0) {
            return ZERO;
        }

        a = a.reduce(reduce);

        BigInteger anum = a.num, aden = a.den;

        // optimization applied: since rationals in simplest form, gcd only applied to complementary numerator and denominator
        // pair. I HAVE NO IDEA if this is actually a speed improvement :)
        // TODO test performance difference

        if (reduce) {
            final var gcd1 = gcd(anum, bden);
            if (gcd1.compareTo(BigInteger.ONE) > 0) {
                anum = anum.divide(gcd1);
                bden = bden.divide(gcd1);
            }

            final var gcd2 = gcd(bnum, aden);
            if (gcd2.compareTo(BigInteger.ONE) > 0) {
                aden = aden.divide(gcd2);
                bnum = bnum.divide(gcd2);
            }
        }

        final var n = anum.multiply(bnum);
        final var d = aden.multiply(bden);
        return new BigRational(signum, n, d, reduce || BigInteger.ONE.equals(n) || BigInteger.ONE.equals(d));
    }


    public BigRational multiply(final long a) {
        return multiply(BigInteger.valueOf(a));
    }

    public BigRational multiply(final BigInteger a) {
        return multiply(a.signum(), a.abs(), BigInteger.ONE, false);
    }

    public BigRational divide(final BigRational b) {
        return multiply(b.signum, b.den, b.num, false);
    }

    public BigRational divideReduce(BigRational b) {
        b = b.reduce();
        return multiply(b.signum, b.den, b.num, true);
    }

    public BigRational divide(final BigInteger b) {
        return multiply(b.signum(), BigInteger.ONE, b.abs(), false);
    }


    public BigRational divideReduce(final BigInteger b) {
        return multiply(b.signum(), BigInteger.ONE, b.abs(), true);
    }

    public BigRational divide(final long divisor) {
        return divide(BigInteger.valueOf(divisor));
    }

    public BigRational divideReduce(final long divisor) {
        return divideReduce(BigInteger.valueOf(divisor));
    }

    public BigRational power(final int exponent) {
        if (simplestForm != null && simplestForm != this) {
            return simplestForm.power(exponent);
        }
        if (signum == 0) {
            if (exponent <= 0) {
                return NaN;
            }
            return ZERO;
        }
        if (exponent == 0) {
            return ONE;
        }
        if (exponent == 1) {
            return this;
        }
        int absexp = Math.abs(exponent);
        BigInteger rnum = num;
        BigInteger rden = den;
        if (absexp > 1) {
            rnum = rnum.pow(absexp);
            rden = rden.pow(absexp);
        }
        if (exponent < 0) {
            BigInteger tmp = rnum;
            rnum = rden;
            rden = tmp;
        }
        // new value has same reducibility
        return new BigRational(absexp % 2 == 0 ? 1 : signum, rnum, rden, simplestForm == this);
    }

    /**
     * numerator, assert: num.signum() >= 0.
     * <p>
     * the sign of the BigRational is encoded in the signum field.
     *
     * @return
     */
    public BigInteger getNumerator() {
        return num;
    }

    /**
     * denominator, assert: den.signum() == 1
     *
     * @return
     */
    public BigInteger getDenominator() {
        return den;
    }

    /**
     * Returns the signum function of this BigRational.
     *
     * @return -1, 0 or 1 as the value of this BigRational is negative, zero or positive.
     * <p>
     * NEGATIVE_INFINITY returns -1
     * POSITIVE_INFINITY returns 1
     * NaN returns 0
     */
    public int signum() {
        return signum;
    }

    public boolean isInfinite() {
        return den.signum() == 0 && signum != 0;
    }

    /**
     * Returns {@code true} if this {@code BigRational} value is
     * a Not-a-Number (NaN), {@code false} otherwise.
     *
     * @return {@code true} if the value represented by this object is
     * NaN; {@code false} otherwise.
     */
    public boolean isNaN() {
        return den.signum() == 0 && signum == 0;
    }

    /**
     * not sure this is useful, this will compute the simplest form
     */
    public boolean isInteger() {
        return simplify().den.compareTo(BigInteger.ONE) == 0;
    }

    public BigRational negate() {
        if (signum == 0) {
            return this;
        }
        return new BigRational(-signum, num, den, isSimplestForm());
    }

    public static BigRational valueOf(final long value) {
        return new BigRational(value, 1);
    }

    public static BigRational valueOf(final double value) {
        return valueOf(BigDecimal.valueOf(value));
    }

    /**
     * This is a lossless translation.
     *
     * @param r
     * @return
     */
    public static BigRational valueOf(final BigDecimal r) {
        final var num = r.unscaledValue();
        if (r.scale() == 0) {
            return new BigRational(num);
        }
        final var scale = BigInteger.TEN.pow(Math.abs(r.scale()));
        if (r.scale() >= 0) {
            return BigRationals.reduce(num, scale);
        }
        return new BigRational(num.multiply(scale));
    }

    /**
     * compare this mapping: -1: -Infinity, 0: Rational, 1: Infinity, 2: NaN
     */
    private static int categoryValue(final int signum, final BigInteger den) {
        if (den.signum() != 0) return 0;
        if (signum == 0) return 2;
        return signum;
    }

    @Override
    public int compareTo(final Rational o) {
        // shortcut
        if (o == this) {
            return 0;
        }
        return compareTo(o.signum(), o.getNumerator(), o.getDenominator());
    }

    /**
     * See {@link Double#compareTo(Double)}
     * <p>
     * -Infinity < Rational < Infinity < NaN
     */
    private int compareTo(final int osignum, final BigInteger onum, final BigInteger oden) {
        // if either are NOT Rational, compare by type
        if (den.signum() == 0 || oden.signum() == 0) {
            // compare this mapping: -1: -Infinity, 0: Rational, 1: Infinity, 2: NaN
            return Integer.compare(categoryValue(signum, den), categoryValue(osignum, oden));
        }
        if (signum == 0) {
            return -osignum;
        }
        if (osignum == 0) {
            return signum;
        }
        if (signum == osignum) {
            int numCmp = num.compareTo(onum);
            int denCmp = oden.compareTo(den);

            if (denCmp == 0) {
                return numCmp * signum;
            }

            if (numCmp == 0) {
                return denCmp * signum;
            }

            if (numCmp == denCmp) {
                return numCmp * signum;
            }

            return num.multiply(oden).compareTo(onum.multiply(den)) * signum;
        }
        // opposite signs, return this sign
        return signum;
    }

    @Override
    public int intValue() {
        return toBigDecimal().intValue();
    }

    public int intValueExact() {
        return toBigDecimal().intValueExact();
    }

    @Override
    public long longValue() {
        return toBigDecimal().longValue();
    }

    public long longValueExact() {
        return toBigDecimal().longValueExact();
    }

    @Override
    public float floatValue() {
        if (den.signum() == 0) return signum / 0.0f;
        return toBigDecimal().floatValue();
    }

    /**
     * value as a double, not particularly efficient
     *
     * @return
     */
    @Override
    public double doubleValue() {
        if (den.signum() == 0) return signum / 0.0;
        return toBigDecimal().doubleValue();
    }

    public BigRational reciprocal() {
        if (isNaN()) return this;
        if (isInfinite()) return ZERO;
        if (signum == 0) return POSITIVE_INFINITY;
        return new BigRational(signum, den, num, isSimplestForm());
    }

    /**
     * two equal Rationals *must* have the same hashCode
     */
    @Override
    public int hashCode() {
        return signum * num.hashCode() * den.hashCode();
    }

    /**
     * hashmap equality: NaN == NaN, 3x / 3y != x / y
     * <p>
     * Does *not* simplify fraction before comparison, specifically 3x / 3y != x / y
     * <p>
     * This is an equality of fields, not the rational they represent.
     * <p>
     * This definition allows hash tables to operate properly.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof BigRational r) {
            return signum == r.signum && num.equals(r.num) && den.equals(r.den);
        }
        return false;
    }

    private BigRational reduce(boolean reduce) {
        return reduce ? reduce() : this;
    }

    public BigRational reduce() {
        if (simplestForm != null) {
            return simplestForm;
        }

        final var n = num;
        final var d = den;

        final var gcd = n.gcd(d);
        if (gcd.compareTo(BigInteger.ONE) > 0) {
            final var value = new BigRational(signum, n.divide(gcd), d.divide(gcd), true);
            this.simplestForm = value;
            return value;
        }
        this.simplestForm = this;
        return this;
    }

    public BigRational simplify() {
        return reduce();
    }

    public boolean isSimplestForm() {
        return simplestForm == this;
    }

    /**
     * simplest form is preserved when observing at the fractional part
     */
    public BigRational frac() {
        return subtract(floor());
    }

    /**
     * @return the largest integer less than or equal to this number
     */
    public BigInteger floor() {
        if (den.signum() == 0) throw new ArithmeticException();
        if (signum == 0) {
            return BigInteger.ZERO;
        }
        final BigInteger[] divmod = num.divideAndRemainder(den);
        final BigInteger div = divmod[0];
        final BigInteger mod = divmod[1];
        if (signum > 0) {
            if (div.signum() == 0) {
                return BigInteger.ZERO;
            }
            return div;
        }
        if (mod.signum() == 0) {
            return div.negate();
        }
        return div.add(BigInteger.ONE).negate();
    }

    public BigInteger ceil() {
        if (den.signum() == 0) throw new ArithmeticException();
        if (signum == 0) {
            return BigInteger.ZERO;
        }
        final BigInteger[] divmod = num.divideAndRemainder(den);
        final BigInteger div = divmod[0];
        final BigInteger mod = divmod[1];
        if (signum < 0) {
            if (div.signum() == 0) {
                return BigInteger.ZERO;
            }
            return div.negate();
        }
        if (mod.signum() == 0) {
            return div;
        }
        return div.add(BigInteger.ONE);
    }

    private BigRational useReducedIfPossible() {
        return simplestForm == null ? this : simplestForm;
    }

    public boolean isZero() {
        return signum == 0 && !isNaN();
    }

    public BigRational min(BigRational a) {
        return compareTo(a) <= 0 ? this : a;
    }

    public BigRational max(BigRational a) {
        return compareTo(a) >= 0 ? this : a;
    }

    public BigRational abs() {
        return signum >= 0 ? this : negate();
    }

    public static BigRational min(BigRational a, BigRational b) {
        return a.compareTo(b) <= 0 ? a : b;
    }

    public static BigRational max(BigRational a, BigRational b) {
        return a.compareTo(b) >= 0 ? a : b;
    }

    public BigRational[] divideAndRemainder(BigRational b) {
        final BigRational q = divide(b);
        return new BigRational[]{q, subtract(q.multiply(b))};
    }
}
