package net.gcvs.util;

import java.util.Random;
import java.util.function.IntBinaryOperator;

public class Sample {

    private Sample() {
    }

    public static int[] naturals(final int n) {
        final int[] list = new int[n];
        for (int i = 0, len = n; i < len; ) {
            list[i] = ++i;
        }
        return list;
    }

    public static int[] concat(final int[] a, final int[] b) {
        final int[] c = new int[Math.addExact(a.length, b.length)];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    public static int[] cross(final int[] as, final int[] bs, final IntBinaryOperator op) {
        final int[] cs = new int[Math.multiplyExact(as.length, bs.length)];
        int idx = 0;
        for (final int a : as) {
            for (final int b : bs) {
                cs[idx++] = op.applyAsInt(a, b);
            }
        }
        return cs;
    }

    public static boolean weightedChoice(final Random random, final double falseWeight, final double trueWeight) {
        if (falseWeight <= 0.0) {
            return true;
        }
        if (trueWeight <= 0.0) {
            return false;
        }
        return random.nextDouble() < trueWeight / (trueWeight + falseWeight);
    }

}
