package net.gcvs.math.polynomial;

import net.gcvs.math.algebra.Algebra;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class SeriesBuilder<V> {
    private Algebra<V> algebra;

    private List<Term<V>> terms = new ArrayList<>();

    public SeriesBuilder<V> algebra(final Algebra<V> algebra) {
        this.algebra = algebra;
        return this;
    }

    public SeriesBuilder<V> term(final Term<V> term) {
        terms.add(term);
        return this;
    }

    public SeriesBuilder<V> term(final long degree, final V coefficient) {
        return term(new Term<>(degree, coefficient));
    }

    public SeriesBuilder<V> constant(final long degree, final long coefficient) {
        return term(new Term<>(degree, algebra.constant(coefficient)));
    }

    public Series<V> build() {
        final List<Term<V>> terms = new ArrayList<>(this.terms);
        terms.sort(Comparator.comparingLong(Term::degree));

        for (int i = terms.size() - 2; i >= 0; i--) {
            Term<V> t1 = terms.get(i + 1);
            Term<V> t0 = terms.get(i);
            if (t1.degree() == t0.degree()) {
                terms.remove(i);
            }
        }
        return terms::iterator;
    }
}
