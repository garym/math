package net.gcvs.primes;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class PrimeGeneratorTest {
    static final int[] REFERENCE_PRIMES = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251};

    static Stream<Arguments> generate() {
        return Stream.of(
                Arguments.of(new PrimeGeneratorA()),
                Arguments.of(new PrimeGeneratorNativeBits()),
                Arguments.of(new PrimeGeneratorOddBits()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll2()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll3()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll4()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll5()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll5CacheLine()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll6()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll6CacheLine()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll6CacheLineReorderMemWrites()),
                Arguments.of(new PrimeGeneratorOddBitsUnroll7()),
                Arguments.of(new PrimeGeneratorOddBitsUnrollN()),
                Arguments.of(new PrimeGeneratorLongOddBits())
        );
    }

    static int primesTo(int n) {
        int idx = Arrays.binarySearch(REFERENCE_PRIMES, n);
        if (idx >= 0) {
            return idx + 1;
        }
        return -idx - 1;
    }

    @ParameterizedTest
    @MethodSource("generate")
    void generate(IntPrimeGenerator primeGenerator) {
        for (int n = 0; n <= 256; n++) {
            int[] expected = Arrays.copyOf(REFERENCE_PRIMES, primesTo(n));
            int[] actual = primeGenerator.intArray(n);

            assertArrayEquals(expected, actual, Integer.toString(n));
        }
    }

    @Test
    @Tag("debug")
    void printProducts() {
        long longs = 1;
        for (int referencePrime : REFERENCE_PRIMES) {
            longs *= referencePrime;
            System.out.println(referencePrime + ": " + (longs / 2));
        }
    }
}
