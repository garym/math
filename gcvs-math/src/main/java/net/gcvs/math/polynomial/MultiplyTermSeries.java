package net.gcvs.math.polynomial;

import net.gcvs.math.algebra.Algebra;

import java.util.Iterator;

public class MultiplyTermSeries<V> implements Series<V> {
    final Algebra<V> algebra;
    final Series<V> series;
    final Term<V> term;

    public MultiplyTermSeries(final Algebra<V> algebra, final Series<V> series, final Term<V> term) {
        this.algebra = algebra;
        this.series = series;
        this.term = term;
    }

    @Override
    public Iterator<Term<V>> iterator() {
        final Iterator<Term<V>> it = series.iterator();
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public Term<V> next() {
                final Term<V> t = it.next();
                return new Term<>(term.degree() + t.degree(), algebra.multiply(t.coefficient(), term.coefficient()));
            }
        };
    }
}
