package net.gcvs.math.polynomial;

import net.gcvs.math.BigRational;
import net.gcvs.math.algebra.Algebra;
import net.gcvs.math.algebra.BigRationalAlgebra;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SeriesAlgebraTest {
    private static final long MAX = 30;

    Algebra<BigRational> algebra = new BigRationalAlgebra();
    SeriesAlgebra<BigRational> seriesAlgebra = new SeriesAlgebra<>(algebra);
    PolyAlgebra<BigRational> polyAlgebra = new PolyAlgebra<>(algebra);

    StandardSeries<BigRational> standardSeries = new StandardSeries<>(algebra);

    @Test
    void add() {
        final Series<BigRational> sin = standardSeries.sin();

        Poly<BigRational> poly = polyAlgebra.poly(new BigRational("1"), new BigRational("1"), new BigRational("1"));

        final Series<BigRational> sum = seriesAlgebra.add(sin, poly);

        BigRational x = new BigRational("7/11");
        final BigRational a = seriesAlgebra.eval(sin, x, 20);
        final BigRational b = seriesAlgebra.eval(poly, x, 20);

        assertEquals(a.add(b).simplify(), seriesAlgebra.eval(sum, x, 20));
    }

    @Test
    void differentiate_sin_cos() {

        final Series<BigRational> sin = standardSeries.sin();
        final Series<BigRational> cos = standardSeries.cos();

        final Series<BigRational> dsin = seriesAlgebra.differentiate(sin);

        System.out.println(seriesAlgebra.toString(cos, MAX));
        System.out.println(seriesAlgebra.toString(dsin, MAX));

        assertEquals(seriesAlgebra.toString(cos, MAX), seriesAlgebra.toString(dsin, MAX));
    }

    @Test
    void differentiate_sin_cos_cycle() {

        final Series<BigRational> sin = standardSeries.sin();

        final Series<BigRational> cos = seriesAlgebra.differentiate(sin);
        final Series<BigRational> msin = seriesAlgebra.differentiate(cos);
        final Series<BigRational> mcos = seriesAlgebra.differentiate(msin);

        final Series<BigRational> dsin = seriesAlgebra.differentiate(mcos);

        System.out.println(seriesAlgebra.toString(sin, MAX));
        System.out.println(seriesAlgebra.toString(dsin, MAX));

        assertEquals(seriesAlgebra.toString(sin, MAX), seriesAlgebra.toString(dsin, MAX));
    }

    @Test
    void integrate() {
        final Series<BigRational> sin = standardSeries.sin();
        final Series<BigRational> cos = standardSeries.cos();
        final Series<BigRational> icos = seriesAlgebra.integrate(cos);

        System.out.println(seriesAlgebra.toString(cos, MAX));
        System.out.println(seriesAlgebra.toString(icos, MAX));

        assertEquals(seriesAlgebra.toString(sin, MAX), seriesAlgebra.toString(icos, MAX));

    }
}