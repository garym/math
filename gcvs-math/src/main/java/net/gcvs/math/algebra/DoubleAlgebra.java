package net.gcvs.math.algebra;

public class DoubleAlgebra implements Algebra<Double> {
    public static final Double ZERO = 0.0;
    public static final Double ONE = 1.0;

    @Override
    public Double zero() {
        return ZERO;
    }

    @Override
    public Double one() {
        return ONE;
    }

    @Override
    public Double constant(long value) {
        return (double) value;
    }

    @Override
    public Double add(Double a, Double b) {
        return a + b;
    }

    @Override
    public Double subtract(Double a, Double b) {
        return a - b;
    }

    @Override
    public Double multiply(Double a, Double b) {
        return a * b;
    }

    @Override
    public Double divide(Double a, Double b) {
        return a / b;
    }

    @Override
    public DivMod<Double> divideAndRemainder(Double a, Double b) {
        double mod = a % b;
        // TODO this works wierdly for negative numbers, fix code, and test
        return new DivMod<>(Math.floor(a / b), mod);
    }

    @Override
    public Double[] newArray(int length) {
        return new Double[length];
    }

    @Override
    public String toString(Double a) {
        return a.toString();
    }
}
