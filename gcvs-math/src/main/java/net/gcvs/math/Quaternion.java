package net.gcvs.math;

public record Quaternion(double p0, double p1, double p2, double p3) {
    public static final Quaternion ONE = new Quaternion(1, 0, 0, 0);

    public Quaternion add(Quaternion q) {
        return new Quaternion(p0 + q.p0, p1 + q.p1, p2 + q.p2, p3 + q.p3);
    }

    public Quaternion subtract(Quaternion q) {
        return new Quaternion(p0 - q.p0, p1 - q.p1, p2 - q.p2, p3 - q.p3);
    }

    public Quaternion conjugate() {
        return new Quaternion(p0, -p1, -p2, -p3);
    }


    public Quaternion multiply(Quaternion q) {
        final double q0 = q.p0, q1 = q.p1, q2 = q.p2, q3 = q.p3;
        // p0q0 - p.q + p0q + q0p + pxq
        return new Quaternion(p0 * q0 - p1 * q1 - p2 * q2 - p3 * q3, // p0q0 - p.q
                p0 * q1 + q0 * p1 + p2 * q3 - p3 * q2,
                p0 * q2 + q0 * p2 + p3 * q1 - p1 * q3,
                p0 * q3 + q0 * p3 + p1 * q2 - p2 * q1
        );
    }

    /**
     * same as p* ⊗ p
     */
    public double norm() {
        return Math.sqrt(p0 * p0 + p1 * p1 + p2 * p2 + p3 * p3);
    }

    public Quaternion inverse() {
        final double n = p0 * p0 + p1 * p1 + p2 * p2 + p3 * p3;
        return new Quaternion(p0 / n, -p1 / n, -p2 / n, -p3 / n);
    }

    public Quaternion unitRotation(double theta) {
        // assume this quaternion is pure, p0 will be ignored
        // assert p0 == 0
        final double half = theta / 2.0;
        final double sinnorm = Math.sin(half) / norm();
        return new Quaternion(Math.cos(half), p1 * sinnorm, p2 * sinnorm, p3 * sinnorm);
    }

}
