package net.gcvs.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IntArrayTest {

    @Test
    void noargs_constructor() {
        final IntArray array = new IntArray();
        assertEquals(0, array.size());
    }

    @Test
    void capacity_constructor() {
        final IntArray array = new IntArray(2);
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        array.add(5);
        assertEquals(5, array.size());
        assertEquals(1, array.get(0));
        assertEquals(2, array.get(1));
        assertEquals(3, array.get(2));
        assertEquals(4, array.get(3));
        assertEquals(5, array.get(4));
    }

    @Test
    void array_constructor() {
        final IntArray array = new IntArray(new int[]{4, 6, 8});
        assertEquals(3, array.size());
        assertEquals(4, array.get(0));
        assertEquals(6, array.get(1));
        assertEquals(8, array.get(2));
    }

    @Test
    void add() {
        final IntArray array = new IntArray();
        array.add(1);
        assertEquals(1, array.get(0));
        array.add(345);
        assertEquals(345, array.get(1));
    }

    @Test
    void toArray() {
        final IntArray array = new IntArray();
        assertArrayEquals(new int[0], array.toArray());

        array.add(7);
        assertArrayEquals(new int[]{7}, array.toArray());

        array.add(9);
        assertArrayEquals(new int[]{7, 9}, array.toArray());
    }

    @Test
    void get() {
        final IntArray array = new IntArray(new int[]{8, 4, 2});
        assertEquals(8, array.get(0));
        assertEquals(4, array.get(1));
        assertEquals(2, array.get(2));
        array.add(9);
        assertEquals(9, array.get(3));

        assertThrows(IndexOutOfBoundsException.class, () -> array.get(4));
    }

    @Test
    void size() {
        final IntArray array = new IntArray();
        assertEquals(0, array.size());
        for (int i = 1; i < 10; i++) {
            array.add(i);
            assertEquals(i, array.size());
        }
    }

    @Test
    void remove() {
        final IntArray array = new IntArray(new int[]{3, 5, 9});
        int removed = array.remove(1);
        assertEquals(5, removed);
        assertEquals(2, array.size());
        assertEquals(3, array.get(0));
        assertEquals(9, array.get(1));

        assertThrows(IndexOutOfBoundsException.class, () -> array.remove(2));
    }

    @Test
    void remove_2() {
        final IntArray array = new IntArray(new int[]{1, 2, 3, 4, 5, 6});
        assertArrayEquals(array.toArray(), new int[]{1, 2, 3, 4, 5, 6});
        array.remove(5);
        assertArrayEquals(array.toArray(), new int[]{1, 2, 3, 4, 5});
        array.remove(0);
        assertArrayEquals(array.toArray(), new int[]{2, 3, 4, 5});
        array.remove(2);
        assertArrayEquals(array.toArray(), new int[]{2, 3, 5});
        array.remove(2);
        assertArrayEquals(array.toArray(), new int[]{2, 3});
        array.remove(0);
        assertArrayEquals(array.toArray(), new int[]{3});
        array.remove(0);
        assertArrayEquals(array.toArray(), new int[]{});
    }

    @Test
    void stream() {
        final IntArray array = new IntArray(new int[]{1, 2, 3, 4, 5, 6});
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, array.stream().toArray());
    }

    @Test
    void testToString() {
        assertEquals("[]", new IntArray().toString());
        assertEquals("[5]", new IntArray(new int[]{5}).toString());
        assertEquals("[1, 2]", new IntArray(new int[]{1, 2}).toString());
        assertEquals("[2, 4, 6]", new IntArray(new int[]{2, 4, 6}).toString());
    }

    @Test
    void contains() {
        final IntArray array = new IntArray(new int[]{1, 2, 3, 5, 6});
        assertEquals(true, array.contains(1));
        assertEquals(true, array.contains(3));
        assertEquals(true, array.contains(5));
        assertEquals(true, array.contains(6));

        assertEquals(false, array.contains(4));
        assertEquals(false, array.contains(9));
    }
}