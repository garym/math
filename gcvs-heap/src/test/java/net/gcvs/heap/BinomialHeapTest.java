package net.gcvs.heap;

import net.gcvs.util.Shuffle;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BinomialHeapTest {

    Shuffle shuffle = new Shuffle(new Random());

    @Test
    public void test() {
        final int size = 1000;
        TotalOrderOracle<Integer> comparator = new TotalOrderOracle<>(size, Integer::compare);
        Heap<Integer> heap = new BinaryHeap<>(size, comparator);

        Integer[] sorted = new Integer[size];
        Arrays.setAll(sorted, i -> i + 1);
        Integer[] unsorted = sorted.clone();
        shuffle.shuffle(unsorted);

        for (Integer i : unsorted) {
            heap.insert(i);
        }

        comparator.report();

        Integer top = heap.extractMin();
        assertTrue(top == sorted[0]);
        for (int i = 1; i < size; i++) {
            Integer min = heap.extractMin();
            assertTrue(min >= top);
            assertTrue(min == sorted[i]);
            top = min;
        }
        comparator.report();
        comparator.reset();

        Integer[] ints = unsorted.clone();
        Arrays.sort(ints, comparator);
        comparator.report();

    }
}
