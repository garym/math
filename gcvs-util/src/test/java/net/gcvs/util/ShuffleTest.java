package net.gcvs.util;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ShuffleTest {

    Random random = new Random(0);
    Shuffle shuffle = new Shuffle(random);

    @Test
    void getRandom() {
        assertEquals(random, shuffle.getRandom());
    }

    @Test
    void shuffleInts() {
        int[] ints = Sample.naturals(10);
        shuffle.shuffle(ints);
        Arrays.sort(ints);
        assertArrayEquals(Sample.naturals(10), ints);
    }

    @Test
    void shuffleLongs() {
        int[] ints = Sample.naturals(10);
        long[] list = new long[ints.length];
        Arrays.setAll(list, i -> ints[i]);
        long[] longs = list.clone();
        shuffle.shuffle(longs);
        Arrays.sort(longs);
        assertArrayEquals(list, longs);
    }

    @Test
    void testShuffle1() {
        int[] ints = Sample.naturals(10);
        Object[] list = new Object[ints.length];
        Arrays.setAll(list, i -> ints[i]);
        Object[] objects = list.clone();
        shuffle.shuffle(objects);
        Arrays.sort(objects);
        assertArrayEquals(list, objects);
    }

    @Test
    void testShuffle2() {
        int[] ints = Sample.naturals(10);
        Integer[] list = new Integer[ints.length];
        Arrays.setAll(list, i -> ints[i]);
        final List<Integer> original = Arrays.asList(list);
        List<Integer> objects = new ArrayList<>(original);
        shuffle.shuffle(objects);
        Collections.sort(objects);
        assertEquals(original, objects);
    }
}