package net.gcvs.primes;

import static net.gcvs.primes.PrimeSet.*;

/**
 * experimental
 */
public class PrimeGeneratorOddBitsUnroll2 implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        // primes = 0..n
        long[] primes = new long[((n - 1) >> 7) + 1];
        final int nextPrime = Stripes.sieveStripe2(primes);

        final int nmod = ((n - 1) >> 1) & 0x3f;
        if (nmod != 0x3f) {
            primes[primes.length - 1] &= (1L << (nmod + 1)) - 1;
        }
        int sqrt = (int) Math.ceil(Math.sqrt(n));

        // first sieve primes less than 64 - they can have more than one multiple per 64bit long
        int minsqrt = Math.min(63, sqrt);
        int p;
        for (p = nextSetBit(primes, nextPrime); p != -1 && p <= minsqrt; p = nextSetBit(primes, p + 2)) {
            final int step = p << 1;
            for (int m = p * p; m >= 0 && m <= n; m += step) {
                clear(primes, m);
            }
        }
        final int max = n >> 1;
        if (p != -1) {
            int q = nextSetBit(primes, p + 2);
            while (q != -1 && q <= sqrt) {
                int mp = (p * p) >> 1;
                int mq = (q * q) >> 1;
                for (; mq <= max; mp += p, mq += q) {
                    primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                }
                for (; mp <= max; mp += p) {
                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                }
                p = nextSetBit(primes, q + 2);
                q = nextSetBit(primes, p + 2);
            }
            if (p != -1 && p <= sqrt) {
                for (int mp = (p * p) >> 1; mp <= max; mp += p) {
                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                }
            }
        }

        return expandToArray(primes);
    }
}
