package net.gcvs.math.polynomial;

import net.gcvs.math.BigRational;
import net.gcvs.math.algebra.BigRationalAlgebra;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplySeriesTest {

    BigRationalAlgebra algebra = new BigRationalAlgebra();
    SeriesAlgebra<BigRational> seriesAlgebra = new SeriesAlgebra<>(algebra);
    PolyAlgebra<BigRational> polyAlgebra = new PolyAlgebra<>(algebra);

    @Test
    void multiply() {
        TermGenerator<BigRational> a = BigRational::valueOf;

//        final Series<BigRational> multiply = seriesAlgebra.multiply(a, a);

        // 3x^2 + 2x + 1
        final Poly<BigRational> poly = polyAlgebra.withRoots(new BigRational(1), new BigRational(2), new BigRational(3));

        final Series<BigRational> prod = seriesAlgebra.multiply(poly, poly);


        System.out.println(poly);
        System.out.println(seriesAlgebra.toString(poly, 100));
        System.out.println(seriesAlgebra.toString(prod, 100));
    }

    @Test
    void multiply_constant_constant() {

        final Series<BigRational> a = seriesAlgebra.constant(2);
        final Series<BigRational> b = seriesAlgebra.constant(3);

        final Series<BigRational> c = seriesAlgebra.multiply(a, b);

        final Poly<BigRational> poly = polyAlgebra.of(c, 10);

        assertEquals(1, poly.length());
        assertEquals(new BigRational(6), poly.coefficient(0));
    }

    @Test
    void multiply_constant_x() {

        final Series<BigRational> a = seriesAlgebra.constant(2);
        final Series<BigRational> b = polyAlgebra.poly(algebra.zero(), new BigRational(5));

        final Series<BigRational> c = seriesAlgebra.multiply(a, b);

        final Poly<BigRational> poly = polyAlgebra.of(c, 10);

        assertEquals(2, poly.length());
        assertEquals(new BigRational(0), poly.coefficient(0));
        assertEquals(new BigRational(10), poly.coefficient(1));
    }

    @Test
    void multiply_linear_linear() {

        final Series<BigRational> a = polyAlgebra.poly(new BigRational(1), new BigRational(1));
        final Series<BigRational> b = polyAlgebra.poly(new BigRational(1), new BigRational(1));

        final Series<BigRational> c = seriesAlgebra.multiply(a, b);

        final Poly<BigRational> poly = polyAlgebra.of(c, 10);

        assertEquals(3, poly.length());
        assertEquals(new BigRational(1), poly.coefficient(0));
        assertEquals(new BigRational(2), poly.coefficient(1));
        assertEquals(new BigRational(1), poly.coefficient(2));
    }

    @Test
    void multiply_linear_linear_term_gaps() {

        final Series<BigRational> a = polyAlgebra.poly(new BigRational(-1), algebra.zero(), new BigRational(1));
        final Series<BigRational> b = polyAlgebra.poly(algebra.zero(), new BigRational(-3), algebra.zero(), new BigRational(2));

        Series<BigRational> c = seriesAlgebra.multiply(a, b);

        for (int i = 0; i < 2; i++) {
            final Poly<BigRational> poly = polyAlgebra.of(c, 10);

            // (x2-1)(2x3 - 3x) = 2x5 -5x3 + 3x

            assertEquals(6, poly.length());
            assertEquals(new BigRational(0), poly.coefficient(0));
            assertEquals(new BigRational(3), poly.coefficient(1));
            assertEquals(new BigRational(0), poly.coefficient(2));
            assertEquals(new BigRational(-5), poly.coefficient(3));
            assertEquals(new BigRational(0), poly.coefficient(4));
            assertEquals(new BigRational(2), poly.coefficient(5));

            // again, with series reversed
            c = seriesAlgebra.multiply(b, a);
        }
    }

    @Test
    @Disabled
    void multiply_series_series() {

        final TermGenerator<BigRational> a = BigRational::valueOf;

        // x + 1
        final Series<BigRational> b = polyAlgebra.poly(new BigRational(1), new BigRational(1));

        final Series<BigRational> c = seriesAlgebra.multiply(a, b);

        final Poly<BigRational> poly = polyAlgebra.of(c, 10);

        assertEquals(3, poly.length());
        assertEquals(new BigRational(1), poly.coefficient(0));
        assertEquals(new BigRational(2), poly.coefficient(1));
        assertEquals(new BigRational(1), poly.coefficient(2));
    }

}