package net.gcvs.math.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static java.util.Objects.isNull;

@FunctionalInterface
public interface TermGenerator<V> extends Series<V> {

    /**
     * @return next term in
     */
    V getCoefficient(long term);

    default Term<V> getTerm(long term) {
        final V coefficient = getCoefficient(term);
        if (isNull(coefficient)) {
            return null;
        }
        return new Term<>(term, coefficient);
    }

    default Iterator<Term<V>> iterator() {
        return new Iterator<>() {
            long term = 0;
            boolean hasNext = true;
            Term<V> next;

            @Override
            public boolean hasNext() {
                if (hasNext && next == null) {
                    // only call generator once next value is required
                    next = getTerm(term);
                    hasNext = next != null;
                }
                return hasNext;
            }

            @Override
            public Term<V> next() {
                if (!hasNext()) throw new NoSuchElementException();
                term++;
                Term<V> value = next;
                // reset next term
                next = null;
                return value;
            }
        };
    }
}
