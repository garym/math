package net.gcvs.math;

import java.math.BigInteger;

public class PellEquation {
    private int d;

    // x0 < y0;
    // y0^2 = d * x0^2 + 1
    private BigInteger y0, x0;

    public PellEquation(final int d) {
        this.d = d;

        long y = 0, x = 0;
        for (int xi = 1; xi < 100; xi++) {
            long y2 = d * xi * xi + 1;
            long yi = (long) Math.sqrt(y2);
            if (yi * yi == y2) {
                x = xi;
                y = yi;
                break;
            }
        }

        this.y0 = BigInteger.valueOf(y);
        this.x0 = BigInteger.valueOf(x);
    }

    public BigInteger[] iterate(int steps) {
        BigInteger y1 = y0;
        BigInteger x1 = x0;


        // (y1 + x1.root(d)) . (x0 + y0.rood(d))
        for (int i = 0; i < steps; i++) {
            BigInteger y2 = y0.multiply(y1).add(x0.multiply(x1).multiply(BigInteger.valueOf(d)));
            BigInteger x2 = y1.multiply(x0).add(y0.multiply(x1));

            y1 = y2;
            x1 = x2;
        }

        return new BigInteger[]{x1, y1};
    }
}
