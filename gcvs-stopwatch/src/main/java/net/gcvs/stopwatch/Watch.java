package net.gcvs.stopwatch;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Watch {

    String value() default "";

    boolean global() default false;

    boolean log() default false;

    boolean before() default false;

    boolean logResult() default false;

    long slowLog() default Long.MAX_VALUE;
}
