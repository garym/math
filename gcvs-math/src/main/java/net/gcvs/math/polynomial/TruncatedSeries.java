package net.gcvs.math.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class TruncatedSeries<V> implements Series<V> {
    private final Series<V> series;
    private final long maxDegree;

    public TruncatedSeries(final Series<V> series, final long maxDegree) {
        this.series = series;
        this.maxDegree = maxDegree;
    }

    public Series<V> getSeries() {
        return series;
    }

    public long getMaxDegree() {
        return maxDegree;
    }

    @Override
    public Iterator<Term<V>> iterator() {
        return new Iterator<>() {
            final Iterator<Term<V>> it = series.iterator();
            boolean hasNext = true;
            Term<V> next;

            @Override
            public boolean hasNext() {
                if (hasNext && next == null) {
                    if (it.hasNext()) {
                        next = it.next();
                        if (next.degree() > maxDegree) {
                            next = null;
                        }
                    }
                    hasNext = next != null;
                }
                return hasNext;
            }

            @Override
            public Term<V> next() {
                if (!hasNext()) throw new NoSuchElementException();
                Term<V> value = next;
                next = null;
                return value;
            }
        };
    }
}
