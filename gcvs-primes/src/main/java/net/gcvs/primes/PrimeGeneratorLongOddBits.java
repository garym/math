package net.gcvs.primes;

import static net.gcvs.primes.PrimeSet.*;

/**
 * experimental
 */
public class PrimeGeneratorLongOddBits implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final long[] EMPTY_LONG_ARRAY = new long[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;
    private static final int BITS_PER_WORD = 1 << ADDRESS_BITS_PER_WORD;

    /* Used to shift left or right for a partial word mask */
    private static final long WORD_MASK = 0xffffffffffffffffL;

    private static final long MAX_LONG = (Integer.MAX_VALUE - 2L) << 7;


    static void clear(long[] words, long bitIndex) {
        bitIndex >>= 1;
        int wordIndex = longWordIndex(bitIndex);
        words[wordIndex] &= ~(1L << bitIndex);
    }

    private static void set(long[] words, long bitIndex) {
        bitIndex >>= 1;
        int wordIndex = longWordIndex(bitIndex);
        words[wordIndex] |= (1L << bitIndex);
    }

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        PrimeSet primes = primeSet(n);

        return expandToArray(primes.words);
    }

    public long[] longArray(long n) {
        if (n < 2) return EMPTY_LONG_ARRAY;
        PrimeSet primes = primeSet(n);

        return expandToLongArray(primes.words);
    }

    public PrimeSet primeSet(long n) {
        if (n < 0 || n > MAX_LONG) {
            throw new IllegalArgumentException();
        }
        // primes = 0..n
        PrimeSet primeSet = new PrimeSet(Math.toIntExact(((n - 1) >> 7) + 1));
        long[] primes = primeSet.words;
        final int nextPrime = Stripes.sieveStripe2(primes);

        // cap at n, clear bits beyond n
        final int nmod = Math.toIntExact(((n - 1) >> 1) & 0x3f);
        if (nmod != 0x3f) {
            primes[primes.length - 1] &= (1L << (nmod + 1)) - 1;
        }

        final int sqrt = (int) Math.ceil(Math.sqrt(n));
        final int minsqrt = Math.min(63, sqrt);
        long p;
        for (p = nextLongSetBit(primes, nextPrime); p != -1 && p <= minsqrt; p = nextLongSetBit(primes, p + 2)) {
            final long step = p << 1;
            for (long m = p * p; m >= 0 && m <= n; m += step) {
                clear(primes, m);
            }
        }
        for (long max = n >> 1; p != -1 && p <= sqrt; p = nextLongSetBit(primes, p + 2)) {
            for (long mp = (p * p) >> 1; mp <= max; mp += p) {
                primes[(int) (mp >> ADDRESS_BITS_PER_WORD)] &= ~(1L << mp);
            }
        }
        return primeSet;
    }
}
