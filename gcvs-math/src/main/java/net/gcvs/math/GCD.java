package net.gcvs.math;

import java.math.BigInteger;

public class GCD {

    public static BigInteger gcd(final BigInteger a, final BigInteger b) {
        if (a.signum() < 0 || b.signum() < 0) {
            throw new ArithmeticException();
        }
        // assert: a >= 0, b >= 0
        if (a.compareTo(b) > 0) {
            return _gcd(b, a);
        }
        return _gcd(a, b);
    }

    /**
     * 0 < a <= b
     */
    private static BigInteger _gcd(final BigInteger a, final BigInteger b) {
        // assert: 0 < a <= b
        if (BigInteger.ONE.compareTo(a) == 0) {
            return BigInteger.ONE;
        }
        // assert: a <= b
        final BigInteger mod = b.mod(a);
        if (mod.signum() == 0) {
            return a;
        }
        return _gcd(mod, a);
    }

    /**
     * returns three BigIntegers: [gcd(a, b), a / gcd(a, b), b / gcd(a, b)]<br>
     * turns out division is not expensive enough to warrant this method.<br>
     * This is faster:<br>
     * f = gcd(a, b); am = a.divide(f); bm = b.divide(f);
     *
     * @param a
     * @param b
     * @return
     */
    public static BigInteger[] gcdm(final BigInteger a, final BigInteger b) {
        if (a.signum() < 0 || b.signum() < 0) {
            throw new ArithmeticException();
        }
        // assert: a >= 0, b >= 0
        if (a.compareTo(b) > 0) {
            // reverse gcd calculations
            final BigInteger[] result = gcdm(b, a);
            // reverse the multiples
            final BigInteger tmp = result[1];
            result[1] = result[2];
            result[2] = tmp;
        }
        // assert: a <= b
        final BigInteger[] divmod = b.divideAndRemainder(a);
        final BigInteger div = divmod[0];
        final BigInteger mod = divmod[1];
        if (mod.signum() == 0) {
            // b = div * a
            // return [a, a / a = 1, b / a]
            return new BigInteger[]{a, BigInteger.ONE, div};
        }

        // b = div * a + mod
        final BigInteger[] result = gcdm(mod, a);
        // result = [gcd(mod, a), mod / gcd(mod, a), a / gcd(mod, a)]
        BigInteger modfactor = result[1];
        BigInteger afactor = result[2];
        result[1] = afactor;
        result[2] = modfactor.multiply(div).add(afactor);
        return result;
    }


    static int func_gcd(final int a, final int b) {
        if (b > a) {
            return ageb_gcd(b, a);
        }
        return ageb_gcd(a, b);
    }

    private static int ageb_gcd(final int a, final int b) {
        if (b == 0) {
            return a;
        }
        if (b == 1) {
            return 1;
        }
        return ageb_gcd(b, a % b);
    }

    public static int gcd(final int _a, final int _b) {
        int x, y;
        if (_a >= _b) {
            x = _a;
            y = _b;
        } else {
            x = _b;
            y = _a;
        }

        int mod = x % y;
        while (mod > 1) {
            x = y;
            y = mod;
            mod = x % y;
        }
        if (mod == 0)
            return y;
        return 1;
    }

    public static long gcd(final long aa, final long bb) {
        long _a = Math.abs(aa), _b = Math.abs(bb);
        long x, y;
        if (_a >= _b) {
            x = _a;
            y = _b;
        } else {
            x = _b;
            y = _a;
        }
        if (y == 0) {
            return x;
        }

        long mod = x % y;
        while (mod > 1) {
            x = y;
            y = mod;
            mod = x % y;
        }
        if (mod == 0)
            return y;
        return 1;
    }

    public static int _1gcd(final int _a, final int _b) {
        final int a0, a1;
        if (_a >= _b) {
            a0 = _a;
            a1 = _b;
        } else {
            a0 = _b;
            a1 = _a;
        }

        int x = a0 % a1;
        if (x == 0)
            return a1;
        if (x == 1)
            return 1;
        int y = a1 % x;
        if (y == 0)
            return x;
        if (y == 1)
            return 1;
        int z = x % y;
        while (z > 1) {
            x = y;
            y = z;
            z = x % y;
        }
        if (z == 0)
            return y;
        return 1;
    }

    public static int __gcd(final int _a, final int _b) {
        final int a0, a1;
        if (_a >= _b) {
            a0 = _a;
            a1 = _b;
        } else {
            a0 = _b;
            a1 = _a;
        }
        final int a2 = a0 % a1;
        if (a2 == 0)
            return a1;
        if (a2 == 1)
            return 1;

//		final int a3 = a1 % a2;
//		if (a3 == 0) return a2;
//		if (a3 == 1) return 1;

        int x = a1 % a2;
        if (x == 0)
            return a2;
        if (x == 1)
            return 1;
        int y = a2 % x;
        if (y == 0)
            return x;
        if (y == 1)
            return 1;
        int z = x % y;
        while (z > 1) {
            x = y;
            y = z;
            z = x % y;
        }
        if (z == 0)
            return y;
        return 1;
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int sum = 0;
        int from = 10000000;
        int to = from + 20000;
        for (int i = from; i < to; i++) {
            for (int j = from; j < to; j++) {
                sum += gcd(i, j);
            }
        }
        System.out.println(sum); // 797778024
        System.out.println((System.currentTimeMillis() - start) + " millis");

        System.out.println("gcd(8, 6) = " + func_gcd(8, 6));
    }
}
