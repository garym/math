package net.gcvs.util;

public interface CheckedRunnable {
    void run() throws Exception;
}
