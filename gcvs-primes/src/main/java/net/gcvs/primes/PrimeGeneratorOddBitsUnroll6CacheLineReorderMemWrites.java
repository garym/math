package net.gcvs.primes;

import static net.gcvs.primes.PrimeSet.expandToArray;
import static net.gcvs.primes.PrimeSet.nextSetBit;

/**
 * experimental
 */
public class PrimeGeneratorOddBitsUnroll6CacheLineReorderMemWrites implements IntPrimeGenerator {
    private static final int[] EMPTY_ARRAY = new int[0];
    private static final int ADDRESS_BITS_PER_WORD = 6;

    /**
     * list of primes up to and including n
     *
     * @param n
     * @return
     */
    @Override
    public int[] intArray(int n) {
        if (n < 2) return EMPTY_ARRAY;
        // primes = 0..n
        long[] primes = new long[((n - 1) >> 7) + 1];
        final int primesLength = primes.length;
        final int nextPrime = Stripes.sieveStripe2(primes);

        final int nmod = ((n - 1) >> 1) & 0x3f;
        if (nmod != 0x3f) {
            primes[primesLength - 1] &= (1L << (nmod + 1)) - 1;
        }
        final int max = n >> 1;
        final int sqrt = (int) Math.ceil(Math.sqrt(n));
        final int minsqrt = Math.min(63, sqrt);
        int p;
        for (p = nextSetBit(primes, nextPrime); p != -1 && p <= minsqrt; p = nextSetBit(primes, p + 2)) {
            int bitIndex = (p * p) >> 1;
            int wordIndex = bitIndex >> ADDRESS_BITS_PER_WORD;
            for (; wordIndex < primesLength; wordIndex++) {
                bitIndex &= 63;
                long strip = primes[wordIndex];
                for (; bitIndex < 64; bitIndex += p) {
                    strip &= ~(1L << bitIndex);
                }
                primes[wordIndex] = strip;
            }
        }
        if (p != -1) {
            int q = nextSetBit(primes, p + 2);

            if (q != -1 && q <= sqrt) {
                int r = nextSetBit(primes, q + 2);

                if (r != -1 && r <= sqrt) {
                    int s = nextSetBit(primes, r + 2);

                    if (s != -1 && s <= sqrt) {
                        int t = nextSetBit(primes, s + 2);

                        if (t != -1 && t <= sqrt) {
                            int u = nextSetBit(primes, t + 2);

                            while (u != -1 && u <= sqrt) {
                                int mp = (p * p) >> 1;
                                int mq = (q * q) >> 1;
                                int mr = (r * r) >> 1;
                                int ms = (s * s) >> 1;
                                int mt = (t * t) >> 1;
                                int mu = (u * u) >> 1;
                                for (; mu <= max; mp += p, mq += q, mr += r, ms += s, mt += t, mu += u) {

                                    primes[mu >> ADDRESS_BITS_PER_WORD] &= ~(1L << mu);

                                    primes[mt >> ADDRESS_BITS_PER_WORD] &= ~(1L << mt);
                                    if (mu - mt >= t) {
                                        mt += t;
                                        primes[mt >> ADDRESS_BITS_PER_WORD] &= ~(1L << mt);
                                    }

                                    primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
                                    if (mt - ms >= s) {
                                        ms += s;
                                        primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
                                    }

                                    primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                    if (ms - mr >= r) {
                                        mr += r;
                                        primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                    }

                                    primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                    if (mr - mq >= q) {
                                        mq += q;
                                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                    }

                                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    if (mq - mp < p) {
                                        mp += p;
                                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    }

                                }
                                for (; mt <= max; mp += p, mq += q, mr += r, ms += s, mt += t) {

                                    primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
                                    if (mt - ms >= s) {
                                        ms += s;
                                        primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
                                    }

                                    primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                    if (ms - mr >= r) {
                                        mr += r;
                                        primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                    }

                                    primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                    if (mr - mq >= q) {
                                        mq += q;
                                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                    }

                                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    if (mq - mp >= p) {
                                        mp += p;
                                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    }

                                    primes[mt >> ADDRESS_BITS_PER_WORD] &= ~(1L << mt);

                                }
                                for (; ms <= max; mp += p, mq += q, mr += r, ms += s) {

                                    primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                    if (ms - mr >= r) {
                                        mr += r;
                                        primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                    }

                                    primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                    if (mr - mq >= q) {
                                        mq += q;
                                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                    }

                                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    if (mq - mp >= p) {
                                        mp += p;
                                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    }

                                    primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);

                                }
                                for (; mr <= max; mp += p, mq += q, mr += r) {

                                    primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                                    if (mr - mq >= q) {
                                        mq += q;
                                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                                    }

                                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    if (mq - mp >= p) {
                                        mp += p;
                                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    }

                                    primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);

                                }
                                for (; mq <= max; mp += p, mq += q) {
                                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    if (mq - mp >= p) {
                                        mp += p;
                                        primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                    }

                                    primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);

                                }
                                for (; mp <= max; mp += p) {
                                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                                }
                                p = nextSetBit(primes, u + 2);
                                q = nextSetBit(primes, p + 2);
                                r = nextSetBit(primes, q + 2);
                                s = nextSetBit(primes, r + 2);
                                t = nextSetBit(primes, s + 2);
                                u = nextSetBit(primes, t + 2);
                            }
                            // drain t
                            if (t != -1 && t <= sqrt) {
                                for (int mt = (t * t) >> 1; mt <= max; mt += t) {
                                    primes[mt >> ADDRESS_BITS_PER_WORD] &= ~(1L << mt);
                                }
                            }
                        }
                        // drain s
                        if (s != -1 && s <= sqrt) {
                            for (int ms = (s * s) >> 1; ms <= max; ms += s) {
                                primes[ms >> ADDRESS_BITS_PER_WORD] &= ~(1L << ms);
                            }
                        }
                    }
                    // drain r
                    if (r != -1 && r <= sqrt) {
                        for (int mr = (r * r) >> 1; mr <= max; mr += r) {
                            primes[mr >> ADDRESS_BITS_PER_WORD] &= ~(1L << mr);
                        }
                    }
                }
                // drain q
                if (q != -1 && q <= sqrt) {
                    for (int mq = (q * q) >> 1; mq <= max; mq += q) {
                        primes[mq >> ADDRESS_BITS_PER_WORD] &= ~(1L << mq);
                    }
                }
            }
            // drain p
            if (p != -1 && p <= sqrt) {
                for (int mp = (p * p) >> 1; mp <= max; mp += p) {
                    primes[mp >> ADDRESS_BITS_PER_WORD] &= ~(1L << mp);
                }
            }
        }

        return expandToArray(primes);
    }
}
