package net.gcvs.math.cf;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;

import net.gcvs.math.BigRational;
import net.gcvs.math.GCD;
import net.gcvs.util.IntArray;

/**
 * Immutable Simple Continued Fraction implementation.<br>
 * These are simple, regular, in canonical form; the fractions' numerators are always 1<br>
 * <p>
 * Credit to this website for supplying info on Continued Fractions.<br>
 * <a href="http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/cfINTRO.html">Continued Fractions</a>
 *
 * @author gary
 */
public class SimpleContinuedFraction implements Comparable<SimpleContinuedFraction> {

    public static final SimpleContinuedFraction PHI = silverMean(1);
    public static final SimpleContinuedFraction E = new SimpleContinuedFraction(new int[]{2},
            3, (period, phase) -> phase == 1 ? 2 * (period + 1) : 1);
    public static final SimpleContinuedFraction SQRT_E = nthRootOfE(2);
    public static final SimpleContinuedFraction E_POW_2 = new SimpleContinuedFraction(new int[]{7, 2},
            5, (period, phase) -> {
        if (phase == 2) {
            return 3 + 3 * period;
        }
        if (phase == 3) {
            return 18 + 12 * period;
        }
        if (phase == 4) {
            return 5 + 3 * period;
        }
        return 1;
    });
    /**
     * an approximation. truncated CF
     */
    public static final SimpleContinuedFraction PI = new SimpleContinuedFraction(3, 7, 15, 1, 292, 1, 1, 1, 2, 1, 3, 1, 14, 2, 1, 1, 2, 2, 2, 2, 1, 84, 2, 1, 1, 15, 3, 13, 1, 4, 2, 6, 6, 99, 1, 2, 2, 6, 3, 5, 1, 1, 6, 8, 1, 7, 1, 2, 3, 7, 1,
            2, 1, 1, 12, 1, 1, 1, 3, 1, 1, 8, 1, 1, 2, 1, 6, 1, 1, 5, 2, 2, 3, 1, 2, 4, 4, 16, 1, 161, 45, 1, 22, 1, 2, 2, 1, 4, 1, 2);

    /**
     * truncated CF
     */
    public static final SimpleContinuedFraction SQRT_PI = new SimpleContinuedFraction(1, 1, 3, 2, 1, 1, 6, 1, 28, 13, 1, 1, 2, 18, 1, 1, 1, 83, 1, 4, 1, 2, 4, 1, 288, 1, 90, 1, 12, 1, 1, 7, 1, 3, 1, 6, 1, 2, 71, 9, 3, 1, 5, 36, 1, 2, 2, 1, 1,
            1, 2, 5, 9, 8, 1, 7, 1, 2, 2, 1, 63, 1, 4, 3, 1, 6, 1, 1, 1, 5, 1, 9, 2, 5, 4, 1, 2, 1, 1, 2, 20, 1, 1, 2, 1, 10, 5, 2, 1, 100, 11, 1, 9, 1, 2, 1, 1, 1, 1, 3);

    /**
     * assert: head == null || head.length > 0<br>
     * i.e. head must not be an empty array.
     */
    private final int[] head;
    private final int[] repeating;
    private final IntUnaryOperator generator;

    /**
     * Create a terminating (finite) Continued Fraction from the given integer sequence<br>
     * <p>
     * [list[0]; list[1], list[2], ..., list[list.length - 1]]
     *
     * @param list
     */
    public SimpleContinuedFraction(final int... list) {
        if (list == null || list.length == 0) {
            throw new ArithmeticException("empty sequence");
        }
        this.head = list.clone();
        this.repeating = null;
        this.generator = null;
        validateHead();
    }

    /**
     * Create an infinite periodic Continued Fraction from a fixed integer sequence prefix and an infinitely repeating
     * sequence
     *
     * @param head
     * @param repeating
     */
    public SimpleContinuedFraction(final int[] head, final int[] repeating) {
        this.head = head == null || head.length == 0 ? null : head.clone();
        this.repeating = repeating == null || repeating.length == 0 ? null : repeating.clone();
        this.generator = null;
        if (this.head == null && this.repeating == null) {
            throw new ArithmeticException("empty sequence");
        }
        validateHead();
    }

    /**
     * Create an infinite Continued Fraction entirely from a generator function.<br>
     * <p>
     * Indexes passed to the generator function start from zero<br>
     *
     * @param generator
     */
    public SimpleContinuedFraction(final IntUnaryOperator generator) {
        this(null, generator);
    }

    /**
     * Create an infinite Continued Fraction from the fixed integer sequence prefix and a generator function.<br>
     * <p>
     * Indexes passed to the generator function start from zero. i.e. indexes start from zero after the initial prefix
     * sequence<br>
     *
     * @param head
     * @param generator
     */
    public SimpleContinuedFraction(final int[] head, final IntUnaryOperator generator) {
        this.head = head == null || head.length == 0 ? null : head.clone();
        this.repeating = null;
        this.generator = generator;
        validateHead();
    }

    /**
     * Create an infinite Continued Fraction from the fixed integer sequence prefix and a periodic generator function.<br>
     * <p>
     * the periodicGenerator is passed the period number and the phase. e.g. for periodLength 3, the sequence is generated
     * by the following calls:<br>
     * (0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2), etc.<br>
     *
     * @param head
     * @param periodLength
     * @param periodicGenerator
     */
    public SimpleContinuedFraction(final int[] head, final int periodLength, final IntBinaryOperator periodicGenerator) {
        this.head = head == null || head.length == 0 ? null : head.clone();
        this.repeating = null;
        this.generator = idx -> periodicGenerator.applyAsInt(idx / periodLength, idx % periodLength);
        validateHead();
    }

    private void validateHead() {
        if (head == null) {
            return;
        }
        for (int i = 1; i < head.length; i++) {
            int v = head[i];
            if (v == 0) {
                throw new RuntimeException();
            }
        }
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        String sep = "";
        if (head != null && head.length > 0) {
            sb.append(head[0]);
            sep = "; ";
            for (int i = 1; i < head.length; i++) {
                sb.append(sep);
                sep = ", ";
                sb.append(head[i]);
            }

        }
        if (repeating != null && repeating.length > 0) {
            for (int i = 0; i < repeating.length; i++) {
                sb.append(sep);
                sep = ", ";
                String num = Integer.toString(repeating[i]);
                for (int j = 0; j < num.length(); j++) {
                    sb.append(num.charAt(j));
                    sb.append("\u0332"); // unicode underline character
                }
            }
        }
        if (generator != null) {
            for (int i = 0; i < 10; i++) {
                sb.append(sep);
                sep = ", ";
                sb.append(head[i]);
            }
            sb.append(sep);
            sb.append("...");
        }
        sb.append(']');
        return sb.toString();
    }

    public int get(final int idx) {
        if (idx < 0) {
            throw new RuntimeException();
        }
        final int headLength = head == null ? 0 : head.length;
        if (idx < headLength) {
            return head[idx];
        }
        if (repeating != null) {
            return repeating[(idx - headLength) % repeating.length];
        }
        if (generator != null) {
            int start = idx - headLength;
            if (generator == null) {
                return 0;
            }

            return generator.applyAsInt(start);
        }
        return 0;
    }

    public BigRational toBigRational() {
        if (!isTerminating()) {
            throw new RuntimeException();
        }
        return backwardConvergent(length());
    }

    public int headLength() {
        return head.length;
    }

    public int repeatingLength() {
        return repeating == null ? 0 : repeating.length;
    }

    /**
     * return the Convergent rational using the backward algorithm.
     *
     * @param prefix
     * @return
     */
    public BigRational backwardConvergent(final int prefix) {
        BigInteger num = BigInteger.valueOf(get(prefix - 1));
        BigInteger den = BigInteger.ONE;
        for (int i = prefix - 2; i >= 0; i--) {
            BigInteger t = num;
            num = den;
            den = t;
            num = num.add(BigInteger.valueOf(get(i)).multiply(den));
        }
        return new BigRational(num, den);
    }

    /**
     * return the Convergent rational using the forward algorithm.
     *
     * @param index
     * @return
     */
    public BigRational forwardConvergent(final int index) {
        BigInteger n0 = BigInteger.ZERO;
        BigInteger d0 = BigInteger.ONE;
        BigInteger n1 = BigInteger.ONE;
        BigInteger d1 = BigInteger.ZERO;
        for (int i = 0; i <= index; i++) {
            BigInteger c = BigInteger.valueOf(get(i));
            BigInteger n2 = c.multiply(n1).add(n0);
            BigInteger d2 = c.multiply(d1).add(d0);
            n0 = n1;
            n1 = n2;
            d0 = d1;
            d1 = d2;
        }
        // TODO n1 / d1 is already in its lowest terms
        return new BigRational(n1, d1);
    }

    /**
     * return the Convergent rational using the forward algorithm. finds the rational with the largest denominator less than
     * or equal to the given denominator.
     *
     * @param denominator
     * @return
     */
    public BigRational getConvergentWithMaxDenominator(final BigInteger denominator) {
        BigInteger n0 = BigInteger.ZERO;
        BigInteger d0 = BigInteger.ONE;
        BigInteger n1 = BigInteger.ONE;
        BigInteger d1 = BigInteger.ZERO;
        for (int i = 0, len = length(); i <= len; i++) {
            BigInteger c = BigInteger.valueOf(get(i));
            BigInteger n2 = c.multiply(n1).add(n0);
            BigInteger d2 = c.multiply(d1).add(d0);

            if (d2.compareTo(denominator) > 0) {
                return new BigRational(n1, d1);
            }

            n0 = n1;
            n1 = n2;
            d0 = d1;
            d1 = d2;
        }
        return new BigRational(n1, d1);
    }

    /**
     * creates a terminating continued fraction up to and including the given index.
     *
     * @param lastIndex
     * @return
     */
    public SimpleContinuedFraction prefix(final int lastIndex) {
        if (lastIndex < 0) {
            throw new RuntimeException();
        }
        int[] head = new int[lastIndex + 1];
        for (int i = 0; i <= lastIndex; i++) {
            head[i] = get(i);
        }
        return new SimpleContinuedFraction(head);
    }

    /**
     * T(n) = n + 1 / T(n)
     *
     * @param n
     * @return
     */
    public static SimpleContinuedFraction silverMean(final int n) {
        final int[] list = {n};
        return new SimpleContinuedFraction(list, list);
    }

    public static SimpleContinuedFraction nthRootOfE(final int n) {
        return new SimpleContinuedFraction(new int[]{1}, 3, (period, phase) -> {
            if (phase == 0) {
                return n * (2 * period + 1) - 1;
            }
            return 1;
        });
    }

    /**
     * Generates the Continued Fraction of sqrt(n)<br>
     * <p>
     * CFs of all square roots are known to be periodic.
     *
     * @param n
     * @return
     */
    public static SimpleContinuedFraction sqrt(final int n) {
        final int m0 = (int) Math.sqrt(n);
        if (m0 * m0 == n) {
            return new SimpleContinuedFraction(m0);
        }

        // maintain an expression: x = (a * sqrt(n) + b) / c
        // initially x = sqrt(n)
        int a = 1;
        int b = 0;
        int c = 1;

        final IntArray list = new IntArray();
        for (; ; ) {
            // System.out.println("x = (" + a + " sqrt(" + n + ") + " + b + " ) / " + c);

            int m = (a * m0 + b) / c;
            // System.out.println("m = " + m);
            list.add(m);

            // (a * sqrt(n) + b) / c = m + 1 / x

            // a * sqrt(n) + b = mc + c/x
            // (a sqrt(n) + (b - mc)) = c/x
            // x = c / (a sqrt(n) + (b - mc))
            // x = c (a sqrt(n) - (b - mc)) / (a*a*n - (b - mc) ^ 2)

            int mcb = m * c - b;
            int a2 = c * a;
            int b2 = c * mcb;
            int c2 = a * a * n - mcb * mcb;

            int gcd = GCD.gcd(GCD.gcd(a2, b2), c2);
            a2 /= gcd;
            b2 /= gcd;
            c2 /= gcd;

            a = a2;
            b = b2;
            c = c2;

            if (a == 1 && b == m0 && c == 1) {
                list.add(2 * m0);
                break;
            }
        }
        list.remove(0);
        return new SimpleContinuedFraction(new int[]{m0}, list.toArray());
    }

    /**
     * returns the CF generated by 1 / this<br>
     * i.e. either adds or removes a zero from the head
     *
     * @return
     * @TODO an optimized approach might yield this.reciprocal().reciprocal() == this
     */
    public SimpleContinuedFraction reciprocal() {
        if (get(0) == 0) {
            return new SimpleContinuedFraction(idx -> get(idx + 1));
        }
        return new SimpleContinuedFraction(idx -> idx <= 0 ? 0 : get(idx - 1));
    }

    public boolean isTerminating() {
        return repeating == null && generator == null;
    }

    public int length() {
        if (isTerminating()) {
            // if head is null, this CF is invalid
            return head.length;
        }
        // TODO is it logically elegant to return this value for practical purposes?
        return Integer.MAX_VALUE;
    }

    public BigInteger h(final int n) {
        if (n >= length()) {
            throw new RuntimeException();
        }
        if (n <= -2) {
            return BigInteger.ZERO;
        }
        if (n == -1) {
            return BigInteger.ONE;
        }
        return h(n - 1).multiply(BigInteger.valueOf(get(n))).add(h(n - 2));
    }

    public BigInteger k(final int n) {
        if (n >= length()) {
            throw new RuntimeException();
        }
        if (n < -2) {
            return BigInteger.ZERO;
        }
        if (n == -2) {
            return BigInteger.ONE;
        }
        if (n == -1) {
            return BigInteger.ZERO;
        }
        return k(n - 1).multiply(BigInteger.valueOf(get(n))).add(k(n - 2));
    }

    public static SimpleContinuedFraction valueOf(final BigRational r) {
        if (r.signum() < 0) {
            throw new RuntimeException("not supported right now, try valueOf(r.negate())");
        }
        final IntArray list = new IntArray();

        BigInteger num = r.getNumerator();
        BigInteger den = r.getDenominator();
        for (; ; ) {
            BigInteger[] divmod = num.divideAndRemainder(den);
            list.add(divmod[0].intValueExact());
            num = den;
            den = divmod[1];
            if (den.signum() == 0) {
                break;
            }
        }

        return new SimpleContinuedFraction(list.toArray());
    }

    public static SimpleContinuedFraction valueOf(final BigDecimal r) {
        return valueOf(BigRational.valueOf(r));
    }

    public BigInteger[] series(final int length) {
        BigInteger[] series = new BigInteger[Math.min(length, length() - 1)];
        BigRational sum = new BigRational(get(0));
        for (int i = 0; i < series.length; i++) {
            BigInteger k0 = k(i);
            BigInteger k1 = k(i + 1);

            BigInteger p = k0.multiply(k1);
            BigRational f = BigRational.of(BigInteger.ONE, p);
            if (i % 2 == 0) {
                sum = sum.add(f);
            } else {
                sum = sum.subtract(f);
            }
            System.out.println("c[" + i + "] = " + f);
            series[i] = p;
        }
        System.out.println("sum = " + sum);
        return series;
    }

    @Override
    public int compareTo(final SimpleContinuedFraction o) {
        // TODO check whether we need to test for end of each sequence, or whether the zero is sufficient
        int mul = 1;
        for (int i = 0; ; i++) {
            int a = get(i);
            int b = o.get(i);
            if (a == b) {
                mul *= -1;
                continue;
            }
            return a > b ? mul : -mul;
        }
    }
}
