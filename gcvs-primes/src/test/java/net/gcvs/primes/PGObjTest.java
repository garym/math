package net.gcvs.primes;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PGObjTest {

    @Test
    @Tag("slow")
    void iterate() {
        PGObj sieve = new PGObj();
        for (int p = sieve.getAsInt(); p != -1; p = sieve.getAsInt()) {
            System.out.println(p);
        }
    }

}