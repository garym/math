package net.gcvs.matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Combinations {
	final Expression target;
	BitSet[] prodSums;
	final List<int[]> methods = new ArrayList<>();
	final List<BitSet> methodProducts = new ArrayList<>();
	final List<int[]> methodLinearCombs = new ArrayList<>();
	int size;

	public Combinations(final Expression target, final int products) {
		this.target = target;
		prodSums = new BitSet[products];
		Arrays.setAll(prodSums, i -> new BitSet());
	}

	public void add(final int[] linear, final int... products) {
		if (methods.size() % 100 == 0) {
			System.out.println(methods.size());
		}
		System.out.println("found");
		final BitSet methodProduct = new BitSet();
		for (int product : products) {
			prodSums[product].set(size);
			methodProduct.set(product);
		}
		methods.add(products);
		methodProducts.add(methodProduct);
		methodLinearCombs.add(linear);

		size++;
	}

	public BitSet usesProduct(final int product) {
		return prodSums[product];
	}

	public void printMethod(final int[] ids, final int method) {
		Map<Integer, String> labels = new HashMap<>();
		int[] products = methods.get(method);
		I: for (int i : products) {
			int j;
			for (j = 0; j < ids.length; j++) {
				if (ids[j] == i) {
					if (labels.put(i, "S" + (j + 1)) != null) {
						throw new RuntimeException("duplicate");
					}
					;
					continue I;
				}
			}
			throw new RuntimeException();
		}

		int[] lc = methodLinearCombs.get(method);

		StringBuilder sb = new StringBuilder();
		String sep = "";
		for (int i = 0; i < products.length; i++) {
			if (lc[i] > 0)
				sb.append(sep);
			sep = " + ";
			if (lc[i] == -1) {
				sb.append(" - ");
			} else if (lc[i] != 1) {
				sb.append(lc[i]);
				sb.append(" * ");
			}
			sb.append("S" + labels.get(products[i]));
		}
		sb.append(" = ").append(target);
		System.out.println(sb);
	}
}