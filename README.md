# README #

General purpose library of mathematical entities and algorithms to aid computation.

## Included in this library

### Stable Features

* BigRational - unlimited precision rational numbers
* Heap - Heap structures and their implementations: BinaryHeap, PairHeap, BinomialHeap
* PartitionGenerator - Generates integer partitions, optionally restricted by size and number. 

### Experimental Features

* ContinuedFraction - Continued fraction implementation

### What is this repository for? ###

I'm collecting utility classes to help with writing code to support mathematical computation in various domains.
Initial draft version.


### How do I get set up? ###

For now, add source code dependency to your projects.
The library does not have any external dependencies, (yet).

### Contribution guidelines ###

I don't expect anyone to be interested in this library, I leave it public so google can index it.

Feel free to write tests :)

### Who do I talk to? ###

Contact garym for any queries.
