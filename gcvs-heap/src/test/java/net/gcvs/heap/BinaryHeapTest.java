package net.gcvs.heap;

import net.gcvs.util.Shuffle;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BinaryHeapTest {

    @Test
    public void test() {
        Shuffle shuffle = new Shuffle(new Random());
        final int size = 10;
        BinaryHeap<Integer> heap = new BinaryHeap<Integer>(size, Integer::compare);

        int[] unsorted = new int[size];
        Arrays.setAll(unsorted, i -> i + 1);
        shuffle.shuffle(unsorted);

        for (int i : unsorted) {
            heap.insert(i);
            System.out.println(heap);
        }
        heap.isHeap();

        System.out.println(Arrays.toString(unsorted));
        int top = heap.extractMin();
        System.out.println(top);
        for (int i = 1; i < size; i++) {
            int min = heap.extractMin();
            System.out.println(min);
            assertTrue(min >= top);
            top = min;
        }

    }
}
