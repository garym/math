package net.gcvs.math.polynomial;

import net.gcvs.math.BigRational;
import net.gcvs.math.algebra.BigRationalAlgebra;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplyTermSeriesTest {
    BigRationalAlgebra algebra = new BigRationalAlgebra();
    SeriesAlgebra<BigRational> seriesAlgebra = new SeriesAlgebra<>(algebra);
    PolyAlgebra<BigRational> polyAlgebra = new PolyAlgebra<>(algebra);


    @Test
    void mul() {

        Series<BigRational> a = Series.<BigRational>builder().algebra(algebra)
                .constant(1, 1)
                .build();
        Series<BigRational> b = Series.<BigRational>builder().algebra(algebra)
                .constant(1, 2)
                .constant(0, 3)
                .build();

        final Series<BigRational> c = seriesAlgebra.multiply(a, b);

        System.out.println(seriesAlgebra.toString(a, 10));
        System.out.println(seriesAlgebra.toString(b, 10));
        System.out.println(seriesAlgebra.toString(c, 10));

    }
}