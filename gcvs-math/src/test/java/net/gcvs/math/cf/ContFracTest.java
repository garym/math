package net.gcvs.math.cf;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.TWO;
import static org.junit.jupiter.api.Assertions.*;

class ContFracTest {


    @Test
    void transform() {
        Generator<BigInteger> generator = Generator.of(ONE, TWO, TWO, TWO, TWO, TWO, TWO, TWO, TWO, TWO, TWO);
        M m = new M(0, 2, -1, 3);

        m = new M(1, 1, 0, 1);
        ContFrac.transform(generator, m);


    }

}